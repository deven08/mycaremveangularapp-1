import { AppService } from './core/services/app.service';

import { Component, OnInit } from '@angular/core';
import { Event, Router, 
     NavigationEnd, 
     NavigationStart, 
     ActivatedRoute, 
     NavigationCancel,
     NavigationError } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map, mergeMap } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'My Care MVE';
  loading: boolean;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private app: AppService) {
      this.router.events.subscribe((event: Event) => {
        switch (true) {
          case event instanceof NavigationStart: {
            this.loading = true;
            break;
          }
  
          case event instanceof NavigationEnd: {
            const currentPath = event['url'].split('/');
            if (currentPath[1] === 'order') {
              this.app.forOrders = true;
            } else {
              this.app.forOrders = false;
            }
          }
          case event instanceof NavigationCancel:
          case event instanceof NavigationError: {
            this.loading = false;
            break;
          }
          default: {
            break;
          }
        }
      });

  }

  ngOnInit() {
    this.router.navigate(['']);
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    ) 
    .subscribe((event) => {
      this.titleService.setTitle(event['title']);
      this.app.validateBanner = ('banner' in event) ?  event['banner'] : false; 
    });
  }
}
