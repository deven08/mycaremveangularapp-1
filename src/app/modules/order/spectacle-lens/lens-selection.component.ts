// import { ApiAlertPipe } from 'src/app/shared/pipes/api-alert.pipe';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { OrderService } from '@app/core/services/order/order.service';
import { AppService, ErrorService } from '@app/core';
import { FramesService } from '@app/core/services/inventory/frames.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { RxService } from '@services/rx/rx.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { MasterDropDown, LensLab, SpectacleMaterial } from '@app/core/models/masterDropDown.model';
import { UtilityService } from '@app/shared/services/utility.service';
import { CommonValidator } from '@app/shared/validator/common-validator';
// import { OrderSearchService } from 'src/app/ConnectorEngine/services/order.service';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
@Component({
    selector: 'app-order-lens-selection',
    templateUrl: 'lens-selection.component.html'
})
export class OrderLensSelectionComponent implements OnInit, OnDestroy {

    segosrequired = false;
    segodrequired = false;
    outsidePhysician = false;
    // Modal
    className: any = 'dialog';
    showDialog: any = false;
    head_pop: any = 'My Care MVE';
    message_pop = '';
    button_pop: any = 'Ok';
    button_pop2: any = '';
    popFor = '';
    popParams: any[];

    // Default Droppers
    sphereDropperDefaultod: any = '';
    sphereDropperDefaultos: any = '';
    cylinderDropperDefaultod: any = '';
    cylinderDropperDefaultos: any = '';
    addDropperDefaultos: any = '';
    addDropperDefaultod: any = '';


    EqualData: any;
    disableOU = false;
    disableOS = false;
    disableOD = false;
    subscriptionTypeOsdata: Subscription;
    TypeOSdata: any = '';
    TypeODdata: any = '';
    subscriptionTypeODdata: Subscription;
    selectSpectacleLensSidebar = false;
    count: number;
    fairvalueOU: number;
    fairvalue = 0;
    fairvalueOD = 0;
    fairvalueOS = 0;
    orderLensSelectionForm: FormGroup;
    eyedetails: any[];
    /**
     * Materials  of order lens selection component for loading lens material
     */
    materials: Array<object>;
    materiallist: any[];
    subscriptionSave: Subscription;
    subscriptionPhysicianId: Subscription;
    subscriptionGetDataByOrderId: Subscription;
    subscriptionLensSaveNew: Subscription;
    subscriptionLensPopUp: Subscription;
    subscriptionOrderCancle: Subscription;
    lensLab: Array<object>;
    LensLablist: any[];
    patientID: any;
    itemIdValueOD = '';
    UPCValueOD: string;
    ItemNameOD: string;
    itemIdValueOS = '';
    UPCValueOS: string;
    ItemNameOS: string;
    lensSelection = 'OU';
    lensSelectionOrderId = '';
    lensSelectionLensODId = '';
    lensSelectionLensOSId = '';

    RxSphereValues: any = [];
    RxCylinderValues: any = [];
    AxisValues: any = [];
    RxAddValues: any = [];
    orderlensId = 'New Order';
    EyeselectionDisable = false;
    ODEnable = false;
    OSEnable = false;
    test = 'OD';

    RxHbaseValues = ['', 'In', 'Out'];


    RxVbaseValues = ['', 'Down', 'Up'];
    public lensSelectionData: any;
    RxItemId = '';
    @ViewChild('alertpopup') alertpopup: DialogBoxComponent;
    equalDataOS: any;
    physician_id: number;
    constructor(
        private _fb: FormBuilder,
        private orderService: OrderService,
        private confirmationService: ConfirmationService,
        private app: AppService,
        private dropdownService: DropdownService,
        private rxService: RxService,
        private error: ErrorService,
        private utility: UtilityService) {

        this.subscriptionLensPopUp = orderService.lensSelectionPopUP$.subscribe(data => {
            this.RxItemId = data;
            if (this.orderlensId === 'New Order' && this.orderService.lensPopupOrderCondi === true && this.RxItemId === '') {
                this.lensSelectionData = [];
                this.patientID = this.app.patientId;
                // this._StateInstanceService.PatientId;
                this.rxService.getLensSelection(this.patientID).subscribe(lensData => {
                    // alert(data['data'])
                    if (lensData['data'].length !== 0) {
                        const dialogObject = {
                            'header': 'Confirmation',
                            'footer': 'Are you sure that you want to load Previous RX details?',
                            'buttonone': 'Yes',
                            'buttontwo': 'No'
                        }
                        this.alertpopup.dialogBox(dialogObject, 1);
                        // this.confirm();
                    } else {
                        this.lensSelectionData = [];
                    }

                });

            } else if (this.RxItemId !== '') {
                this.lensSelectionData = [];
                this.patientID = this.app.patientId;
                this.orderService.getLensSelectionBasedOnSelectedRx(this.patientID, this.RxItemId).subscribe(
                    selectrxData => {
                        // alert(data['data'])
                        //  console.log(data);
                        // let index = ['data'].length - 1;
                        this.lensSelectionData = selectrxData;
                        // console.log(this.lensSelectionData[index])
                        this.getformGroupData(this.lensSelectionData[0]);
                        this.orderLensSelectionForm.controls['id'].setValue('');
                        this.orderService.lensPopupOrderCondi = false;
                    });
            }
        });
        this.subscriptionLensSaveNew = orderService.orderSaveNew$.subscribe(data => {
            this.lensSelectionLensOSId = '';
            this.lensSelectionLensODId = '';
            this.orderLensSelectionForm.controls['id'].patchValue('');

        });
        this.subscriptionGetDataByOrderId = orderService.orderselectionId$.subscribe(data => {
            // alert("lensselection")
            if (data === 'New Order' && this.orderService.lensPopupOrderCondi === true &&
                this.orderService.orderScreenSavePath === 'Lens Selection') {
                const dialogObject = {
                    'header': 'Confirmation',
                    'footer': 'Are you sure that you want to load Previous RX details?',
                    'buttonone': 'Yes',
                    'buttontwo': 'No'
                }
                this.alertpopup.dialogBox(dialogObject, 1);
                // this.confirm();
            }
            this.lensSelectionLensODId = '';
            this.lensSelectionLensOSId = '';

            this.orderlensId = 'New Order';
            if (data !== 'New Order') {
                this.orderService.lensSelectionUPCStatus = 'lens';
                this.EyeselectionDisable = true;
                this.formGroupData();
                this.orderlensId = data;
                this.TypeOSdata = '';
                this.TypeODdata = '';
                this.ODEnable = false;
                this.OSEnable = false;
                this.orderService.getOrderItemDetails(data).subscribe((value: any[]) => {
                    value.forEach(element => {
                        if (element['lens_vision']) {
                            if (element['lens_vision'] === 'OD') {
                                this.lensSelectionLensODId = element.id;
                                this.TypeODdata = element['upc_code'] + '(' + element['item_name'] + ')' + '(' + element['item_id'] + ')';
                                this.itemIdValueOD = element['item_id'];
                                this.UPCValueOD = element['upc_code'];
                                this.ItemNameOD = element['item_name'];
                                this.ODEnable = true;
                                // this.orderService.ODNAME.next(element['item_name']);
                                // this.orderService.ODITEID.next(element['item_id'])
                            }
                            if (element['lens_vision'] === 'OS') {
                                this.lensSelectionLensOSId = element.id;
                                this.TypeOSdata = element['upc_code'] + '(' + element['item_name'] + ')' + '(' + element['item_id'] + ')';
                                this.itemIdValueOS = element['item_id'];
                                this.UPCValueOS = element['upc_code'];
                                this.ItemNameOS = element['item_name'];
                                this.OSEnable = true;
                                // this.orderService.OSNAME.next(element['item_name']);
                                // this.orderService.OSITEID.next(element['item_id']);
                            }
                        }
                    });
                    if (this.ODEnable === true && this.OSEnable === true) {
                        this.disableOD = false;
                        this.disableOS = false;
                        this.disableOU = false;
                    }
                    if (this.ODEnable === true && this.OSEnable === false) {
                        this.disableOS = true;
                        this.disableOD = false;
                        this.disableOU = true;
                    }
                    if (this.ODEnable === false && this.OSEnable === true) {
                        this.disableOD = true;
                        this.disableOS = false;
                        this.disableOU = true;
                    }

                    this.lensSelectionData = [];
                    this.patientID = this.app.patientId;
                    this.rxService.getLensSelectionByOrderId(this.patientID, data).subscribe(
                        lensValue => {
                            this.lensSelectionData = lensValue['data'];
                            this.getformGroupData(this.lensSelectionData[0]);
                            // this.lensSelectionData.forEach(element => {
                            //     if (element.order_id === parseInt(data, 10)) {
                            //         this.lensSelectionOrderId = element.id;
                            //         this.getformGroupData(element);
                            //     }
                            // });
                        });
                });
            } else {
                this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
                this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
                // this.loadLensSelection();
                this.EyeselectionDisable = false;
                this.formGroupData();
                this.itemIdValueOS = '';
                this.itemIdValueOD = '';
                this.TypeODdata = '';
                this.TypeOSdata = '';
                // localStorage.removeItem('Order_id');
                this.ODEnable = true;
                this.OSEnable = true;
                this.disableOD = false;
                this.disableOS = false;
                this.disableOU = false;
                this.lensSelection = 'OU';

            }
            //   console.log(data);
        });
        this.subscriptionOrderCancle = orderService.OrderCancleValue$.subscribe(data => {
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            // this.loadLensSelection();
            this.EyeselectionDisable = false;
            this.formGroupData();
            this.itemIdValueOS = '';
            this.itemIdValueOD = '';
            this.TypeODdata = '';
            this.TypeOSdata = '';
            // localStorage.removeItem('Order_id');
            this.disableOD = false;
            this.disableOS = false;
            this.disableOU = false;
            this.lensSelection = 'OU';
        });
        // this.frameService.errorHandle.msgs = [];
        this.subscriptionSave = orderService.orderLensSelectionSave$.subscribe(data => {
            // if(data == "2" && (this.segodrequired==true || this.segosrequired==true) &&
            // this.orderLensSelectionForm.controls['seg_od'].value=="" ||this.orderLensSelectionForm.controls['seg_os'].value )
            // {
            //     this.WarningShow("Seg height is mandatory");
            //     return;
            // }
            if (data === '2') {
                //   alert();
                if (this.app.patientId != null) {
                    // OU
                    if (this.lensSelection === 'OU') {
                        // if (this.orderLensSelectionForm.controls['sphere_od'].value !== '' &&
                        //     this.orderLensSelectionForm.controls['sphere_os'].value !== '') {
                        // if (this.orderLensSelectionForm.controls['dist_pd_od'].value === '' ||
                        //     this.orderLensSelectionForm.controls['dist_pd_os'].value === '' ||
                        //     this.orderLensSelectionForm.controls['del_status'].value === '' ||
                        //     this.orderLensSelectionForm.controls['near_pd_od'].value === '' ||
                        //     this.orderLensSelectionForm.controls['near_pd_os'].value === '' ||
                        //     this.orderLensSelectionForm.controls['rx_dos'].value === '') {
                        //     this.frameService.displayError({
                        //         severity: 'error',
                        //         summary: 'Error Message', detail: 'PD Power is required.'
                        //     });
                        //     return false;
                        //     // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'PD Power is required.' });

                        // }
                        if (this.orderLensSelectionForm.controls['id'].value === '') {
                            this.saveOrderLensData();
                        } else {
                            this.updateOrderLensData();
                        }
                        // } else {
                        //     this.frameService.displayError({
                        //         severity: 'error',
                        //         summary: 'Error Message', detail: 'Please select SphereOD SphereOS'
                        //     });
                        // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select SphereOD SphereOS' });
                        // return false
                        // }
                    }
                    // OD
                    if (this.lensSelection === 'OD') {
                        // if (this.orderLensSelectionForm.controls['sphere_od'].value !== '') {
                        // if (this.orderLensSelectionForm.controls['dist_pd_od'].value === '' ||
                        //     this.orderLensSelectionForm.controls['near_pd_od'].value === '') {
                        // this.frameService.displayError({
                        //     severity: 'error',
                        //     summary: 'Error Message', detail: 'PD Power is required.'
                        // });

                        // // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'PD Power is required.' });
                        // return false
                        // }
                        if (this.orderLensSelectionForm.controls['id'].value === '') {
                            this.orderLensSelectionForm.controls['sphere_os'].patchValue('PL');
                            this.saveOrderLensData();
                        } else {
                            this.orderLensSelectionForm.controls['sphere_os'].patchValue('PL');
                            this.updateOrderLensData();
                        }
                        // } else {
                        //     this.frameService.displayError({
                        //         severity: 'error',
                        //         summary: 'Error Message', detail: 'Please select SphereOD'
                        //     });
                        //     return false
                        //     // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select SphereOD' });

                        // }
                    }
                    // OS
                    if (this.lensSelection === 'OS') {
                        // if (this.orderLensSelectionForm.controls['sphere_os'].value !== '') {
                        //     if (this.orderLensSelectionForm.controls['dist_pd_os'].value !== '' ||
                        //         this.orderLensSelectionForm.controls['near_pd_os'].value !== '') {
                        //         this.frameService.displayError({
                        //             severity: 'error',
                        //             summary: 'Error Message', detail: 'PD Power is required.'
                        //         });
                        //         return false
                        //         // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'PD Power is required.' });

                        //     }
                        if (this.orderLensSelectionForm.controls['id'].value === '') {
                            this.orderLensSelectionForm.controls['sphere_od'].patchValue('PL');
                            this.saveOrderLensData();
                        } else {
                            this.orderLensSelectionForm.controls['sphere_od'].patchValue('PL');
                            this.updateOrderLensData();
                        }
                        // } else {
                        //     this.frameService.displayError({
                        //         severity: 'error',
                        //         summary: 'Error Message', detail: 'Please select SphereOD'
                        //     });
                        //     return false
                        //     // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select SphereOD' });

                        // }
                    }
                } else {
                    this.error.displayError({
                        severity: 'error',
                        summary: 'Error Message', detail: 'Please select patient'
                    });
                    return false
                    // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select patient' });

                }
                if (this.lensSelection === 'OD' && this.itemIdValueOD !== '') {
                    if (this.lensSelectionLensODId === '') {
                        const rxLensSelection = {
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOD,
                            'upc_code': this.UPCValueOD,
                            'item_name': this.ItemNameOD,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': this.lensSelection,
                            'qty': 1,
                            'price_retail': this.EqualData.retail_price
                        };
                        $(this.orderService.otherItemRows).each(function (i, obj) {
                            if (obj['item_id'] === rxLensSelection.item_id && obj['module_type_id'] === 2) {
                                rxLensSelection.qty = obj['qty'];
                            }
                        });


                        this.orderService.saveOrderLensSelection(rxLensSelection).subscribe(dataSet => {
                            // console.log(data)
                        });
                    }
                    if (this.lensSelectionLensODId !== '') {
                        const rxLensSelection = {
                            'id': this.lensSelectionLensODId,
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOD,
                            'upc_code': this.UPCValueOD,
                            'item_name': this.ItemNameOD,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': this.lensSelection,
                            'qty': 1,
                            'ordertypeid': '7'
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelection.item_id && obj['module_type_id'] === 2) {
                                rxLensSelection.qty = obj['qty'];
                            }
                        });

                        this.orderService.updateOrderLensSelection(rxLensSelection, rxLensSelection.id).subscribe(dataSet2 => {
                            // console.log(data)
                            // alert("rx values")
                        });

                    }

                }
                if (this.lensSelection === 'OS' && this.itemIdValueOS !== '') {
                    if (this.lensSelectionLensOSId === '') {
                        const rxLensSelection = {
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOS,
                            'upc_code': this.UPCValueOS,
                            'item_name': this.ItemNameOS,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': this.lensSelection,
                            'qty': 1,
                            'price_retail': this.equalDataOS.retail_price
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelection.item_id && obj['module_type_id'] === 2) {
                                rxLensSelection.qty = obj['qty'];
                            }
                        });

                        this.orderService.saveOrderLensSelection(rxLensSelection).subscribe(dataSet3 => {
                            // console.log(data)
                        });
                    }
                    if (this.lensSelectionLensOSId !== '') {
                        const rxLensSelection = {
                            'id': this.lensSelectionLensOSId,
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOS,
                            'upc_code': this.UPCValueOS,
                            'item_name': this.ItemNameOS,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': this.lensSelection,
                            'qty': 1,
                            'ordertypeid': '7',
                            'price_retail': this.equalDataOS.RetailPrice
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelection.item_id && obj['module_type_id'] === 2) {
                                rxLensSelection.qty = obj['qty'];
                            }
                        });

                        this.orderService.updateOrderLensSelection(rxLensSelection, rxLensSelection.id).subscribe(dataSet4 => {
                            // console.log(data)
                        });

                    }
                }
                if (this.itemIdValueOS !== '' && this.itemIdValueOD !== '') {
                    if (this.lensSelectionLensOSId === '' && this.lensSelectionLensODId === '') {
                        const rxLensSelectionOD = {
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOD,
                            'upc_code': this.UPCValueOD,
                            'item_name': this.ItemNameOD,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': 'OD',
                            'qty': 1,
                            'price_retail': this.EqualData.retail_price

                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelectionOD.item_id && obj['module_type_id'] === 2) {
                                rxLensSelectionOD.qty = obj['qty'];
                            }
                        });

                        this.orderService.saveOrderLensSelection(rxLensSelectionOD).subscribe(dataSet5 => {
                            // console.log(data)
                        });
                        const rxLensSelectionOS = {
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOS,
                            'upc_code': this.UPCValueOS,
                            'item_name': this.ItemNameOS,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': 'OS',
                            'qty': 1,
                            'price_retail': this.equalDataOS.retail_price
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelectionOS.item_id && obj['module_type_id'] === 2) {
                                rxLensSelectionOS.qty = obj['qty'];
                            }
                        });

                        this.orderService.saveOrderLensSelection(rxLensSelectionOS).subscribe(dataSet6 => {
                            // console.log(data)
                        });
                    }
                    if (this.lensSelectionLensOSId !== '' && this.lensSelectionLensODId !== '') {
                        const rxLensSelectionOD = {
                            'id': this.lensSelectionLensODId,
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOD,
                            'upc_code': this.UPCValueOD,
                            'item_name': this.ItemNameOD,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': 'OD',
                            'qty': 1,
                            'ordertypeid': '7',
                            'price_retail': this.EqualData.RetailPrice,
                            'order_status_id': this.orderService.orderStatusGlobal,
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelectionOD.item_id && obj['module_type_id'] === 2) {
                                rxLensSelectionOD.qty = obj['qty'];
                            }
                        });

                        this.orderService.updateOrderLensSelection(rxLensSelectionOD, rxLensSelectionOD.id).subscribe(dataSet7 => {
                            // console.log(data)
                        });
                        const rxLensSelectionOS = {
                            'id': this.lensSelectionLensOSId,
                            'patient_id': this.app.patientId,
                            'item_prac_code': '2',
                            'loc_id': this.app.locationId,
                            'item_id': this.itemIdValueOS,
                            'upc_code': this.UPCValueOS,
                            'item_name': this.ItemNameOS,
                            'module_type_id': '2',
                            'material_id_od': '',
                            'lens_vision': 'OS',
                            'qty': 1,
                            'ordertypeid': '7',
                            'price_retail': this.EqualData.RetailPrice,
                            'order_status_id': this.orderService.orderStatusGlobal,
                        };

                        $(this.orderService.otherItemRows).each(function (i, obj) {

                            if (obj['item_id'] === rxLensSelectionOS.item_id && obj['module_type_id'] === 2) {
                                rxLensSelectionOS.qty = obj['qty'];
                            }
                        });

                        this.orderService.updateOrderLensSelection(rxLensSelectionOS, rxLensSelectionOS.id).subscribe(dataSet8 => {
                            // console.log(data);
                        });
                    }



                }


                this.orderService.orderspectacleLensSave.next('3');
            }

        });
        if (this.app.patientId != null) {
            this.loadLensSelection();
        }
        this.subscriptionPhysicianId = orderService.lensPhysicianId$.subscribe(data => {
            this.orderLensSelectionForm.patchValue({
                physician_id: data
            });
        });
        this.subscriptionTypeODdata = orderService.orderRowSeletedUPCOD$.subscribe(data => {
            if (data === 'close') {
                this.selectSpectacleLensSidebar = false;
            } else {
                this.EqualData = data;
                if (data.StyleName === 'Progressive' || data.StyleName === 'Bifocal' || data.StyleName === 'Trifocal') {

                    this.segodrequired = true;
                }

                this.orderService.spectaclelensOD = data.id;
                this.TypeODdata = data.upc_code + '(' + data.name + ')' + '(' + data.id + ')';
                const dataobjOD = {
                    'UPC': data.upc_code,
                    'Name': data.name,
                    'ModuleTypeId': 2,
                    'uniqueid': '2'
                };
                this.orderService.ODNAME.next(dataobjOD);
                this.orderService.ODITEID.next(data.id);
                this.itemIdValueOD = data.id;
                this.ItemNameOD = data.name;
                this.UPCValueOD = data.upc_code;
                this.selectSpectacleLensSidebar = false;
            }
        });
        this.subscriptionTypeOsdata = orderService.orderRowSeletedUPCOS$.subscribe(data => {
            this.equalDataOS = data
            if (data.StyleName === 'Progressive' || data.StyleName === 'Bifocal' || data.StyleName === 'Trifocal') {
                this.segosrequired = true;
            }
            this.orderService.spectaclelensOS = data.id
            this.TypeOSdata = data.upc_code + '(' + data.name + ')' + '(' + data.id + ')';
            const dataobjOS = {
                'UPC': data.upc_code,
                'Name': data.name,
                'ModuleTypeId': 2,
                'uniqueid': '3'
            };
            this.orderService.OSNAME.next(dataobjOS);
            this.orderService.OSITEID.next(data.id);
            this.itemIdValueOS = data.id;
            this.ItemNameOS = data.name;
            this.UPCValueOS = data.upc_code;
            this.selectSpectacleLensSidebar = false;
        });
    }




    /**
     * Values assigner
     * @param ele for getting selected element name
     * @param event for getting delected value
     */
    valuesAssigner(ele, event) {
        this.orderLensSelectionForm.controls[ele].patchValue(event.target.value);
    }


    /**
     * on init for initialize methods
     */
    ngOnInit(): void {
        this.eyedetails = [
            { label: 'OU', value: { Id: 'OU', Name: 'OU' } },
            { label: 'OD', value: { Id: 'OD', Name: 'OD' } },
            { label: 'OS', value: { Id: 'OS', Name: 'OS' } }
        ];
        this.orderService.eye_Status = 'OU';
        this.orderService.spectaclelensOD = "";
        this.orderService.spectaclelensOS = "";
        this.orderService.lensPopupOrderCondi = true;
        // Sphere Powers
        this.RxSphereValues = this.orderService.genDropDowns(
            { start: -20, end: 20, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'PL' });

        // Cylinder Power
        this.RxCylinderValues = this.orderService.genDropDowns(
            { start: -10, end: 10, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'D.S.' });

        // Axis Degrees
        this.AxisValues = this.orderService.genDropDowns(
            { start: 1, end: 180, step: 1, sign: false, roundOff: 0, slice: 3, neutral: '0' });

        // Add Values
        this.RxAddValues = this.orderService.genDropDowns(
            { start: 0.25, end: 3.75, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: '0' });

        this.lensSelection = 'OU';
        this.orderService.mandatoryFields_Validation = {
            'dist_pd_od': { label: 'Far OD', required: false },
            'dist_pd_os': { label: 'Far OS', required: false },
            'sphere_od': { label: 'Sphere OD', required: false },
            'sphere_os': { label: 'Sphere OS', required: false }
        }
        this.formGroupData();
        this.LoadMasters();

        if (this.app.patientId == null) {
            // this.frameService.displayError({
            //     severity: 'error',
            //     summary: 'Error Message', detail: 'Please select Patient'
            // });
            // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select Patient' });

        }
        this.ODEnable = true;
        this.OSEnable = true;

        // Custom Selector
        this.sphereDropperDefaultod = this.orderLensSelectionForm.controls['sphere_od'].value;
        this.sphereDropperDefaultos = this.orderLensSelectionForm.controls['sphere_os'].value;
        this.cylinderDropperDefaultod = this.orderLensSelectionForm.controls['cyl_od'].value;
        this.cylinderDropperDefaultos = this.orderLensSelectionForm.controls['cyl_os'].value;
        this.addDropperDefaultos = this.orderLensSelectionForm.controls['add_os'].value;
        this.addDropperDefaultod = this.orderLensSelectionForm.controls['add_od'].value;
        this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
        // console.log(this.RxSphereValues);
        // this.materials = [];
        // this.materials = this.dropdownService.getSpectaclesLensMaterialListDetails('', '', '', '', '', '', '');
    }




    // RX Validator for Sphere, Cylinder and Add
    rxValidator(e, pl: boolean = false, ds: boolean = false) {
        const ele_name = e.target.attributes.formControlName.value;
        const val_int = e.target.value;
        if (val_int === '' || val_int == null) {
            this.orderLensSelectionForm.controls[ele_name].patchValue(0);
        } else if (!isNaN(val_int)) {


            if (val_int > 0) {
                // For Positive
                this.orderLensSelectionForm.controls[ele_name].patchValue('+' + parseFloat(val_int).toFixed(2));
            } else {
                // For Negative
                this.orderLensSelectionForm.controls[ele_name].patchValue(parseFloat(val_int).toFixed(2));
            }
        } else {
            if (pl && val_int !== 'PL') {
                this.orderLensSelectionForm.controls[ele_name].patchValue('PL');
            } else if (ds && val_int !== 'D.S.') {
                this.orderLensSelectionForm.controls[ele_name].patchValue('D.S.');
            } else {
                this.orderLensSelectionForm.controls[ele_name].patchValue(0.00);
            }

        }
    }

    // Prism Validator
    /**
     * Prisms validate
     * dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
     */
    prismValidate(e) {
        const ele_name = e.target.attributes.formControlName.value;
        const val_int = e.target.value;
        if (!isNaN(val_int)) {
            if (val_int > '20' || val_int < '0') {
                this.showDialog = true;
                this.className = 'dialog card ';
                this.message_pop = 'You have entered a power <b>' + val_int + '</b> larger than 20.00';
                this.button_pop = 'Continue';
                this.popFor = 'HPRISM';
                this.popParams = [{ [ele_name]: e.target.save_value }];
                this.button_pop2 = 'No';
                const dialogObject = {
                    'header': 'Warning!',
                    'footer': 'You have entered a power ' + val_int + ' larger than 20.00',
                    'buttonone': 'Continue',
                    'buttontwo': 'No'
                }
                this.alertpopup.dialogBox(dialogObject);
            } else {
                this.setDecimals(2, ele_name);
            }
        } else {
            this.orderLensSelectionForm.controls[ele_name].setValue('PL');
        }
    }

    onChanges(): void {





        /*// V Prism OD
        this.orderLensSelectionForm.get('Vprism_od').valueChanges.subscribe(val => {

            let od_power = val;


            //Check for number
            let od_power_int = od_power;
            if (!isNaN(od_power_int)) {
                if (od_power_int > 20) {
                    this.showDialog = true;
                    this.className = "dialog card ";
                    this.message_pop = "You have entered a power <b>" + od_power + "</b> larger than 20.00";
                    this.button_pop = "Continue";

                } else if (od_power_int < 0) {
                    this.showDialog = true;
                    this.className = "dialog card bg-danger text-white";
                    this.message_pop = "Power must be Postive";
                    this.orderLensSelectionForm.controls['Vprism_od'].setValue(Math.abs(od_power));
                    this.button_pop = "OK";
                }
            }
            if (isNaN(od_power_int)) {

                if (val.toUpperCase() != "PL") {
                    this.orderLensSelectionForm.controls['Vprism_od'].setValue("PL");
                }


            }

        });


        // V Prism OS
        this.orderLensSelectionForm.get('Vprism_os').valueChanges.subscribe(val => {

            let os_power = val;


            //Check for number
            let os_power_int = os_power;
            if (!isNaN(os_power_int)) {
                if (os_power_int > 20) {
                    this.showDialog = true;
                    this.className = "dialog card ";
                    this.message_pop = "You have entered a power <b>" + os_power + "</b> larger than 20.00";
                    this.button_pop = "Continue";

                } else if (os_power_int < 0) {
                    this.showDialog = true;
                    this.className = "dialog card bg-danger text-white";
                    this.message_pop = "Power must be Postive";
                    this.orderLensSelectionForm.controls['Vprism_os'].setValue(Math.abs(os_power));
                    this.button_pop = "OK";
                }
            }
            if (isNaN(os_power_int)) {

                if (val.toUpperCase() != "PL") {
                    this.orderLensSelectionForm.controls['Vprism_os'].setValue("PL");
                }


            }

        }); */


        // Axis OD and OS
        this.orderLensSelectionForm.get('axis_od').valueChanges.subscribe(val => {
            const od_axis = val;

            if (od_axis > '180') {
                this.showDialog = true;
                this.className = 'dialog card bg-warning text-white';
                this.message_pop = 'Axis entered is outside of the standard range';
                this.orderLensSelectionForm.controls['axis_od'].setValue(0);
                this.button_pop = 'OK';
                // dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
                const dialogObject = {
                    'header': 'Warning!',
                    'footer': 'Axis entered is outside of the standard range',
                    'buttonone': 'OK',
                    'buttontwo': ''
                }
                this.alertpopup.dialogBox(dialogObject);
            }

            if (od_axis < 0) {
                this.orderLensSelectionForm.controls['axis_od'].setValue(Math.abs(od_axis));
            }
        });

        this.orderLensSelectionForm.get('axis_os').valueChanges.subscribe(val => {
            const os_axis = val;

            if (os_axis > '180') {
                this.showDialog = true;
                this.className = 'dialog card bg-warning text-white';
                this.message_pop = 'Axis entered is outside of the standard range';
                this.orderLensSelectionForm.controls['axis_os'].setValue(0);
                this.button_pop = 'OK';
                // dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
                const dialogObject = {
                    'header': 'Warning!',
                    'footer': 'Axis entered is outside of the standard range',
                    'buttonone': 'OK',
                    'buttontwo': ''
                }
                this.alertpopup.dialogBox(dialogObject);
            }

            if (os_axis < 0) {
                this.orderLensSelectionForm.controls['axis_os'].setValue(Math.abs(os_axis));
            }
        });


    }


    ngOnDestroy(): void {
        this.subscriptionSave.unsubscribe();
        this.subscriptionPhysicianId.unsubscribe();
        this.subscriptionTypeODdata.unsubscribe();
        this.subscriptionTypeOsdata.unsubscribe();
        this.subscriptionGetDataByOrderId.unsubscribe();
        this.subscriptionLensSaveNew.unsubscribe();
        this.subscriptionLensPopUp.unsubscribe();
        this.subscriptionOrderCancle.unsubscribe();

    }
    saveOrderLensData() {
        this.physician_id = this.orderService.physicianID;
        this.orderLensSelectionForm.controls.physician_id.patchValue(this.orderService.physicianID);
        // alert(this.orderLensSelectionForm.controls['custom_rx'].value);
        this.orderService.saveSpecticalRX(this.orderLensSelectionForm.value).subscribe(data => {
            if (data['status'] === 'New Rx added successfully') {
                // this.formGroupData();
                // this.SuccessMsg()
                this.orderService.lensSelectionPopCondition = false;
                //  this.subscriptionSave.unsubscribe();
            }
        });
    }
    updateOrderLensData() {
        this.physician_id = this.orderService.physicianID;
        this.orderLensSelectionForm.controls.physician_id.patchValue(this.orderService.physicianID);
        const RxId = this.orderLensSelectionForm.controls['id'].value;
        this.orderService.updateSpecticalRX(this.orderLensSelectionForm.value, RxId).subscribe(data => {
            if (data['status'] === 'Rx updated successfully') {
                //  this.formGroupData();
                // this.SuccessUpdateMsg()
                this.orderService.lensSelectionPopCondition = false;
            }
        });
    }

    /**
     * Customs rx is for the related to custom or normal rx
     * @param {object} event this is the event for true or false
     */
    customRx(event) {
        this.orderLensSelectionForm.controls['custom_rx'].patchValue(event.target.checked)
    }


    /**
     * Forms group data for initilize form group
     */
    formGroupData() {
        this.orderLensSelectionForm = this._fb.group({
            // accessToken: this._StateInstanceService.accessTokenGlobal,
            rxId: '',
            PatientId: this.app.patientId,
            id: '',
            patient_id: '',
            physician_id: '',
            physician_name: '',
            sphere_od: '',
            cyl_od: '',
            axis_od: '',
            axis_od_va: '',
            add_od: '',
            add_od_va: '',
            prism_od: '',
            base_od: '',
            dist_pd_od: ['', [CommonValidator.numberDecimalValidator]],
            near_pd_od: ['', [CommonValidator.numberDecimalValidator]],
            oc_od: '',
            seg_od: '',
            sphere_os: '',
            cyl_os: '',
            axis_os: '',
            axis_os_va: '',
            add_os: '',
            add_os_va: '',
            prism_os: '',
            base_os: '',
            dist_pd_os: ['', [CommonValidator.numberDecimalValidator]],
            near_pd_os: ['', [CommonValidator.numberDecimalValidator]],
            oc_os: '',
            seg_os: '',
            mr_od_p: '',
            mr_od_prism: '',
            mr_od_splash: '',
            mr_od_sel: '',
            mr_os_p: '',
            mr_os_prism: '',
            mr_os_splash: '',
            mr_os_sel: '',
            telephone: '',
            location_id: '',
            del_status: '',
            rx_dos: '',
            balance_od: '',
            balance_os: '',
            Vprism_od: '',
            Vprism_os: '',
            Vbase_od: '',
            Vbase_os: '',
            order_id: this.orderService.orderId,
            det_order_id: '0',
            vertex_od: '',
            vertex_os: '',
            BC_od: '',
            BC_os: '',
            dec_od: '',
            dec_os: '',
            Insert_od: '',
            insert_os: '',
            Totaldec_od: '',
            totaldec_os: '',
            EyeStatus: 'OU',
            custom_rx: true,
            price_retail: '',
        });
        this.TypeODdata = '';
        this.TypeOSdata = '';
        this.orderService.spectaclelensOD = "";
        this.orderService.spectaclelensOS = "";
        this.onChanges();
        this.orderService.PDsavingValidation = false;
        this.orderService.SphereValidation = false;
        this.mandatoryField_array();
    }
    /**
     * Getforms group data 
     * @param lensformdata for retriving the form data
     */
    getformGroupData(lensformdata) {
        this.physician_id = lensformdata.physician_id;
        this.orderLensSelectionForm = this._fb.group({
            // accessToken: this._StateInstanceService.accessTokenGlobal,
            rxId: '',
            PatientId: this.app.patientId,
            id: lensformdata.id,
            patient_id: lensformdata.patient_id,
            physician_id: this.physician_id,
            physician_name: lensformdata.physician_name,
            sphere_od: lensformdata.sphere_od,
            cyl_od: lensformdata.cyl_od,
            axis_od: lensformdata.axis_od,
            axis_od_va: lensformdata.axis_od_va,
            add_od: lensformdata.add_od,
            add_od_va: lensformdata.add_od_va,
            prism_od: lensformdata.prism_od,
            base_od: lensformdata.base_od,
            dist_pd_od: [lensformdata.dist_pd_od, [CommonValidator.numberDecimalValidator]],
            near_pd_od: [lensformdata.near_pd_od, [CommonValidator.numberDecimalValidator]],
            oc_od: lensformdata.oc_od,
            seg_od: lensformdata.seg_od,
            sphere_os: lensformdata.sphere_os,
            cyl_os: lensformdata.cyl_os,
            axis_os: lensformdata.axis_os,
            axis_os_va: lensformdata.axis_os_va,
            add_os: lensformdata.add_os,
            add_os_va: lensformdata.add_os_va,
            prism_os: lensformdata.prism_os,
            base_os: lensformdata.base_os,
            dist_pd_os: [lensformdata.dist_pd_os, [CommonValidator.numberDecimalValidator]],
            near_pd_os: [lensformdata.near_pd_os, [CommonValidator.numberDecimalValidator]],
            oc_os: lensformdata.oc_os,
            seg_os: lensformdata.seg_os,
            mr_od_p: lensformdata.mr_od_p,
            mr_od_prism: lensformdata.mr_od_prism,
            mr_od_splash: lensformdata.mr_od_splash,
            mr_od_sel: lensformdata.mr_od_sel,
            mr_os_p: lensformdata.mr_os_p,
            mr_os_prism: lensformdata.mr_os_prism,
            mr_os_splash: lensformdata.mr_os_splash,
            mr_os_sel: lensformdata.mr_os_sel,
            telephone: lensformdata.telephone,
            location_id: lensformdata.location_id,
            del_status: parseInt(lensformdata.dist_pd_od, 10) + parseInt(lensformdata.dist_pd_os, 10),
            rx_dos: parseInt(lensformdata.near_pd_od, 10) + parseInt(lensformdata.near_pd_os, 10),
            balance_od: lensformdata.balance_od,
            balance_os: lensformdata.balance_os,
            Vprism_od: lensformdata.Vprism_od,
            Vprism_os: lensformdata.Vprism_os,
            Vbase_od: lensformdata.Vbase_od,
            Vbase_os: lensformdata.Vbase_os,
            order_id: this.orderService.orderId,
            det_order_id: '0',
            vertex_od: lensformdata.vertex_od,
            vertex_os: lensformdata.vertex_os,
            BC_od: lensformdata.BC_od,
            BC_os: lensformdata.BC_os,
            dec_od: lensformdata.dec_od,
            dec_os: lensformdata.dec_os,
            Insert_od: lensformdata.Insert_od,
            insert_os: lensformdata.insert_os,
            Totaldec_od: lensformdata.Totaldec_od,
            totaldec_os: lensformdata.totaldec_os,
            EyeStatus: '',
            custom_rx: (lensformdata.custom_rx === 0) ? false : true,
            // vertex_od: null,
            // vertex_os: null,
            // BC_od: null,
            // BC_os: null,
            // dec_od: null,
            // dec_os: null,
            // Insert_od: null,
            // insert_os: null,
            // Totaldec_od: null,
            // totaldec_os: null,
            // far_ou: null,
            // Near_ou: null
        });

        if (this.ODEnable === true && this.OSEnable === true) {
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            this.lensSelection = 'OU';
            this.orderService.eye_Status = 'OU';
        }
        if (this.ODEnable === true && this.OSEnable === false) {
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OD', Name: 'OD' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OD', Name: 'OD' });
            this.lensSelection = 'OD';
            this.orderService.eye_Status = 'OD';
        }
        if (this.ODEnable === false && this.OSEnable === true) {
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OS', Name: 'OS' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OS', Name: 'OS' });
            this.lensSelection = 'OS';
            this.orderService.eye_Status = 'OS';
        }
        this.onChanges();

        this.setDecimals(1, 'del_status');
        this.setDecimals(1, 'rx_dos');
        this.mandatoryField_array();
        for (var items in this.orderService.mandatoryFields_Validation) {
            this.MandatoryValidation(items)
        }
        this.orderService.spectaclelensOD = this.TypeODdata;
        this.orderService.spectaclelensOS = this.TypeOSdata;
        if (this.TypeODdata || this.TypeOSdata) {
            this.orderService.order_SpectacleLens_Valid = true;
        }

        // this.sphereValidation();
        // this.powerPdValidation();
    }


    /**
     * Mandatorys validation
     * @param data for getting element names
     */
    MandatoryValidation(data) {
        if (this.orderLensSelectionForm.controls[data].value) {
            this.orderService.mandatoryFields_Validation[data].required = true;
        } else {
            this.orderService.mandatoryFields_Validation[data].required = false;
        }
    }

    /**
     * Mandatorys field array
     * @returns display the mandatory feilds
     */
    mandatoryField_array() {
        if (this.orderLensSelectionForm.controls.EyeStatus.value.Id === 'OU'
         || this.orderLensSelectionForm.controls.EyeStatus.value === 'OU') {
            this.orderService.eye_Status = 'OU';
            this.orderService.mandatoryFields_Validation = {
                dist_pd_od: { label: 'Far OD', required: false },
                dist_pd_os: { label: 'Far OS', required: false },
                sphere_od: { label: 'Sphere OD', required: false },
                sphere_os: { label: 'Sphere OS', required: false }
            };
            for (var items in this.orderService.mandatoryFields_Validation) {
                this.MandatoryValidation(items);
            }
        }
        if (this.orderLensSelectionForm.controls.EyeStatus.value.Id === 'OS' ||
         this.orderLensSelectionForm.controls.EyeStatus.value === 'OS') {
            this.orderService.eye_Status = 'OS';
            this.orderService.mandatoryFields_Validation = {
                dist_pd_os: { label: 'Far OS', required: false },
                sphere_os: { label: 'Sphere OS', required: false }
            };
            for (let items in this.orderService.mandatoryFields_Validation) {
                this.MandatoryValidation(items);
            }
        }
        if (this.orderLensSelectionForm.controls.EyeStatus.value.Id === 'OD'
        || this.orderLensSelectionForm.controls.EyeStatus.value === 'OD') {
            this.orderService.eye_Status = 'OD';
            this.orderService.mandatoryFields_Validation = {
                dist_pd_od: { label: 'Far OD', required: false },
                sphere_od: { label: 'Sphere OD', required: false }
            };
            for (var items in this.orderService.mandatoryFields_Validation) {
                this.MandatoryValidation(items);
            }
        }
    }
    LoadMasters() {
        this.LoadEyedetails();
        this.LoadMaterialTypeDetails();
        this.LoadLensDetails();
    }

    /**
     * Btns act
     * @param event for getting btn confirm data
     */
    btnAct(event) {

        if (event.id === 0) {
            if (event.action === true) {
                // Call If Yes /Continue
                this.popCallBackRoute(event.action);

                // Hide Dialogue
                this.showDialog = false;
            }

            if (event.action === false) {
                // Call If No
                this.popCallBackRoute(event.action);

                // Hide Dialogue
                this.showDialog = false;
            }
        } else {
            if (event.action === true) {
                this.orderService.lensPopupOrderCondi = false;
                this.lensSelectionData = [];
                this.patientID = this.app.patientId;
                this.rxService.getLensSelection(this.patientID).subscribe(
                    data => {
                        // const index = ['data'].length - 1;
                        this.lensSelectionData = data['data'];
                        this.getformGroupData(this.lensSelectionData[0]);
                        this.orderLensSelectionForm.controls['id'].setValue('');
                    });
                this.showDialog = false;
            }

            if (event.action === false) {
                this.formGroupData();
                this.orderService.lensPopupOrderCondi = false;
                this.showDialog = false;
            }
        }

    }




    // PD OS OD updater
    pdOSODUpdate(res: boolean = false) {
        // Far PD Values
        const far_od = this.orderLensSelectionForm.controls['dist_pd_od'];
        const far_os = this.orderLensSelectionForm.controls['dist_pd_os'];
        const far_ou = this.orderLensSelectionForm.controls['del_status'];

        const float_far_od = (far_od.value != null && far_od.value !== '') ? far_od.value : 0;
        const float_far_os = (far_os.value != null && far_os.value !== '') ? far_os.value : 0;
        if (res) {
            const temp_ou = parseFloat(float_far_od) + parseFloat(float_far_os);
            far_ou.patchValue(temp_ou);
            this.setDecimals(1, 'dist_pd_od');
            this.setDecimals(1, 'dist_pd_os');
            this.setDecimals(1, 'del_status');

        } else {
            if (this.popParams[0].far_od !== undefined) {
                const prev_od_value: number = this.popParams[0].far_od;
                far_od.patchValue(prev_od_value);
                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            } else if (this.popParams[0].far_os !== undefined) {
                const prev_os_value: number = this.popParams[0].far_os;
                far_os.patchValue(prev_os_value);
                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            }
        }

    }


    // PD OU updater
    pdOUUpdate(res: boolean = false) {
        // Far PD Values
        const far_od = this.orderLensSelectionForm.controls['dist_pd_od'];
        const far_os = this.orderLensSelectionForm.controls['dist_pd_os'];
        const far_ou = this.orderLensSelectionForm.controls['del_status'];

        if (res) {
            const half_ou_far = far_ou.value / 2;
            far_od.patchValue(half_ou_far);
            far_os.patchValue(half_ou_far);

            this.setDecimals(1, 'dist_pd_od');
            this.setDecimals(1, 'dist_pd_os');
            this.setDecimals(1, 'del_status');
        } else {

            const prev_ou_value: number = this.popParams[0].far_ou;

            // Check Previous OU and OS value sum equal to OU Value
            const add = parseFloat(far_od.value) + parseFloat(far_os.value);
            if (add === prev_ou_value) {
                // If sum match.
                far_ou.patchValue(add);
                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            } else {

                // if sum do not match
                const half_ou_far_h = prev_ou_value / 2;
                far_od.patchValue(half_ou_far_h);
                far_os.patchValue(half_ou_far_h);
                far_ou.patchValue(prev_ou_value);

                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            }
        }
    }


    // Seg Update
    segUpdate(res) {
        if (res) {
            if (this.popParams[0].seg_od !== undefined) {
                this.setDecimals(1, 'seg_od');
            } else if (this.popParams[0].seg_os !== undefined) {
                this.setDecimals(1, 'seg_os');
            }
        } else if (!res) {
            if (this.popParams[0].seg_od !== undefined) {
                const prev_seg_value: number = this.popParams[0].seg_od;
                this.orderLensSelectionForm.controls['seg_od'].patchValue(prev_seg_value);
                this.setDecimals(1, 'seg_od');
            } else if (this.popParams[0].seg_os !== undefined) {
                const prev_seg_value: number = this.popParams[0].seg_os;
                this.orderLensSelectionForm.controls['seg_os'].patchValue(prev_seg_value);
                this.setDecimals(1, 'seg_os');
            }
        }
    }


    // H Prism Update
    hprismUpdate(res) {
        if (res) {
            // HPrism
            if (this.popParams[0].mr_od_p !== undefined) {
                this.setDecimals(2, 'mr_od_p');
            } else if (this.popParams[0].mr_os_p !== undefined) {
                this.setDecimals(2, 'mr_os_p');
            }

            // VPrism
            if (this.popParams[0].Vprism_od !== undefined) {
                this.setDecimals(2, 'Vprism_od');
            } else if (this.popParams[0].Vprism_os !== undefined) {
                this.setDecimals(2, 'Vprism_os');
            }
        } else if (!res) {
            // H Prism
            if (this.popParams[0].mr_od_p !== undefined) {
                const prev_hprism_value: number = this.popParams[0].mr_od_p;
                this.orderLensSelectionForm.controls['mr_od_p'].patchValue(prev_hprism_value);
                this.setDecimals(2, 'mr_od_p');
            } else if (this.popParams[0].mr_os_p !== undefined) {
                const prev_hprism_value: number = this.popParams[0].mr_os_p;
                this.orderLensSelectionForm.controls['mr_os_p'].patchValue(prev_hprism_value);
                this.setDecimals(2, 'mr_os_p');
            }


            // V Prism
            if (this.popParams[0].Vprism_od !== undefined) {
                const prev_hprism_value: number = this.popParams[0].Vprism_od;
                this.orderLensSelectionForm.controls['Vprism_od'].patchValue(prev_hprism_value);
                this.setDecimals(2, 'Vprism_od');
            } else if (this.popParams[0].Vprism_os !== undefined) {
                const prev_hprism_value: number = this.popParams[0].Vprism_os;
                this.orderLensSelectionForm.controls['Vprism_os'].patchValue(prev_hprism_value);
                this.setDecimals(2, 'Vprism_os');
            }
        }
    }


    // POP CALL Router
    popCallBackRoute(res: boolean = false) {
        const pop_to_call = this.popFor;


        // For PD OU
        if (pop_to_call === 'PD-OU') {
            this.pdOUUpdate(res);
        }

        // For PD-OS-OD
        if (pop_to_call === 'PD-OS-OD') {
            this.pdOSODUpdate(res);
        }

        // For Seg Height
        if (pop_to_call === 'SEG-OS-OD') {
            this.segUpdate(res);
        }

        // HPrism
        if (pop_to_call === 'HPRISM') {
            this.hprismUpdate(res);
        }
    }


    // Seg Height Validate
    /**
     * Segs validate
     * dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
     */
    segValidate(event: any) {
        const ele_name_sg = event.target.attributes.formControlName.value;
        const ele_val: number = event.target.value;
        const sg_height = event.target.value;
        if (sg_height < '10' || sg_height > '30') {


            // Setting Up Old value
            if (ele_name_sg === 'seg_os') {
                this.popParams = [{ seg_os: event.target.save_value }];
            } else if (ele_name_sg === 'seg_od') {
                this.popParams = [{ seg_od: event.target.save_value }];
            }
            // Prompt
            this.showDialog = true;
            this.popFor = 'SEG-OS-OD';

            this.showDialog = true;
            this.className = 'dialog card ';
            this.message_pop = 'Add value is outside of the standard range. Are you sure you want to continue?';
            this.button_pop = 'Yes';
            this.button_pop2 = 'No';
            const dialogObject = {
                'header': 'Warning!',
                'footer': 'Add value is outside of the standard range. Are you sure you want to continue?',
                'buttonone': 'Yes',
                'buttontwo': 'No'
            }
            this.alertpopup.dialogBox(dialogObject);
        } else {
            this.setDecimals(1, ele_name_sg);
        }
    }


    // OU Validate
    /**
     * Determines whether pdvalidator ou
     *  dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
     */
    ouPDvalidator(e) {
        const ele_val: number = e.target.value;
        const ele_name_pd = e.target.attributes.formControlName.value;
        if (ele_name_pd === 'del_status') {
            if (ele_val >= 40 && ele_val <= 80) {
                const half_ou_far: number = e.target.value / 2;
                this.orderLensSelectionForm.controls['dist_pd_od'].patchValue(half_ou_far);
                this.orderLensSelectionForm.controls['dist_pd_os'].patchValue(half_ou_far);

                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            } else {

                // Setting Up Old value
                this.popParams = [{ far_ou: e.target.save_value }];
                // Prompt
                this.showDialog = true;
                this.popFor = 'PD-OU';
                this.className = this.className + '';
                this.message_pop = 'Binocular PD Far entered is outside of the standard range. Are you sure you want to continue ?';
                this.button_pop = 'Yes';
                this.button_pop2 = 'No';
                const dialogObject = {
                    'header': 'Warning!',
                    'footer': 'Binocular PD Far entered is outside of the standard range. Are you sure you want to continue ?',
                    'buttonone': 'Yes',
                    'buttontwo': 'No'
                }
                this.alertpopup.dialogBox(dialogObject);
            }
        } else {
            const half_ou_near: number = e.target.value / 2;
            this.orderLensSelectionForm.controls['near_pd_od'].patchValue(half_ou_near);
            this.orderLensSelectionForm.controls['near_pd_os'].patchValue(half_ou_near);

            this.setDecimals(1, 'near_pd_od');
            this.setDecimals(1, 'near_pd_os');
            this.setDecimals(1, 'rx_dos');

        }
    }

    // OD OS Validate
    /**
     * od pdvalidator
     * dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
     */
    os_odPDvalidator(e) {
        const ele_name_pd = e.target.attributes.formControlName.value;
        const ele_val: number = e.target.value;
        if (ele_name_pd === 'dist_pd_od' || ele_name_pd === 'dist_pd_os') {
            const far_od = this.orderLensSelectionForm.controls['dist_pd_od'];
            const far_os = this.orderLensSelectionForm.controls['dist_pd_os'];

            if (ele_val >= 20 && ele_val <= 40) {
                const add = parseFloat(far_od.value) + parseFloat(far_os.value);
                this.orderLensSelectionForm.controls['del_status'].patchValue(add);
                this.setDecimals(1, 'dist_pd_od');
                this.setDecimals(1, 'dist_pd_os');
                this.setDecimals(1, 'del_status');
            } else {
                // Setting Up Old value
                if (ele_name_pd === 'dist_pd_os') {
                    this.popParams = [{ far_os: e.target.save_value }];
                } else if (ele_name_pd === 'dist_pd_od') {
                    this.popParams = [{ far_od: e.target.save_value }];
                }

                // Prompt
                this.showDialog = true;
                this.popFor = 'PD-OS-OD';
                this.className = this.className + '';
                this.message_pop = 'Monocular PD Far entered is outside of the standard range. Are you sure you want to continue ?';
                this.button_pop = 'Yes';
                this.button_pop2 = 'No';
                const dialogObject = {
                    'header': 'Warning!',
                    'footer': 'Monocular PD Far entered is outside of the standard range. Are you sure you want to continue ?',
                    'buttonone': 'Yes',
                    'buttontwo': 'No'
                }
                this.alertpopup.dialogBox(dialogObject);
            }
        } else {
            const near_od = this.orderLensSelectionForm.controls['near_pd_od'];
            const near_os = this.orderLensSelectionForm.controls['near_pd_os'];
            const add = parseFloat(near_od.value) + parseFloat(near_os.value);
            this.orderLensSelectionForm.controls['rx_dos'].patchValue(add);
            this.setDecimals(1, 'near_pd_od');
            this.setDecimals(1, 'near_pd_os');
            this.setDecimals(1, 'rx_dos');
            // Showing pop up message
            this.showDialog = true;
            this.popFor = 'PD-OS-OD';
            this.className = this.className + '';
            this.message_pop = 'Monocular PD Far entered is outside of the standard range. Are you sure you want to continue ?';
            this.button_pop = 'Yes';
            this.button_pop2 = 'No';
            const dialogObject = {
                'header': 'Warning!',
                'footer': 'Monocular PD Far entered is outside of the standard range. Are you sure you want to continue ?',
                'buttonone': 'Yes',
                'buttontwo': 'No'
            }
            this.alertpopup.dialogBox(dialogObject);
        }
    }

    // Main PD Validate
    /**
     * Determines whether pdvalidator c
     * @param e for getting enter event
     */
    cPDvalidator(e) {
        const ele_name_pd = e.target.attributes.formControlName.value;

        const ele_val = e.target.value;

        const hist_val = e.target.save_value;

        if (ele_name_pd === 'del_status' || ele_name_pd === 'rx_dos') {
            this.ouPDvalidator(e);
        } else {
            this.os_odPDvalidator(e);
        }
    }

    setDecimals(max: number = 1, ele: string = null) {
        // Get Element Value
        const ele_val = this.orderLensSelectionForm.controls[ele].value;
        if (ele_val !== 0 && ele_val !== '' && ele_val != null) {
            this.orderLensSelectionForm.controls[ele].patchValue(parseFloat(ele_val).toFixed(max));
        }
    }


    /*fairValues(data, type) {
 
        if (this.orderLensSelectionForm.controls['dist_pd_od'].value == null &&
         this.orderLensSelectionForm.controls['dist_pd_os'].value == null) {
            this.orderLensSelectionForm.controls['del_status'].patchValue("");
        }
        if ((this.orderLensSelectionForm.controls['dist_pd_od'].value == null ||
         this.orderLensSelectionForm.controls['dist_pd_od'].value == "") &&
          this.orderLensSelectionForm.controls['dist_pd_os'].value != null) {
            this.orderLensSelectionForm.controls['del_status'].patchValue(this.orderLensSelectionForm.controls['dist_pd_os'].value);
        }
        if (this.orderLensSelectionForm.controls['dist_pd_od'].value != null &&
         (this.orderLensSelectionForm.controls['dist_pd_os'].value == null ||
          this.orderLensSelectionForm.controls['dist_pd_os'].value == "")) {
            this.orderLensSelectionForm.controls['del_status'].patchValue(this.orderLensSelectionForm.controls['dist_pd_od'].value);
        }
        if (this.orderLensSelectionForm.controls['dist_pd_od'].value != null &&
         this.orderLensSelectionForm.controls['dist_pd_os'].value != null) {
            this.orderLensSelectionForm.controls['del_status'].patchValue((
                parseFloat(this.orderLensSelectionForm.controls['dist_pd_os'].value)) + (
                    parseFloat(this.orderLensSelectionForm.controls['dist_pd_od'].value)))
        }
    }
    nearValues(data, type) {
 
        if (this.orderLensSelectionForm.controls['near_pd_od'].value == null &&
         this.orderLensSelectionForm.controls['near_pd_os'].value == null) {
            this.orderLensSelectionForm.controls['rx_dos'].patchValue("");
        }
        if ((this.orderLensSelectionForm.controls['near_pd_od'].value == null ||
         this.orderLensSelectionForm.controls['near_pd_od'].value == "") &&
          this.orderLensSelectionForm.controls['near_pd_os'].value != null) {
            this.orderLensSelectionForm.controls['rx_dos'].patchValue(
                this.orderLensSelectionForm.controls['near_pd_os'].value);
        }
        if (this.orderLensSelectionForm.controls['near_pd_od'].value != null &&
         (this.orderLensSelectionForm.controls['near_pd_os'].value == null ||
          this.orderLensSelectionForm.controls['near_pd_os'].value == "")) {
            this.orderLensSelectionForm.controls['rx_dos'].patchValue(this.orderLensSelectionForm.controls['near_pd_od'].value);
        }
        if (this.orderLensSelectionForm.controls['near_pd_od'].value != null &&
         this.orderLensSelectionForm.controls['near_pd_os'].value != null) {
            this.orderLensSelectionForm.controls['rx_dos'].patchValue((
                parseFloat(this.orderLensSelectionForm.controls['near_pd_os'].value)) + (
                    parseFloat(this.orderLensSelectionForm.controls['near_pd_od'].value)))
        }
    }
 
    fairValuesOU(OU) {
 
        if (this.orderLensSelectionForm.controls['del_status'].value == null) {
            this.orderLensSelectionForm.controls['dist_pd_od'].patchValue("0")
            this.orderLensSelectionForm.controls['dist_pd_os'].patchValue("0")
        } else {
            this.count = parseFloat(this.orderLensSelectionForm.controls['del_status'].value) / 2
            this.orderLensSelectionForm.controls['dist_pd_od'].patchValue(this.count)
            this.orderLensSelectionForm.controls['dist_pd_os'].patchValue(this.count)
 
        }
 
    }
    nearValuesOU(OU) {
 
        if (this.orderLensSelectionForm.controls['rx_dos'].value == null) {
            this.orderLensSelectionForm.controls['near_pd_od'].patchValue("0")
            this.orderLensSelectionForm.controls['near_pd_os'].patchValue("0")
        } else {
            this.count = parseFloat(this.orderLensSelectionForm.controls['rx_dos'].value) / 2
            this.orderLensSelectionForm.controls['near_pd_od'].patchValue(this.count)
            this.orderLensSelectionForm.controls['near_pd_os'].patchValue(this.count)
 
        }
 
    } */


    LoadEyedetails() {
        // this.eyedetails = [
        // { Id: 'OU', Name: 'OU' },
        // { Id: 'OD', Name: 'OD' },
        // { Id: 'OS', Name: 'OS' }
        // ];
        // // this.eyedetails.push({ Id: null, Name: "Select Eye Details" });
        // this.eyedetails.push({ Id: 'OU', Name: 'OU' });
        // this.eyedetails.push({ Id: 'OD', Name: 'OD' });
        // this.eyedetails.push({ Id: 'OS', Name: 'OS' });


    }
    /**
     * Loads material type details
     * drop down data loading
     */
    LoadMaterialTypeDetails() {
        this.materials = this.dropdownService.materials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    this.materials.push({ value: '', name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materials.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.materials = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    LoadLensDetails() {
        this.lensLab = this.dropdownService.lensLab;
        if (this.utility.getObjectLength(this.lensLab) === 0) {
            this.dropdownService.getLensLabDetails().subscribe((values: LensLab) => {
                try {
                    const dropData = values.data;
                    this.lensLab.push({ Id: '', Name: 'Select Lab' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.lensLab.push({ Id: dropData[i].id, Name: dropData[i].lab_name });
                    }
                    this.dropdownService.lensLab = this.lensLab;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads lens selection
     */
    loadLensSelection() {
        this.lensSelectionData = [];
        this.patientID = this.app.patientId;
        this.rxService.getLensSelection(this.patientID).subscribe(
            data => {
                this.lensSelectionData = data['data'][0];
            });

    }

    // Says that type of spectacleslens are the selected here
    spectaclesLensUPC(type) {
        if (type === 'OD') {
            this.orderService.spectaclesLensType = type;
            this.selectSpectacleLensSidebar = true;
            this.orderService.lensSelectionUPCStatus = 'lens';
            this.orderService.order_SpectacleLens_Valid = true;
        }
        if (type === 'OS') {
            // this.orderService.orderLensTypeUPC.next(type);
            this.orderService.spectaclesLensType = type;
            this.selectSpectacleLensSidebar = true;
            this.orderService.lensSelectionUPCStatus = 'lens';
            this.orderService.order_SpectacleLens_Valid = true;
        }

    }
    onEquals() {
        // this.orderService.orderLensTypeUPC.next(type);
        this.equalDataOS = '';
        this.TypeOSdata = '';
        this.equalDataOS = this.EqualData
        this.TypeOSdata = this.TypeODdata;

        const dataobjOS = {
            'UPC': this.EqualData.upc_code,
            'Name': this.EqualData.name,
            'ModuleTypeId': 2,
            'uniqueid': '3'
        };

        this.orderService.OSNAME.next(dataobjOS);
        this.orderService.OSITEID.next(this.EqualData.id);
        this.orderService.spectaclelensOS = this.EqualData.id
        this.itemIdValueOS = this.EqualData.id;
        this.ItemNameOS = this.EqualData.name;
        this.UPCValueOS = this.EqualData.upc_code;

        this.selectSpectacleLensSidebar = false;
    }
    /**
     * Determines whether eye change on
     * @param data for dropdown change event
     */
    onEyeChange(data) {
        this.lensSelection = this.orderLensSelectionForm.controls['EyeStatus'].value.Id;
        if (this.lensSelection === 'OS') {
            this.disableOD = true;
            this.disableOS = false;
            this.disableOU = true;
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OS', Name: 'OS' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OS', Name: 'OS' });
            this.orderService.eye_Status = 'OS';
        }
        if (this.lensSelection === 'OD') {
            this.disableOS = true;
            this.disableOD = false;
            this.disableOU = true;
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OD', Name: 'OD' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OD', Name: 'OD' });
            this.orderService.eye_Status = 'OD';
        }
        if (this.lensSelection === 'OU') {
            this.disableOD = false;
            this.disableOS = false;
            this.disableOU = false;
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            this.orderLensSelectionForm.controls['EyeStatus'].patchValue({ Id: 'OU', Name: 'OU' });
            this.orderService.eye_Status = 'OU';
        }
        // if (data.target.value == "Select Eye Details") {
        //     this.disableOD = false;
        //     this.disableOS = false;
        //     this.disableOU = false;
        // }

        this.mandatoryField_array();
    }

    /**
     * Confirms order lens selection component
     */
    confirm() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to load Previous RX details? ',
            header: 'Confirmation',
            // icon: 'fa fa-exclamation-triangle',
            accept: () => {
                this.orderService.lensPopupOrderCondi = false;
                // this.lensSelectionData.forEach(element => {
                //     if (element.order_id == parseInt(this.orderlensId)) {
                //         this.lensSelectionOrderId = element.id;
                //         this.getformGroupData(element);
                //     }
                // });

                this.lensSelectionData = [];
                this.patientID = this.app.patientId;
                this.rxService.getLensSelection(this.patientID).subscribe(
                    data => {
                        // alert(data['data'])
                        //  console.log(data);
                        // const index = ['data'].length - 1;
                        this.lensSelectionData = data['data'];
                        //  console.log(this.lensSelectionData[index])
                        this.getformGroupData(this.lensSelectionData[0]);
                        this.orderLensSelectionForm.controls['id'].setValue('');
                    });
            },
            reject: () => {
                this.formGroupData();
                this.orderService.lensPopupOrderCondi = false;
            }
        });
    }
    /**
     * Orders lens form change
     */
    orderLensFormChange() {
        // this.sphereValidation();
        // this.powerPdValidation();
        if (this.orderService.orderSaveUpdateStatus === false) {
            this.orderService.orderFormChangeStatus = false;
        }
    }

    /**
     * Shows outside physician
     * @param event show/hide the Outside Physician
     */
    showOutsidePhysician(event) {
        if (event === true) {
            this.outsidePhysician = true;
        } else {
            this.outsidePhysician = false;
        }
    }
    /**
     * Decimals validator
     * @param controller validation for od and os value
     */
    decimalValidator(controller: string) {
        if (this.orderLensSelectionForm.controls[controller].value > 99) {
            this.orderLensSelectionForm.controls[controller].patchValue(null);
        }
        if (this.orderLensSelectionForm.get(controller).hasError('invalidDecimal') === true) {
            this.orderLensSelectionForm.controls[controller].patchValue(null);
        }
    }
    // WarningShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Success Message', detail: message });
    // }
}
