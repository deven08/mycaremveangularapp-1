import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
    AfterSalesComponent,
    DeliveryComponent,
    OrderContactLensMainComponent,
    OrderMainComponent,
    OrderFrameMainComponent,
    OrderSpectacleLensMainComponent,
    OrderSearchComponent,
    OrderPaymentsComponent,
    OrderOtherDetailsMainComponent,
    OrderHardContactLensMainComponent,
    OrderSoftContactLensMainComponent,
    ClaimordersComponent
    
} from './';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'order',
                component: OrderMainComponent,
                data: { 'title': 'Orders', 'banner' : true }
            },
            {
                path: 'frame',
                component: OrderFrameMainComponent,
                data: { 'title': 'Frames  Orders', 'banner' : true }
            },
            {
                path: 'after-sales',
                component: AfterSalesComponent,
                data: { 'title': 'After Sales  Orders', 'banner' : true }
            },
            {
                path: 'delivery',
                component: DeliveryComponent,
                data: { 'title': 'Delivery  Orders', 'banner' : true }
            },
            {
                path: 'spectacle-lens',
                component: OrderSpectacleLensMainComponent,
                data: { 'title': 'Spectacle Lens  Orders', 'banner' : true }
            },
            {
                path: 'search',
                component: OrderSearchComponent,
                data: { 'title': 'Search  Orders', 'banner' : true }
            },
            {
                path: 'contact-lens',
                component: OrderContactLensMainComponent,
                data: { 'title': 'Contact Lens  Orders', 'banner' : true }
            },
            {
                path: 'payments',
                component: OrderPaymentsComponent,
                data: { 'title': 'Payments  Orders', 'banner' : true }
            },
            {
                path: 'claims',
                component: ClaimordersComponent,
                data: { 'title': 'claims  Orders', 'banner' : true }
            },
            {
                path: 'other-details',
                component: OrderOtherDetailsMainComponent,
                data: { 'title': 'Other Details  Orders', 'banner' : true }
            },
            {
                path: 'hard-contact-lens',
                component: OrderHardContactLensMainComponent,
                data: { 'title': 'Hard Contact Lens  Orders', 'banner' : true }
            },
            {
                path: 'soft-contact-lens',
                component: OrderSoftContactLensMainComponent,
                data: { 'title': 'Soft Contact Lens  Orders', 'banner' : true }
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrderRoutingModule { }

