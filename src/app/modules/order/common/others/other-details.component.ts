import { Component, OnInit, OnDestroy } from '@angular/core';
// import { ContactlensService } from '../../ConnectorEngine/services/contactlens.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Subject, Subscription } from 'rxjs';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { OrderService } from '@services/order/order.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService, ErrorService } from '@app/core';
import { ContactlensService } from '@services/order/contactlens.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { orderSave, OtherDetailsModel, LabIntegration } from '@app/core/models/order/order.model';
import { vendorDetails } from '@app/core/models/order/vendor-details.model';
import { TreatmentCategory } from '@app/core/models/masterDropDown.model';

interface City {
    name: string;
}

@Component({
    selector: 'app-order-other-details',
    templateUrl: 'other-details.component.html'
})

export class OrderOtherDetailsComponent implements OnInit, OnDestroy {

    /**
     * Category this variable is used for the binding the data to the model
     */
    categoryList: any = '';
    hardContactLensOrderdetailsOS: Subscription;
    hardContactLensOrderdetailsOD: Subscription;
    contactlensotherdetailsvalues: Subscription;
    softContactLensOrderdetailsOS: Subscription;
    softContactLensOrderdetailsOD: Subscription;
    framename: any;
    subscriptionFrameID: Subscription;
    framesonly: Subscription;
    subscriptionFrameNAME: Subscription;
    subscriptionOSITEID: Subscription;
    subscriptionOSNAME: Subscription;
    subscriptionODITEID: Subscription;
    subscriptionODNAME: Subscription;
    subscriptionOrderCancle: Subscription;
    displayname: string;
    cols: any;
    Select: any = 'select';
    orderTryLabelSidebar: boolean;
    orderDiscountSidebar: boolean;
    orderStatusSidebar: boolean;
    modifySoftContactSidebar: boolean;
    promisedatehistorySidebar: boolean;
    addOrderOtherDetails: boolean;
    laborderhistory: boolean;
    modifyFramesSidebar: boolean;
    modifyLensTreatmentSidebar: boolean;
    modifySpectacleLensSidebar: boolean;
    chargecolumns: any[];
    addPatientInfoSidebar;
    afterSalesSidebar = false;
    //  returnWizardSidebar : boolean = false;
    paymentDetails;
    orderTypes: any;
    public chargevalues: any;
    public chargehistoryvalues: any;
    public pushchargevalues: any;
    sort: City[];
    ischecked: boolean;
    public copycharges: any;
    orderid: Subscription;
    orderSpectacleLensSave: Subscription;
    subscriptionLocationId: Subscription;
    subscriptionOrderId: Subscription;
    valueforitem: any;
    retailPrice: any;
    public orderId = new Subject<any>();
    orderId$ = this.orderId.asObservable();
    public LensTreatments: any[];
    public lensTreatmentslist: any;
    public Soldby: any[];
    public Soldbylist: any;
    public fittedby: any[];
    public fittedbylist: any;
    public inspectedbydata: any[];
    public inspectedbylist: any;
    public deliveredbdata: any[];
    public deliveredbylist: any;
    public labby: any[];
    public labsList: any[];
    public Reprocess: any[];
    public Reprocesslist: any[];
    public labstatusdata: any[];
    public labstatuslist: any;
    val1 = 'Options';
    public Odname: any;
    public OdID: any;
    public OSname: any;
    public OSID: any;
    public previousODID: any;
    public previousOSID: any;
    public FrameID: any;
    public previousFrameID: any;
    public itemIDStorage = false;
    saveData = [];
    LocationId = '';
    isActive = false;
    subscriptionEqualNAME: Subscription;
    subscriptionEqualITEID: Subscription;
    OrderEncounterID: Subscription;
    frameUPCid = '';
    lensUPCODid = '';
    lensUPCOSid = '';
    public totalbalance: any;
    public pricebalance: any;
    framebalance: any;
    lensselectionbalance: any;
    otherdetailsbalance: any;
    encounterID: any;
    paymentbutton = false;
    returnReasonSidebar = false;

    copay: any = '';
    patient = '';
    patientAmount: any = '';
    insuranceAmount: any = '';
    /**
     * Notes  of order other details component
     */
    notes: any = '';
    itemTotal: any = '';
    // lab_id: any = '';
    TrayNumber: any = '';
    LabStatus: any = '';
    order_status: any = '';
    due_date: any = '';
    LabOrder: any = '';
    reprocessreason_id: any = '';
    sold_by: any = '';
    sold_datetime: any = '';
    employee_name: any = '';
    FiitedBy: any = '';
    FittedDatetime: any = '';
    InspectedBy: any = '';
    InspectedDatetime: any = '';
    DeliveredBy: any = '';
    DeliveredDatetime: any = '';
    Labotherdetails: FormGroup;
    returnInputValues: any[] = [];
    treatCategory: any;
    treatCategorylist: any;
    modifyItemId: any[] = [];
    discountItemsData: any = [];
    totalRetailPrice: any = 0;

    /**
     * Frames  of order other details component
     */
    frames: number = 0;

    /**
     * Frames string of order other details component
     */
    framesString: string = '';

    /**
     * Lens  of order other details component
     */
    lens: number = 0;

    /**
     * Lens string of order other details component
     */
    lensString: string = '';

    /**
     * Service  of order other details component
     */
    service: number = 0;

    /**
     * Service stirng of order other details component
     */
    serviceStirng: string = '';

    /**
     * Category  of order other details component
     */
    category: number = 0;
    StatusDropDis: boolean = false;

    /**
     * Category stirng of order other details component
     */
    categoryStirng: string = '';

    /**
     * Total amount benfit of order other details component is total benfit after discount
     */
    totalAmountBenfit: any = 0;

    /**
     * Retail price total of order other details component
     */
    retailPriceTotal: any = 0;
    /**
     * Transcation date of order other details component
     */
    trans_del_date: any;
    /**
     * Emp name of order other details component
     */
    emp_name: any;
    /**
     * Encounter id of order other details component
     */
    encounter_id: any;
    /**
     * Write of amount of order other details component
     */
    writeOfAmount: any;

    /**
     * Starting length charges of order other details component
     */
    startingLengthCharges: any = 0;

    /**
     * Only othersaving of order other details component
     */
    onlyOthersaving: Subscription;
    /**
     * Lab int disable of order other details component
     */
    labIntDisable: boolean = true;

    /**
     * Order save id of order other details component
     */
    orderSaveId: any = '';

    /**
     * Lab data for item vendor details of the item of order other details component
     */
    labDataForItem: vendorDetails;

    /**
     * Integration type of is what type of lab integration we are going to made order other details component
     */
    integrationType: string;
    /**
     * Upc search is the values binded to the model what you had enterd based on that caterogy list will be filtered.
     */
    upcSearch: string;

    /**
     * Lab integration check here we are checking the order integrated  to the  lab or not.
     */
    labIntegrationCheck: string;

    /**
     * Lab selected for order here we binding lab of selected order.
     */
    labSelectedForOrder: string;
    /**
     * Creates an instance of order other details component. 
     * @param contactlensService for contact lens methods
     * @param _fb  for formbuilder
     * @param spectcalelensServicee for spectacle lens methods
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     * @param app global service for instanceses
     */
    constructor(
        public orderService: OrderService,
        private router: Router,
        private _fb: FormBuilder,
        private contactlensService: ContactlensService,
        public app: AppService,
        private dropdownService: DropdownService,
        private errorService: ErrorService,
        private utility: UtilityService,
    ) {
        this.upcSearch = '';

        this.integrationType = '';
        this.OrderEncounterID = orderService.orderEncounterId$.subscribe(data => {
            this.encounterID = data;
            this.encounter_id = this.encounterID;
            this.paymentbutton = true;
        })
        this.orderid = orderService.orderSaveNew$.subscribe((dataSet: string) => {
            this.orderService.orderId = 'New Order';
            this.itemIDStorage = false;
            this.saveData.forEach(saveDataSet => {
                saveDataSet.id = '';
            });
            this.chargevalues.forEach(values => {
                values.id = '';
            });
        });
        //here we are laoding the contactlens orders  data when selected from
        // the history dropdown either order or new order
        this.contactlensotherdetailsvalues = this.orderService.orderContactLensById$.subscribe((value: string) => {
            if (value !== 'New Order') {
                this.chargevalues = [];
                this.contactlensService.contactlensotherdetails = [];
                // alert("contactlens")
                this.Loadorderhistorydetails(value);
                this.itemIDStorage = true;
                this.orderService.orderFormChangeStatus = false;
            } else {
                this.chargevalues = [];
                this.Labotherdetails.controls.lab_id.setValue(null)
                this.contactlensService.contactlensotherdetails = [];
                this.itemIDStorage = false;
                this.totalbalance = 0;
                this.retailPriceTotal = 0;
                this.chargevalues = [];

            }
        });


        this.framesonly = orderService.FramesOnly$.subscribe((data: string) => {
            if (data === 'close') {
                this.orderTryLabelSidebar = false;
            }
            if (data === 'Spectacle lens') {
                this.isActive = true;
            } else {
                this.isActive = false;
            }
        });
        this.subscriptionEqualNAME = orderService.EqualNAME$.subscribe(data => {
            this.Odname = data;
        });
        this.subscriptionEqualITEID = orderService.EqualITEID$.subscribe(data => {

            this.OdID = data;
            const valuetoremoves = this.chargevalues.findIndex(x => x.uniqueid === '3');
            if (valuetoremoves !== -1) {
                this.chargevalues.splice(valuetoremoves, 1);
            }

            const valuetoremove = this.chargevalues.findIndex(x => x.uniqueid === '2');
            if (valuetoremove !== -1) {
                this.chargevalues.splice(valuetoremove, 1);
            }

            this.getpricebyIdname(this.Odname, this.OdID, 'OD');

            const a = this.chargevalues.findIndex(x => x.id === this.previousODID);
            if (a !== -1) {
                this.chargevalues.splice(a, 1);
            }
            this.previousODID = data;
        });

        this.subscriptionODNAME = orderService.ODNAME$.subscribe(data => {
            this.Odname = data;
        });
        this.subscriptionODITEID = orderService.ODITEID$.subscribe(data => {


            this.OdID = data;
            const valuetoremove = this.chargevalues.findIndex(x => x.uniqueid === '2');
            if (valuetoremove !== -1) {
                this.chargevalues.splice(valuetoremove, 1);
            }
            this.getpricebyIdname(this.Odname, this.OdID, 'OD');

            const a = this.chargevalues.findIndex(x => x.id === this.previousODID);
            if (a !== -1) {
                this.chargevalues.splice(a, 1);
            }
            this.previousODID = data;

        });
        this.subscriptionOSNAME = orderService.OSNAME$.subscribe(data => {
            this.OSname = data;
        });
        this.subscriptionOSITEID = orderService.OSITEID$.subscribe(data => {
            this.OSID = data;
            const valuetoremove = this.chargevalues.findIndex(x => x.uniqueid === '3');
            if (valuetoremove !== -1) {
                this.chargevalues.splice(valuetoremove, 1);
            }
            this.getpricebyIdname(this.OSname, this.OSID, 'OS');

            const a = this.chargevalues.findIndex(x => x.id === this.previousOSID);
            if (a !== -1) {
                this.chargevalues.splice(a, 1);
            }
            this.previousOSID = data;

        });

        this.subscriptionFrameID = orderService.FrameNAMEID$.subscribe(frame => {
            this.framename = frame;

        });

        this.subscriptionFrameNAME = orderService.FrameNAME$.subscribe(data => {

            this.FrameID = data;
            const valuetoremove = this.chargevalues.findIndex(x => x.uniqueid === '1');
            if (valuetoremove !== -1) {
                this.chargevalues.splice(valuetoremove, 1);
            }
            this.getpricebyIdname(this.framename, this.FrameID, '');

            const a = this.chargevalues.findIndex(x => x.id === this.previousFrameID);
            if (a !== -1) {
                this.chargevalues.splice(a, 1);
            }
            this.previousFrameID = data;

        });
        this.onlyOthersaving = orderService.onlyOtherItemsSaving$.subscribe(data => {
            this.otherOrdersaving(data);
        })

        // here is for saving the other type category details for spectacle lens and saving lab for all type of orders
        this.orderSpectacleLensSave = orderService.orderspectacleLensSave$.subscribe(data => {
            if (this.chargevalues.length !== 0) {
                if (this.Labotherdetails.controls['lab_id'].value !== null) {
                    this.orderService.updateLaborderotherdetails(this.Labotherdetails.value).subscribe(updateLabData => {

                    });
                }
                //  else {
                //     return false
                // }
                for (let i = 0; i < this.chargevalues.length; i++) {
                    if (this.chargevalues[i].module_type_id === '1' || this.chargevalues[i].module_type_id === 1 || this.chargevalues[i].module_type_id === '2' || this.chargevalues[i].module_type_id === 2) {

                    }
                    else if (this.chargevalues[i].module_type_id === '5' || this.chargevalues[i].module_type_id === 5) {

                        if (this.chargevalues[i].id === '') {

                            const reqbody = {
                                patient_id: this.app.patientId,
                                item_prac_code: this.chargevalues[i].item_prac_code,
                                loc_id: this.app.locationId,
                                item_id: this.chargevalues[i].item_id,
                                upc_code: this.chargevalues[i].upc_code,
                                item_name: this.chargevalues[i].Charges,
                                module_type_id: this.chargevalues[i].module_type_id,
                                qty: this.chargevalues[i].qty,
                                price_retail: this.chargevalues[i].procCharges,
                                order_status_id: this.orderService.orderStatusGlobal,
                                // ordertypeid: "2"
                            };
                            let savedata: any = []
                            this.orderService.saveOrderOtherDetails(reqbody).subscribe(
                                saveOrderData => {
                                    this.errorService.displayError({ severity: 'success', summary: 'Success Message', detail: 'Saved Successfully' });
                                });
                        } else {
                            const reqbodyUpdate = {

                                patient_id: this.app.patientId,
                                item_id: this.chargevalues[i].item_id,
                                item_prac_code: this.chargevalues[i].item_prac_code,
                                upc_code: this.chargevalues[i].upc_code,
                                item_name: this.chargevalues[i].Charges,
                                module_type_id: this.chargevalues[i].module_type_id,
                                qty: this.chargevalues[i].qty,
                                loc_id: this.app.locationId,
                                ordertypeid: '1',
                                price_retail: this.chargevalues[i].procCharges,
                                order_status_id: this.orderService.orderStatusGlobal
                            };
                            this.orderService.updateOrderOtherDetails(reqbodyUpdate, this.chargevalues[i].order_id, this.chargevalues[i].id).subscribe(
                                (updateOrderData: object) => {
                                    const len = i + 1;
                                    if (this.chargevalues.length === len) {
                                        this.errorService.displayError({
                                            severity: 'success',
                                            summary: 'Success Message', detail: 'Updated Successfully'
                                        });
                                    }
                                });

                            // var reqotherlabdata = {

                            //     "lab_id": this.lab_id,
                            //     "TrayNumber": this.TrayNumber,
                            //     "LabStatus": this.LabStatus,
                            //     "order_status": this.order_status,
                            //     "due_date": this.due_date,
                            //     "LabOrder": this.LabOrder,
                            //     "FiitedBy": this.FiitedBy,
                            //     "FittedDatetime": this.FittedDatetime,
                            //     "InspectedBy": this.inspectedby,
                            //     "InspectedDatetime": this.InspectedDatetime,
                            //     "DeliveredBy": this.deliveredby,
                            //     "DeliveredDatetime": this.DeliveredDatetime,
                            //     "sold_by": this.sold_by,
                            //     "sold_datetime": this.sold_datetime,
                            //     "reprocessreason_id": this.reprocessreason_id,
                            // }
                            // this.orderService.updateLaborderotherdetails(reqotherlabdata, this.saveData[i].order_id).subscribe(data => {

                            // })

                        }
                    }
                }
                setTimeout(() => this.orderService.orderselection.next(this.orderService.orderId), 1000);
                // localStorage.setItem('Order_id',data['order_id']);
                const encounter = this.orderService.orderId;
                // this.orderService.Order_id;
                let orderDataList: any = [];
                this.orderService.getOrderStatusData(encounter).subscribe(orderData => {
                    orderDataList = orderData;
                    this.orderService.orderEncounterId.next(orderDataList.order_enc_id);

                });
                // if (this.orderService.paymentOrderNavigation === true) {
                //     // const encounter = this.orderService.Order_id;
                //     let orderDataListdata: any = [];
                //     this.orderService.getOrderStatusData(encounter).subscribe(orderData => {
                //         orderDataListdata = orderData;
                //         this.router.navigate(['/order/payments'], { queryParams: { encounter_id: orderDataListdata.order_enc_id } });
                //         this.orderService.paymentOrderNavigation = false;
                //     });

                // }



            }
        });
        this.subscriptionLocationId = orderService.orderLocationId$.subscribe(data => {
            if (data !== '') {
                this.LocationId = data;
            } else {
                this.LocationId = data;
            }
        });
        this.subscriptionOrderId = orderService.orderselectionId$.subscribe((value: string) => {
            // alert("otherorderdetails")
            if (value !== 'New Order') {
                this.orderSaveId = value;
                this.chargevalues = [];
                this.contactlensService.contactlensotherdetails = [];
                // alert("hitting here")
                this.Loadorderhistorydetails(value);
                this.itemIDStorage = true;
                this.orderTypes = [];
                this.categoryList = '';
                // this.LoadLenstreatmentdetails();
                this.valueforitem = [];
                //  this.LoadLenstreatmentdetails();



            } else {
                this.orderSaveId = value;
                this.LoadLenstreatmentdetails();
                this.loadformdata();
                this.itemIDStorage = false;
                this.chargevalues = [];
                // this.LoadLenstreatmentdetails();
                this.categoryList = '';
                this.orderTypes = [];
                this.contactlensService.contactlensotherdetails = [];
                // localStorage.removeItem('Order_id');

                this.orderService.framesPrice.next('');
                this.orderService.lensSelectionODPrice.next('');
                this.orderService.lensSelectionOSPrice.next('');
                this.orderService.orderDetails.next('');
                this.totalbalance = 0;
                this.retailPriceTotal = 0
                this.labIntDisable = true;
                // this.Labotherdetails.controls.order_status_id.enable();
            }
        });
        this.subscriptionOrderCancle = orderService.OrderCancleValue$.subscribe(data => {
            this.LoadLenstreatmentdetails();
            this.itemIDStorage = false;
            this.chargevalues = [];
            this.orderTypes = [];
            this.contactlensService.contactlensotherdetails = [];
            // localStorage.removeItem('Order_id');
            this.orderService.framesPrice.next('');
            this.orderService.lensSelectionODPrice.next('');
            this.orderService.lensSelectionOSPrice.next('');
            this.orderService.orderDetails.next('');
            this.totalbalance = 0;
            this.retailPriceTotal = 0
        });
        // Declarations for soft and hardcontactlens
        this.softContactLensOrderdetailsOD = this.contactlensService.SoftContactOtherODvalue$.subscribe(value => {
            if (value !== 'null') {
                const values = value.data;
                this.contactlensService.contactlensotherdetails = [];
                this.chargevalues.forEach(element => {
                    const valuetoremove = this.chargevalues.findIndex(x => x.Eye === 'OD');
                    if (valuetoremove !== -1) {
                        this.chargevalues.splice(valuetoremove, 1);

                    }
                });

                const name = {
                    Name: values.Softcontactlensod.name
                };
                this.getpricebyIdname(name, values.Softcontactlensod.id, value.LensType);

                this.contactlensService.contactlensotherdetails = this.chargevalues;

            }
        });

        this.softContactLensOrderdetailsOS = this.contactlensService.SoftContactOtherOSvalue$.subscribe(value => {
            if (value !== 'null') {
                this.contactlensService.contactlensotherdetails = [];
                const values = value.data;
                this.chargevalues.forEach(element => {
                    const valuetoremove = this.chargevalues.findIndex(x => x.Eye === 'OS');
                    if (valuetoremove !== -1) {
                        this.chargevalues.splice(valuetoremove, 1);
                    }
                });

                const name = {
                    Name: values.Softcontactlensod.name
                };
                this.getpricebyIdname(name, values.Softcontactlensod.id, value.LensType);
                this.contactlensService.contactlensotherdetails = this.chargevalues;


            }
        });
        this.hardContactLensOrderdetailsOS = this.contactlensService.HardContactOtherOSvalue$.subscribe(value => {
            this.contactlensService.contactlensotherdetails = [];
            // const values = value.data;
            this.chargevalues.forEach(element => {
                const valuetoremove = this.chargevalues.findIndex(x => x.Eye === 'OS');
                if (valuetoremove !== -1) {
                    this.chargevalues.splice(valuetoremove, 1);
                }
            });
            if (value !== 'null') {
                const values = value.data.Hardcontactlensod;

                const name = {
                    Name: values.name
                };
                this.getpricebyIdname(name, values.id, value.LensType);
                this.contactlensService.contactlensotherdetails = this.chargevalues;
            }

        });

        this.hardContactLensOrderdetailsOD = this.contactlensService.HardContactOtherODvalue$.subscribe(value => {
            this.contactlensService.contactlensotherdetails = [];
            // const values = value.data;
            this.chargevalues.forEach(element => {
                const valuetoremove = this.chargevalues.findIndex(x => x.Eye === 'OS');
                if (valuetoremove !== -1) {
                    this.chargevalues.splice(valuetoremove, 1);
                }
            });
            if (value !== 'null') {
                const values = value.data.Hardcontactlensod;

                const name = {
                    Name: values.name
                };
                this.getpricebyIdname(name, values.id, value.LensType);
                this.contactlensService.contactlensotherdetails = this.chargevalues;
            }

        });




        this.sort = [
            { name: 'Option 1' },
            { name: 'Option 2' },
            { name: 'Option 3' },
            { name: 'Option 4' },
            { name: 'Option 5' }
        ];
    }

    ngOnDestroy() {
        this.onlyOthersaving.unsubscribe();
        this.orderid.unsubscribe();
        this.orderSpectacleLensSave.unsubscribe();
        this.subscriptionOrderId.unsubscribe();
        this.subscriptionOSITEID.unsubscribe();
        this.subscriptionOSNAME.unsubscribe();
        this.subscriptionODITEID.unsubscribe();
        this.subscriptionODNAME.unsubscribe();
        this.framesonly.unsubscribe();
        this.subscriptionFrameID.unsubscribe();
        this.subscriptionFrameNAME.unsubscribe();
        this.subscriptionEqualNAME.unsubscribe();
        this.subscriptionEqualITEID.unsubscribe();
        // this.OrderEncounterID.unsubscribe();
        this.softContactLensOrderdetailsOD.unsubscribe();
        this.softContactLensOrderdetailsOS.unsubscribe();
        this.contactlensotherdetailsvalues.unsubscribe();
        this.hardContactLensOrderdetailsOD.unsubscribe();
        this.hardContactLensOrderdetailsOD.unsubscribe();
        this.subscriptionOrderCancle.unsubscribe();

    }



    ngOnInit(): void {
        this.orderService.discountObject = '';
        this.isActive = this.orderService.showCategory;
        this.orderService.ofterSalesView = false;
        this.orderService.othersView = true;
        this.orderService.showorderdiv.next(true);
        this.orderTypes = [
            { id: '1', name: '92002 - New Patient - intermediate Exam', value: '', price: '' },
        ];

        this.chargecolumns = [
            { field: 'Print', header: 'Print' },
            { field: 'Charges', header: 'Charges' },
            { field: 'Eye', header: 'Eye' },
            { field: 'Copay', header: 'Copay' },
            { field: '', header: 'Patient' },
            { field: 'balForProc', header: 'Patient Total' },
            { field: 'insurance', header: 'Insurance' },
            { field: 'totalAmount', header: 'Item Total' },
            { field: 'procCharges', header: 'Retail' },
            { field: 'ID', header: 'Unit' },
            { field: 'price', header: 'Price' },

        ];

        this.loadformdata();
        this.chargevalues = [];
        this.orderTypes = [];
        this.LoadLenstreatmentdetails();
        this.Calculatethetotalbalance();
        this.LoadReporcessReason();
        this.vendorDropData();
        this.treatCategoryDropDown();
        this.Loadlabstatus();
        // this.userDropDownData();
        this.LoadSoldbymasterdata();
        this.Loadfittedbymasterdata();
        this.Loadinspectedbymasterdata();
        this.LoadDeliveredbymasterdata();
        this.StatusDropDis = false;

        if (this.app.ordersearchvalueService !== undefined) {
            if (this.app.ordersearchvalueService.length !== 0) {
                this.orderService.OrderSearchFilterValue.next((
                    {
                        Id: this.app.ordersearchvalueService[0].Id,
                        OrderType: this.app.ordersearchvalueService[0].OrderType
                    })
                );
                this.app.ordersearchvalueService = [];
            }
        }
        // this.Labotherdetails.controls.order_status_id.enable()
        // this.LoadLabmasterdata();
        // this.LensTreatments = [];
        // this.lensTreatmentslist = [];
        // this.LensTreatments = this.dropdownService.treatCategoryDropDown();
        // this.lensTreatmentslist = this.dropdownService.treatCategoryDropDown();
        // this.labstatusdata = [];
        // this.labstatuslist = [];
        // this.labstatusdata = this.dropdownService.labStatusDropDown();
        // this.labstatuslist = this.dropdownService.labStatusDropDown();


        // this.Soldby = [];
        // this.Soldbylist = [];

        // this.Soldby = this.dropdownService.userDropDownData();
        // this.Soldbylist = this.dropdownService.userDropDownData();

        // this.fittedby = [];
        // this.fittedbylist = [];

        // this.fittedby = this.dropdownService.userDropDownData();
        // this.fittedbylist = this.dropdownService.userDropDownData();

        // this.inspectedbydata = [];
        // this.inspectedbylist = [];

        // this.inspectedbydata = this.dropdownService.userDropDownData();
        // this.inspectedbylist = this.dropdownService.userDropDownData();

        // this.deliveredbdata = [];
        // this.deliveredbylist = [];

        // this.deliveredbdata = this.dropdownService.userDropDownData();
        // this.deliveredbylist = this.dropdownService.userDropDownData();
    }


    /**
     * Loadformdata order other details form intialization.
     */
    loadformdata() {
        this.Labotherdetails = this._fb.group({
            lab_id: null,
            TrayNumber: '',
            LabStatus: '',
            order_status_id: ['9', [Validators.required]],
            due_date: '',
            LabOrder: '',
            FiitedBy: '',
            FittedDatetime: '',
            InspectedBy: '',
            InspectedDatetime: '',
            DeliveredBy: '',
            DeliveredDatetime: '',
            sold_by: '',
            sold_datetime: '',
            employee_name: '',
            reprocessreason_id: null,
        });


    }
    getpricebyIdname(name, id, eye) {


        if (id !== '') {
            if (this.itemIDStorage === false) {
                let itemvalueData: any = [];
                this.orderService.getLensTreatmentPrice(id).subscribe(itemvalue => {
                    itemvalueData = itemvalue;
                    const checkedvalueobj = {
                        Charges: name.Name,
                        Eye: eye,
                        checked: true,
                        id: '',
                        insurance: '',
                        itemId: id,
                        item_id: id,
                        item_name: name.Name,
                        item_prac_code: '2',
                        module_type_id: (name.hasOwnProperty('ModuleTypeId')) ? name.ModuleTypeId : 5,
                        patient_id: '',
                        procCharges: itemvalueData.retailPrice,
                        qty: '1',
                        quantity: '1',
                        save_id: '',
                        totalAmount: '',
                        unitprice: '',
                        upc_code: name.UPC,
                        uniqueid: name.uniqueid,
                        price: (1 * itemvalueData.retailPrice),


                    };
                    this.chargevalues.push(checkedvalueobj);
                    this.saveData = this.chargevalues;
                    // this.orderService.discountChargeList = this.chargevalues;
                    this.orderService.otherItemRows = this.saveData;
                    this.Calculatethetotalbalance();
                });
                // this.Calculatethetotalbalance();
                // this.LoadLenstreatmentdetails();
            }
        }
        if (this.itemIDStorage === true) {
            let frameid = '';
            if (name.uniqueid === '1') {
                frameid = this.frameUPCid;
            }
            if (name.uniqueid === '2') {
                frameid = this.lensUPCODid;
            }
            if (name.uniqueid === '3') {
                frameid = this.lensUPCOSid;
            }
            if (!name.uniqueid) {
                frameid = id;
            }
            let itemvalueList: any = [];
            this.orderService.getLensTreatmentPrice(id).subscribe(itemvalue => {
                itemvalueList = itemvalue
                const checkedvalueobj = {
                    Charges: name.Name,
                    Eye: eye,
                    checked: true,
                    id: '',
                    insurance: '',
                    itemId: frameid,
                    item_id: frameid,
                    item_name: name.Name,
                    item_prac_code: '2',
                    module_type_id: (name.hasOwnProperty('ModuleTypeId')) ? name.ModuleTypeId : 5,
                    patient_id: '',
                    procCharges: itemvalueList.retailPrice,
                    qty: '1',
                    quantity: '1',
                    save_id: '',
                    totalAmount: '',
                    unitprice: '',
                    upc_code: name.UPC,
                    uniqueid: name.uniqueid,
                    price: (1 * itemvalueList.retailPrice),

                };
                this.chargevalues.push(checkedvalueobj);
                this.saveData = this.chargevalues;
                this.orderService.otherItemRows = this.saveData;
                this.Calculatethetotalbalance();
            });
            // this.Calculatethetotalbalance();
        }
    }


    // new micro API
    /**
     * Loads lenstreatmentdetails
     * @param {Array} LensTreatments for dropdown data binding
     * @returns {object}  for dropdown data binding
     */
    LoadLenstreatmentdetails() {
        this.LensTreatments = this.dropdownService.lensTreatments;
        if (this.utility.getObjectLength(this.LensTreatments) === 0) {
            this.dropdownService.getLensCategories().subscribe(
                (Values: TreatmentCategory) => {
                    try {
                        const dropData = Values.data;
                        this.LensTreatments.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.LensTreatments.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                        }
                        this.dropdownService.lensTreatments = this.LensTreatments;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Treats category drop down
     * @param {Array} treatCategory for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    treatCategoryDropDown() {
        let treatcatlist: any = [];
        this.treatCategory = [];
        this.treatCategory = this.dropdownService.treatCategory;
        if (this.utility.getObjectLength(this.treatCategory) === 0) {
            this.treatCategory.push({ Id: '', Name: 'Select Category' });
            this.dropdownService.getLensCategories().subscribe(
                data => {
                    try {
                        treatcatlist = data;
                        this.treatCategorylist = treatcatlist.data;
                        for (let i = 0; i <= this.treatCategorylist.length - 1; i++) {
                            if (!this.treatCategory.find(a => a.Name === this.treatCategorylist[i].categoryname)) {
                                this.treatCategory.push({
                                    Id: this.treatCategorylist[i].id,
                                    Name: this.treatCategorylist[i].categoryname
                                });
                            }
                        }
                        this.dropdownService.treatCategory = this.treatCategory;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }

    }

    /**
     * Lenstreatment here loading the values of the lenstreatment details based on the category and UPC code selected.      
     */
    lenstreatmentchange() {
        const lenstreatmentid = this.categoryList;
        const upc = this.upcSearch;
        this.orderTypes = [];
        let lensTreatmentData: any = [];
        if (upc === '' && lenstreatmentid === '') {
            this.orderTypes = [];
            return;
        }
        this.orderService.getLensTreatmentswithcategory('', '', upc, lenstreatmentid, '', '', '').subscribe(
            data => {
                lensTreatmentData = data;
                this.orderTypes = lensTreatmentData.result.Optical.LensTreatment;
            });
    }



    cleardetails() {
        this.orderTypes = [];
    }

    /**
     * Loads soldbymasterdata
     * @param {Array} Soldby for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadSoldbymasterdata() {
        this.Soldby = [];
        this.Soldbylist = [];
        this.Soldby = this.dropdownService.soldBy;
        if (this.utility.getObjectLength(this.Soldby) === 0) {
            this.Soldby.push({ Id: '', Name: 'Select User' });
            this.dropdownService.getUsers().subscribe(
                data => {
                    try {
                        this.Soldbylist = data['data'];
                        for (let i = 0; i <= this.Soldbylist.length - 1; i++) {
                            if (!this.Soldby.find(a => a.Name === this.Soldbylist[i].username)) {
                                this.Soldby.push({ Id: this.Soldbylist[i].id.toString(), Name: this.Soldbylist[i].username });
                            }
                        }
                        this.Soldbylist = this.Soldby;
                        this.dropdownService.soldBy = this.Soldby;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });

        }
    }


    /**
     * Loadfittedbymasterdatas order other details component
     * @param {Array} fittedby for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    Loadfittedbymasterdata() {
        this.fittedby = [];
        this.fittedbylist = [];
        this.fittedby = this.dropdownService.fittedby;
        if (this.utility.getObjectLength(this.fittedby) === 0) {
            this.fittedby.push({ Id: '', Name: 'Select User' });
            this.dropdownService.getUsers().subscribe(
                data => {
                    try {
                        this.fittedbylist = data['data'];
                        for (let i = 0; i <= this.fittedbylist.length - 1; i++) {
                            if (!this.fittedby.find(a => a.Name === this.fittedbylist[i].username)) {
                                this.fittedby.push({ Id: this.fittedbylist[i].id.toString(), Name: this.fittedbylist[i].username });
                            }
                        }
                        this.fittedbylist = this.fittedby;
                        this.dropdownService.fittedby = this.fittedby;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }

    /* For displaying users list */
    userDropDownData() {
        this.fittedby = [];
        let UsersData: any = [];
        this.dropdownService.getUsers().subscribe(
            data => {
                UsersData = data;
                this.fittedby.push({ Id: '', Name: 'Select User' });
                this.fittedbylist = UsersData.data;
                for (let i = 0; i <= this.fittedbylist.length - 1; i++) {
                    if (!this.fittedby.find(a => a.Name === this.fittedbylist[i].username)) {
                        this.fittedby.push({
                            Id: this.fittedbylist[i].id.toString(), Name: this.fittedbylist[i].fname.concat(this.fittedbylist[i].lname)
                        });
                    }
                }
            });
    }

    /**
     * Loadinspectedbymasterdatas order other details component
     * @param {Array} inspectedbydata for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    Loadinspectedbymasterdata() {
        this.inspectedbydata = [];
        this.inspectedbylist = [];
        this.inspectedbydata = this.dropdownService.inspectedbydata;
        if (this.utility.getObjectLength(this.inspectedbydata) === 0) {
            this.inspectedbydata.push({ Id: '', Name: 'Select User' });
            this.dropdownService.getUsers().subscribe(
                data => {
                    try {
                        this.inspectedbylist = data['data'];
                        for (let i = 0; i <= this.inspectedbylist.length - 1; i++) {
                            if (!this.inspectedbydata.find(a => a.Name === this.inspectedbylist[i].username)) {
                                this.inspectedbydata.push(
                                    { Id: this.inspectedbylist[i].id.toString(), Name: this.inspectedbylist[i].username });

                            }
                        }
                        this.inspectedbylist = this.inspectedbydata;
                        this.dropdownService.inspectedbydata = this.inspectedbydata;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }

    }

    /**
     * Loads deliveredbymasterdata
     * @param {Array} deliveredbdata for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadDeliveredbymasterdata() {
        this.deliveredbdata = [];
        this.deliveredbylist = [];
        this.deliveredbdata = this.dropdownService.deliveredbdata;
        if (this.utility.getObjectLength(this.deliveredbdata) === 0) {
            this.deliveredbdata.push({ Id: '', Name: 'Select User' });
            this.dropdownService.getUsers().subscribe(
                data => {
                    this.deliveredbylist = data['data'];
                    for (let i = 0; i <= this.deliveredbylist.length - 1; i++) {
                        if (!this.deliveredbdata.find(a => a.Name === this.deliveredbylist[i].username)) {
                            this.deliveredbdata.push(
                                { Id: this.deliveredbylist[i].id.toString(), Name: this.deliveredbylist[i].username });
                        }
                    }
                    this.deliveredbylist = this.deliveredbdata;
                    this.dropdownService.deliveredbdata = this.deliveredbdata;
                });
        }
    }


    /**
     * Vendors drop data here we can load all the master data of labs
     * @param {Array} labsList for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    vendorDropData() {
        this.labsList = [];
        let labsVenderData: any = [];
        this.labsList = this.dropdownService.lablist;
        if (this.utility.getObjectLength(this.labsList) === 0) {
            this.labsList.push({ id: null, vendor_name: 'Select Lab' });
            this.dropdownService.getLabsVender().subscribe(
                data => {
                    try {
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            this.labsList.push(labsVenderData.data[i]);
                        }
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.dropdownService.lablist = this.labsList;
                });
        }

    }

    /**
     * Loads reporcess reason
     * @param {Array} Reprocess for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadReporcessReason() {
        this.Reprocess = [];
        this.Reprocesslist = [];
        this.Reprocess = this.dropdownService.Reprocessreason;
        let reprocessData: any = [];
        if (this.utility.getObjectLength(this.Reprocess) === 0) {
            this.Reprocess.push({ Id: '', Name: 'Select Reason' });
            this.dropdownService.GetReporcessdata().subscribe(
                data => {
                    try {
                        reprocessData = data;
                        this.Reprocesslist = reprocessData.data;
                        for (let i = 0; i <= this.Reprocesslist.length - 1; i++) {
                            if (!this.Reprocess.find(a => a.Name === this.Reprocesslist[i].description)) {
                                this.Reprocess.push({ Id: this.Reprocesslist[i].id.toString(), Name: this.Reprocesslist[i].description });
                            }
                        }
                        this.Reprocesslist = this.Reprocess;
                        this.dropdownService.Reprocessreason = this.Reprocess;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loadlabstatus order other details component
     * @param {Array} labstatusdata for binding dropdown data
     * @returns {object}  for dropdown data binding
     * 
     */
    Loadlabstatus() {
        this.labstatusdata = [];
        this.labstatusdata = this.dropdownService.labstatus;
        if (this.utility.getObjectLength(this.labstatusdata) === 0) {
            this.labstatusdata.push({ Id: '', Name: 'Select Status' });
            this.orderService.Getlaborderstatusdata().subscribe(
                data => {
                    try {
                        this.labstatuslist = data;
                        for (let i = 0; i <= this.labstatuslist.length - 1; i++) {
                            if (!this.labstatusdata.find(a => a.Name === this.labstatuslist[i].description)) {
                                this.labstatusdata.push({ Id: this.labstatuslist[i].id.toString(), Name: this.labstatuslist[i].description });
                            }
                        }
                        // this.orderService.orderStatusGlobal = '9'
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.dropdownService.labstatus = this.labstatusdata;
                });
        }
    }


    /**
     * Loadorderhistorydetails order other details component Here in this function we are loading the details 
     * of the particular selected in the history drop down like "order status, lab integration etc "
     * @param orderid 
     */
    Loadorderhistorydetails(orderid: string) {
        this.frameUPCid = '';
        this.lensUPCODid = '';
        this.lensUPCOSid = '';
        this.chargevalues = [];
        this.orderService.orderSaveUpdateStatus = true;
        this.orderService.getOrderItemDetails(orderid).subscribe(data => {
            // alert("hittingHere")
            const chargefiledvalues = data;
            this.chargehistoryvalues = data;
            let chargelistValues = [];
            chargelistValues.push(data);

            this.chargevalues = data;
            this.retailPriceTotal = 0;
            this.totalbalance = 0;
            for (let i = 0; i <= chargelistValues[0].length - 1; i++) {
                if ((chargelistValues[0][i]['module_type_id'] !== 1 && chargelistValues[0][i]['module_type_id'] !== 5) || (chargelistValues[0][i]['module_type_id'] !== '1' && chargelistValues[0][i]['module_type_id'] !== '5')) {
                    chargelistValues[0][i]['Eye'] = chargelistValues[0][i]['lens_vision']
                }

                chargelistValues[0][i]['Charges'] = chargelistValues[0][i]['item_name']
                chargelistValues[0][i]['checked'] = true;
                chargelistValues[0][i]['discountAmount'] = chargelistValues[0][i]['discount'] == null ? '0' : chargelistValues[0][i]['discount'];
                chargelistValues[0][i]['procCharges'] = chargelistValues[0][i]['price_retail'];
                chargelistValues[0][i]['quantity'] = chargelistValues[0][i]['qty'];
                chargelistValues[0][i]['tax'] = chargelistValues[0][i]['tax1_paid'];
                chargelistValues[0][i]['tax2'] = chargelistValues[0][i]['tax2_paid'];
                chargelistValues[0][i]['afterDiscountAmount'] = chargelistValues[0][i]['price_retail'] - chargelistValues[0][i]['discount'];
                chargelistValues[0][i]['returnDate'] = chargelistValues[0][i]['modified_date'];
                // chargelistValues[0][i]['itemTotal'] = chargelistValues[0][i]['itemTotal'];
                // alert( chargelistValues[0][i]['discount'])
                // chargelistValues[0][i]['notes'] = chargelistValues[0][i]['notes'];
                if (chargelistValues[0][i]['discount'] !== '') {
                    //  chargelistValues[0][i].price = (costno *  chargelistValues[0][i]['afterDiscountAmount']);
                    // chargelistValues[0][i]['itemTotal'] =chargelistValues[0][i]['afterDiscountAmount'] * chargelistValues[0][i]['qty'];
                    chargelistValues[0][i]['price'] = chargelistValues[0][i]['afterDiscountAmount'] * chargelistValues[0][i]['qty'];
                } else {
                    // chargelistValues[0][i]['itemTotal']  = chargelistValues[0][i]['qty'] * chargelistValues[0][i]['price_retail'];
                    chargelistValues[0][i].price = chargelistValues[0][i]['qty'] * chargelistValues[0][i]['price_retail'];
                }


                this.retailPriceTotal = this.retailPriceTotal + chargelistValues[0][i]['price_retail']
                this.totalbalance = this.totalbalance + chargelistValues[0][i]['price']

            }
            this.chargevalues = chargelistValues[0];

            this.orderService.discountChargeList = this.chargehistoryvalues;

            if (this.chargehistoryvalues.length <= 1) {
                this.chargehistoryvalues.forEach(element => {
                    if (element.module_type_id === 1) {
                        element.uniqueid = '1';
                        this.frameUPCid = element.item_id;
                    }
                });
            }
            if (this.chargehistoryvalues.length >= 3) {
                this.chargehistoryvalues.forEach(element => {
                    if (element.module_type_id === 1) {
                        element.uniqueid = '1';
                        this.frameUPCid = element.item_id;
                    }
                    if (element.lens_vision === 'OD') {
                        element.uniqueid = '2';
                        this.lensUPCODid = element.item_id;
                    }
                    if (element.lens_vision === 'OS') {
                        element.uniqueid = '3';
                        this.lensUPCOSid = element.item_id;
                    }
                });
            }
            const obj1 = {
                filter: [
                    {
                        field: 'encounter_id',
                        operator: '=',
                        value: this.encounter_id
                    },
                    {
                        field: 'patient_id',
                        operator: '=',
                        value: this.app.patientId,
                    }
                ]
            };
            this.orderService.getthechargevaluesfororder(obj1).subscribe(details => {
                const eachpricevalues = details[0].charge_list_detail;
                this.copay = details[0].copay;
                this.patient = details[0].patientAmt;
                this.patientAmount = this.copay + this.patient;
                this.insuranceAmount = details[0].insAmt;
                this.notes = details[0].charge_list_detail[0].notes;
                this.trans_del_date = details[0].trans_del_date === '0000-00-00 00:00:00' ? '' : details[0].trans_del_date;
                // this.itemTotal =  this.patientAmount + this.insuranceAmount;
                this.emp_name = details[0].employee_name;
                this.writeOfAmount = details[0].charge_list_detail[0].payment_write_off[0] === undefined ? '' : details[0].charge_list_detail[0].payment_write_off[0].write_off_amount;
                this.chargehistoryvalues.forEach(obj => {
                    const valueaddtoorder = eachpricevalues.filter(x => x.opt_order_detail_id === obj.id);
                    // this.LoadChargedetailsvalue(obj, details, valueaddtoorder[0]);
                });
                this.Calculatethetotalbalance();
            });
            // this.writeOfAmount = this.patientAmount- this.discount;

            this.loadformdata();
            let details: any = [];
            this.orderService.GetotherlabDetails(orderid).subscribe((labdetails: object) => {

                details = labdetails;
                this.labIntegrationCheck = details.LabOrder;
                if (details.lab_id) {
                    this.labChange(details.lab_id.toString(), 'labpresent');
                    // this.labIntDisable = false;
                } else {
                    this.labIntDisable = true;
                }
                this.Labotherdetails = this._fb.group({
                    lab_id: [details.lab_id, [Validators.required]],
                    TrayNumber: [details.TrayNumber, [Validators.required]],
                    LabStatus: [details.LabStatus, [Validators.required]],
                    order_status_id: [details.order_status_id.toString(), [Validators.required]],
                    due_date: details.due_date === '0000-00-00' ? '' : details.due_date,
                    LabOrder: details.LabOrder,
                    FiitedBy: details.FiitedBy,
                    FittedDatetime: details.FittedDatetime === '0000-00-00' ? '' : details.FittedDatetime,
                    InspectedBy: details.InspectedBy,
                    InspectedDatetime: details.InspectedDatetime === '0000-00-00' ? '' : details.InspectedDatetime,
                    DeliveredBy: details.DeliveredBy,
                    DeliveredDatetime: details.DeliveredDatetime === '0000-00-00' ? '' : details.DeliveredDatetime,
                    sold_by: details.sold_by,
                    sold_datetime: details.sold_datetime === '0000-00-00' ? '' : details.sold_datetime,
                    reprocessreason_id: details.reprocessreason_id,
                    employee_name: details.employee_name,
                });
                this.employee_name = details.employee_name;





                //  this.lab_id=details["lab_id"];
                //  this.TrayNumber=details["TrayNumber"];
                //  this.LabStatus=details["LabStatus"];
                //  this.order_status=details["order_status_id"];

                //  if(details["due_date"]!="0000-00-00")
                //  {
                //     this.due_date=details["due_date"];
                //  }
                //  else
                //  {
                //     this.due_date="";
                //  }

                //  this.LabOrder=details["LabOrder"];
                //  this.FiitedBy=details["FiitedBy"];

                //  if(details["FittedDatetime"]!="0000-00-00")
                //  {
                //     this.FittedDatetime=details["FittedDatetime"];
                //  }
                //  else
                //  {
                //     this.FittedDatetime="";
                //  }

                //  this.inspectedby=details["InspectedBy"];

                //  if(details["InspectedDatetime"]!="0000-00-00")
                //  {
                //     this.InspectedDatetime=details["InspectedDatetime"];
                //  }
                //  else
                //  {
                //     this.InspectedDatetime="";
                //  }


                //  this.deliveredby=details["DeliveredBy"];

                //  if(details["DeliveredDatetime"]!="0000-00-00")
                //  {
                //     this.DeliveredDatetime=details["DeliveredDatetime"];
                //  }
                //  else
                //  {
                //     this.DeliveredDatetime="";
                //  }


                //  this.sold_by=details["sold_by"];

                //  if(details["sold_datetime"]!="0000-00-00")
                //  {
                //     this.sold_datetime=details["sold_datetime"];
                //  }
                //  else
                //  {
                //     this.sold_datetime="";
                //  }


                //  this.reprocessreason_id=details["reprocessreason_id"];


            });



            this.contactlensService.contactlensotherdetails = this.chargevalues;

        });

    }

    LoadChargedetailsvalue(chargedata, details, valuesforcharge) {

        this.pushchargevalues = details;
        const getobject = details;
        let uniqdata = '';
        if (chargedata.uniqueid) {
            uniqdata = chargedata.uniqueid;
        } else {
            uniqdata = '';
        }
        const pushvalue = {
            itemId: chargedata.item_id,
            checked: true,
            id: chargedata.id,
            Charges: chargedata.item_name,
            Eye: chargedata.lens_vision,
            patient_id: '',
            totalAmount: valuesforcharge.totalAmount,
            insurance: '',
            procCharges: valuesforcharge.procCharges,
            price: (chargedata.qty * valuesforcharge.procCharges),
            // procCharges:  valuesforcharge.procCharges,
            unitprice: '',
            quantity: chargedata.qty,
            item_prac_code: chargedata.item_prac_code,
            qty: chargedata.qty,
            module_type_id: chargedata.module_type_id,
            item_name: chargedata.item_name,
            item_id: chargedata.item_id,
            save_id: chargedata.id,
            upc_code: chargedata.upc_code,
            order_id: chargedata.order_id,
            uniqueid: uniqdata
        };
        this.chargevalues.push(pushvalue);
        this.copycharges = this.chargevalues;
        this.saveData = this.chargevalues;
        this.orderService.otherItemRows = this.saveData;
        // this.Calculatethetotalbalance();
        // this.LoadLenstreatmentdetails();
    }
    quantitychange(quantityobj, quantityvalue) {
        if (quantityvalue !== '') {
            const checkordervalues = this.chargevalues.filter(d => d.id === quantityobj.itemId);

            if (checkordervalues != null) {
                const quantitytotal = quantityvalue.target.value;
                checkordervalues[0].quantity = quantitytotal;
            }
        }
    }



    /**
     * Changevalues 
     * @param {object} data 
     * @param {object} add 
     * @param {number} index 
     */
    changevalue(data, add, index) {
        this.orderService.orderSaveUpdateStatus = true;
        if (add === true) {
            const checkedvalueobj = {
                price: data.retail_price, checked: true, id: '', Charges: data.Name, Eye: '', patient_id: '', totalAmount: '',
                insurance: '',
                procCharges: data.retail_price, unitprice: data.retail_price, quantity: '1',
                item_prac_code: '2', qty: '1', module_type_id: '5', upc_code: data.UPC, item_name: '',
                item_id: data.itemId, save_id: '', uniqueid: ''
            };

            this.chargevalues.push(checkedvalueobj);
        }

        if (add === false) {
            const a = this.chargevalues.findIndex(x => x.item_id === data.itemId);
            if (a !== -1) {
                this.chargevalues.splice(a, 1);
            }
        }
        this.saveData = this.chargevalues;
        this.orderService.otherItemRows = this.saveData;
        this.Calculatethetotalbalance();
    }
    SaveSelectValues(data, event) {

        this.saveData = this.chargevalues.filter(x => x.checked === true);
        this.contactlensService.contactlensotherdetails = this.saveData;
        // this.modifyClick();
    }

    Calculatethetotalbalance() {
        this.pricebalance = 0;
        this.framebalance = 0;
        this.lensselectionbalance = 0;
        this.otherdetailsbalance = 0;
        this.retailPriceTotal = 0;
        if (this.chargevalues !== undefined) {
            for (let i = 0; i <= this.chargevalues.length - 1; i++) {
                this.retailPriceTotal = this.retailPriceTotal + parseFloat(this.chargevalues[i]['procCharges']);
            }
            this.chargevalues.forEach(obj => {
                if (obj.module_type_id === '1') {
                    // Value to bind for frames
                    this.framebalance = this.framebalance + obj.price;
                    this.orderService.framesPrice.next(this.framebalance);

                }
                if (obj.module_type_id === '2') { // Value to bind for lens selection
                    this.lensselectionbalance = this.lensselectionbalance + obj.price;
                    this.orderService.lensSelectionODPrice.next(this.lensselectionbalance);
                }
                if (obj.module_type_id === '5') {
                    // Value to bind for other details selection
                    this.otherdetailsbalance = this.otherdetailsbalance + obj.price;
                    this.orderService.lensSelectionOSPrice.next(this.otherdetailsbalance);
                }
                this.pricebalance = this.pricebalance + parseFloat(obj.price);
                this.orderService.orderDetails.next(this.pricebalance);
            });
        }
        this.totalbalance = this.pricebalance;
        // subcribe the values here
    }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    QtyValueschange(data, event, i) {
        this.orderService.orderSaveUpdateStatus = true;
        this.chargevalues[i].qty = event.currentTarget.value;
        const costno = this.chargevalues[i].qty;
        // this.chargevalues[i]['afterDiscountAmount'] = '';
        if (this.orderService.discountObject !== '') {
            this.chargevalues[i].price = (costno * this.chargevalues[i]['afterDiscountAmount']);
        } else {
            this.chargevalues[i].price = (costno * this.chargevalues[i].procCharges);
        }


        this.saveData = this.chargevalues;
        this.orderService.otherItemRows = this.saveData;
        this.Calculatethetotalbalance();
        this.contactlensService.contactlensotherdetails = this.saveData;
    }

    afterSales() {
        this.afterSalesSidebar = true;
    }
    returnClick() {
        this.returnReasonSidebar = true;
        if (this.chargevalues !== undefined || this.chargevalues !== '') {
            this.returnInputValues = this.chargevalues;
        }
    }
    /**
     * Returnevents order other details component
     * @param {object} event for getting return events
     */
    returnevent(event) {
        if (event === 'close') {
            this.returnReasonSidebar = false;
        }
        if (event === 'save') {
            this.returnReasonSidebar = false;
            this.Loadorderhistorydetails(this.orderService.orderId);
        }
    }

    /**
     * Discountevents order other details component this is on which items discount given
     * @param {object} event for the chargelines.
     */
    discountevent(event) {
        if (event === 'close') {
            this.orderDiscountSidebar = false;
            this.orderService.discountObject = ''
        } else {
            for (let i = 0; i <= this.chargevalues.length - 1; i++) {
                this.chargevalues[i]['afterDiscountAmount'] = 0;
                this.chargevalues[i]['discountAmount'] = 0;
                this.chargevalues[i]['price'] = this.chargevalues[i]['procCharges']
            }
            this.totalAmountBenfit = 0;
            this.orderService.totalAmountDiscount = 0;
            this.orderService.discountObject = ''
            this.orderService.discountObject = event;
            this.orderDiscountSidebar = false;
            this.totalbalance = 0;
            this.totalRetailPrice = 0
            // for (let j = 0; j <= this.chargevalues.length - 1; j++) {
            if (event.discountOnItems.includes('Material') === true) {
                this.frames = 1;
                this.framesString = '1';
                this.lens = 2;
                this.lensString = '2';
            }
            if (event.discountOnItems.includes('Services') === true) {
                this.service = 8;
                this.serviceStirng = '8';
            }
            if (event.discountOnItems.includes('Other') === true) {
                this.category = 5;
                this.categoryStirng = '5';
            }
            for (let j = 0; j <= this.chargevalues.length - 1; j++) {
                if (this.chargevalues[j].module_type_id === this.frames || this.chargevalues[j].module_type_id === this.framesString ||
                    this.chargevalues[j].module_type_id === this.lens || this.chargevalues[j].module_type_id === this.lensString
                    || this.chargevalues[j].module_type_id === this.category || this.chargevalues[j].module_type_id === this.categoryStirng
                    || this.chargevalues[j].module_type_id === this.service || this.chargevalues[j].module_type_id === this.serviceStirng) {
                    this.totalRetailPrice = this.totalRetailPrice + parseFloat(this.chargevalues[j].procCharges);
                }
            }



            if (event.discountOnItems.includes('Material') === true) {
                const amountPercent = event.discountNumber / this.totalRetailPrice;
                for (let j = 0; j <= this.chargevalues.length - 1; j++) {
                    if (this.chargevalues[j].module_type_id === 1 || this.chargevalues[j].module_type_id === '1' ||
                        this.chargevalues[j].module_type_id === 2 || this.chargevalues[j].module_type_id === '2') {
                        if (event.discountType === 'Amount') {
                            // this.orderService.totalAmountDiscount = event.discountNumber;
                            this.AmountCalulationMethod(j, amountPercent);

                        }
                        if (event.discountType === 'Percentage') {
                            this.percentageCalculationMethod(j, event.discountNumber);


                        }

                    }
                }
            }
            if (event.discountOnItems.includes('Services') === true) {
                const amountPercent = event.discountNumber / this.totalRetailPrice;
                for (let k = 0; k <= this.chargevalues.length - 1; k++) {
                    if (this.chargevalues[k].module_type_id === 8 || this.chargevalues[k].module_type_id === '8') {
                        if (event.discountType === 'Amount') {
                            // this.orderService.totalAmountDiscount = event.discountNumber;
                            this.AmountCalulationMethod(k, amountPercent);
                            // this.totalbalance = this.totalbalance + this.chargevalues[k].price
                        }
                        if (event.discountType === 'Percentage') {
                            this.percentageCalculationMethod(k, event.discountNumber);
                            // this.totalbalance = this.totalbalance + this.chargevalues[k].price
                        }
                    }
                }
            }
            if (event.discountOnItems.includes('Other') === true) {
                // for (let z = 0; z <= this.chargevalues.length - 1; z++) {
                //     this.totalRetailPrice = this.totalRetailPrice + this.chargevalues[z].procCharges
                // }
                const amountPercent = event.discountNumber / this.totalRetailPrice;
                for (let l = 0; l <= this.chargevalues.length - 1; l++) {
                    if (this.chargevalues[l].module_type_id === 5 || this.chargevalues[l].module_type_id === '5') {
                        if (event.discountType === 'Amount') {
                            //this.orderService.totalAmountDiscount = event.discountNumber;
                            this.AmountCalulationMethod(l, amountPercent);
                            //this.totalbalance = this.totalbalance + this.chargevalues[l].price
                        }
                        if (event.discountType === 'Percentage') {
                            this.percentageCalculationMethod(l, event.discountNumber);
                            //this.totalbalance = this.totalbalance + this.chargevalues[l].price
                        }
                    }
                }
            }
            for (let m = 0; m <= this.chargevalues.length - 1; m++) {
                this.totalAmountBenfit = this.totalAmountBenfit + this.chargevalues[m]['discountAmount']
            }
            this.orderService.discountChargeList = this.chargevalues;
            this.orderService.totalAmountDiscount = this.totalAmountBenfit.toFixed(2);
            // alert(this.orderService.totalAmountDiscount);
        }
    }

    /**
     * Amounts calulation method discount based on the amount
     * @param {number} index 
     * @param {number } number 
     */
    AmountCalulationMethod(index, discPercent) {
        const discountAmountMinus = this.chargevalues[index].procCharges * discPercent;
        this.chargevalues[index]['discountAmount'] = discountAmountMinus;
        // this.chargevalues[index]['discountAmount'] = number;
        this.chargevalues[index]['afterDiscountAmount'] = this.chargevalues[index].procCharges - discountAmountMinus;
        const costno = this.chargevalues[index].qty;
        this.chargevalues[index].price = (costno * this.chargevalues[index]['afterDiscountAmount'])
        this.totalbalance = this.totalbalance + this.chargevalues[index].price        // this.totalAmountBenfit = totalDisc;

    }

    /**
     * Percentages calculation method i discount based on the percentage.
     * @param {number} index 
     * @param {number} number 
     */
    percentageCalculationMethod(index, number) {
        const denominator = number / 100;
        const discountAmountMinus = this.chargevalues[index].procCharges * denominator;
        this.chargevalues[index]['discountAmount'] = discountAmountMinus;
        //this.totalAmountBenfit = this.totalAmountBenfit + this.chargevalues[index]['discountAmount']
        this.chargevalues[index]['afterDiscountAmount'] =
            this.chargevalues[index].procCharges - discountAmountMinus;
        const costno = this.chargevalues[index].qty;
        this.chargevalues[index].price = (costno * this.chargevalues[index]['afterDiscountAmount'])
        this.totalbalance = this.totalbalance + this.chargevalues[index].price
        // this.orderService.totalAmountDiscount = this.totalAmountBenfit;
    }
    modifysoftevent(event) {
        if (event === 'close') {
            this.modifySoftContactSidebar = false;
        }
        if (event === 'ok') {
            this.modifySoftContactSidebar = false;
        }

    }



    // returnWizard(){
    //     this.returnWizardSidebar = true;
    // }
    // var methodCond: boolean = true
    // this.chargevalues.forEach(element => {
    //     methodCond = true
    //     if (element.upc_code == this.Odname.UPC) {
    //         methodCond = false
    //     }
    // });
    // if (methodCond == true) {
    //     this.getpricebyIdname(this.Odname, this.OdID);
    // }

    /**
     * Modifys click
     * @returns order detailsitem
     */
    modifyClick() {
        this.modifyItemId = [];
        if (this.saveData['data'].id !== 0 && this.saveData['data'].id !== '') {
            this.modifyItemId.push(this.saveData['data']);
        }
        if (this.saveData['data'].module_type_id === '5') {
            this.modifyItemId.push(this.saveData['data']);
        }
        if (this.modifyItemId.length === 1) {
            this.modifySoftContactSidebar = true;
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'modifyTax',
                detail: 'Please Select only one'
            });
        }

    }
    /**
     * Orders discount sidebar click for the opening of the of the discount sidebar
     * with the selected items
     */
    orderDiscountSidebarClick() {
        this.orderDiscountSidebar = true;
        //  this.discountItemsData = [];
        for (let i = 0; i <= this.chargevalues.length - 1; i++) {
            // if (this.saveData[i].checked === true) {
            this.discountItemsData.push(this.chargevalues[i]);

        }
        //  }

    }

    statusChange() {
        this.orderService.orderLabValid = this.Labotherdetails.valid;
        this.orderService.orderStatusGlobal = this.Labotherdetails.controls.order_status_id.value;
    }
    otherLabFormChange() {
        this.orderService.orderLabValid = this.Labotherdetails.valid;
        if (this.orderService.orderSaveUpdateStatus === false) {
            this.orderService.orderFormChangeStatus = false;
        }
    }

    /**
     * Labs change is on lab change we are check for the values of the optivison and vision web
     * @param {string} event Here we can get the lab id what we had selected.
     * @param {string} type Here we are setting the type for the enable and disable of the integration button.
     */
    labChange(event: string, type: string) {
        if (type === 'labpresent') {
            this.labSelectedForOrder = event;
            this.labIntDisable = false;
        } else if ('labChange') {
            if (this.labSelectedForOrder === event) {
                this.labIntDisable = false;
            } else {
                this.labIntDisable = true;
            }
        }
        const labIdselected = event;
        this.Labotherdetails.controls.lab_id.setValue(labIdselected);
        this.orderService.GetlabFTpconnection(labIdselected).subscribe((data: vendorDetails) => {
            try {
                this.labDataForItem = data;
                if (this.labDataForItem.jobtransmissionmethod === 2
                    && this.orderService.orderId !== ''
                    && this.orderService.orderId !== null
                    && this.orderService.orderId !== 'New Order'
                ) {
                    // this.labIntDisable = false;
                    this.integrationType = 'VISIONWEB';
                }
                if (this.labDataForItem.jobtransmissionmethod === 4
                    && this.orderService.orderId !== ''
                    && this.orderService.orderId !== null
                    && this.orderService.orderId !== 'New Order') {
                    // this.labIntDisable = false;
                    this.integrationType = 'OPTIVISION';
                }
            } catch (error) {
                this.errorService.syntaxErrors(error);
            }

        })

        this.orderService.orderLabValid = this.Labotherdetails.valid;
    }

    /**
     * Others ordersaving here we can saving the other type of order with items we selected.
     * @param {string} data 
     */
    otherOrdersaving(data) {
        if (data === 'onlyOtherSaving') {
            this.startingLengthCharges = 1;
            if (this.chargevalues[0].id === '') {
                const reqbody = {
                    patient_id: this.app.patientId,
                    item_prac_code: this.chargevalues[0].item_prac_code,
                    loc_id: this.app.locationId,
                    item_id: this.chargevalues[0].item_id,
                    upc_code: this.chargevalues[0].upc_code,
                    item_name: this.chargevalues[0].Charges,
                    module_type_id: this.chargevalues[0].module_type_id,
                    qty: this.chargevalues[0].qty,
                    price_retail: this.chargevalues[0].procCharges,
                    order_status_id: this.orderService.orderStatusGlobal,
                    ordertypeid: '6'
                };
                let savedata: any = []
                this.orderService.saveOrderFrames(reqbody).subscribe(
                    (saveOrderData: orderSave) => {
                        try {
                            this.orderService.orderId = saveOrderData.order_id;
                            for (let i = this.startingLengthCharges; i < this.chargevalues.length; i++) {
                                if (this.chargevalues[i].module_type_id === '1' || this.chargevalues[i].module_type_id === 1 || this.chargevalues[i].module_type_id === '2' || this.chargevalues[i].module_type_id === 2) {

                                }
                                else if (this.chargevalues[i].module_type_id === '5' || this.chargevalues[i].module_type_id === 5) {

                                    if (this.chargevalues[i].id === '') {

                                        const reqbody = {
                                            patient_id: this.app.patientId,
                                            item_prac_code: this.chargevalues[i].item_prac_code,
                                            loc_id: this.app.locationId,
                                            item_id: this.chargevalues[i].item_id,
                                            upc_code: this.chargevalues[i].upc_code,
                                            item_name: this.chargevalues[i].Charges,
                                            module_type_id: this.chargevalues[i].module_type_id,
                                            qty: this.chargevalues[i].qty,
                                            price_retail: this.chargevalues[i].procCharges,
                                            order_status_id: this.orderService.orderStatusGlobal,
                                            // ordertypeid: "2"
                                        };
                                        let savedata: any = []
                                        this.orderService.saveOrderOtherDetails(reqbody).subscribe(
                                            (saveOrderData: orderSave) => {
                                            });
                                    }
                                }
                            }
                            this.Loadorderhistorydetails(this.orderService.orderId);
                            this.orderService.updateLaborderotherdetails(this.Labotherdetails.value)
                                .subscribe((resp: OtherDetailsModel) => {

                                });
                            this.orderService.orderFormUpdate.next('success');
                            this.errorService.displayError({
                                severity: 'success',
                                summary: 'Success Message', detail: saveOrderData.status
                            });
                        } catch (error) {
                            this.errorService.syntaxErrors(error);
                        }
                    });
            }

        }

    }

    /**
     * Labs integration here we can integrate the order with the lab what are the lab selected. 
     * @returns  false for the stoping of the functionality to go further
     */
    labIntegration() {
        if (this.labIntegrationCheck === '-1'
            || this.labIntegrationCheck === '' ||
            this.labIntegrationCheck === null
        ) {
            if (this.integrationType === 'VISIONWEB') {
                if (!this.labDataForItem.bill_account ||
                    !this.labDataForItem.ship_account ||
                    !this.labDataForItem.vw_login ||
                    !this.labDataForItem.vw_password
                ) {
                    this.errorService.displayError({
                        severity: 'warn',
                        summary: 'warn Message',
                        detail: 'Please check Bill Account Or ShipAccount Or UserName Or Password are missing'
                    });
                    return false;
                }
            }
            if (this.integrationType === 'OPTIVISION') {
                if (!this.labDataForItem.facid ||
                    !this.labDataForItem.labid) {
                    this.errorService.displayError({
                        severity: 'warn',
                        summary: 'warn Message',
                        detail: 'LabId or Facility id missed Please configure'
                    });
                    return false;

                }
            }
            const Payload = {
                integrations: [
                    {
                        integrationType: this.integrationType,
                        recordId: this.orderService.orderId
                    }
                ]
            };
            this.orderService.labIntegrationUpload(Payload).subscribe((data: LabIntegration) => {
                this.Labotherdetails.controls.order_status_id.patchValue(1);
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'Lab status',
                    detail: 'Order sent to the lab successfully'
                });
                this.orderService.GetotherlabDetails(this.orderService.orderId).subscribe((resp: OtherDetailsModel) => {
                    this.labIntegrationCheck = resp.LabOrder;
                });
            });
        } else {
            this.errorService.displayError({
                severity: 'warn',
                summary: 'warn Message',
                detail: 'Particular Order Already sent to the LAB'
            });
            return false;
        }
    }
    /**
     * Determines whether row select on
     * @param event this event will binded with data on row select here we can hold the selected item data
     */
    onRowSelect(event: any) {
        this.saveData = event;
    }
}
