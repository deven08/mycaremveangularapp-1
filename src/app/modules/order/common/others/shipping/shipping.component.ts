import { Component, OnInit } from '@angular/core';

// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OrderService } from '@app/core/services/order/order.service';
import { AppService, ErrorService } from '@app/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';
// import { OrderSearchService } from '../../ConnectorEngine/services/order.service';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';


@Component({
    selector: 'app-order-shipping-details',
    templateUrl: 'shipping.component.html'
})
export class OrderShippingDetailsComponent implements OnInit {
    paymentDetails;
    ShippingdetailsForm: FormGroup;
    isprimary = false;
    public locations: any = [];
    customAddress: object;
    
    /**
     * Creates an instance of order shipping details component.
     * @param _fb  for formbuilder
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     * @param app global service for instanceses 
     */
    constructor(private _fb: FormBuilder, private orderService: OrderService, public app: AppService,
        private dropdownService: DropdownService, private utility: UtilityService,
        private error: ErrorService,
    ) {

    }


    ngOnInit(): void {
        this.LoadPatientshippingdata();
        this.LoadLocations();
        this.customAddress = {
            addressline1: '',
            addressline2: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
            phone: '',
            email: '',
            method: '',
            status: '',
        }
    }

    LoadPatientshippingdata() {
        this.ShippingdetailsForm = this._fb.group({
            addressline1: '',
            addressline2: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
            phone: '',
            email: '',
            method: '',
            status: '',
        });
        const selectedpatientid = this.app.patientId;

        if (selectedpatientid != null) {

            this.isprimary = true;
            this.dropdownService.getDemographics(selectedpatientid).subscribe(
                (data: any) => {
                    const demographic = data['result']['Demographics']['Demographic']
                    this.customAddress['addressline1'] = demographic['Street1'],
                        this.customAddress['addressline2'] = demographic['Street2'],
                        this.customAddress['city'] = demographic['City'],
                        this.customAddress['state'] = demographic['State'],
                        this.customAddress['zipcode'] = demographic['Zip'],
                        this.customAddress['country'] = demographic['Country'],
                        this.customAddress['phone'] = demographic['PhoneNumber'],
                        this.customAddress['email'] = demographic['Email'],
                        this.customAddress['method'] = '',
                        this.customAddress['status'] = '',
                    this.ShippingdetailsForm = this._fb.group({

                        addressline1: demographic['Street1'],
                        addressline2: demographic['Street2'],
                        city: demographic['City'],
                        state: demographic['State'],
                        zipcode: demographic['Zip'],
                        country: demographic['Country'],
                        phone: demographic['PhoneNumber'],
                        email: demographic['Email'],
                        method: '',
                        status: '',
                    });
                });



        }



    }

    /**
     * Loads locations
     * @param {Array} locations for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
    /**
     * Customs Address form grp
     *use for the Address changes
     */
    shippingAddressFormGrp() {
        this.ShippingdetailsForm = this._fb.group({
            addressline1: '',
            addressline2: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
            phone: '',
            email: '',
            method: '',
            status: '',
        });
    }

    /**
     * Radiobtns check
     * @param name for getting radio button name
     */
    shippingAddress(name) {
        if (name === 'Primary') {
            this.ShippingdetailsForm.controls.addressline1.patchValue(this.customAddress['addressline1'])
            this.ShippingdetailsForm.controls.addressline2.patchValue(this.customAddress['addressline2'])
            this.ShippingdetailsForm.controls.city.patchValue(this.customAddress['city'])
            this.ShippingdetailsForm.controls.state.patchValue(this.customAddress['state'])
            this.ShippingdetailsForm.controls.zipcode.patchValue(this.customAddress['zipcode'])
            this.ShippingdetailsForm.controls.country.patchValue(this.customAddress['country'])
            this.ShippingdetailsForm.controls.phone.patchValue(this.customAddress['phone'])
            this.ShippingdetailsForm.controls.email.patchValue(this.customAddress['email'])
            this.ShippingdetailsForm.controls.method.patchValue(this.customAddress['method'])
            this.ShippingdetailsForm.controls.status.patchValue(this.customAddress['status'])
        }
        if (name == 'Billing') {
            this.shippingAddressFormGrp();
        }
        if (name == 'Shippng') {
            this.shippingAddressFormGrp();
        }
        if (name == 'Custom') {
            this.shippingAddressFormGrp();
        }
    }
}