import { Component, OnInit, OnDestroy } from '@angular/core';

// import { OrderSearchService } from '../../ConnectorEngine/services/order.service';
import { Subscription } from 'rxjs';
import { OrderService } from '@app/core/services/order/order.service';
import { AppService } from '@app/core';

@Component({
    selector: 'app-order-other-details-tab',
    templateUrl: './other-details-tab.component.html'
})
export class OrderOtherDetailsMainComponent implements OnInit, OnDestroy {

    checked = false;

    addPatientInfoSidebar;
    paymentDetails;
    orderTypes: any[];
    table2: any[];
    ShippingEnableStatus = false;
    OtherScreenVisible = true;
    ShippingScreenVisible = false;
    subscribeShippingStatus: Subscription;
    constructor(private orderService: OrderService, private app: AppService) {
        this.subscribeShippingStatus = orderService.ShippingDetalesEnable$.subscribe(data => {
            if (data === true) {
                this.ShippingEnableStatus = true;
                this.ShippingScreenVisible = true;
                this.OtherScreenVisible = false;
                // this.orderService.ShippingTo.next(true);
            } else {
                this.ShippingEnableStatus = false;
                this.ShippingScreenVisible = false;
                this.OtherScreenVisible = true;
            }

        });
    }

    ngOnDestroy() {
        this.subscribeShippingStatus.unsubscribe();
    }
    ngOnInit(): void {
        this.orderService.orderId = 'New Order';
        this.orderService.orderScreenMultipleSavePath = 'others';
        this.orderService.orderOtherSelected = true;
        this.orderService.showCategory = true;
        this.orderService.orderSelectTypeobj.next(6);

        this.orderTypes = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];
        this.table2 = [
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '0.00', itemtotal: '6.00', retail: '3.00', unitprice: '6.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '6.00', total: '3.00',
                insurance: '6.00', itemtotal: '6.00', retail: '6.00', unitprice: '1.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '10.00', itemtotal: '12.00', retail: '3.00', unitprice: '0.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '', total: '6.00',
                insurance: '', itemtotal: '6.00', retail: '3.00', unitprice: '12.00'
            },
        ];
        
    }
    PageClick(event) {
        const data = event.index;
        if (data === 0) {
            //  this.orderService.orderScreenSavePath = "Other Details"
            this.OtherScreenVisible = true;
        }
        if (data === 1) {
            // this.orderService.orderScreenSavePath = "Shipping Details"
            this.OtherScreenVisible = false;
        }

    }
}
