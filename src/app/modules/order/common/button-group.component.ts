import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderService } from '@app/core/services/order/order.service';
import { AppService, ErrorService } from '@app/core';
import { ContactlensService } from '@app/core/services/order/contactlens.service';

@Component({
    selector: 'app-bottom-button-group',
    templateUrl: 'button-group.component.html'
})
export class OrderButtonGroupComponent implements OnInit, OnDestroy {
    paymentbutton = false;
    encounterID: any = [];

    /**
     * Save encounter id of order button group component is for the encounterid
     */
    saveEncounterId = [];
    OrderEncounterID: Subscription;
    @Output() Saved = new EventEmitter<string>();
    checked = false;
    saveEnable = true;
    paymentDetails = false;
    orderprintselection = false;
    orderedItemsSidebar = false;
    Subscriptionsavebutton: Subscription;
    ShippingToStatus: Subscription;
    shippingDetailsCheck = false;
    ShippingStatus = false;
    shippingEnable = false;

    /**
     * Discount charge list items of order button group component
     */
    discountChargeListItems: any = [];

    constructor(
        public orderService: OrderService,
        private router: Router,
        private app: AppService,
        private errorService: ErrorService,
        private contactlensService: ContactlensService, ) {
        // this.Subscriptionsavebutton = orderService.orderFrameSaveEnable$.subscribe(data=>{
        //     this.saveEnable = data;this.orderService.ShippingTo.next(false)
        // })
        this.OrderEncounterID = orderService.orderEncounterId$.subscribe(data => {
            this.encounterID = data;
            this.paymentbutton = true;
        });
    }
    ngOnDestroy() {
        this.OrderEncounterID.unsubscribe();
    }
    ngOnInit(): void {
        this.orderService.orderSavePaymentSavingStatus = true;
        this.orderService.orderSaveUpdateStatus = true;
        this.orderService.orderFormChangeStatus = true;
    }
    cancleClick() {
        this.orderService.OrderCancleValue.next('ordercancle');
    }


    /**
     * Saves button click
     * @returns  {object} for saving orders frames only and spectacle lens
     */
    saveButtonClick() {
        if (this.orderService.orderSaveUpdateStatus === true) {
            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select a Patient' });
                return false;
            }
            switch (this.orderService.orderScreenMultipleSavePath) {
                case "Frames Only": {
                    if (this.orderService.frameItemId == "") {
                        this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select Frame Item' });
                        return false;
                    }
                    this.orderService.orderFrame.next('Frames Only');
                    this.orderService.orderSavePaymentSavingStatus = false;
                    break;
                }
                case "Spectacle Lens": {
                    if (this.orderService.frameItemId == "") {
                        this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select Frame Item' });
                        return false;
                    }
                    switch (this.orderService.eye_Status) {
                        case "OD": {
                            if (this.orderService.spectaclelensOD == "") {
                                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select SpectacleLens Items' });
                                return false;
                            }
                            break;
                        }
                        case "OS": {
                            if (this.orderService.spectaclelensOS == "") {
                                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select SpectacleLens Items' });
                                return false;
                            }
                            break;
                        }
                        case "OU": {
                            if (this.orderService.spectaclelensOD == "" || this.orderService.spectaclelensOS == "") {
                                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select SpectacleLens Items' });
                                return false;
                            }
                            break;
                        }
                    }
                    if (this.orderService.order_SpectacleLens_Valid === true) {
                        this.orderService.orderFrame.next('Spectacle Lens');
                        this.orderService.orderSavePaymentSavingStatus = false;
                    } else {
                        this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Enter the required fields' });
                        return false;
                    }
                    break;
                }
                case "Contact Lens": {
                    if (this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
                        if (this.orderService.hardContactLensODvalue == "" || this.orderService.hardContactLensOSvalue == "") {
                            this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select HardContactLens Items' });
                            return false;
                        }
                    } else {
                        if (this.orderService.softContactlensOS == "" || this.orderService.softContactlensOd == "") {
                            this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select SoftContactLens Items' });
                            return false;
                        }
                    }
                    this.orderService.orderFrame.next('Contact Lens');
                    this.orderService.orderSavePaymentSavingStatus = false;
                    break;
                }
                case 'others': {
                if (this.orderService.orderId === '' || this.orderService.orderId === 'New Order' || this.orderService.orderId === null) {
                        this.orderService.onlyOtherItemsSaving.next('onlyOtherSaving');

                    } else {
                        this.orderService.orderspectacleLensSave.next('1');
                    }
                break;
                }
            }
        } else {
            if (this.orderService.orderFormChangeStatus === false) {
                switch (this.orderService.orderScreenMultipleSavePath) {
                    case "Frames Only": {
                        this.orderService.orderFrame.next('Frames Only');
                        this.orderService.orderSavePaymentSavingStatus = false;
                        break;
                    }
                    case "Spectacle Lens": {
                        this.orderService.orderFrame.next('Spectacle Lens');
                        this.orderService.orderSavePaymentSavingStatus = false;
                        break;
                    }
                }
            }
        }
    }
    newSameClick() {
        this.orderService.orderSaveNew.next('NewSave');
    }
    checkedShipTo() {
        // this.shippingDetailsCheck =  !this.shippingDetailsCheck ;Frames Only
        this.orderService.ShippingDetalesEnable.next(this.ShippingStatus);
        console.log(this.ShippingStatus);
    }
    NewButton() {
        this.orderService.discountObject = '';
        this.orderService.orderSaveNew.next(null);
        this.orderService.orderselection.next('New Order');
        this.orderService.framesPrice.next('');
        this.orderService.lensSelectionODPrice.next('');
        this.orderService.lensSelectionOSPrice.next('');
        this.orderService.orderDetails.next('');
        if (this.orderService.orderScreenMultipleSavePath === 'Contact Lens') {
            this.orderService.orderSoftContactLens.next(null);
        }
    }
    paymentClick() {
        if (this.orderService.orderSaveUpdateStatus === true) {
            if (this.orderService.orderSavePaymentSavingStatus === true) {
                if (this.orderService.orderScreenMultipleSavePath === 'Frames Only') {
                    this.orderService.paymentOrderNavigation = true;
                    this.orderService.orderFrame.next('Frames Only');
                }
                if (this.orderService.orderScreenMultipleSavePath === 'Spectacle Lens') {
                    this.orderService.paymentOrderNavigation = true;
                    this.orderService.orderFrame.next('Spectacle Lens');
                }
                if (this.orderService.orderScreenMultipleSavePath === 'Contact Lens') {
                    this.orderService.paymentOrderNavigation = true;
                    this.orderService.orderFrame.next('Contact Lens');
                }
            } else {
                if (this.encounterID != null) {
                    this.router.navigate(['/order/payments'], { queryParams: { encounter_id: this.encounterID } });
                    this.orderService.orderSavePaymentSavingStatus = true;
                }

            }
        } else {
            if (this.orderService.orderFormChangeStatus === false) {
                if (this.orderService.orderSavePaymentSavingStatus === true) {
                    if (this.orderService.orderScreenMultipleSavePath === 'Frames Only') {
                        this.orderService.paymentOrderNavigation = true;
                        this.orderService.orderFrame.next('Frames Only');
                    }
                    if (this.orderService.orderScreenMultipleSavePath === 'Spectacle Lens') {
                        this.orderService.paymentOrderNavigation = true;
                        this.orderService.orderFrame.next('Spectacle Lens');
                    }
                    if (this.orderService.orderScreenMultipleSavePath === 'Contact Lens') {
                        this.orderService.paymentOrderNavigation = true;
                        this.orderService.orderFrame.next('Contact Lens');
                    }
                } else {
                    if (this.encounterID != null) {
                        this.router.navigate(['/order/payments'], { queryParams: { encounter_id: this.encounterID } });
                        this.orderService.orderSavePaymentSavingStatus = true;
                    }

                }
            } else {
                if (this.encounterID != null) {
                    this.router.navigate(['/order/payments'], { queryParams: { encounter_id: this.encounterID } });
                }

            }
        }

    }
    printevent(event) {
        if (event === 'close') {
            this.orderprintselection = false;
        }
    }
    orderedItemevent(event) {
        if (event === 'close') {
            this.orderedItemsSidebar = false;
        }
    }


    /**
     *  Paymentsaving order button group component is for the process of payment
     * @param {string} buttonType 
     */
    paymentsaving(buttonType:string) {

        if (this.orderService.orderStatusGlobal != '9') {
            this.orderService.getOrderItemDetails().subscribe(resp => {
                this.discountChargeListItems = resp;
                this.saveEncounterId = [];
                this.savePaymentEncounters(0, buttonType);
            });
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'payments',
                detail: 'Please select exact orderstatus not QUOTE'
            });
        }

    }




    /**
     * Saves payment encounters
     * @param {number} [i] 
     * @param {string} buttonType 
     */
    savePaymentEncounters(i = 0, buttonType:string) {
        const paymentsdata = {
            order_id: this.orderService.orderId,
            order_status_id: this.orderService.orderStatusGlobal,
            patient_id: this.app.patientId,
            loc_id: this.app.locationId,
            ordertypeid: this.discountChargeListItems[i]['module_type_id'],
            upc_code: this.discountChargeListItems[i]['upc_code'],
            item_id: this.discountChargeListItems[i]['item_id'],
            order_details_id: this.discountChargeListItems[i]['id'],
            item_name: this.discountChargeListItems[i]['item_name'],
            module_type_id: this.discountChargeListItems[i]['module_type_id'],
            price_retail: this.discountChargeListItems[i]['price_retail'],
            qty: this.discountChargeListItems[i]['qty']
        };

        if (this.discountChargeListItems[i]['module_type_id'] == 2 || this.discountChargeListItems[i]['module_type_id'] == 3) {
            paymentsdata['lens_vision'] = this.discountChargeListItems[i]['lens_vision'];
        }
        this.orderService.ItemsAddingTothePaymentpayments(paymentsdata).subscribe(resp => {
            try {
                const response = resp;
                this.saveEncounterId.push(response['encounterId']);
                i++;

                if (i === this.discountChargeListItems.length) {
                    if (buttonType == 'payments') {
                        this.router.navigate(['/order/payments'], { queryParams: { encounter_id: this.saveEncounterId[0] } });
                    } else {
                        this.router.navigate(['/order/claims'], { queryParams: { encounter_id: this.saveEncounterId[0] } });
                    }
                    this.orderService.orderSavePaymentSavingStatus = true;
                }
                this.savePaymentEncounters(i, buttonType);

            } catch (error) {
                this.errorService.syntaxErrors(error);
            }
        });
    }
}
