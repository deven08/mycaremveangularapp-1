import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { OrderService } from '@services/order/order.service';
import { AppService } from '@services/app.service';
import { ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import {  FrameColorData} from '@app/core/models/inventory/Colors.model';
import { InventoryModel } from '@app/core/models/inventory/inventoryItems.model';
import { MasterDropDown, FrameBrands, LocationsMaster } from '@app/core/models/masterDropDown.model';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameBrand } from '@app/core/models/inventory';
import { GettingFramesDetails } from '@app/core/models/inventory/frames.model';
import { ManufacturerDropData } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-order-select-inventory',
    templateUrl: 'select-inventory.component.html'
})
export class OrderSelectInventoryComponent implements OnInit {
    orderTypes: any[];

    /**
     * Framelist  is grid data for the frames items
     */
    // framelist: Array<any> = [];
    Accesstoken = '';
    Framecolumns = [];
    framename = [];

    /**
     * frameDataList  of grid data  order select inventory component
     * stores grid data
     */
    frameDataList: Array<object>;

    /**
     * Searchmanufa for ng model varable for binding filtering of the data
     */
    searchManufa = '';

    /**
     * Manufacturers master data for binding the api responce  of manufactures master dropdown order select inventory component
     */
    Manufacturers: Array<MasterDropDown>;

    /**
     * Manufacturerlist  master data binding for binding the api responce  of manufactures  order select inventory component
     */
    public manufacturerlist: Array<Manufacturer>;

    onhandquantity: string;
    @ViewChild('dt') public dt: any;
    maufacturerid = '';
    searchUPC = '';

    /**
     * Selected frame of item selected from the grid data of order select inventory component
     */
    selectedFrame: any;

    /**
     * Searchbrand filters for filtering of the data
     */
    searchbrand = '';
    Brands: Array<MasterDropDown>;
    checked: boolean;
    Brandlist: Array<FrameBrand>;
    Mainurl = '';
    /**
     * Pagination page of order select inventory component
     * declare the default pagination number
     */
    paginationPage: number;

    

    /**
     * Colors  of master data for loading the colors dropdown order select inventory component
     */
    public colors: Array<MasterDropDown>;
    APIstring = '';
    data: any = null;
    mainpart: string;
    @Output() FrameInventory = new EventEmitter<any>();
    SelectCondition = false;

    /**
     * filterData  is object for the filtering of the Data
     */
    filterData: object;
    /**
     * Pagination value of order select inventory component
     * stores current page value
     */
    paginationValue: number;
    /**
     * Total records of order select inventory component
     * stores total number of records
     */
    totalRecords: string;
    /**
     * Frame name of order select inventory component for filtering of the data
     */
    frameName: string;
    /**
     * Locations  of order select inventory component for filtering of the data
     */
    locations: Array<LocationsMaster>;
    /**
     * Search location of order select inventory component for filtering of the data
     */
    searchLocation: string;
    constructor(
        private orderService: OrderService,
        private dropdownService: DropdownService,
        public app: AppService,
        public utility: UtilityService,
        private error: ErrorService,

    ) {
        this.paginationPage = 0;
        this.paginationValue = 0;

    }
    ngOnInit() {
        this.frameName = '';
        this.searchLocation = '';
        // this.LoadFrameName();
        this.Framecolumns = [
            { field: 'id', header: 'ID' },
            { field: 'manufacturer', header: 'Manufacturer' },
            { field: 'brand', header: 'Brand' },
            { field: 'name', header: 'Name' },
            { field: 'color', header: 'Color' },
            { field: 'color_code', header: 'Color Code' },
            { field: 'qty_on_hand', header: 'On Hand' },
            { field: 'upc_code', header: 'UPC' },
            { field: 'del_status', header: 'Status' }
        ];


        // this.Manufacturers = [];
        // this.Manufacturers = this.dropdownService.getManufactures();
        // this.Brands = [];
        // this.Brands = this.dropdownService.getBrands();
        // this.frameDataList = [];
        // this.frameDataList = this.dropdownService.frameDataList();
        this.getFrameItemsData();
        this.LoadColors();
        this.LoadBrands();
        this.LoadManufacturers();
        this.LoadLocations();
    }

    /**
      * Loads manufacturers
      *  @param {Array} Brands for binding dropdown data
      * @returns {object}  for dropdown data binding
      */
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loads colors
     * @returns color list details
     */
    LoadColors() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }


    /**
      * Loads manufacturers
      *  @param {Array} Manufacturers for binding dropdown data
      * @returns {object}  for dropdown data binding
      */

    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }


    /**
     *  pay load for the filter of grid data loading the frames items grid data
     */
    filterPayLoad() {
        const spectaclelens = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "1"
                },
                {
                    field: "brand_id",
                    operator: "=",
                    value: this.searchbrand,
                },
                {
                    field: "upc_code",
                    operator: 'LIKE',
                    value: '%' + this.searchUPC + '%',
                },
                {
                    field: "manufacturer_id",
                    operator: "=",
                    value: this.searchManufa
                },
                {
                    field: 'name',
                    operator: 'LIKE',
                    value: '%' + this.frameName + '%'
                },
                {
                    field: 'loc_id',
                    operator: '=',
                    value: this.searchLocation
                },
            ],
        };
        const resultData = spectaclelens.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.filterData = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }
            ]
        };
    }

    /**
     * Gets frame items data for the selection of the item to order
     */
    getFrameItemsData() {
        this.filterPayLoad();
        this.orderService.getUpcItemsGridData(this.filterData).subscribe((resp: InventoryModel) => {
            try {

                this.frameDataList = resp.data;
                this.totalRecords = resp['total'];
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        })
    }


    /**
     * Objs changed is for filtering of the grid data when the data changed for the filtering.
     */
    objChanged() {
        this.getFrameItemsData();
    }

    /**
     * Determines whether row frame select on
     * @param {object} event 
     */
    onRowFrameSelect(event) {
        this.selectedFrame = event.data;
        this.SelectCondition = true;

    }

    /**
     * Determines whether row frame unselect on
     * @param {object} event 
     */
    onRowFrameUnselect(event) {
        this.selectedFrame = '';
        this.SelectCondition = false;
    }

    /**
     * Selects frame item should add to order
     */
    selectFrame() {

        if (this.selectedFrame !== '') {
            const data = {
                UPC: this.selectedFrame.upc_code,
                Name: this.selectedFrame.name,
                ModuleTypeId: this.selectedFrame.module_type_id,
                uniqueid: '1',
                onhandquantity: this.selectedFrame.qty_on_hand,
            };
            this.onhandquantity = data.onhandquantity;
            this.orderService.FrameNAMEID.next(data);
            this.orderService.FrameNAME.next(this.selectedFrame.id);
            if (this.SelectCondition === true) {
                this.FrameInventory.emit(this.selectedFrame);

            }
        }
    }

    /**
     * Doubles click orderframes
     * @param {object} data 
     * @returns data if less quantity
     */
    DoubleClickOrderframes(data) {
        const value = {
            UPC: data.upc_code,
            Name: data.name,
            ModuleTypeId: data.module_type_id,
            uniqueid: '1',
            onhandquantity: data.qty_on_hand,
        };
        this.onhandquantity = value.onhandquantity;
        this.orderService.FrameNAMEID.next(value);
        this.orderService.FrameNAME.next(data.id);
        this.FrameInventory.emit(data);


        if (this.orderService.allowNegativeInventory === '1') {
            if (data.QtyOnHand <= '0') {
                this.error.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'The selected Item is out of stock.Please select another Item'
                });
                // this.WarningShow('The selected Item is out of stock.Please select another Item');

                return;

            }
        }


    }

    close() {
        this.app.enableSidebar = false;
    }

    /**
     * Refreshs order select inventory component reset  of the filters
     */
    refresh() {
        this.searchManufa = '';
        this.searchUPC = '';
        this.searchbrand = '';
        this.frameName = '',
        this.searchLocation = '',
        this.getFrameItemsData();
        this.dt.reset();
    }


    /**
     * Gets frame list by paginator
     * @param event  current page number
     */
    getFrameListByPaginator(event) {
        this.paginationValue = event.first;
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.loadFrameData(page);
    }
    /**
     * Loads frame data
     * @param [page] loading pagewise data
     */
    loadFrameData(page: number = 0) {
        this.filterPayLoad();
        this.orderService.getDataByPagination(this.filterData, page).subscribe(
            (data: GettingFramesDetails) => {
                const frameData = data.data;
                this.totalRecords = data.total;
                this.frameDataList = frameData;
            },
        );
    }
    /**
     * Loads locations
     * @param {Array} locations for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
}
