// parent component
export * from './after-sales.component';

// child components
export * from './return-wizard/returnas.component';
export * from './select-inventory/select-inventory.component';
