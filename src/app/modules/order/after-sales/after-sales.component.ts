import { Component, OnInit } from '@angular/core';
import { OrderService } from '@app/core/services/order/order.service';
import { AppService } from '@app/core';


@Component({
    selector: 'app-after-sales',
    templateUrl: 'after-sales.component.html'
})

export class AfterSalesComponent implements OnInit {

    returnWizardSidebar = false;
    aftersales: any[];
    ofterSalesView: boolean;
    othersView: boolean;
    constructor(
        private orderService: OrderService,
        public app: AppService) {

    }
    ngOnInit(): void {
        this.ofterSalesView = this.orderService.ofterSalesView;
        this.othersView = this.orderService.othersView;
        this.aftersales = [
            { field: 'charges', header: 'Charges' },
            { field: 'eye', header: 'Eye' },
            { field: 'copay', header: 'Copay' },
            { field: 'patient', header: 'Patient' },
            { field: 'patienttotal', header: 'Patient Total' },
            { field: 'insurance', header: 'Insurance' },
            { field: 'itemtotal', header: 'Item Total' },
            { field: 'retail', header: 'Retail' },
            { field: 'qty', header: 'Qty' },
            { field: 'tax', header: 'Tax' },
            { field: 'tax2', header: 'Tax 2' },
            { field: 'discount', header: 'Discount' },
            { field: 'print', header: 'Print' },
            { field: 'notes', header: 'Notes' },
            { field: 'transactiondt', header: 'Transaction Dt' },
            { field: 'updatedt', header: 'Update Dt' },
            { field: 'employee', header: 'Employee' },
            { field: 'writeoffampunt', header: 'Write Off Amount' },
        ];
    }

    returnWizard() {
        this.returnWizardSidebar = true;
    }
}

