import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-returnas-wizard',
    templateUrl: 'returnas.component.html',
})

export class ReturnasWizardComponent implements OnInit {

    selectInventorySidebar = false;
    ngOnInit(): void {
    }

    selectInventory() {
        this.selectInventorySidebar = true;
    }

}
