import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppService, ErrorService } from '@app/core';
import { OrderService } from '@app/core/services/order/order.service';
import { ContactlensService } from '@app/core/services/order/contactlens.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ManufacturerDropData } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-order-hard-contact-lens',
    templateUrl: 'hard-contact-lens.component.html'
})
export class OrderHardContactLensComponent implements OnInit, OnDestroy {
    GethardcontactLensbyid: Subscription;
    addPatientInfoSidebar;
    orderTypes: any[];
    table2: any[];
    checked: boolean;
    selectContactlensSidebar: boolean;
    subscriptionSavevaluesdetails: Subscription;
    subscriptioncontactlensOrderIdvalues: Subscription;
    HardcontatlensOS: Subscription;
    HardcontatlensOD: Subscription;
    subscriptionSavevalues: Subscription;
    RxSphereValues: any = [];
    orderLensHardLensForm: FormGroup;
    RxCylinderValues: any = [];
    AxisValues: any = [];
    RxAddValues: any = [];
    // Default Droppers
    sphereDropperDefaultod: any = '';
    sphereDropperDefaultos: any = '';
    cylinderDropperDefaultod: any = '';
    cylinderDropperDefaultos: any = '';
    addDropperDefaultos: any = '';
    addDropperDefaultod: any = '';
    contactlensvalue: any;
    Hardcontactlensodsavevalue: any;
    Hardcontactlensossavevalue: any;
    Hardcontactpostosvalues: any;
    Hardcontactpostodvalues: any;

    orderid: any;
    orderitemid: any;
    ordersavedvalues: any;
    ordervalueupdate = false;
    Hardcontactlens = false;
    disablecheckbox = false;
    parameters: any;
    parametersrequired: any;
    isHardcontactlensvalid = false;
    public manufacturerlist: Manufacturer[];
    public Manufacturers: any = [];

    /**
     * Subscription location id of order hard contact lens component for location dropdown status
     */

    subscriptionLocationId: Subscription;
    /**
     * Location id of order hard contact lens component for location value
     */
    LocationId: '';
    /**
     * Subscription physician id of order hard contact lens component for physician dropdown status
     */
    subscriptionPhysicianId: Subscription;

    /**
     * Subscription save new of order hard contact lens component
     */
    subscriptionSaveNew: Subscription;

    /**
     * Retail price od of order hard contact lens component
     * for storing the OD retail price form saved order
     */
    retail_PriceOD: string;

    /**
     * Retail price os of order hard contact lens component
     * for storing the OS retail price form saved order
     */
    retail_PriceOS: string;
    /**
     * Creates an instance of order hard contact lens component.
     
     * @param router for navigation
     * @param contactlensService for contactlens methods
     * @param _fb  for formbuilder
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     * @param app global service for instanceses 
     */
    constructor(private _fb: FormBuilder,
        private router: Router, private orderService: OrderService, private orderSearchService: OrderService,
        private contactlensService: ContactlensService, public app: AppService, private dropdownService: DropdownService,
        private errorService: ErrorService, private utility: UtilityService) {

        // this.formGroupData();
        // this.getmandatoryfieldsparameters();

        this.subscriptionSavevaluesdetails = orderSearchService.orderFrameSave$.subscribe(
            (datavalue: string) => {
                const physicianValue = this.orderLensHardLensForm.controls.physician_id.value;
                if (physicianValue !== '' && physicianValue !== 0) {
                    if (this.ordervalueupdate === true && datavalue === 'Contact Lens' && this.orderid != null && this.orderid !== undefined &&
                        this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
                        this.UpdatecompleteOrdervalues(this.orderid, this.orderLensHardLensForm.controls['ODitemid'].value,
                            this.orderLensHardLensForm.controls['OSitemid'].value, '');
                        // this.Enablecheckbox=true;
                        // this.Hardcontactlens=true;
                    } else if (datavalue === 'Contact Lens' && this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
                        this.saveHardcontactlenscompleteorder(true);
                    }
                } else {
                    this.errorService.displayError({
                        severity: 'info',
                        summary: 'Required',
                        detail: 'Please select physician'
                    });

                }

            });

        this.subscriptionSaveNew = orderService.orderSaveNew$.subscribe((data: string) => {
            if (this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
                this.orderid = null;
                this.Hardcontactlensodsavevalue.id = this.Hardcontactlensodsavevalue.item_id;
                this.Hardcontactlensossavevalue.id = this.Hardcontactlensossavevalue.item_id;
                this.contactlensService.savecontactlensupdate = 'Hard Contact Lens';
            }
        });



        this.HardcontatlensOD = this.contactlensService.HardContactODvalue$.subscribe(data => {


            const value = data;
            this.orderService.hardContactLensODvalue = value.Hardcontactlensod.id;
            this.selectContactlensSidebar = false;
            this.SetRequiredfiledsbyconfiqID(value.Hardcontactlensod.id, 'od');
            if (value.Hardcontactlensod.manufacturer) {
                this.orderLensHardLensForm.controls['manufactureod'].patchValue(value.Hardcontactlensod.manufacturer.manufacturer_name);
            }
            this.orderLensHardLensForm.controls['nameod'].patchValue(value.Hardcontactlensod.name);
            this.orderLensHardLensForm.controls['colorod'].patchValue(value.Hardcontactlensod.color_code);
            this.orderLensHardLensForm.controls['upcod'].patchValue(value.Hardcontactlensod.upc_code);
            this.orderLensHardLensForm.controls['sourceod'].patchValue(value.Hardcontactlensod.sourceod);
            this.orderLensHardLensForm.controls['qtyod'].patchValue('1');
            this.Hardcontactlensodsavevalue = value.Hardcontactlensod;
            this.retail_PriceOD = value.Hardcontactlensod.retail_price;
            const values = {
                'LensType': 'OD',
                'data': data
            };
            this.Hardcontactlensodsavevalue.item_id = value.Hardcontactlensod.id;
            this.Hardcontactlensodsavevalue.item_name = value.Hardcontactlensod.name;
            this.contactlensService.contactlensotherdetails = [];
            this.isHardcontactlensvalid = true;
            this.contactlensService.HardContactOtherODvalue.next(values);
        });

        this.HardcontatlensOS = this.contactlensService.HardContactOSvalue$.subscribe(data => {
            const value = data;
            this.orderService.hardContactLensOSvalue = value.Hardcontactlensod.id;
            this.selectContactlensSidebar = false;
            this.SetRequiredfiledsbyconfiqID(value.Hardcontactlensod.id, 'os');
            if (value.Hardcontactlensod.manufacturer) {
                this.orderLensHardLensForm.controls['manufactureos'].patchValue(value.Hardcontactlensod.manufacturer.manufacturer_name);
            }
            this.orderLensHardLensForm.controls['nameos'].patchValue(value.Hardcontactlensod.name);
            this.orderLensHardLensForm.controls['coloros'].patchValue(value.Hardcontactlensod.color_code);
            this.orderLensHardLensForm.controls['upcos'].patchValue(value.Hardcontactlensod.upc_code);
            this.orderLensHardLensForm.controls['sourceos'].patchValue(value.Hardcontactlensod.sourceod);
            this.orderLensHardLensForm.controls['qtyos'].patchValue('1');
            this.Hardcontactlensossavevalue = value.Hardcontactlensod;
            this.retail_PriceOS = value.Hardcontactlensod.retail_price;
            const values = {
                'LensType': 'OS',
                'data': data
            };
            this.Hardcontactlensossavevalue.item_id = value.Hardcontactlensod.id;
            this.Hardcontactlensossavevalue.item_name = value.Hardcontactlensod.name;
            this.contactlensService.contactlensotherdetails = [];
            this.isHardcontactlensvalid = true;
            this.contactlensService.HardContactOtherOSvalue.next(values);
        });


        this.subscriptioncontactlensOrderIdvalues = this.contactlensService.HardContactbyorderID$.subscribe(value => {
            if (value !== 'New Order') {
                this.orderLensHardLensForm.reset();
                this.getmandatoryfieldsparameters();
                this.contactlensService.contactlensotherdetails = [];
                this.orderid = value;
                this.getcontactlensordervaluewithorderid(value);
                this.orderid = value;
                this.orderLensHardLensForm.controls['module_type_id'].patchValue('3');
                this.orderLensHardLensForm.controls['patient_id'].patchValue(this.app.patientId);
                this.orderLensHardLensForm.controls['order_id'].patchValue(value);
                this.orderLensHardLensForm.controls['location_id'].patchValue(this.app.locationId);
                this.orderLensHardLensForm.controls['physician_id'].patchValue('');
                this.orderService.orderContactLensById.next(value);
            } else {
                this.disablecheckbox = false;
                this.Hardcontactlens = false;
                this.orderLensHardLensForm.reset();
                this.getmandatoryfieldsparameters();
                // this.contactlensService.Disablehardcontactcomponent.next(true);
                // localStorage.removeItem('orderId');
                this.orderid = value;
                this.ordervalueupdate = false;
                this.isHardcontactlensvalid = false;
                this.contactlensService.contactlensotherdetails = [];
                this.Hardcontactlensodsavevalue = [];
                this.Hardcontactlensossavevalue = [];
                this.orderLensHardLensForm.controls['module_type_id'].patchValue('3');
                this.orderLensHardLensForm.controls['patient_id'].patchValue(this.app.patientId);
                this.orderLensHardLensForm.controls['sphere_od'].patchValue('PL');
                this.orderLensHardLensForm.controls['sphere_os'].patchValue('PL');
                this.orderLensHardLensForm.controls['location_id'].patchValue(this.app.locationId);
                this.orderLensHardLensForm.controls['physician_id'].patchValue('');
                this.orderService.hardContactLensODvalue = '';
                this.orderService.hardContactLensOSvalue = '';
                this.orderService.orderContactLensById.next(value);
            }
        });
        this.subscriptionLocationId = orderService.orderLocationId$.subscribe(data => {
            if (data !== '') {
                this.LocationId = data;
                this.orderLensHardLensForm.controls['location_id'].patchValue(data);
                // this.orderService.orderFrameSave.next(false)
            } else {
                this.LocationId = data;
                this.orderLensHardLensForm.controls['location_id'].patchValue(data);
                // this.orderService.orderFrameSave.next(false)
            }
        });
        this.subscriptionPhysicianId = orderService.lensPhysicianId$.subscribe(data => {
            this.orderLensHardLensForm.patchValue({
                physician_id: data
            });
        });
    }

    checkunchekevent(value) {
        // this.contactlensService.Disablehardcontactcomponent.next(false);
        this.contactlensService.Disablehardcontactcomponent.next(value);
        this.Hardcontactlens = value;

    }

    validationcheck(data) {
        // this.errorService.displayError = [];
        if (data === 'Contact Lens') {
            // localStorage.setItem('PatientId',"1");
            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please Select a Patient'
                });
                // this.WarningShow('Please Select a Patient');


                return;
            }
            if (this.Hardcontactlensossavevalue.length === 0 && this.Hardcontactlensodsavevalue.length === 0) {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please Enter the values'
                });
                // this.WarningShow('Please Enter the values');


                return;
            }
            if (this.orderLensHardLensForm.controls['sphere_od'].value === '' ||
                this.orderLensHardLensForm.controls['sphere_od'].value == null ||
                this.orderLensHardLensForm.controls['sphere_od'].value === '' ||
                this.orderLensHardLensForm.controls['sphere_od'].value == null) {
                this.orderService.OrderAlertPopup.next('required');
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Sphere OD and OS mandatory'
                });
                // this.WarningShow('Sphere OD and OS mandatory');

                return false;
            }

        }
    }

    /**
     * on destroy for unsubscribing observables
     */
    ngOnDestroy(): void {

        //   this.subscriptionSavevalues.unsubscribe();
        //   this.subscriptioncontactlensOrderIdvalues.unsubscribe();
        this.subscriptionSavevaluesdetails.unsubscribe();
        this.HardcontatlensOD.unsubscribe();
        this.HardcontatlensOS.unsubscribe();
        this.subscriptioncontactlensOrderIdvalues.unsubscribe();
        this.subscriptionLocationId.unsubscribe();
        this.subscriptionPhysicianId.unsubscribe();
        this.subscriptionSaveNew.unsubscribe();
    }




    formGroupData() {
        this.orderLensHardLensForm = this._fb.group({
            // accessToken: localStorage.getItem('tokenKey'),
            patient_id: this.app.patientId,
            id: '0',
            base_od: ['', [Validators.required]],
            base_os: ['', [Validators.required]],
            diameter_od: '',
            diameter_os: '',
            sphere_od: ['PL', [Validators.required]],
            sphere_os: ['PL', [Validators.required]],
            axis_od: '',
            axis_os: '',
            add_od: '',
            add_os: '',
            cylinder_od: '',
            cylinder_os: '',
            multifocalid_os: '',
            multifocalid_od: '',
            manufactureod: '',
            nameod: '',
            colorod: '',
            upcod: '',
            sourceod: '',
            qtyod: '',
            manufactureos: '',
            nameos: '',
            coloros: '',
            upcos: '',
            sourceos: '',
            qtyos: '',
            module_type_id: '3',
            ODitemid: '',
            OSitemid: '',
            patientrxid: '',
            warranty_od: '',
            warranty_os: '',
            order_id: '',
            location_id: this.app.locationId.toString(),
            oz_od: '',
            oz_os: '',
            dot_od: '',
            dot_os: '',
            sphere2_od: '',
            sphere2_os: '',
            pcr2_od: '',
            pcw2_od: '',
            pcr3_od: '',
            pcw3_od: '',
            pcr2_os: '',
            pcw2_os: '',
            pcr3_os: '',
            pcw3_os: '',
            segdi_od: '',
            segdi_os: '',
            seght_od: '',
            seght_os: '',
            bc2_od: '',
            bc2_os: '',
            dia2_od: '',
            dia2_os: '',
            custom_rx: '1',
            det_order_id: '1',
            outside_rx: '',
            physician_id: this.orderService.physicianID,
            hard_contact: 1,
            aspheric_od: '',
            ct_od: '',
            aspheric_os: '',
            ct_os: ''

        }
        );
    }



    ngOnInit(): void {
        this.formGroupData();
        this.loadformvalues();
        this.LoadMasters();
        this.getmandatoryfieldsparameters();
        this.Hardcontactlensossavevalue = [];
        this.Hardcontactlensodsavevalue = [];
        this.contactlensService.contactlensotherdetails = [];
        this.orderSearchService.orderScreenMultipleSavePath = 'Contact Lens';
        this.disablecheckbox = false;
        this.Hardcontactlens = false;
        this.ordervalueupdate = false;
        if (this.orderid != null) {
            this.getcontactlensordervaluewithorderid(this.orderid);
        }
        // this.contactlensService.errorHandle.msgs = [];
        const patientid = this.app.patientId;
        if (patientid == null || patientid === undefined) {
            this.errorService.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select a Patient'
            });
            // this.WarningShow('Please Select a Patient');

            return;
        }
        // this.Manufacturers = [];
        // this.Manufacturers = this.dropdownService.getManufactures();
        // this.manufacturerlist = this.Manufacturers;
        // localStorage.removeItem('orderId');
        this.Loadmanufactures();
    }
    /**
     * Loadmanufactures order hard contact lens component
     * @param {Array} Manufacturers for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    Loadmanufactures() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }



    getmandatoryfieldsparameters() {
        this.parametersrequired = [];
        this.parameters = [];
        this.parametersrequired = [{}];
        this.contactlensService.getcontactParametersDetails().subscribe(data => {
            this.parameters = data['data'];
            for (let i = 0; i <= this.parameters.length - 1; i++) {
                this.parameters[i].Availablebool = false;
                this.parameters[i].requiredbool = false;
                const elementname = this.parameters[i].measurementname.replace(/\s/g, '');
                const elementrequired = elementname.trim() + 'required' + 'od';
                const elementavailable = elementname.trim() + 'available';
                // OD
                this.parametersrequired[0][elementavailable + 'od'] = false;
                this.parametersrequired[0][elementrequired + 'od'] = false;
                // OS
                this.parametersrequired[0][elementavailable + 'os'] = false;
                this.parametersrequired[0][elementrequired + 'os'] = false;
            }
            this.parametersrequired = this.parametersrequired;
        });
    }

    SetRequiredfiledsbyconfiqID(confiqID, type) {
        this.contactlensService.getcontactParametersByConfiqID(confiqID).subscribe(data => {
            this.parameters = data['data'];
            this.parametersrequired = [{}];
            this.parameters.forEach(element => {
                const a = this.parameters.findIndex(x => x.measurementname === element.measurementname);
                if (a !== -1) {
                    if (this.parameters[a].measurementname === element.measurementname) {
                        this.parameters[a].checkedvalues = true;
                        this.parameters[a].Availablebool = element.available === 1 ? true : false;
                        this.parameters[a].requiredbool = element.required === 1 ? true : false;
                        const elementname = element.measurementname.replace(/\s/g, '');
                        const elementrequired = elementname.trim() + 'required';
                        const elementavailable = elementname.trim() + 'available';
                        this.parametersrequired[0][elementavailable + type] = this.parameters[a].Availablebool;
                        this.parametersrequired[0][elementrequired + type] = this.parameters[a].requiredbool;
                        if (this.parameters[a].requiredbool === true) {
                            this.parametersrequired[0][elementavailable + type] = false;
                        }

                        if (this.parameters[a].Availablebool === false) {
                            this.parametersrequired[0][elementavailable + type] = false;
                        }
                        //    if(this.parameters[a].requiredbool==true)
                        //    {
                        //        alert(elementavailable+type +  this.parametersrequired[0][elementavailable+type]);
                        //     this.parametersrequired[0][elementavailable+type] =true;
                        //    }



                        //     this.parametersrequired[0].elementavailable= this.parameters[a].Availablebool;
                        //     this.parametersrequired[0].elementrequired= this.parameters[a].requiredbool;
                    }
                }
            });
        });
    }

    /**
     * Saveupdatevalues order hard contact lens component
     * @param {number} patientid 
     * @returns  
     */
    saveupdatevalues(patientid) {
        if (this.orderLensHardLensForm.controls['physician_id'].value !== '') {
            const itemid = this.orderLensHardLensForm.controls['id'].value;
            if (itemid === '0') {
                this.savecontactlens(patientid);
            } else {
                this.updatecontactlens(patientid, itemid);
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select physician'
            });
            return false
        }
    }

    savecontactlens(patientid: any) {
        // this.contactlensService.errorHandle.msgs = [];
        if (patientid != null || patientid !== undefined) {
            this.orderSearchService.SaveContactLens(patientid, this.orderLensHardLensForm.value).subscribe(data => {
                this.loadformvalues();
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Saved Successfully.'
                });
                // this.SuccessShow('Saved Successfully.');
                this.orderLensHardLensForm.reset();

            });
        }
    }

    updatecontactlens(patientid: any, itemid) {       
        // this.contactlensService.errorHandle.msgs = [];
        if (patientid != null || patientid !== undefined) {
            this.orderSearchService.UpdateContactLens(patientid, this.orderLensHardLensForm.value, itemid).subscribe(data => {
                // this.SuccessShow('Updated Successfully.');
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Updated Successfully.'
                });

                this.loadformvalues();
            });
        }
    }

    loadformvalues() {
        this.formGroupData();
        const patientid = this.app.patientId;
        if (patientid != null) {
            // this.loadgroupdetails(patientid);
        }
    }

    equalvalues() {

        this.orderLensHardLensForm.controls['diameter_os'].patchValue(this.orderLensHardLensForm.controls['diameter_od'].value);
        this.orderLensHardLensForm.controls['base_os'].patchValue(this.orderLensHardLensForm.controls['base_od'].value);
        this.orderLensHardLensForm.controls['sphere_os'].patchValue(this.orderLensHardLensForm.controls['sphere_od'].value);
        this.orderLensHardLensForm.controls['cylinder_os'].patchValue(this.orderLensHardLensForm.controls['cylinder_od'].value);
        this.orderLensHardLensForm.controls['axis_os'].patchValue(this.orderLensHardLensForm.controls['axis_od'].value);
        this.orderLensHardLensForm.controls['add_os'].patchValue(this.orderLensHardLensForm.controls['add_od'].value);
        this.orderLensHardLensForm.controls['multifocalid_os'].patchValue(this.orderLensHardLensForm.controls['multifocalid_od'].value);
        this.orderLensHardLensForm.controls['sphere2_os'].patchValue(this.orderLensHardLensForm.controls['sphere2_od'].value);
        this.orderLensHardLensForm.controls['bc2_os'].patchValue(this.orderLensHardLensForm.controls['bc2_od'].value);
        this.orderLensHardLensForm.controls['dia2_os'].patchValue(this.orderLensHardLensForm.controls['dia2_od'].value);

    }


    saveHardcontactlenscompleteorder(Issave) {
        // Issave true indicated a new order id true
        if (this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
            const messagedetails = {
                'messagetype': 'required',
                'message': 'Please Enter the required fields'
            };
            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                messagedetails.message = 'Please Select a Patient';
                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }
            if (this.isHardcontactlensvalid === false) {
                messagedetails.message = 'Please Select Contact Lens OD/OS';
                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }

            if (!this.orderLensHardLensForm.valid) {
                messagedetails.message = 'Please Enter the required fields';

                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }
            this.CreateNewcontactlensorder();
            // rx save update
            // this.saveupdatevalues(this._StateInstanceService.PatientId) ;
        }
    }


    // OD values save and update method

    /**
     * Creates newcontactlensorder
     * @returns  false not execute rest all functions
     */
    CreateNewcontactlensorder() {
        // this.contactlensService.errorHandle.msgs = [];
        if (this.orderLensHardLensForm.controls['sphere_od'].value === '' ||
            this.orderLensHardLensForm.controls['sphere_od'].value == null ||
            this.orderLensHardLensForm.controls['sphere_od'].value === '' ||
            this.orderLensHardLensForm.controls['sphere_od'].value == null) {
            this.orderService.OrderAlertPopup.next('required');
            this.errorService.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Sphere OD and OS mandatory'
            });
            // this.WarningShow('Sphere OD and OS mandatory');
            return false;
        }
        let odqty;
        let praccode = '';
        if (this.Hardcontactlensodsavevalue != null && this.Hardcontactlensodsavevalue !== undefined) {
            const value = this.contactlensService.contactlensotherdetails.filter(
                x => x.itemId === this.Hardcontactlensodsavevalue.item_id);
            if (value.length === 0) {
                odqty = 1;
                praccode = '';
            } else {
                odqty = value[0].qty;
                praccode = value[0].item_prac_code;
            }
        }
        const Hardcontactlensodvaluessave = {
            'patient_id': this.app.patientId,
            'item_prac_code': praccode,
            'loc_id': this.app.locationId,
            'item_id': this.Hardcontactlensodsavevalue.id,
            'upc_code': this.Hardcontactlensodsavevalue.upc_code,
            'item_name': this.Hardcontactlensodsavevalue.item_name,
            'module_type_id': this.orderLensHardLensForm.controls['module_type_id'].value,
            'lens_vision': 'OD',
            'qty': odqty, // this.orderLensHardLensForm.controls['qtyod'].value,
            'ordertypeid': '4',
            'manufacturer_id': this.Hardcontactlensodsavevalue.manufacturer_id,
            'price_retail': this.retail_PriceOD,
            'order_status_id': this.orderService.orderStatusGlobal
        };
        this.Hardcontactpostodvalues = Hardcontactlensodvaluessave;
        if (this.Hardcontactpostodvalues.upc_code !== '' && this.Hardcontactpostodvalues.upc_code !== undefined) {
            this.orderService.saveOrderFrames(this.Hardcontactpostodvalues).subscribe(data => {
                // if (data['status'] === 'New Order Crated') {
                // this.orderService.orderId = data['order_id'];
                this.orderService.orderId = data['order_id'];
                this.orderid = this.orderService.orderId;
                // this.orderService.orderId ;
                this.createneworderOS(this.orderService.orderId);
                this.savepatientrxdetails(this.orderid);
                // }
            });
        }
        if (this.Hardcontactpostodvalues.upc_code === '' || this.Hardcontactpostodvalues.upc_code === undefined) {
            this.createneworderOS(this.orderService.orderId);
        }
    }

    /**
     * Updatecontactlensodvaluesbyorderids order hard contact lens component
     * @param {number} orderidvalue 
     * @param {number} oditemid 
     * @param  {number} ositemvalues 
     */
    updatecontactlensodvaluesbyorderid(orderidvalue, oditemid, ositemvalues) {
        let odqty;
        let praccode = '';
        if (this.Hardcontactlensodsavevalue != null && this.Hardcontactlensodsavevalue.length !== 0) {
            const value = this.contactlensService.contactlensotherdetails.filter(
                x => x.itemId === this.Hardcontactlensodsavevalue.item_id);
            if (value.length === 0) {
                odqty = 1;
                praccode = '';
            } else {
                odqty = value[0].qty;
                praccode = value[0].item_prac_code;
            }
        }

        if (this.Hardcontactlensodsavevalue.upc_code !== '' && this.Hardcontactlensodsavevalue.upc_code !== undefined) {
            const updatecontactlensodvalues = {
                'patient_id': this.app.patientId,
                'item_prac_code': praccode,
                'loc_id': this.app.locationId,
                'item_id': this.Hardcontactlensodsavevalue.item_id,
                'upc_code': this.Hardcontactlensodsavevalue.upc_code,
                'item_name': this.Hardcontactlensodsavevalue.item_name,
                'module_type_id': this.orderLensHardLensForm.controls['module_type_id'].value,
                'lens_vision': 'OD',
                'qty': odqty,
                'ordertypeid': '4',
                'manufacturer_id': this.Hardcontactlensodsavevalue.manufacturer_id,
                'price_retail': this.retail_PriceOD,
                'order_status_id': this.orderService.orderStatusGlobal

            };
            this.Hardcontactpostodvalues = updatecontactlensodvalues;

            if (oditemid !== '' && oditemid !== null) {
                this.orderService.updateOrderFrames(this.Hardcontactpostodvalues, orderidvalue, oditemid).subscribe(data => {
                    // this.Savecontactlensosvalueswithorderid(orderidvalue,false,ositemvalues);
                    // if (data['status'] === 'New Order Crated') {
                    this.orderService.orderId = data['order_id'];
                    // }
                });
            } else if ((oditemid === '' || oditemid == null) && orderidvalue !== '') {
                this.orderService.saveorderdetailswithorderId(this.Hardcontactpostodvalues, orderidvalue).subscribe(data => {
                    // if (data['status'] === 'New Item added to the order.') {
                    // this.Savecontactlensosvalueswithorderid(orderidvalue,false,ositemvalues);
                    this.orderService.orderId = data['order_id'];
                    this.orderid = this.orderService.orderId;

                    // }
                });
            }
        }
    }


    UpdatecompleteOrdervalues(orderid, oditemvalue, ositemvalues, patientrxid) {
        if (this.contactlensService.savecontactlensupdate === 'Hard Contact Lens') {
            const messagedetails = {
                'messagetype': 'required',
                'message': 'Please Enter the required fields'
            };
            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                messagedetails.message = 'Please Select a Patient';
                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }
            if (!this.orderLensHardLensForm.valid) {
                messagedetails.message = 'Please Enter the required fields';
                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }
            this.updatecontactlensodvaluesbyorderid(orderid, oditemvalue, ositemvalues);
            this.Savecontactlensosvalueswithorderid(orderid, false, ositemvalues);
            this.updatepatientrxdetails(this.app.patientId, this.orderLensHardLensForm.controls['patientrxid'].value);
        }
    }


    /**
     * Createneworders os is hardcontactlens OS order
     * @param {number} orderid 
     */
    createneworderOS(orderid) {
        let osqty;
        let praccode = '';
        if (this.Hardcontactlensossavevalue !== undefined && this.Hardcontactlensossavevalue !== '' &&
            this.Hardcontactlensossavevalue !== undefined) {
            const value = this.contactlensService.contactlensotherdetails.filter(
                x => x.itemId === this.Hardcontactlensossavevalue.item_id);

            if (value.length === 0) {
                osqty = 1;
                praccode = '';
            } else {
                osqty = value[0].qty;
                praccode = value[0].item_prac_code;
            }


            const Hardcontactlensosvaluedetails = {
                'patient_id': this.app.patientId,
                'item_prac_code': praccode,
                'loc_id': this.app.locationId,
                'item_id': this.Hardcontactlensossavevalue.id,
                'upc_code': this.Hardcontactlensossavevalue.upc_code,
                'item_name': this.Hardcontactlensossavevalue.item_name,
                'module_type_id': this.orderLensHardLensForm.controls['module_type_id'].value,
                'lens_vision': 'OS',
                'qty': osqty,
                'ordertypeid': '4',
                'manufacturer_id': this.Hardcontactlensossavevalue.manufacturer_id,
                'price_retail': this.retail_PriceOS,
                'order_status_id': this.orderService.orderStatusGlobal
            };

            this.Hardcontactpostosvalues = Hardcontactlensosvaluedetails;

            if (orderid === '' || orderid == null) {
                this.orderService.saveOrderFrames(this.Hardcontactpostosvalues).subscribe(data => {

                    // if (data['status'] === 'New Order Crated') {
                    this.orderService.orderId = data['order_id'];
                    this.orderid = this.orderService.orderId;
                    this.savepatientrxdetails(this.orderid);
                    // }
                });
            } else {
                this.orderService.saveorderdetailswithorderId(this.Hardcontactpostosvalues, orderid).subscribe(data => {
                    // if (data['status'] === 'New Item added to the order.') {
                    this.orderService.orderId = data['order_id'];

                    this.orderid = this.orderService.orderId;
                    /* this.savepatientrxdetails(this.orderid); */
                    // }
                });
            }
        }
    }



    /**
     * Savecontactlensosvalueswithorderids order hard contact lens component
     * @param {number} orderid order id     for paticular order
     * @param {boolean} newsave  differ the new or old order
     * @param {number} ositemid  item id of each and every item.
     */
    Savecontactlensosvalueswithorderid(orderid, newsave, ositemid) {
        let osqty;
        let praccode = '';
        if (this.Hardcontactlensossavevalue !== undefined && this.Hardcontactlensossavevalue.length !== 0 &&
            this.Hardcontactlensossavevalue !== '') {
            const value = this.contactlensService.contactlensotherdetails.filter(
                x => x.itemId === this.Hardcontactlensossavevalue.item_id);

            if (value.length === 0) {
                osqty = 1;
                praccode = '';
            } else {
                osqty = value[0].qty;
                praccode = value[0].item_prac_code;
            }
        }

        const Hardcontactlensosvaluedetails = {
            'patient_id': this.app.patientId,
            'item_prac_code': praccode,
            'loc_id': this.app.patientId,
            'item_id': this.Hardcontactlensossavevalue.item_id,
            'upc_code': this.Hardcontactlensossavevalue.upc_code,
            'item_name': this.Hardcontactlensossavevalue.item_name,
            'module_type_id': this.orderLensHardLensForm.controls['module_type_id'].value,
            'lens_vision': 'OS',
            'qty': osqty,
            'ordertypeid': '4',
            'manufacturer_id': this.Hardcontactlensossavevalue.manufacturer_id,
            'price_retail': this.retail_PriceOS,
            'order_status_id': this.orderService.orderStatusGlobal
        };

        this.Hardcontactpostosvalues = Hardcontactlensosvaluedetails;
        if (ositemid !== '' && ositemid !== null) {
            this.orderService.updateOrderFrames(this.Hardcontactpostosvalues, orderid, ositemid).subscribe(data => {
                // if (data['status'] === 'New Order Created') {
                this.orderService.orderId = data['order_id'];

                // }
            });
        } else if ((ositemid === '' || ositemid == null) && orderid !== '') {
            this.orderService.saveorderdetailswithorderId(this.Hardcontactpostosvalues, orderid).subscribe(data => {
                // if (data['status'] === 'New Item added to the order.') {
                this.orderService.orderId = data['order_id'];
                this.orderid = this.orderService.orderId;
                // }
            });
        }

    }

    HardcontactDataClick(value) {

        this.selectContactlensSidebar = true;
        const data = {
            'IshardContactlens': '1',
            'LensType': value
        };
        this.contactlensService.IsHardContactLens.next(data);
    }


    /**
     * Saves patientrx details of order hard contact lens type.
     * @param {string} orderid getting order id
     * @returns {object} for saving rxdetails
     */
    savepatientrxdetails(orderid) {
        // this.orderLensHardLensForm.controls['outside_rx'].patchValue(false);
        const outsidevalue = this.orderLensHardLensForm.controls['outside_rx'].value;
        if (outsidevalue !== true) {
            this.orderLensHardLensForm.controls['outside_rx'].patchValue(false);
        }
        this.orderLensHardLensForm.controls['custom_rx'].patchValue('1');
        this.orderLensHardLensForm.controls['det_order_id'].patchValue('1');
        this.orderLensHardLensForm.controls['physician_id'].patchValue(this.orderService.physicianID);
        this.orderLensHardLensForm.controls.hard_contact.patchValue(1);
        if (this.orderLensHardLensForm.controls['physician_id'].value !== '') {
            this.orderLensHardLensForm.controls['order_id'].patchValue(this.orderService.orderId);
            if (this.app.patientId) {
                if (this.orderLensHardLensForm.value) {
                    this.orderSearchService.savepatientlensdetails(this.orderLensHardLensForm.value,
                        this.app.patientId).subscribe(data => {
                            this.orderService.OrderAlertPopup.next('save');
                            this.orderService.orderFormUpdate.next('success');
                            this.ordervalueupdate = true;
                            // TO DO
                            // let encounter = this.orderService.orderId ;
                            // this.orderService.getOrderStatusData(encounter).subscribe(data=>{
                            //     this.orderService.orderEncounterId.next( data['order_enc_id']);
                            // })
                            // this.SuccessShow("Saved Successfully.");

                        });
                    this.orderService.orderspectacleLensSave.next('5');
                }
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select physician'
            });
        }
    }

    /**
     * Update patientrx details order hard contact lens type
     * @param {string} orderid getting order id
     * @returns {object} for update rxdetails
     */
    updatepatientrxdetails(patientid, patientrx) {
        // this.contactlensService.errorHandle.msgs = [];
        // tslint:disable-next-line:no-unused-expression
        this.orderLensHardLensForm.controls.custom_rx.patchValue('1');
        this.orderLensHardLensForm.controls.det_order_id.patchValue('1');
        this.orderLensHardLensForm.controls.physician_id.patchValue(this.orderService.physicianID);
        this.orderLensHardLensForm.controls.hard_contact.patchValue(1);
        if (this.orderLensHardLensForm.controls.physician_id.value !== '') {
            if (this.orderLensHardLensForm.value,
                this.app.patientId !== undefined && this.orderLensHardLensForm.value,
                this.app.patientId !== '') {
                this.orderSearchService.updatepatientlensdetails(this.orderLensHardLensForm.value, patientid, patientrx).subscribe(data => {
                    this.errorService.displayError({
                        severity: 'success',
                        summary: 'success Message', detail: 'Updated Successfully.'
                    });
                    // this.SuccessShow('Updated Successfully.');
                    if (this.orderid != null) {
                        this.orderService.orderspectacleLensSave.next('5');
                        if (this.orderSearchService.paymentOrderNavigation === true) {
                            this.orderService.getOrderStatusData(this.orderid).subscribe(DataSet => {
                                this.orderSearchService.orderSaveUpdateStatus = false;
                                this.orderSearchService.orderFormChangeStatus = true;
                                this.router.navigate(['/order/payments'], { queryParams: { encounter_id: DataSet['order_enc_id'] } });
                                // this.orderService.orderEncounterId.next(data['order_enc_id']);
                                // console.log(this.locationId)
                                this.orderService.OrderAlertPopup.next('update');
                            });
                        }
                    }
                });

            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select physician'
            });
        }
    }

    getcontactlensordervaluewithorderid(orderidvalue) {
        this.orderid = orderidvalue;
        this.orderSearchService.getOrderItemDetails(orderidvalue).subscribe(orderdetails => {
            this.ordersavedvalues = [];
            this.Hardcontactlensodsavevalue = [];
            this.Hardcontactlensossavevalue = [];
            this.ordersavedvalues = orderdetails;
            this.orderLensHardLensForm.controls['module_type_id'].patchValue('3');
            this.orderLensHardLensForm.controls['patient_id'].patchValue(this.app.patientId);
            const odsavedvalues = this.ordersavedvalues.filter(x => x.lens_vision === 'OD' && x.order_id === parseInt(orderidvalue, 10));
            this.isHardcontactlensvalid = true;
            if (odsavedvalues !== '' && odsavedvalues !== undefined) {
                this.orderService.hardContactLensODvalue = odsavedvalues[0].item_id;
                const oddetails = odsavedvalues;
                this.Hardcontactlensodsavevalue = odsavedvalues;
                this.Hardcontactlensodsavevalue = odsavedvalues[0];
                this.Hardcontactlensodsavevalue.item_id = odsavedvalues[0].item_id;
                this.Hardcontactlensodsavevalue.item_name = odsavedvalues[0].item_name;
                this.retail_PriceOD = odsavedvalues[0].price_retail;
                this.orderLensHardLensForm.controls['ODitemid'].patchValue(odsavedvalues[0].id);
                this.Hardcontactlensodsavevalue.name = odsavedvalues[0].item_name;
                this.orderLensHardLensForm.controls['manufactureod'].patchValue(odsavedvalues[0].manufacturename);
                // tslint:disable-next-line:triple-equals
                const manufacturename = this.Manufacturers.filter(x => x.Id == odsavedvalues[0].manufacturer_id);
                if (manufacturename !== undefined && manufacturename.length !== 0) {
                    this.orderLensHardLensForm.controls['manufactureod'].patchValue(manufacturename[0].Name);
                }
                // this.orderLensHardLensForm.controls['manufactureodid'].patchValue(value.Hardcontactlensod.manufacturer_id);
                this.orderLensHardLensForm.controls['nameod'].patchValue(odsavedvalues[0].item_name);
                this.orderLensHardLensForm.controls['colorod'].patchValue(odsavedvalues[0].color_id);
                this.orderLensHardLensForm.controls['upcod'].patchValue(odsavedvalues[0].upc_code);
                // this.orderLensHardLensForm.controls['sourceod'].patchValue(odsavedvalues[0].sourceod);
                this.orderLensHardLensForm.controls['qtyod'].patchValue(odsavedvalues[0].qty);
            }
            const OSsavesvalues = this.ordersavedvalues.filter(x => x.lens_vision === 'OS' && x.order_id === parseInt(orderidvalue, 10));
            if (OSsavesvalues !== '' && OSsavesvalues !== undefined) {
                this.orderService.hardContactLensOSvalue = OSsavesvalues[0].item_id;
                this.Hardcontactlensossavevalue = OSsavesvalues;
                this.Hardcontactlensossavevalue = OSsavesvalues[0];
                this.Hardcontactlensossavevalue.item_id = OSsavesvalues[0].item_id;
                this.Hardcontactlensossavevalue.item_name = OSsavesvalues[0].item_name;
                this.retail_PriceOS = OSsavesvalues[0].price_retail;
                this.orderLensHardLensForm.controls['OSitemid'].patchValue(OSsavesvalues[0].id);
                this.orderLensHardLensForm.controls['manufactureos'].patchValue(OSsavesvalues[0].manufacturename);
                // tslint:disable-next-line:triple-equals
                const manufactureosname = this.Manufacturers.filter(x => x.Id == OSsavesvalues[0].manufacturer_id);
                if (manufactureosname !== undefined && manufactureosname.length !== 0) {
                    this.orderLensHardLensForm.controls['manufactureos'].patchValue(manufactureosname[0].Name);
                }
                // this.orderLensHardLensForm.controls['manufactureos'].patchValue(value.Hardcontactlensod.manufacturename);
                this.orderLensHardLensForm.controls['nameos'].patchValue(OSsavesvalues[0].item_name);
                this.orderLensHardLensForm.controls['coloros'].patchValue(OSsavesvalues[0].color_code);
                this.orderLensHardLensForm.controls['upcos'].patchValue(OSsavesvalues[0].upc_code);
                // this.orderLensHardLensForm.controls['sourceos'].patchValue(OSsavesvalues[0].sourceod);
                this.orderLensHardLensForm.controls['qtyos'].patchValue(OSsavesvalues[0].qty);
            }
            this.orderLensHardLensForm.controls['order_id'].patchValue(orderidvalue);

            this.getpatientrxdetailsbyorderid(this.orderid);
            this.ordervalueupdate = true;

        },
            error => {

            });
    }

    getpatientrxdetailsbyorderid(orderid) {
        const filter = {
            'filter': [{
                'field': 'order_id',
                'operator': '=',
                'value': orderid
            }], 'sort': [
                {
                    'field': 'id',
                    'order': 'ASC'
                }]
        };

        this.orderSearchService.getPatientrxContactLens(filter, this.app.patientId).subscribe(

            valuepaientrx => {
                this.contactlensvalue = [];
                const data = valuepaientrx['data'];
                this.contactlensvalue = valuepaientrx['data'];
                const index = this.contactlensvalue.length - 1;
                this.orderLensHardLensForm.controls['id'].patchValue(data[index].id);
                this.orderLensHardLensForm.controls['patientrxid'].patchValue(data[index].id);
                this.orderLensHardLensForm.controls['base_od'].patchValue(data[index].base_od);
                this.orderLensHardLensForm.controls['base_os'].patchValue(data[index].base_os);
                this.orderLensHardLensForm.controls['diameter_od'].patchValue(data[index].diameter_od);
                this.orderLensHardLensForm.controls['diameter_os'].patchValue(data[index].diameter_os);
                this.orderLensHardLensForm.controls['sphere_od'].patchValue(data[index].sphere_od);
                this.orderLensHardLensForm.controls['sphere_os'].patchValue(data[index].sphere_os);
                this.orderLensHardLensForm.controls['axis_od'].patchValue(data[index].axis_od);
                this.orderLensHardLensForm.controls['axis_os'].patchValue(data[index].axis_os);
                this.orderLensHardLensForm.controls['add_od'].patchValue(data[index].add_od);
                this.orderLensHardLensForm.controls['add_os'].patchValue(data[index].add_os);
                this.orderLensHardLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensHardLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensHardLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensHardLensForm.controls['cylinder_os'].patchValue(data[index].cylinder_os);
                this.orderLensHardLensForm.controls['multifocalid_os'].patchValue(data[index].multifocalid_os);
                this.orderLensHardLensForm.controls['multifocalid_od'].patchValue(data[index].multifocalid_od);
                this.orderLensHardLensForm.controls['bc2_od'].patchValue(data[index].bc2_od);
                this.orderLensHardLensForm.controls['bc2_os'].patchValue(data[index].bc2_os);
                this.orderLensHardLensForm.controls['sphere2_os'].patchValue(data[index].sphere2_os);
                this.orderLensHardLensForm.controls['sphere2_od'].patchValue(data[index].sphere2_od);
                this.orderLensHardLensForm.controls['dia2_od'].patchValue(data[index].dia2_od);
                this.orderLensHardLensForm.controls['dia2_os'].patchValue(data[index].dia2_os);
                this.orderLensHardLensForm.controls['aspheric_os'].patchValue(data[index].aspheric_os);
                this.orderLensHardLensForm.controls['aspheric_od'].patchValue(data[index].aspheric_od);
                this.orderLensHardLensForm.controls['ct_od'].patchValue(data[index].ct_od);
                this.orderLensHardLensForm.controls['ct_os'].patchValue(data[index].ct_os);
                this.orderLensHardLensForm.controls['segdi_od'].patchValue(data[index].segdi_od);
                this.orderLensHardLensForm.controls['seght_od'].patchValue(data[index].seght_od);
                this.orderLensHardLensForm.controls['oz_od'].patchValue(data[index].oz_od);
                this.orderLensHardLensForm.controls['pcr2_od'].patchValue(data[index].pcr2_od);
                this.orderLensHardLensForm.controls['pcr3_od'].patchValue(data[index].pcr3_od);
                this.orderLensHardLensForm.controls['pcw3_od'].patchValue(data[index].pcw3_od);
                this.orderLensHardLensForm.controls['dot_od'].patchValue(data[index].dot_od);
                this.orderLensHardLensForm.controls['warranty_od'].patchValue(data[index].warranty_od);
                this.orderLensHardLensForm.controls['pcw2_od'].patchValue(data[index].pcw2_od);
                this.orderLensHardLensForm.controls['segdi_os'].patchValue(data[index].segdi_os);
                this.orderLensHardLensForm.controls['seght_os'].patchValue(data[index].seght_os);
                this.orderLensHardLensForm.controls['oz_os'].patchValue(data[index].oz_os);
                this.orderLensHardLensForm.controls['pcr2_os'].patchValue(data[index].pcr2_os);
                this.orderLensHardLensForm.controls['pcr3_os'].patchValue(data[index].pcr3_os);
                this.orderLensHardLensForm.controls['pcw3_os'].patchValue(data[index].pcw3_os);
                this.orderLensHardLensForm.controls['dot_os'].patchValue(data[index].dot_os);
                this.orderLensHardLensForm.controls['warranty_os'].patchValue(data[index].warranty_os);
                this.orderLensHardLensForm.controls['pcw2_os'].patchValue(data[index].pcw2_os);
                this.orderLensHardLensForm.controls.physician_id.patchValue(data[index].physician_id);
                this.orderService.physicianID = data[index].physician_id;
                if (data[index].outside_rx === 1) {
                    this.orderLensHardLensForm.controls['outside_rx'].patchValue(true);
                } else {
                    this.orderLensHardLensForm.controls['outside_rx'].patchValue(false);
                }


            },
            error => {

            });
    }

    equalvaluesofOS(value) {
        if (this.Hardcontactlensodsavevalue.item_id !== undefined && this.Hardcontactlensodsavevalue.item_id != null) {
            const data = {
                'IshardContactlens': '1',
                'Hardcontactlensod': this.Hardcontactlensodsavevalue
            };
            this.equalvalues();
            this.contactlensService.HardContactOSvalue.next(data);
        }
    }

    getparametersbyconfiqID(confiqID) {
        this.contactlensService.getcontactParametersByConfiqID(confiqID).subscribe(data => {
            this.parameters = data['data'];
            this.parametersrequired = [{}];
            this.parameters.forEach(element => {
                const a = this.parameters.findIndex(x => x.measurementname === element.measurementname);
                if (a !== -1) {
                    if (this.parameters[a].measurementname === element.measurementname) {
                        this.parameters[a].checkedvalues = true;
                        this.parameters[a].Availablebool = element.available === 1 ? true : false;
                        this.parameters[a].requiredbool = element.required === 1 ? true : false;
                        const elementname = element.measurementname.replace(/\s/g, '');
                        const elementrequired = elementname.trim() + 'required';
                        const elementavailable = elementname.trim() + 'available';

                        this.parametersrequired[0].elementavailable = this.parameters[a].Availablebool;
                        this.parametersrequired[0].elementrequired = this.parameters[a].requiredbool;
                    }
                }
            });
        });
    }

    LoadMasters() {
        this.RxSphereValues = this.orderSearchService.genDropDowns(
            { start: -20, end: 20, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'PL' });
        this.RxCylinderValues = this.orderSearchService.genDropDowns(
            { start: -10, end: 10, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'D.S.' });

        // Axis Degrees
        this.AxisValues = this.orderSearchService.genDropDowns(
            { start: 1, end: 180, step: 1, sign: false, roundOff: 0, slice: 3, neutral: '0' });

        // Add Values
        this.RxAddValues = this.orderSearchService.genDropDowns(
            { start: 0.25, end: 3.75, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: '0' });

    }

    // SuccessShow(message) {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: message });
    // }
    // WarningShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: message });
    // }
    selectevent(event) {
        if (event === 'close') {
            this.selectContactlensSidebar = false;
        }
    }


}
