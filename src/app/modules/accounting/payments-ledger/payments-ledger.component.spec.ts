import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsLedgerComponent } from './payments-ledger.component';

describe('PaymentsLedgerComponent', () => {
  let component: PaymentsLedgerComponent;
  let fixture: ComponentFixture<PaymentsLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
