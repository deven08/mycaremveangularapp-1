// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';





// Setting Up Component
@Component({
  selector: 'app-exam',
  templateUrl: './complete-exam.component.html'
})
export class CompleteExamComponent implements OnInit {

  ExamUrl: Object;

  constructor(private service: IframesService) {
     this.ExamUrl = this.service.routeUrl('ExamUrl', true);
  }

  ngOnInit() {}
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
