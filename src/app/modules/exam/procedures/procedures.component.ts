// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';




// Setting Up Component
@Component({
  selector: 'app-procedures',
  templateUrl: './procedures.component.html'
})
export class ProceduresComponent implements OnInit {

  PatientProceduresUrl: Object;

  constructor(private service: IframesService) {

     this.PatientProceduresUrl = this.service.routeUrl('PatientProceduresUrl', true);

  }

  ngOnInit() {}
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
