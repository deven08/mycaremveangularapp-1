import { ContactLensExamComponent } from './contact-lens-exam/contact-lens-exam.component';
import { CompleteExamComponent } from './complete-exam/complete-exam.component';
import { NgModule } from '@angular/core';

// components
import { PatientInstructionsComponent } from './patient-instructions/patient-instructions.component';
import { ProceduresComponent } from './procedures/procedures.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'complete-exam',
        component: CompleteExamComponent,
        data: { 'title': 'Complete Exam', 'banner': true }
      },
      {
        path: 'contact-lens-exam',
        component: ContactLensExamComponent,
        data: { 'title': 'Contact Lens Exam', 'banner': true }
      },
      {
        path: 'patient-instructions',
        component: PatientInstructionsComponent,
        data: { 'title': 'Patient Instructions', 'banner': true }
      },
      {
        path: 'procedures',
        component: ProceduresComponent,
        data: { 'title': ' Procedures', 'banner': true }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ExamRoutingModule { }
