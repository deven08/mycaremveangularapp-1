import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaintenanceRoutingModule } from './/maintenance-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import {
  OpenDateComponent,
  PrintPreviewComponent,
  MediaCountComponent,
  ClosingDetailsComponent,
  DailyClosingComponent
} from './';

@NgModule({
  imports: [
    CommonModule,
    // CommonLoadsModule,
    SharedModule,
    MaintenanceRoutingModule
  ],
  declarations: [
    DailyClosingComponent,
    ClosingDetailsComponent,
    OpenDateComponent,
    MediaCountComponent,
    PrintPreviewComponent,
  ],
  //  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class MaintenanceModule { }
