import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisionPrescriptionComponent } from './vision-rx/vision-prescription.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'vision-prescription',
        component: VisionPrescriptionComponent,
        data: { 'title': 'Vision Prescription - rx', 'banner' : true }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RxRoutingModule { }
