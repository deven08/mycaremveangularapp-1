import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '@services/app.service';
import { RxService } from '@app/core/services/rx/rx.service';
import { ContactLensRXData, ContactLensRX } from '@app/core/models/contactLensRx.model';
import { SpectacleLensRx, SpectacleLensExamRx, SpectacleLensCustomRx, VisionRxMapping } from '@app/core/models/spectcleLensRx.model';

@Component({
    selector: 'app-vision-prescription',
    templateUrl: 'vision-prescription.component.html'
})
export class VisionPrescriptionComponent implements OnInit {
    servicesdata: any[];
    selectedValues: any[];
    servicesAdvancedFilter;
    addSoftContactRxSidebar;
    addSpectacleLensRxSidebar;
    addHardContactRxSidebar = false;
    rowData: any;
    patientID: string;
    lensRxValues: any[];
    visiondata: any[];

    /**
     * Rx data of vision prescription component stores the rxdata
     */
    RxData: any = [];

    /**
     * Contact lens rx here we can can get data to bind the vision rx grid with contactlens data
     * both like "SoftcontactLens" and "HardContactLens"
     */
    contactLensRxData: Array<ContactLensRXData>;

    /**
     * Spectacle rx here we can can get data to bind the vision rx grid with SpectacleLens ExamRx data
     */
    spectacleRxExam: Array<SpectacleLensExamRx>;

    /**
     * Spectacle rx here we can can get data to bind the vision rx grid with SpectacleLens CustomRx data
     */
    spectacleRxCustom: Array<SpectacleLensCustomRx>;

    /**
     * Vision rx mappinggrid of vision prescription component is object for filling
     * the all types of data into , taking  some of the items to fill the grid
     */
    visionRxMappinggrid: VisionRxMapping;

    /**
     * Vision rx grid data  is for filling the vision rx grid by pushing multiple types of data
     * as an object like "customRx","ExamRx","SoftContactLens"and "HardContactlens"
     */
    visionRxGridData: Array<VisionRxMapping>;
    ngOnInit() {
        this.servicesdata = [
            { field: 'Type', header: 'Prescription Type Usage' },
            { field: 'Date', header: 'Physician Rx Date' },
            { field: 'Description', header: 'Description' },
            { field: 'Cylinder', header: 'Cylinder' },
            { field: 'Axis', header: 'Axis' },
            { field: 'Add', header: 'Add' },
            { field: 'Prism', header: 'Prism' },
            { field: 'Base', header: 'Base' },
            { field: 'Prism 2', header: 'Prism 2' },
            { field: 'Base 2', header: 'Base 2' },
            { field: 'Order', header: 'Order Now' }
        ];
    }
    constructor(
        private router: Router,
        private rxService: RxService,
        private app: AppService) {
        this.getRxvaluesBypatient();
    }

    /**
     * Gets rxvalues bypatient rx values of spectacle lens for particular patient you had selected.
     */
    getRxvaluesBypatient() {
        this.patientID = this.app.patientId;
        this.rxService.getSpectaclensRx(this.patientID).subscribe((resp: SpectacleLensRx) => {
            this.spectacleRxExam = resp.chartNote;
            this.spectacleRxCustom = resp.customRx;
            this.visionRxGridData = [];
            this.specExamRx(this.spectacleRxExam);
            this.specCustomRx(this.spectacleRxCustom);
            this.getContactLensRxData();
        });
    }
    onRowSelect(event) {
        console.log(event);
    }
    orderNowWithExistingRx(data) {
        this.app.RxValue = this.lensRxValues[data].id;
        this.router.navigateByUrl('/order/spectacle-lens');
    }

    /**
     * Handles data is handling the customrx and all rx
     * @param {boolean} event is checkbox event true or false
     */
    handleData(event) {
        if (event === true) {
            this.lensRxValues = [];
            for (let i = 0; i <= this.RxData.length - 1; i++) {
                if (this.RxData[i].custom_rx === 0) {
                    this.lensRxValues.push(this.RxData[i]);
                }
            }
        } else if (event === false) {
            this.lensRxValues = [];
            for (let i = 0; i <= this.RxData.length - 1; i++) {
                this.lensRxValues.push(this.RxData[i]);
            }
        }

    }

    /**
     * Gets contact lens rx data this is for the getting the contactlens Rx data of a particular patient.
     * and filling the vision rx grid with the type like "SoftcontactLens" and "HardContactLens"
     */
    getContactLensRxData() {
        this.patientID = this.app.patientId;
        this.rxService.getContactLensRx(this.patientID).subscribe((resp: ContactLensRX) => {
            this.contactLensRxData = resp.data;
            if (this.contactLensRxData.length > 0) {
                for (let j = 0; j <= this.contactLensRxData.length - 1; j++) {
                    const visionContactlens = {
                        sphere_od: this.contactLensRxData[j].sphere_od,
                        sphere_os: this.contactLensRxData[j].sphere_os,
                        cylinder_od: this.contactLensRxData[j].cylinder_od,
                        cylinder_os: this.contactLensRxData[j].cylinder_os,
                        axis_od: this.contactLensRxData[j].axis_od,
                        axis_os: this.contactLensRxData[j].axis_os,
                        prism1_od: this.contactLensRxData[j].pcr2_od,
                        prism1_os: this.contactLensRxData[j].pcr2_os,
                        prism2_od: this.contactLensRxData[j].pcr3_od,
                        prism2_os: this.contactLensRxData[j].pcr3_os,
                        add_od: this.contactLensRxData[j].add_od,
                        add_os: this.contactLensRxData[j].add_os,
                        orderType: this.contactLensRxData[j].hard_contact === 0 ? 'SoftContactLens' : 'HardContactLens',
                        rxtype: this.contactLensRxData[j].hard_contact === 0 ? 'SoftContactLens' : 'HardContactLens',
                        physician: this.contactLensRxData[j].physician_name,
                        rxdate: this.contactLensRxData[j].rx_dos,
                        balance_od: '',
                        balance_os: '',
                        id: this.contactLensRxData[j].id,
                        di_od: '',
                        base1_od: this.contactLensRxData[j].base_od,
                        base1_os: this.contactLensRxData[j].base_os,
                        base2_os: '',
                        base2_od: '',
                        description: '',
                        usage: '',
                        multifocal: '',
                        notes: '',
                        di_os: '',
                    };

                    this.visionRxGridData.push(visionContactlens);
                }

            }
        });
    }

    /**
     * Specs exam rx is the data for filling the exam vision rx in the  vision rx grid, the data is based on the patient selected.
     * @param {Array} examRx 
     */
    specExamRx(examRx: Array<SpectacleLensExamRx>) {
        if (examRx.length > 0) {
            for (let i = 0; i <= examRx.length - 1; i++) {
                for (let j = 0; j <= examRx[i].get_vision.length - 1; j++) {
                    const visionExamRx = {
                        sphere_od: examRx[i].get_vision[j].vis_mr_od_s,
                        sphere_os: examRx[i].get_vision[j].vis_mr_os_s,
                        cylinder_od: examRx[i].get_vision[j].vis_mr_od_c,
                        cylinder_os: examRx[i].get_vision[j].vis_mr_os_c,
                        axis_od: examRx[i].get_vision[j].vis_mr_od_a,
                        axis_os: examRx[i].get_vision[j].vis_mr_os_a,
                        prism1_od: examRx[i].get_vision[j].vis_mr_od_p,
                        prism1_os: examRx[i].get_vision[j].vis_mr_os_p,
                        prism2_od: examRx[i].get_vision[j].vis_mr_od_slash,
                        prism2_os: examRx[i].get_vision[j].vis_mr_os_slash,
                        add_od: examRx[i].get_vision[j].vis_mr_od_add,
                        add_os: examRx[i].get_vision[j].vis_mr_os_add,
                        orderType: 'SpectacleLens',
                        rxtype: 'SpectacleLens',
                        physician: examRx[i].primary_care_physician,
                        rxdate: examRx[i].date_of_service,
                        balance_od: '',
                        balance_os: '',
                        id: examRx[i].get_vision[j].vis_id,
                        di_od: '',
                        base1_od: examRx[i].get_vision[j].vis_mr_od_sel_1,
                        base1_os: examRx[i].get_vision[j].vis_mr_os_sel_1,
                        base2_os: examRx[i].get_vision[j].vis_mr_od_prism,
                        base2_od: examRx[i].get_vision[j].vis_mr_os_prism,
                        description: '',
                        usage: '',
                        multifocal: '',
                        notes: '',
                        di_os: ''
                    };

                    this.visionRxGridData.push(visionExamRx);
                }

            }
        }
    }

    /**
     * Specs custom rx is data for filling the vision rx grid with the custom rx, this data is coming based on the patient selected.
     * @param {Array} customRx
     */
    specCustomRx(customRx: Array<SpectacleLensCustomRx>) {
        if (customRx.length > 0) {
            for (let j = 0; j <= customRx.length - 1; j++) {
                const visioncustomRx = {
                    sphere_od: customRx[j].sphere_od,
                    sphere_os: customRx[j].sphere_os,
                    cylinder_od: customRx[j].cyl_od,
                    cylinder_os: customRx[j].cyl_os,
                    axis_od: customRx[j].axis_od,
                    axis_os: customRx[j].axis_os,
                    prism1_od: customRx[j].prism_od,
                    prism1_os: customRx[j].prism_os,
                    prism2_od: customRx[j].mr_od_p,
                    prism2_os: customRx[j].mr_os_p,
                    add_od: customRx[j].add_od,
                    add_os: customRx[j].add_os,
                    orderType: 'SpectacleLens',
                    rxtype: 'SpectacleLens',
                    physician: customRx[j].physician_name,
                    rxdate: customRx[j].rx_dos,
                    balance_od: customRx[j].balance_od,
                    balance_os: customRx[j].balance_os,
                    id: customRx[j].id,
                    di_od: customRx[j].dist_pd_od,
                    base1_od: customRx[j].base_od,
                    base1_os: customRx[j].base_os,
                    base2_os: '',
                    base2_od: '',
                    description: '',
                    usage: customRx[j].rx_usage,
                    multifocal: '',
                    notes: customRx[j].notes,
                    di_os: customRx[j].dist_pd_os,

                };

                this.visionRxGridData.push(visioncustomRx);
            }

        }
    }
}
