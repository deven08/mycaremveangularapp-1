import {
  AddHardContactRxComponent,
  AddSoftContactRxComponent,
  AddSpectacleLensRxComponent,
  VisionPrescriptionComponent
} from './';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { RxRoutingModule } from './rx-routing.module';
// Rx
// import { VisionPrescriptionComponent } from './vision-rx/vision-prescription.component';
import { CommonModule } from '@angular/common';
import { RxService } from '@app/core/services/rx/rx.service';



@NgModule({
  imports: [
    CommonModule,
    RxRoutingModule,
    SharedModule
  ],
  declarations: [
    VisionPrescriptionComponent,
    AddSpectacleLensRxComponent,
    AddSoftContactRxComponent,
    AddHardContactRxComponent
  ],
  providers: [RxService],
})
export class RxModule { }
