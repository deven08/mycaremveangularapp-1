// Core Modules 
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';


// CPT Table
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class CPTComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframeService: IframesService) { 
    this.BillingUrl = this.iframeService.routeUrl('CPTUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframeService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// FeeTable
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class FeeTableComponent implements OnInit {

  BillingUrl: any = '';


  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) {
    this.BillingUrl = this.iframesService.routeUrl('FeeTableUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// DX Codes
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class DXCodesComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) {
    this.BillingUrl = this.iframesService.routeUrl('DXCodesUrl', true);
  }

  // Get Parameters on Intializ
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// ICD1- Url
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class ICD10Component implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) {
    this.BillingUrl = this.iframesService.routeUrl('ICD10Url', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Writeoffcodes
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class WriteOffCodesComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) { 
    this.BillingUrl = this.iframesService.routeUrl('WriteOffCodesUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Modifiers
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class ModifiersComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) {
    this.BillingUrl = this.iframesService.routeUrl('ModifiersUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class ManagePOSComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) {
    this.BillingUrl = this.iframesService.routeUrl('ManagePOSUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

/**
 * Policies Iframe screen from IMW
*/
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class PoliciesComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) { 
    this.BillingUrl = this.iframesService.routeUrl('PoliciesUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

/**
 * Electronic Iframe screen from IMW
*/
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class ElectronicComponent implements OnInit {

  BillingUrl: any = '';

  /**
   * Creates an instance of modifiers component.
   * @param router Angular Router Library for handling routes.
   * @param iframesService for calling iframes methods
   */
  constructor(private iframesService: IframesService) { 
    this.BillingUrl = this.iframesService.routeUrl('ElectronicUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {

  }

  // Loading
  onLoad() {
    this.iframesService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
