import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



// Components
import {
  CalendarSetupComponent,
  CommissionStructuresComponent,
  DocumentsComponent,
  InsuranceSetupComponent,
  MarketingSetupComponent,
  SupplierComponent,
  CompanyComponent,
  TaxComponent
} from './';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'supplier',
        component: SupplierComponent
      },
      {
        path: 'calendar',
        component: CalendarSetupComponent
      },
      {
        path: 'documents',
        component: DocumentsComponent
      },
      {
        path: 'commission-stuctures',
        component: CommissionStructuresComponent
      },
      {
        path: 'insurance',
        component: InsuranceSetupComponent
      },
      {
        path: 'marketing',
        component: MarketingSetupComponent
      },
      {
        path: 'company',
        component: CompanyComponent
      },
      {
        path: 'tax',
        component: TaxComponent
      }

    ]
  }
];

@NgModule({

  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsRoutingModule { }
