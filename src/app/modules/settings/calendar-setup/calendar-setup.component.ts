// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-calendar-setup',
  templateUrl: './calendar-setup.component.html'
})
export class CalendarSetupComponent implements OnInit {
  CommonUrl: Object;


  constructor(private service: IframesService) {
      this.CommonUrl = this.service.routeUrl('AvailableUrl', true);
  }

  changeFrameUrl(frameKey) {
     this.CommonUrl = this.service.routeUrl(frameKey, true);
  }

  ngOnInit() {

  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
