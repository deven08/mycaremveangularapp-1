import { Component} from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

@Component({
  selector: 'app-insurance-groups',
  templateUrl: './insurance-groups.component.html'
})
export class InsuranceGroupsComponent {

  InsuranceGroupUrl: Object;
  constructor(private service: IframesService) {
      this.InsuranceGroupUrl = this.service.routeUrl('InsuranceGroupUrl', true);
  }

  onLoad() {
    this.service.resizeIframe(true);
    $('.loader-iframe').fadeOut();
  }

}
