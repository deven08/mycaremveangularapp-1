// Core Modules
import { Component} from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-insurance-company',
  templateUrl: './insurance-company.component.html'
})
export class InsuranceCompanyComponent {

  InsuranceSetupUrl: Object;
  constructor(private service: IframesService) {
    this.InsuranceSetupUrl = this.service.routeUrl('InsuranceSetupUrl', true);
  }

  onLoad(e) {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
