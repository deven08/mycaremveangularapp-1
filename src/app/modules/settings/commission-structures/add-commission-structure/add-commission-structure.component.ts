import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AddUpdateCommissionStructure, Emitter
} from '@app/core/models/settings/commission-structure.model';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-commission-structure',
  templateUrl: './add-commission-structure.component.html',
  styleUrls: ['./add-commission-structure.component.css']
})
export class AddCommissionStructureComponent implements OnInit {
  /**
   * Commission form of commission structures component
   *  fromgroup details
   */
  commissionForm: FormGroup;
  /**
   * Saving pay load of commission structures component
   * stored from controller details
   */
  savingPayLoad: AddUpdateCommissionStructure;

  /**
   * Input  of add commission structure component
   * getting commission structure status
   */
  @Input() statusSaveUpdate;

  /**
   * Output  of add commission structure component
   * Send commission structure to parent screen
   */
  @Output() saveUpdateEmit: EventEmitter<Emitter> = new EventEmitter();
  /**
   * Creates an instance of add commission structure component.
   * @param commissionStrutureService provide commission struture service data
   * @param fb provide FormBuilder
   * @param utility provide utility service data
   * @param error UtilityService error service
   */
  constructor(
    private fb: FormBuilder) {


  }
  ngOnInit() {
    if (this.statusSaveUpdate.status === 'New') {
      this.loadFormData();
    }
    if (this.statusSaveUpdate.status === 'Update') {
      console.log(this.statusSaveUpdate.value);
      this.loadFormData();
      this.gettingCommissionData();
    }
  }

  /**
   * Loadformdatas commission structures component
   * @returns formControllers names
   */
  loadFormData() {
    this.commissionForm = this.fb.group({
      structure_name: ['', [Validators.required]],
      commissiontype_id: ['', [Validators.required]],
      gross_percentage: '',
      margin_percentage_before: '',
      margin_percentage_after: '',
      margin_percentage_amount: '',
      amount: '',
      spiff: ['', [Validators.required]],
    });
  }
  /**
   * Save commission details
   * Save commission structure deatails using commission structure api
   */
  saveCommissionDetails() {
    this.savingReqPayLoad();
    const data: number = (this.statusSaveUpdate.status === 'New') ? null :  this.statusSaveUpdate.value.id;
    this.saveUpdateEmit.emit({ id: data, cancel: null, value: this.savingPayLoad});
  }
  /**
   * Saving req pay load
   * Getting formControllers values
   */
  savingReqPayLoad() {
    this.savingPayLoad = {
      structure_name: this.commissionForm.controls.structure_name.value,
      commissiontype_id: this.commissionForm.controls.commissiontype_id.value,
      gross_percentage: this.commissionForm.controls.gross_percentage.value,
      margin_percentage_before: this.commissionForm.controls.margin_percentage_before.value,
      margin_percentage_after: this.commissionForm.controls.margin_percentage_after.value,
      margin_percentage_amount: this.commissionForm.controls.margin_percentage_amount.value,
      amount: this.commissionForm.controls.amount.value,
      spiff: this.commissionForm.controls.spiff.value
    };
  }
  /** *getting Commission Data
   *  Getting selected item details from api
   */
  gettingCommissionData() {
    const selectedCommissionId: number = this.statusSaveUpdate.value;
    if (selectedCommissionId) {
          this.commissionForm = this.fb.group({
            structure_name: this.statusSaveUpdate.value.structure_name,
            commissiontype_id: this.statusSaveUpdate.value.commissiontype_id,
            gross_percentage: this.statusSaveUpdate.value.gross_percentage,
            margin_percentage_before: this.statusSaveUpdate.value.margin_percentage_before,
            margin_percentage_after: this.statusSaveUpdate.value.margin_percentage_after,
            margin_percentage_amount: this.statusSaveUpdate.value.margin_percentage_amount,
            amount: this.statusSaveUpdate.value.amount,
            spiff: this.statusSaveUpdate.value.spiff,
          });
        }
    }
  /**
   * Cancel data
   * Close the advanced sideBar window
   */
  cancelData() {
    this.saveUpdateEmit.emit({ id: null, value: null, cancel: 'cancel'});
  }

}
