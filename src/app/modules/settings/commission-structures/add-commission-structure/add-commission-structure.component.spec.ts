import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommissionStructureComponent } from './add-commission-structure.component';

describe('AddCommissionStructureComponent', () => {
  let component: AddCommissionStructureComponent;
  let fixture: ComponentFixture<AddCommissionStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCommissionStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommissionStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
