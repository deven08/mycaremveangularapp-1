
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { SupplierService } from '@app/core/services/inventory/supplier.service';
import { ErrorService } from '@app/core/services/error.service';
import { UtilityService } from '@app/shared/services/utility.service';
// import { SupplierService } from '../../../ConnectorEngine/services/supplier.service';

@Component({
    selector: 'app-add-supplier-lab',
    templateUrl: './add-supplier-labs.component.html'
})
export class AddSupplierLabsComponent implements OnInit {

    details: any[];
    orderTypes: any[];
    shippings: any[];
    contacts: any[];
    SupplierForm: FormGroup;
    batchtransmissionlist: any;
    Accesstoken = '';
    postsuppliers: any;
    bathtransmissions: any[];
    locationlist: any;
    locations: any[];
    transmissionselecteddata: any;
    ftpresponse: any;
    ftpbuttonName: string;
    ftptype: string;

    @Output() addSuplierEvent = new EventEmitter<any>();
    constructor(private _fb: FormBuilder,
        private supplierService:SupplierService,
        private utilityService:UtilityService,
        private error: ErrorService) {}

    /**
     * Loadformdata creates a formgroup to assign data to the form and also includes logic to
     *  display data for the selected item from the list
     */
    Loadformdata() {
        this.SupplierForm = this._fb.group({
            id: '',
            vendor_name: ['', [Validators.required]],
            vendor_address: '',
            tel_num: '',
            mobile: '',
            fax: '',
            email: '',
            city: '',
            zip: '',
            zip_ext: '',
            state: '',
            contact_name: '',
            vsp_code: '',
            locationid: '',
            bill_account: '',
            ship_account: '',
            vw_login: '',
            vw_password: '',
            isWarehouse: '',
            facid: '',
            contacttitle: '',
            notes: '',
            frames: false,
            lenses: false,
            contacts: false,
            other: false,
            lab: false,
            address2: '',
            website: '',
            labid: '',
            jobtransmissionmethod: null,
            ftptype: '',
            ftp_userid: '',
            ftp_pwd: '',
            ftp_server: '',
            ftp_port: '',
            initial_dir: '',
            group_locations: [],
            shipping: [],
        });

        this.SupplierForm.controls.ftptype.patchValue('FTP');
        this.ftpbuttonName = 'Test FTP Connection';
        const selectedsupplierid = this.supplierService.editsupplierid;

        if (selectedsupplierid) {

            this.supplierService.GetSuppliersDetailsdatabyID(selectedsupplierid).subscribe(
                data => {
                    this.SupplierForm = this._fb.group({

                        id: data['id'],
                        vendor_name: data['vendor_name'],
                        vendor_address: data['vendor_address'],
                        tel_num: data['tel_num'],
                        mobile: data['mobile'],
                        fax: data['fax'],
                        email: data['email'],
                        city: data['city'],
                        zip: data['zip'],
                        zip_ext: data['zip_ext'],
                        state: data['state'],
                        contact_name: data['contact_name'],
                        vsp_code: data['vsp_code'],
                        locationid: data['locationid'],
                        bill_account: data['bill_account'],
                        ship_account: data['ship_account'],
                        vw_login: data['vw_login'],
                        vw_password: data['vw_password'],
                        isWarehouse: data['isWarehouse'],
                        facid: data['facid'],
                        contacttitle: data['contacttitle'],
                        notes: data['notes'],
                        frames: data['frames'] === null ? false : data['frames'],
                        lenses: data['lenses'] === null ? false : data['lenses'],
                        contacts: data['contacts'] === null ? false : data['contacts'],
                        other: data['other'] === null ? false : data['other'],
                        lab: data['lab'] === null ? false : data['lab'],
                        address2: data['address2'],
                        website: data['website'],
                        labid: data['labid'],
                        jobtransmissionmethod: data['jobtransmissionmethod'],
                        ftptype: data['ftptype'],
                        ftp_userid: data['ftp_userid'],
                        ftp_pwd: data['ftp_pwd'],
                        ftp_server: data['ftp_server'],
                        ftp_port: data['ftp_port'],
                        initial_dir: data['initial_dir'],
                        group_locations: [],
                        shipping: [],
                    });
                    this.Bindftptype(data['ftptype']);
                }
            );
        }
    }


    /**
     * Loadtransmissionstatus binds transmissionstatus data for the suppliers
     */
    Loadtransmissionstatus() {
        this.bathtransmissions = [];
        this.supplierService.getsupplierstransmissiondata().subscribe(
            data => {
                this.batchtransmissionlist = data;
                for (let i = 0; i <= this.batchtransmissionlist.length - 1; i++) {
                    if (!this.bathtransmissions.find(a => a.Name === this.batchtransmissionlist[i]['description'])) {
                        this.bathtransmissions.push(
                            { Id: this.batchtransmissionlist[i]['id'], Name: this.batchtransmissionlist[i]['description'] });
                    }
                }
            });
    }

    /**
     * Bindftptypes is used to bind the Ftytype with below parameter
     * @param ftyptype
     */
    Bindftptype(ftyptype) {

        if (ftyptype === 'FTP') {
            this.SupplierForm.controls.ftptype.patchValue('FTP');
            this.ftpbuttonName = 'Test FTP Connection';
        } else if (ftyptype === 'SFTP') {
            this.SupplierForm.controls.ftptype.patchValue('SFTP');
            this.ftpbuttonName = 'Test SFTP Connection';
        } else {
            this.SupplierForm.controls.ftptype.patchValue('FTP');
            this.ftpbuttonName = 'Test FTP Connection';
        }

    }


    /**
     * Selectchanges handels an event once trasmissionstatus is modifed and send data selected to below parameter
     * @param args
     */
    selectchange(args) {
        this.transmissionselecteddata = args.target.options[args.target.selectedIndex].text;
        if (this.transmissionselecteddata === 'Select Transmission') {
            this.SupplierForm.controls['lab'].patchValue(false);
        } else {
            this.SupplierForm.controls['lab'].patchValue(true);
        }
    }


    /**
     * onclick() triggers on the Selection of FTP,SFTP option and alters the caption of testing connectivity button
     */
    onClick() {
        if (this.SupplierForm.controls.ftptype.value === 'FTP') {
            this.ftpbuttonName = 'Test SFTP Connection';
        } else if (this.SupplierForm.controls.ftptype.value === 'SFTP') {
            this.ftpbuttonName = 'Test FTP Connection';
        }
    }


    /**
     * Verifyingexistingtranmissionstatus method is used to load the existing transimitionstatus data for the supplier on loading
     */
    verifyingexistingtranmissionstatus() {
        // if (!this.SupplierForm.controls.jobtransmissionmethod.value) {
        //     if (!this.batchtransmissionlist) {
                for (let i = 0; i <= this.batchtransmissionlist.length - 1; i++) {
                    if (parseInt(this.batchtransmissionlist[i]['id'], 10) ===
                        parseInt(this.SupplierForm.controls.jobtransmissionmethod.value, 10)) {
                        this.transmissionselecteddata = this.batchtransmissionlist[i]['description'];
                    }
                }
        //     }
        // }
    }


    /**
     * on init loads formdata,masters realted to page
     */
    ngOnInit() {
        this.Loadtransmissionstatus();
        this.transmissionselecteddata = 'Select Transmission';
        this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
        this.Loadformdata();
    }

    /**
     * the below data has yet to be build and will be revisited once API'S are ready
     */
    screendatapendingforapi() {
        this.details = [
            { location: 'MVE: MyVision' },
            { location: 'MVE: MyVision' },
            { location: 'MVE: MyVision' },
            { location: 'MVE: MyVision' },
            { location: 'MVE: MyVision' }
        ];
        this.orderTypes = [
            { type: 'Frame Only' },
            { type: 'Frame Parts' },
            { type: 'Rx Pads' },
            { type: 'Hard Contact Lenses' },
            { type: 'Soft Contact Lenses' },
            { type: 'Other' },
            { type: 'Spectacle Lens' },
            { type: 'Service' }
        ];
        this.shippings = [
            { mve: 'UPS' },
            { mve: '3 Days ground' },
            { mve: 'Air' },
            { mve: 'FedEx' },
            { mve: 'Friend Pick-Up' },
            { mve: 'Patient Pick-Up' },
            { mve: 'Relative Pick-Up' }
        ];
        this.contacts = [
            { action: 'Action', ctname: 'Krishna Lens' },
            { action: 'Action', ctname: 'Krishna Lens' },
            { action: 'Action', ctname: 'Krishna Lens' }
        ];
    }


    /**
     * Cancles suppliers
     */
    cancleSuppliers() {
        this.addSuplierEvent.emit('cancle');
        // this.supplierService.cancleSupplierObj.next('');
    }


    /**
     * Saves supplier saves the entire page data and verifies suppliername with manditatory
     * logic and also includes transmission status validations
     */
    SaveSupplier() {
        this.supplierService.errorHandle.msgs = [];
        if (!this.SupplierForm.valid) {
            return;
        }

        this.verifyingexistingtranmissionstatus();

        if (Boolean(this.SupplierForm.value.lab) === true) {
            if (this.transmissionselecteddata === 'Select Transmission' || (!this.transmissionselecteddata)) {
                this.error.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please select Transmission status'
                });
                return;
            }
            if (this.transmissionselecteddata === 'VSP Lab') {
                if (!this.SupplierForm.value.vsp_code) {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'Please enter VSP Lab Code'
                    });
                    return;

                }

            }
            if (this.transmissionselecteddata === 'VisionWeb Lab') {
                if (!this.SupplierForm.value.bill_account || !this.SupplierForm.value.ship_account ||
                    !this.SupplierForm.value.vw_login || !this.SupplierForm.value.vw_password) {

                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message',
                        detail: '[BillAccount,ShipAccount,Login,Password] are Mandatory for VisionWeb Transmission status'
                    });
                    return;

                }

            }

            if (this.transmissionselecteddata === 'Labzilla Lab') {
                if (!this.SupplierForm.value.labid) {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'Please enter Lab ID'
                    });
                    return;
                }
            }

            if (this.transmissionselecteddata === 'Optivision Lab' || this.transmissionselecteddata === 'Innovations Lab') {
                if (!this.SupplierForm.value.email) {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'Please enter Email ID'
                    });
                    return;
                }
            }

            if (this.transmissionselecteddata === 'DVI Lab') {
                if (!this.SupplierForm.value.vw_login || !this.SupplierForm.value.vw_password) {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: '[Login,Password] are Mandatory for DVI Lab Transmission status'
                    });
                    return;
                }

            }

        } else {
            if (this.transmissionselecteddata === 'Select Transmission' || (!this.transmissionselecteddata)) {
                this.SupplierForm.controls.jobtransmissionmethod.patchValue('') ;
            }
        }

        this.supplierService.SaveSupplierdata(this.utilityService.format(this.SupplierForm), this.supplierService.editsupplierid)
            .subscribe(data => {
                this.postsuppliers = data;
                this.error.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Supplier Saved Successfully'
                });
                // setTimeout(() => this.supplierService.cancleSupplierObj.next(''), 1000);
                this.addSuplierEvent.emit('');
                // this._router.navigate(['supplier']);
            },
                error => {
                    this.error.displayError({
                        severity: 'error',
                        summary: 'error Message', detail: 'Save Failed'
                    });
                }

            );
    }


    /**
     * Testftpconnections includes a logic to test the FTP connectivity for the supplier.The [user,password,server]
     * are manditaory for the item to test the ftp connectivity
     */
    Testftpconnection() {

        this.ftptype = this.SupplierForm.value.ftptype;
        this.supplierService.errorHandle.msgs = [];
        if (this.supplierService.editsupplierid === null) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'The Supplier should be saved for saving the FTP connection'
            });
            return;
        } else {
            if (!this.SupplierForm.value.ftp_server || !this.SupplierForm.value.ftp_userid ||
                !this.SupplierForm.value.ftp_pwd) {
                this.error.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: '[ftpuserID,ftppassword,ftpserver] is required for testing FTP connectivity'
                });
                return;
            }
            this.supplierService.GetsupplierFTpconnection(this.supplierService.editsupplierid)
                .subscribe(
                    Response =>
                        this.error.displayError({
                            severity: 'success',
                            summary: 'success Message', detail: this.ftptype + ' Connection is successful'
                        }),
                    err =>

                        this.error.displayError({
                            severity: 'warn',
                            summary: 'Warning Message', detail: this.ftptype + ' Connection is unsuccessful'
                        })
                );
        }
    }
}
