import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { SupplierService } from '@app/core/services/inventory/supplier.service';
import { ErrorService } from '@app/core/services/error.service';


@Component({
    selector: 'app-supplier-component',
    templateUrl: './supplier.component.html'
})

export class SupplierComponent implements OnInit, OnDestroy {
    searchstate = '';
    searchcity = '';
    searchzip = '';
    searchmobile = '';
    searchaddress = '';
    searchemail = '';
    searchphone = '';
    searchfax = '';
    searchName = '';
    searchValue = '';
    samplevisible = true;
    visible = true;
    display = false;
    sortallinventory = false;
    supplierAdvancedFilterSidebar = false;
    supplierscolumns: any[];
    addSupplierLabs = false;
    sales: any[];
    value: true;
    suppliergridobj: any[];
    @ViewChild('dt') public dt: any;

    subscriptionSupliersCancleObj: Subscription;
    // constructor(private supplierService: any,
    //     private _InventoryCommonService: any) {
    // }
    constructor(private supplierService:SupplierService,
        private error: ErrorService){}

    ngOnInit(): void {
        this.value = true;
        this.GetSupplierList();
        this.GetSupplierDetailsdata();
    }

    GetSupplierList(): void {
        this.supplierscolumns = [
            { field: 'id', header: 'Id', checkbox: false, visible: this.visible },
            { field: 'vendor_name', header: 'Supplier', checkbox: false, visible: this.visible },
            { field: 'fax', header: 'Fax', checkbox: false, visible: this.visible },
            { field: 'tel_num', header: 'Phone', checkbox: false, visible: this.visible },
            { field: 'email', header: 'Email', checkbox: false, visible: this.visible },
            { field: 'vendor_address', header: 'Address', checkbox: false, visible: this.visible },
            { field: 'mobile', header: 'Mobile', checkbox: false, visible: this.visible },
            { field: 'zip', header: 'ZipCode', checkbox: false, visible: this.visible },
            { field: 'city', header: 'City', checkbox: false, visible: this.visible },
            { field: 'state', header: 'State', checkbox: false, visible: this.visible },
        ];

    }
    DataViewdialogBox() {
        this.display = true;
    }
    checkBoxClick(item, e) {

        if (item === 'selectAll') {
            this.visible = e.checked;
            this.GetSupplierList();
            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    applyDataViewChanges(item, selected) {
        this.supplierscolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;
            }
        });
        const colSpanVar = this.supplierscolumns.filter(x => x.visible).length;
        if (colSpanVar < this.supplierscolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }



    }





    advanceFilter(data) {
        // this.clearsearchvalues();
        // let name= data["Name"] === null ? data["Name"] = "" : data["Name"]
        const id = data['Id'] === null ? data['Id'] = '' : data['Id'];
        this.GetSupplierDetails('', '', '', id);
        // this.inventoryLenstreatmentAdvancedFilter = false;
    }




    // TO load the grid for the Suppliers table

    GetSupplierDetails(includeInactive = '', maxRecord = '', offset = '', id = '') {
        this.supplierService.GetSuppliersDetails(includeInactive, maxRecord, offset, id).subscribe(
            data => {
                this.sales = data['result']['Optical']['Vendors'];

                this.suppliergridobj = data['result']['Optical']['Vendors'];
                if (this.suppliergridobj !== null && this.suppliergridobj !== undefined) {
                    this.suppliergridobj.forEach((childObj, i) => {
                        this.suppliergridobj[i].ContactName = childObj.ContactFirstName + childObj.ContactLastName;
                        if (this.suppliergridobj[i].ContactName === null || this.suppliergridobj[i].ContactName === '') {
                            this.suppliergridobj[i].contact = false;

                        } else {
                            this.suppliergridobj[i].contact = false;

                        }
                    });
                }
            });
    }


    GetSupplierDetailsdata() {
        this.supplierService.GetSuppliersDetailsdata().subscribe(
            data => {
                this.suppliergridobj = data['data'];
            });
    }



    onRowSelect(event) {
        // localStorage.removeItem('editsupplierid');
        // localStorage.setItem('editsupplierid', event.data.id);
        // this.supplierService.editsupplierid = event.data.id;
        this.supplierService.editsupplierid = event.data.id;
    }
    onRowUnselect(event){
        this.supplierService.editsupplierid = '';
    }


    updateSuppliers() {
        this.dt.selection = true;
        const selectedSupplierid = this.supplierService.editsupplierid;
        if (!selectedSupplierid) {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Supplier.' });
            this.dt.selection = false;
        } else {
            this.addSupplierLabs = true;

        }
    }


    DoubleClickSuppliers(rowData) {
        this.supplierService.FortrasactionItemID = rowData.id;
        if (!this.supplierService.FortrasactionItemID) {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Supplier.' });
            this.dt.selection = false;
        } else {
            this.supplierService.editsupplierid = rowData.id;
            this.addSupplierLabs = true;

        }
    }

    addSupplierLabsclick() {
        this.dt.selection = true;
        this.dt.selection = false;
        this.supplierService.FortrasactionItemID = '';
        this.supplierService.editsupplierid = '';
        this.addSupplierLabs = true;
    }
    ngOnDestroy(): void {
        // this.subscriptionSupliersCancleObj.unsubscribe();
    }
    reset(dt) {
        // this.dt.reset();
        dt.reset();
        this.dt.selection = false;
        this.searchValue = '';
        this.searchName = '';
        this.searchfax = '';
        this.searchphone = '';
        this.searchemail = '';
        this.searchaddress = '';
        this.searchmobile = '';
        this.searchzip = '';
        this.searchcity = '';
        this.searchstate = '';
        // localStorage.removeItem('editsupplierid');
        this.GetSupplierDetailsdata();
    }

    supplierAdvancedFilterClick() {
        this.supplierAdvancedFilterSidebar = true;
    }
    supplierAdvSearchEvent(event) {
        // this.advanceFilter(event);
    }
    addSuplierEvent(event) {
        if (event === 'cancle') {
            this.addSupplierLabs = false;
        } else {
            this.addSupplierLabs = false;
            this.GetSupplierDetailsdata();
        }

    }
}
