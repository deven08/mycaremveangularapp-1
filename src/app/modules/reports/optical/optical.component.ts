// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IframesService } from '@app/shared/services/iframes.service';

// App Configuration Module

// Setting Up Component
@Component({
  selector: 'app-optical',
  templateUrl: './optical.component.html',
  styleUrls: ['./optical.component.css']
})
export class OpticalComponent implements OnInit, OnDestroy {

  OpticalUrl: any = '';
  ShowPage: String = '';
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        console.log(params);
        this.ShowPage = params['showpage'];
        this.OpticalUrl = this.service.routeUrl('OpticalUrl', true, '?showpage=' + this.ShowPage);
      });
   
  }

  // Unsubcribe Service
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
