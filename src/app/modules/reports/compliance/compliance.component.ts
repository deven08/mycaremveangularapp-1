// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-compliance',
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.css']
})
export class ComplianceComponent implements OnInit, OnDestroy {

  ComplianceUrl: any = '';
  SchedulerId: number;
  params: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.SchedulerId = +params['sch_temp_id'] || null;
        this.ComplianceUrl = this.service.routeUrl('ComplianceUrl', true, '?sch_temp_id=' + this.SchedulerId);
      });
    
  }

  // Unsubcribe Service
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}



// QRDA
@Component({
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.css']
})
export class ComplianceQRDAComponent implements OnInit {
  ComplianceUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.ComplianceUrl = this.service.routeUrl('ComplianceQRDAUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// CQM
@Component({
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.css']
})
export class ComplianceCQMComponent implements OnInit {
  ComplianceUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.ComplianceUrl = this.service.routeUrl('ComplianceCQMUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
