// Core Modules
import { Component, OnInit } from '@angular/core';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.css']
})

// Dat Appointment
export class RemindersDayApptComponent implements OnInit {

  ReminderUrl: any = '';

  constructor(private service: IframesService) {
    this.ReminderUrl = this.service.routeUrl('ReminderDayApptUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

@Component({
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.css']
})
// Recalls
export class RemindersRecallsComponent implements OnInit {

  ReminderUrl: any = '';

  constructor(private service: IframesService) {
    this.ReminderUrl = this.service.routeUrl('ReminderRecallsUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


@Component({
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.css']
})
// Reminder List
export class RemindersListComponent implements OnInit {

  ReminderUrl: any = '';

  constructor(private service: IframesService) {
    this.ReminderUrl = this.service.routeUrl('ReminderListUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
