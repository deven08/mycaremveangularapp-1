import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {
    SchedulerComponent,
    SchedulerPatientMonitorComponent,
    SchedulerFaceSheetComponent,
    SchedulerApptInfoComponent,
    SchedulerPtDocsComponent,
    SchedulerSxPlanningComponent,
    SchedulerRecallUrlComponent,
    SchedulerConsultLetterComponent,
    SchedulerReportComponent,
    SchedulerPatientCSVExportComponent,
    ComplianceComponent,
    ComplianceQRDAComponent,
    ComplianceCQMComponent,
    FinancialsComponent,
    FinancialsPreviousHCFAComponent,
    FinancialsEIDStatusComponent,
    FinancialsEIDPaymentComponent,
    FinancialsNewStatementComponent,
    FinancialsPreviousStatementComponent,
    RemindersDayApptComponent,
    RemindersRecallsComponent,
    RemindersListComponent,
    PracticeAnalyticComponent,
    ApiComponent,
    ApiCallLogComponent,
    StateComponent,
    StateTNComponent,
    CcdComponent,
    OpticalComponent,
    ClinicalComponent,
    CommonlyUsedComponent
} from '.';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'commonly-used',
                component: CommonlyUsedComponent,
                data: { 'title': 'Commonly Used', 'banner': false }
            },
            {
                path: 'scheduler',
                children: [
                    { path: '', component: SchedulerComponent, pathMatch: 'full' },
                    {
                        path: 'patient-monitor',
                        component: SchedulerPatientMonitorComponent,
                        data: { 'title': 'Patient Monitor Scheduler', 'banner': false }
                    },
                    {
                        path: 'day-face-sheet',
                        component: SchedulerFaceSheetComponent,
                        data: { 'title': 'Day Face Sheet  Scheduler', 'banner': false }
                    },
                    {
                        path: 'appointment-information',
                        component: SchedulerApptInfoComponent,
                        data: { 'title': 'Appointment Information Scheduler', 'banner': false }
                    },
                    {
                        path: 'patient-documents',
                        component: SchedulerPtDocsComponent,
                        data: { 'title': 'Patient Documents Scheduler', 'banner': false }
                    },
                    {
                        path: 'sx-planning',
                        component: SchedulerSxPlanningComponent,
                        data: { 'title': 'SX Planning Scheduler', 'banner': false }
                    },
                    {
                        path: 'recall-fulfillment',
                        component: SchedulerRecallUrlComponent,
                        data: { 'title': 'Recall Fulfillment Scheduler', 'banner': false }
                    },
                    {
                        path: 'consult-letters',
                        component: SchedulerConsultLetterComponent,
                        data: { 'title': 'Consult Letters Scheduler', 'banner': false }
                    },
                    {
                        path: 'scheduler-report',
                        component: SchedulerReportComponent,
                        data: { 'title': 'Scheduler Report Scheduler', 'banner': false }
                    },
                    {
                        path: 'patient-csv-reports',
                        component: SchedulerPatientCSVExportComponent,
                        data: { 'title': 'Patient CSV Reports Scheduler', 'banner': false }
                    }
                ]
            },
            {
                path: 'practice-analytic',
                component: PracticeAnalyticComponent,
                data: { 'title': 'Practice Analytic Practice', 'banner': false }
            },
            {
                path: 'compliance',
                children: [
                    {
                        path: '',
                        component: ComplianceComponent,
                        data: { 'title': 'Compliance', 'banner': false }
                    },
                    {
                        path: 'qrda',
                        component: ComplianceQRDAComponent,
                        data: { 'title': 'QRDA Compliance', 'banner': false }
                    },
                    {
                        path: 'cqm-imports',
                        component: ComplianceCQMComponent,
                        data: { 'title': 'CQM Imports Compliance', 'banner': false }
                    }
                ]
            },
            {
                path: 'api',
                children: [
                    {
                        path: 'access-log',
                        component: ApiComponent,
                        data: { 'title': 'Access Log API', 'banner': false }
                    },
                    {
                        path: 'call-log',
                        component: ApiCallLogComponent,
                        data: { 'title': 'Call Log API', 'banner': false }
                    }
                ]
            },
            {
                path: 'state',
                children: [
                    {
                        path: 'ky-state-report',
                        component: StateComponent,
                        data: { 'title': 'KY State Report', 'banner': false }
                    },
                    {
                        path: 'tn-state-report',
                        component: StateTNComponent,
                        data: { 'title': 'TN State Report', 'banner': false }
                    }
                ]
            },
            {
                path: 'optical',
                children: [
                    {
                        path: 'contact-lens-reports',
                        component: OpticalComponent,
                        data: { 'title': 'Contact Lens Reports', 'banner': false }
                    },
                    {
                        path: 'contact-lens-orders',
                        component: OpticalComponent,
                        data: { 'title': 'Contact Lens Orders Optical', 'banner': false }
                    },
                    {
                        path: 'glasses',
                        component: OpticalComponent,
                        data: { 'title': 'Glasses Optical', 'banner': false }
                    }
                ]
            },
            {
                path: 'reminders',
                children: [
                    {
                        path: 'day-appointments',
                        component: RemindersDayApptComponent,
                        data: { 'title': 'Day Appointments Reminders', 'banner': false }
                    },
                    {
                        path: 'recalls',
                        component: RemindersRecallsComponent,
                        data: { 'title': 'Recalls Reminders', 'banner': false }
                    },
                    {
                        path: 'lists',
                        component: RemindersListComponent,
                        data: { 'title': 'Lists Reminders', 'banner': false }
                    }
                ]
            },
            {
                path: 'ccd',
                component: CcdComponent,
                data: { 'title': 'CCD', 'banner': false }
            },
            {
                path: 'clinical',
                component: ClinicalComponent,
                data: { 'title': 'Clinical', 'banner': false }
            },
            {
                path: 'financials',
                children: [
                    {
                        path: '',
                        component: FinancialsComponent,
                        data: { 'title': 'Financials', 'banner': false }
                    },
                    {
                        path: 'pt-collections/previous-hcfa',
                        component: FinancialsPreviousHCFAComponent,
                        data: { 'title': 'previous HCFA  Financials', 'banner': false }
                    },
                    {
                        path: 'pt-collections/eid-status',
                        component: FinancialsEIDStatusComponent,
                        data: { 'title': 'EID Status Financials', 'banner': false }
                    },
                    {
                        path: 'pt-collections/eid-payments',
                        component: FinancialsEIDPaymentComponent,
                        data: { 'title': 'EID Payments Financials', 'banner': false }
                    },
                    {
                        path: 'statements/new-statement',
                        component: FinancialsNewStatementComponent,
                        data: { 'title': 'New Statement Financials', 'banner': false }

                    },
                    {
                        path: 'statements/previous-statement',
                        component: FinancialsPreviousStatementComponent,
                        data: { 'title': 'Previous Statement Financials', 'banner': false }

                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ReportsRoutingModule { }
