// Core Modules
import { Component, OnInit } from '@angular/core';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-ccd',
  templateUrl: './ccd.component.html',
  styleUrls: ['./ccd.component.css']
})
export class CcdComponent implements OnInit {

  CCDUrl: any = '';

  constructor(private service: IframesService) {
    this.CCDUrl = this.service.routeUrl('CCDUrl', true);
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
