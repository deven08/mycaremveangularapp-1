import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClaimsComponent, SingleClaimComponent } from '.';


const routes: Routes = [
  {
    path: 'claim-search',
    component: ClaimsComponent,
    data: { 'title': 'Claim Search', 'banner': true }
  },
  {
    path: 'single-claim',
    component: SingleClaimComponent,
    data: { 'title': 'Single Claim', 'banner': true }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ClaimsRoutingModule { }
