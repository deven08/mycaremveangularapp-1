import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferingProviderComponent } from './refering-provider.component';

describe('ReferingProviderComponent', () => {
  let component: ReferingProviderComponent;
  let fixture: ComponentFixture<ReferingProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferingProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferingProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
