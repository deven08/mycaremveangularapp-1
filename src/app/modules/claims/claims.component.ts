import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-claims',
    templateUrl: 'claims.component.html'
})
export class ClaimsComponent implements OnInit {

    searchClaimSidebar = false;

    constructor() { }

    ngOnInit() {
    }

    searchClaim() {
        this.searchClaimSidebar = true;
    }

}
