import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';


// App Configuration Module





// Setting Up Component
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  EmployeesUrl: any = '';
  constructor(private service: IframesService) {
      this.EmployeesUrl = this.service.routeUrl('EmployeesUrl', true);
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
