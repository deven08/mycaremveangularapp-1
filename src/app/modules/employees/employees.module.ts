import { CommonModule } from '@angular/common';
import { EmployeesRoutingModule } from './employees-routing.module';
import {
  EmployeesComponent,
  EmployeeTimeClockComponent,
  EmployeesCenterComponent
} from '.';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EmployeesRoutingModule
  ],
  declarations: [
    EmployeesComponent,
    EmployeeTimeClockComponent,
    EmployeesCenterComponent
  ],
  // schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class EmployeesModule { }
