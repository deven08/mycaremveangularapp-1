import { Component, OnInit } from '@angular/core';
import { AppService } from '@app/core';

@Component({
    selector: 'time-clock',
    templateUrl: 'timeclock.component.html'
})

export class EmployeeTimeClockComponent implements OnInit {
    constructor(public app: AppService) {

    }
    timeClocklist: any[];
    rowData: any;
    display = false;
    ngOnInit(): void {
        this.timeClocklist = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];

    }
    onRowSelect() {

    }
}
