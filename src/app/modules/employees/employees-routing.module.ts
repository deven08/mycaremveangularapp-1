
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesComponent, 
  EmployeeTimeClockComponent, 
  EmployeesCenterComponent } from '.';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: EmployeesComponent
      },
      {
        path: 'time-clock',
        component: EmployeeTimeClockComponent
      },
      {
        path: 'center',
        component: EmployeesCenterComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeesRoutingModule { }
