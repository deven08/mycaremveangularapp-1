import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SortEvent, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
// import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { Optical } from '@app/core/models/inventory/Brands.model';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { DeactivateComponent } from '../deactivate/deactivate.component';
import { ImportCsvComponent } from '../import-csv/Import-csv.component';
import { SpectacleLensMaterial, LensStyleMaster, ManufacturerDropData } from '@app/core/models/masterDropDown.model';
@Component({
    selector: 'app-inventory-spectaclelens',
    templateUrl: 'inventory-spectaclelens.component.html'
})

export class SpectacleLensComponent implements OnInit, OnDestroy {
    addLoctionSpecType = false;
    addPrismGrid: boolean;
    addGrid: boolean;
    sortallinventory: boolean;
    addLoction: boolean;
    searchManufa = '';
    searchDesign: '';
    searchMaterial = '';
    searchUPC: '';
    searchLensType = '';
    searchName: '';
    searchValue: '';
    samplevisible = true;
    spectaclelensReActivate = false;
    visible = true;
    maufacturerid = '';
    display = false;
    Accesstoken = '';
    upc = '';
    Specatablecolumns: any[];
    Specatablelist: Optical[];
    public Manufacturers: any[];
    public manufacturerlist: Manufacturer[];
    addSpectaclesSidebar = false;
    advancedFilterSidebar = false;
    spectaclelensDetailsSidebar = false;
    MaterialListDropDown = [];
    spectacleMultipleSelection = [];
    @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    @ViewChild('deactivateRow') deactivateRows: DeactivateComponent;
    @ViewChild('importcsvBox') importCsvdialog: ImportCsvComponent;
    subscriptionspectsChangeOb: Subscription;
    subscriptionspectacleclose: Subscription;
    subscriptionadvanceSearch: Subscription;
    subscriptionCSV: Subscription;
    reActivateItems = false;
    addbarcodelabels = false;
    @ViewChild('dt') public dt: any;
    styles = [];
    lensstylcopy: any[];
    locationFrameId: any;
    subscriptionAddLocation: any;
    paginationValue: any = 0;
    totalRecords: string;
    paginationPage: any = 0;
    reqbodysearch: any;
    activeFileId: number = 0;
    /**
    * Row id  is a boolean variable
    */
    rowId = true;
    /**
     * Row name is a boolean variable
     */
    rowName = true;
    /**
     * Row manufacturer is a boolean variable
     */
    rowManufacturer = true;
    /**
     * Row styles is a boolean variable
     */
    rowStyles = true;
    /**
     * Row material is a boolean variable
     */
    rowMaterial = true;
    /**
     * Row upc is a boolean variable
     */
    rowUPC = true;
    /**
     * Filterdata stroge the filter details
     */
    filterdata: any;
    /**
     * Sort by name of the spectacle lens component of assigning names
     */
    sortbyName: any;
    /**
     * Sortby value of the spectacle lens component of sort by id
     */
    sortbyValue: any = "id";
    /**
     * thenbyValue  of spectacle lens component of sort items
     */
    thenbyValue: any = [];
    ngOnInit() {
        this.loadMaterialList();
        this.loadSpectablelenscolumns();
        this.GetSpectablelenssList();
        this.Loadmasters();
        this.LoadManufacturers();
        this._InventoryCommonService.InventoryType = 'SpectacleLens';
    }
    constructor(private spectcalelensServicee: SpectaclelensService,
                private _InventoryCommonService: InventoryService,
                private dropdownService: DropdownService,
                private error: ErrorService,
                public utility: UtilityService) {

    }

    deleteRows() {
        // this.DeleteRows.dialogBox();
    }
    /**
     * Deactive use for items deactivate
     */
    deActive() {
        if (this.spectacleMultipleSelection.length !== 0) {
            const spectalcleData = {
                'SpectacleSelection': this.spectacleMultipleSelection
            };
            this.deactivateRows.deactivate(spectalcleData);
        }

    }

    Loadmasters() {
        this.LoadLensStyle();
    }
    importcsv() {
        this.importCsvdialog.importCsvbox('spectacle');
    }

    /**
     * Loads spectablelenscolumns loading table grid heders
     * 
     */
    loadSpectablelenscolumns() {
        this.Specatablecolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'name', header: 'Name', visible: this.visible },
            { field: 'manufacturer', header: 'Manufacturer', visible: this.visible },
            { field: 'StyleName', header: 'Lens Style', visible: this.visible },
            { field: 'spectacle_material', header: 'Material', visible: this.visible },
            { field: 'upc_code', header: 'UPC', visible: this.visible }
        ];
    }
    /**
     * Get Data view 
     * for displaying the Data view 
     */
    DataViewdialogBox() {
        this.display = true;
    }
    checkBoxClick(item, e) {
        if (item === 'selectAll') {
            this.visible = e.checked;
            this.loadSpectablelenscolumns();
            this.rowId = e.checked;
            this.rowName = e.checked;
            this.rowManufacturer = e.checked;
            this.rowStyles = e.checked;
            this.rowMaterial = e.checked;
            this.rowUPC = e.checked;
            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    applyDataViewChanges(item, selected) {
        this.Specatablecolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;
            }
        });
        this.displayCols(item, selected);
        const colSpanVar = this.Specatablecolumns.filter(x => x.visible).length;
        if (colSpanVar < this.Specatablecolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }
    }

    displayCols(item, selected) {
        if (item == 'ID') {
            this.rowId = selected
        }
        if (item == 'Name') {
            this.rowName = selected
        }
        if (item == 'Manufacturer') {
            this.rowManufacturer = selected
        }
        if (item == 'Lens Style') {
            this.rowStyles = selected
        }
        if (item == 'Material') {
            this.rowMaterial = selected
        }
        if (item == 'UPC') {
            this.rowUPC = selected
        }
    }
    /**
     * Filters pay load
     *  @returns Items filtes data view
     */
    filterPayLoad() {
        const spectaclelens = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "2"
                },
                {
                    field: "id",
                    operator: 'LIKE',
                    value: '%' + this.searchValue + '%'
                },
                {
                    field: "upc_code",
                    operator: 'LIKE',
                    value: '%' + this.searchUPC + '%'
                },
                {
                    field: "material_id",
                    operator: "=",
                    value: this.searchMaterial
                },
                {
                    field: "manufacturer_id",
                    operator: "=",
                    value: this.searchManufa
                },
                {
                    field: "type_id",
                    operator: "=",
                    value: this.searchLensType
                },
                {
                    field: "name",
                    operator: 'LIKE',
                    value: '%' + this.searchName + '%'
                }
            ],
        };
        const resultData = spectaclelens.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.filterdata = {
            filter: resultData,
            sort: [
                {
                    field: this.sortbyValue,
                    order: "DESC"
                }
            ]
        };
        if (this.thenbyValue.length) {
            for (let i = 0; i <= this.thenbyValue.length - 1; i++) {
                this.filterdata.sort.push({
                    field: this.thenbyValue[i],
                    order: 'DESC'
                });
            }
        }
    }
    GetSpectablelenssList() {
        this.Specatablelist = [];
        this.filterPayLoad();
        this.spectcalelensServicee.getSpectaclens(this.filterdata).subscribe(
            data => {
                this.Specatablelist = [];
                this.totalRecords = data['total'];
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    if (data['data'][i]['del_status'] == 0) {
                        this.Specatablelist.push(data['data'][i]);
                    }
                }
            }
        );
    }

    /**
     * Loads manufacturers
     * @param {array} manufacturers for binding manufacturersdropdown data
     * @returns {object} for dropdown data
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
    * Params spectacle lens component
    * Loads MaterialList
    * @param {array} MaterialLists for binding MaterialListDropDown data
    * @returns {object} for dropdown data
    */
    loadMaterialList() {
        this.MaterialListDropDown = this.spectcalelensServicee.MaterialListDropDownData;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.spectcalelensServicee.getSpectaclesMasterList().subscribe((values: SpectacleLensMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ Id: '', Name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ Id: dropData[i].id, Name: dropData[i].material_name });
                    }
                    this.spectcalelensServicee.MaterialListDropDownData = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads lens style
     * @param {array} Styles for binding StylesDropDown data
     * @returns {object} for dropdown data
     */
    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        this.lensstylcopy = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    this.lensstylcopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Advances filter
     * @param data filter the data
     */
    advanceFilter(data) {
        this.searchUPC = data.upc_code;
        this.searchManufa = data.manufacturer_id;
        this.searchValue = data.id;
        this.searchName = data.name;
        this.searchMaterial = data.material_id;
        this.searchLensType = data.type_id,
        this.GetSpectablelenssList();
        this.advancedFilterSidebar = false;
    }

    /**
     * Filters spectacle lens
     * @returns filter items data
     */
    filterSpectacleLens() {
        this.dt.reset();
        this.GetSpectablelenssList();
    }

    /**
     * Determines whether row select on
     * @param {string} event getting click events 
     */
    onRowSelect(event) {
        this.spectcalelensServicee.selectedspectacleupc = event.data.upc_code;
    }
    onRowUnselect(event) {
        this.spectcalelensServicee.selectedspectacleupc = '';
        this.spectcalelensServicee.editDuplicatespectacleupc = '';
    }
    objChanged(event: any) {

        if (event.target.name === 'upc') {
            this.upc = event.target.value;
            this.dt.reset();
            this.GetSpectablelenssList();

        } else if (event.target.name === 'Manufacturer') {
            this.dt.reset();
            this.GetSpectablelenssList();
        }
    }

    /**
     * Get Reactivate items
     * for displaying the reactivate side-bar
     */
    GetreActivateItems() {
        this.spectaclelensReActivate = true;
    }
    /**
     * Gets sort all
     * for displaying the sort all side-bar
     */
    getSortAll(){
        this.sortallinventory = true;
        this.sortbyName = 'spectacle-lens';
    }
    /**
     * Adds spectacles
     * for displaying the add new spectacle lens side-bar
     */
    addSpectacles() {
        this._InventoryCommonService.FortrasactionItemID = '';
        this.locationFrameId = undefined;
        this.spectacleMultipleSelection = [];
        this.dt.selection = true;
        this.dt.selection = false;
        this.addSpectaclesSidebar = true;
        this.spectcalelensServicee.selectedspectacleupc = '';
        this.spectcalelensServicee.selectedspectacleupcDoubleCLick = '';
        this._InventoryCommonService.FortrasactionItemID = '';
    }

    /**
     * Updates spectacle lens
     * for update item details
     */
    updateSpectacles() {
        this.spectcalelensServicee.editDuplicatespectacleupc = '';
        // this.frameService.errorHandle.msgs = [];
        this._InventoryCommonService.FortrasactionItemID = this.spectacleMultipleSelection[0].id;
        if (this.spectacleMultipleSelection.length === 1) {
            this.spectcalelensServicee.selectedspectacleupc = this.spectacleMultipleSelection[0].upc_code;
            this._InventoryCommonService.multiLocationForOtherInventory = this.spectacleMultipleSelection[0].id;
            this.locationFrameId = this.spectacleMultipleSelection[0].id;
            const selectedFrameid = this.spectcalelensServicee.selectedspectacleupc;

            if (!selectedFrameid) {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Please Select Spectacle Lens.'
                });
            } else {
                this.addSpectaclesSidebar = true;
            }
        } else {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select One Item.' });
        }
    }
    /**
     * Duplicates spectacle lens
     * for duplicate items
     */
    duplicateSpectacleLens() {
        // this.frameService.errorHandle.msgs = [];
        if (this.spectacleMultipleSelection.length === 1) {
            this.spectcalelensServicee.selectedspectacleupc = '';
            this.spectcalelensServicee.selectedspectacleupcDoubleCLick = '';
            this.spectcalelensServicee.editDuplicatespectacleupc = this.spectacleMultipleSelection[0].upc_code;
            this._InventoryCommonService.FortrasactionItemID = this.spectacleMultipleSelection[0].id;
          
            this.addSpectaclesSidebar = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select One Item.'
            });
        }
        this.spectacleMultipleSelection = [];
    }
    /**
     * Doubles click spectacles
     * @param {object} rowData getting selected item from grid 
     */
    DoubleClickSpectacles(rowData) {
        this.spectcalelensServicee.editDuplicatespectacleupc = '';
        this._InventoryCommonService.FortrasactionItemID = rowData.id;
        this.spectacleMultipleSelection = [];
        this.spectcalelensServicee.selectedspectacleupcDoubleCLick = rowData.upc_code;
        this.spectacleMultipleSelection.push(rowData);
        this.addSpectaclesSidebar = true;
        this.locationFrameId = rowData.id;
    }

    /**
     * Resets spectacle lens component
     * @param table for reset the table items data
     */
    reset(table) {
        this.dt.selection = false;
        table.reset();
        this.clearsearchvalues();
        this.sortbyValue = 'id';
        this.GetSpectablelenssList();
        this.paginationValue = 0;
    }

    /**
     * Clearsearchvalues spectacle lens component
     * @returns for clear the search input
     */
    clearsearchvalues() {
        this.searchValue = '';
        this.searchName = '';
        this.searchLensType = '';
        this.searchUPC = '';
        this.searchMaterial = '';
        this.searchDesign = '';
        this.searchManufa = '';
        this.upc = '';
        this.maufacturerid = '';
    }

    ngOnDestroy(): void {
    }

    /**
     * Advanced filter sidebar set
     * for displaying the Advanced filter sidebar
     */
    advancedFilterSidebarSet() {
        this.advancedFilterSidebar = true;
    }
    /**
     * Adds grid set
     * for displaying the add grid sidebar
     */
    addGridSet() {
        this.addGrid = true;
    }
    /**
     * Adds prism grid set
     * for displaying the Add prism grid sidebar
     */
    addPrismGridSet() {
        this.addPrismGrid = true;
    }

    /**
     * Add loction spectacle
     * for select the location 
     */
    addLoctionSpectacle() {
        this._InventoryCommonService.InventoryType = 'SpectacleLens';
        // this.frameService.errorHandle.msgs = [];
        if (this.spectacleMultipleSelection.length === 1) {
            this._InventoryCommonService.FortrasactionItemID = this.spectacleMultipleSelection[0].id;
            this.addLoctionSpecType = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select  Only One SpectacleLens'
            });
        }
    }
    /**
     * Add Barcodelabels set
     * for the displaying bar code labels
     */
    addbarcodelabelsSet() {
        if (this.spectacleMultipleSelection.length !== 0) {
            this._InventoryCommonService.barCodelist = this.spectacleMultipleSelection;
            this._InventoryCommonService.barCodeLable = 'SpectacleBarcode';
        } else {
            this._InventoryCommonService.barCodelist = [];
            this._InventoryCommonService.barCodeLable = 'SpectacleBarcode';
        }
        this.addbarcodelabels = true;
    }
    /**
     * Details outputevent
     * @param {string} event getting status from spectacleDetails by output event
     */
    detailsOutputevent(event) {
        if (event === 'close') {
            this.spectaclelensDetailsSidebar = false;
        }
        if (event === 'save') {
            this.spectaclelensDetailsSidebar = false;
            // this.dt.reset();
            this.GetSpectablelenssList();
        }
    }
    /**
     * Sortallevents spectacle lens component
     * @param event for sort the data grid
     */
    sortallevent(event) {
        this.Specatablelist = event.gridData;
        this.sortallinventory = false;
        this.sortbyValue = event.sortValue;
        this.thenbyValue =event.thenValue;
    }
    addgridevent(event) {
        if (event === 'close') {
            this.addGrid = false;
        }
    }
    addPrismgridevent(event) {
        if (event === 'close') {
            this.addPrismGrid = false;
        }
    }

    /**
     * Gets spectacle list by paginator
     * @param itemId getting the data
     * @param [page] for the pagination data
     */
    GetSpectacleListByPaginator(page: number = 0) {
        this.filterPayLoad();
        this.spectcalelensServicee.getSpectaclespagenation(this.filterdata, page).subscribe(
            data => {
                this.Specatablelist = data['data'];
            },
        );
    }
    /**
    * Lazys load file items
    * @returns Load lazyLoad Spectacle lens data
    */
    loadLazySpectaclelensItems(event: LazyLoadEvent) {
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.GetSpectacleListByPaginator(page);
    }

    customSort(event: SortEvent) {
        this.dt.first = this.paginationValue;
        const tmp = this.Specatablelist.sort((a: any, b: any): number => {
            if (event.field) {
                return a[event.field] > b[event.field] ? 1 : -1;
            }
        });
        if (event.order < 0) {
            tmp.reverse();
        }
        const thisRef = this;
        this.Specatablelist = [];
        tmp.forEach(function (row: any) {
            thisRef.Specatablelist.push(row);
        });
    }
    /**
     * Specs deactive event
     * @param event handle the Deactivate side-bar
     */
    specDeactiveEvent(event) {
        if (event === 'specDeactivate') {
            this.addSpectaclesSidebar = false;
            this.spectaclelensReActivate = false;
            this.spectacleMultipleSelection = [];
            this.GetSpectablelenssList();
        }
    }
    /**
     * Spects locations event
     * @param event handle the Location 
     */
    spectLocationsEvent(event) {
        if (event === 'spec') {
            this.addLoctionSpecType = false;
        } else {
            this.addLoctionSpecType = false;
            this.GetSpectablelenssList();
        }
    }
    /**
     * Spects reactivate event
     * @param event handle the reactivate
     */
    spectReactivateEvent(event) {
        if (event === 'specClose') {
            this.spectaclelensReActivate = false;
        } else {
            this.spectaclelensReActivate = false;
            this.GetSpectablelenssList();
        }
    }
    /**
     * Spectacles add item event
     * @param event handle the add items
     */
    spectacleAddItemEvent(event) {
        if (event === '') {
            this.addSpectaclesSidebar = false;
        } else {
            this.addSpectaclesSidebar = false;
            this.GetSpectablelenssList();
        }
    }
    /**
     * Spectacles adv filter event
     * @param event handle the advance filter
     */
    spectacleAdvFilterEvent(event) {
        this.advanceFilter(event);
    }
    /**
     * Spectaclelens details
     * @param {array} spectacleMultipleSelection for multi item selection
     * @returns displays the details page 
     */
    spectaclelensDetails() {
        if (this.spectacleMultipleSelection.length !== 0) {
            this.spectaclelensDetailsSidebar = true;
        } else {
            this.error.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select One or More Items.' });
        }

    }
}
