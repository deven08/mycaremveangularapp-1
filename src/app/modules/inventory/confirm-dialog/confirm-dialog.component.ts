import { Component, OnInit } from '@angular/core';
import { ConfirmationService, Message } from 'primeng/api';


@Component({
    selector: 'confirm-dialog',
    templateUrl: 'confirm-dialog.component.html'
})
export class ConfirmDialogComponent implements OnInit {

    msgs: Message[] = [];
    constructor(private confirmationService: ConfirmationService) {}
    ngOnInit() { }

    confirm() {
        this.confirmationService.confirm({
            message: 'Do you what to save changes before leaving the page?',
            header: 'Confirmation',
            accept: () => {
                this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'You have accepted'}];
            },
            reject: () => {
                this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
            }
        });
    }

}

