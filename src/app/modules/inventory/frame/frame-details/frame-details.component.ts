import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { FramesService } from '@services/inventory/frames.service';
import { FrameBrand } from '@app/core/models/inventory';
import {
    CommissionStructure,
    CommissionType,
    FrameTypeMaster,
    InvUpcType,
    FrameStyleMaster,
    ManufacturerDropData,
    RimType,
    FrameBrands,
    SpectacleMaterial
} from '@app/core/models/masterDropDown.model';

/**
 * Component
 * Description : this component is used for rendering router-out let
 */
@Component({
    selector: 'app-frame-details',
    templateUrl: 'frame-details.component.html'
})
export class FrameDetailsComponent implements OnInit {
    public manufacturerlist: Manufacturer[];
    FarmesForm: FormGroup;
    /**
     * Farme details form of frame details component
     * formgroup variable
     */
    FarmeDetailsForm: FormGroup;
    Manufacturers: any[];
    /**
     * Frames type drop down of frame details component
     * frametype dropdown variable
     */
    framesTypeDropDown: any[];

    framesRimType: any[];
    genderDropDown = [{ 'Id': 'Male', 'Name': 'Male' }, { 'Id': 'Female', 'Name': 'Female' }, { 'Id': 'Unisex', 'Name': 'Unisex' }];
    activestatus: any[];
    StructureDropData: any[] = [];
    TypeDropData: any[] = [];
    procedurecoderesults: any[];
    filteredprocedurescodes: any;
    filteredProcCodes: any[];
    LensRangeDataDropdown: boolean;
    LensRangeData: any;
    selectedRangeData: any;

    DisaplayLensRangeData: any;
    upcdisable = false;
    ostframes: any;
    activatedeacticeframesvalues = [];
    framesMultipleSelection = [];
    frameid: any;
    addFramesSidebar = false;
    uploadedFiles: any[] = [];
    uploadedTraceFiles: any[] = [];
    postframes: any;
    savelensReqbody: any[];
    frameItemsId: any[];
    selectLensRange = false;
    framesUpcType = [];
    public collectionList: FrameBrand[];
    Brands: any[];
    stylecheckedvalueobj: any;
    valuestopushinstylecheckbox: any;
    stylcopy: any[];
    /**
     * Materials  of frame details component
     * storing collection of materials
     */
    materials: Array<object>;
    /**
     * Styles  of frame details component
     * storing  styles 
     */
    styles = [];
    /**
     * Style ids of frame details component
     * styleids variable
     */
    styleIds: any = [];
    /**
     * Material ids of frame details component
     * materialid vriable
     */
    materialIds: any = [];


    /**
     * Frames group forms of frame details
     * For Handling all forms in one group
     */
    FramesGroupForms: FormGroup;
    @Output() frameDetailsOutput = new EventEmitter<any>();
    // @Output() detailsClose = new EventEmitter<any>();
    @Input() framesCountVal: any;
    ngOnInit() {
        this.LoadLensStyle();
        console.log(this.framesCountVal);
        this.LoadMaster();
        this.Loadformdata();
    }


    /**
     * Creates an instance of frame details component
     * @param error storing ErrorService data
     * @param utility storing UtilityService data
     * @param dropdownService  storing dropdown service data
     * @param frameService storing frame service data
     * @param _fb storing FormBuilder data
     * @param inventoryService storing inventory service data
    
     */
    constructor(
        // inventoryCommonService: InventoryService,
        // private _DropdownsPipe: DropdownsPipe,
        private error: ErrorService, private utility: UtilityService,
        private dropdownService: DropdownService,
        private frameService: FramesService,
        private _fb: FormBuilder,
        private inventoryService: InventoryService,
    ) {

    }





    /**
     * Loadformdatas frame details component
     *  defining fromcontroler names
     */
    Loadformdata() {
        let frameGet: any = [];
        this.FramesGroupForms = this._fb.group({
            'FramesDetailsForm': this._fb.group({
                itemIds: [],
                manufacturer_id: '',
                brand_id: '',
                type_id: '',
                rimtypeid: '',
                gender: '',
                status: '',
                upctypeid: '',
                eyesize: '',
                a: '',
                b: '',
                ed: '',
                bridge: '',
                temple: '',
                frame_lens_range_id: '',
                retail_price: '',
                wholesale_cost: '',
                groupcost: '',
                sourceid: '',
                msrp: '',
                procedure_code: '',
                structure_id: '',
                spiff: '',
                commission_type_id: '',
                amount: '',
                gross_percentage: '',
                module_type_id: '1',
            }),
            "FramesStylesForm": this._fb.group({
                styleIds: '',
                materialIds: ''
            })
        });
    }


    /**
     * Loads master
     * calling the funtions
     */
    LoadMaster() {
        this.LoadManufacturers();
        this.loadFramesType();
        this.loadRimType();
        this.Loadstatus();
        this.loadStructures();
        this.loadUpcType();
        this.LoadBrands();
        this.LoadRangTypes();
        this.Loadmaterials();
        this.loadCommitionType();

    }



    /**
     * Loads rang types
     * @returns  displaying dropdown data for lensrange
     */
    LoadRangTypes() {
        const selectedFrameid = this.frameService.editframeid || this.frameService.editframeidDoubleClick;
        if (selectedFrameid) {
            this.upcdisable = true;
            let frameGet: any = [];

            this.frameService.getframes(selectedFrameid).subscribe(
                data => {
                    frameGet = data;
                    let controls = 'controls';
                    if (frameGet.frame_lens_range_id !== '') {
                        // alert(frameGet["result"]["Optical"]["Frames"][0]["FrameLensRangeId"])
                        this.frameService.getLensRangeDetail(frameGet.frame_lens_range_id).subscribe
                            (lensData => {
                                console.log(lensData);
                                this.selectedRangeData = lensData;
                                this.DisaplayLensRangeData = '';
                                this.DisaplayLensRangeData = lensData['id'];
                                //  alert(this.selectedRangeData)
                                this.FramesGroupForms[controls].FramesDetailsForm[controls]
                                    .frame_lens_range_id.patchValue(this.DisaplayLensRangeData);

                            });
                    }
                });
        }
    }






    /**
     * Loads manufacturers
     * @returns displaying dropdown data for manufactures
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }


    /**
     * Loads frames type
     * @returns displaying dropdown data for frametypes
     */
    loadFramesType() {
        this.framesTypeDropDown = this.frameService.framesTypeDropDownData;
        if (this.utility.getObjectLength(this.framesTypeDropDown) === 0) {
            this.frameService.getFramesType().subscribe((values: FrameTypeMaster) => {
                try {
                    const dropData = values.data;
                    this.framesTypeDropDown.push({ Id: '', Name: 'Select Frametype' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesTypeDropDown.push({ Id: dropData[i].id, Name: dropData[i].type_name });
                    }
                    this.frameService.framesTypeDropDownData = this.framesTypeDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    /**
     * Loads rim type
     * @returns displaying dropdown data for rimtype
     */
    loadRimType() {
        this.framesRimType = this.dropdownService.rimType;
        if (this.utility.getObjectLength(this.framesRimType) === 0) {
            this.dropdownService.getRimType().subscribe((values: RimType[]) => {
                try {
                    this.framesRimType.push({ Id: '', Name: 'Select Rim Type', ColorCode: '' });
                    for (let i = 0; i <= values.length - 1; i++) {
                        this.framesRimType.push({ Id: values[i].id, Name: values[i].rimtype, ColorCode: values[i].colorcode });
                    }
                    this.dropdownService.rimType = this.framesRimType;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



    /**
     * Loadstatus frame details component
     *  @returns displaying dropdown data for status
     */
    Loadstatus() {
        this.activestatus = [];
        this.activestatus.push({ Id: '0', Name: 'Active' });
        this.activestatus.push({ Id: '1', Name: 'Discontinue' });

    }



    /**
     * Loads commition type
     * @returns displaying dropdown data for commitionType
     */
    loadCommitionType() {
        this.TypeDropData = this.inventoryService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.inventoryService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this.inventoryService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads structures
     * @returns displaying dropdown data for structures
     */
    loadStructures() {
        this.StructureDropData = this.inventoryService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.inventoryService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this.inventoryService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



    /**
     * Filters brands
     * @returns displaying dropdown data procedure
     */
    filterProceduresCodes(event) {
        this.frameService.errorHandle.msgs = [];
        this.procedurecoderesults = [];
        this.filteredprocedurescodes = [];
        const values = {

            filter: [{
                field: 'cpt4_code',
                operator: 'LIKE',
                value: event.query.toLowerCase() + '%'
            }]
        };
        this.frameService.getProcedureCode(values).subscribe(data => {
            this.procedurecoderesults = data['data'];
            this.filteredprocedurescodes = this.filteredprocedurescodes;
            this.filteredProcCodes = [];
            for (let i = 0; i < this.procedurecoderesults.length; i++) {
                const brand = this.procedurecoderesults[i].cpt_desc;
                if (brand.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                    this.filteredProcCodes.push(brand);
                }
            }
            if (this.filteredProcCodes.length === 0) {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'No Data Found.'
                });

            }
        },
            error => {
                // this.brand = "";
                this.filteredProcCodes = [];
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'No Data Found.'
                });

            });
    }


    /**
     * Lens range data disaplay
     * @returns displaying dropdown data for lensrange
     */
    LensRangeDataDisaplay() {


        this.LensRangeDataDropdown = !this.LensRangeDataDropdown;
        this.frameService.getFrameLensRange().subscribe(data => {
            let key = 'data';
            this.LensRangeData = data[key];
        });
    }


    /**
     * Saves frame details component
     * @returns display the modifying data for selected items
     
     */
    save() {
        if (!this.FramesGroupForms.valid) {
            let controls = 'controls';
            // For Handling Form Errors
            this.error.displayFormErrors(this.FramesGroupForms[controls].FramesDetailsForm[controls]);
            this.error.displayFormErrors(this.FramesGroupForms[controls].FramesStylesForm[controls]);
            return false;
        }
        if (this.framesCountVal.length !== 0) {
            this.frameItemsId = [];
            for (let i = 0; i <= this.framesCountVal.length - 1; i++) {
                this.frameItemsId.push(this.framesCountVal[i].itemId);
            }
            let controls = 'controls';
            this.FramesGroupForms[controls].FramesDetailsForm[controls].itemIds.setValue(this.frameItemsId);
            // Creating Payload and Removing Null Values
            let framesDetailsData = this.FramesGroupForms[controls].FramesDetailsForm;
            framesDetailsData = this.utility.format(framesDetailsData);
            this.frameService.saveFrameMoreDetails(framesDetailsData).subscribe(data => {
                // console.log(data);
                this.error.displayError({
                    severity: 'success',
                    summary: 'Inventory Status', detail: 'Items Detail updated successfully'
                });

                // if (data['Status'] === 'Items Detail updated successfully') {

                //     this.frameDetailsOutput.emit('ok');
                // }
                this.frameDetailsOutput.emit('ok');

            });
        }

        this.saveStyles();
        this.saveMaterials();



    }
    /**
     * Saves styles
     * modify the selected styles for selected items
     */
    saveStyles() {
        let controls = 'controls';
        if (this.FramesGroupForms[controls].FramesStylesForm[controls].styleIds.value.length !== 0) {
            this.styleIds = [];
            let datas = this.FramesGroupForms[controls].FramesStylesForm[controls].styleIds.value.length;
            for (let i = 0; i < (datas); i++) {
                this.styleIds.push(this.FramesGroupForms[controls].FramesStylesForm[controls].styleIds.value[i].value);
            }

            this.FramesGroupForms[controls].FramesStylesForm[controls].styleIds.setValue(this.frameItemsId);
            let framesStylesForm = this.FramesGroupForms[controls].FramesStylesForm;
            framesStylesForm = this.utility.format(framesStylesForm);
            let reqBody = {
                'itemIds': this.frameItemsId,
                'module_type_id': 1,
                'styleIds': this.styleIds
            };
            this.frameService.saveStyles(reqBody).subscribe(data => {
                console.log(data);
                this.frameDetailsOutput.emit('ok');
            });
        }
    }

    /**
     * Saves materials
     *  modify the selected materials for selected items
     */
    saveMaterials() {
        let controls = 'controls';
        if (this.FramesGroupForms[controls].FramesStylesForm[controls].materialIds.value.length !== 0) {
            this.materialIds = [];
            let datas = this.FramesGroupForms[controls].FramesStylesForm[controls].materialIds.value.length;
            for (let i = 0; i < (datas); i++) {
                this.materialIds.push(this.FramesGroupForms[controls].FramesStylesForm[controls].materialIds.value[i].value);
            }

            this.FramesGroupForms[controls].FramesStylesForm[controls].materialIds.setValue(this.frameItemsId);
            let framesStylesForm = this.FramesGroupForms[controls].FramesStylesForm;
            framesStylesForm = this.utility.format(framesStylesForm);
            let reqBody = {
                'itemIds': this.frameItemsId,
                'module_type_id': 1,
                'materialIds': this.materialIds
            };
            this.frameService.saveMaterials(reqBody).subscribe(data => {
                this.frameDetailsOutput.emit('ok');
            });
        }
    }



    /**
     * Doubles click lens range
     * @returns display seleted data for textbox
     */
    DoubleClickLensRange(data) {
        let controls = 'controls';
        this.selectedRangeData = data;
        this.DisaplayLensRangeData = '';
        this.DisaplayLensRangeData = data['id'];
        this.FramesGroupForms[controls].FramesDetailsForm[controls].frame_lens_range_id.patchValue(this.selectedRangeData.id);
        this.FramesGroupForms[controls].FramesDetailsForm[controls].frame_lens_range_id.patchValue(this.DisaplayLensRangeData);
        this.LensRangeDataDropdown = false;

    }


    /**
     * Lens rangeevent
     * @returns lensrange details
     */
    lensRangeevent(event) {
        let controls = 'controls';
        if (event === 'close') {
            this.selectLensRange = false;
        } else {
            this.selectedRangeData = event;
            this.DisaplayLensRangeData = '';
            this.DisaplayLensRangeData = this.selectedRangeData.sphere_max + '  ' + 'to' + '  ' + this.selectedRangeData.sphere_min;
            // alert(this.selectedRangeData)
            // this.FarmesForm.controls['FrameLensRangeId']=this.selectedRangeData['id']
            // this.FarmesForm.controls['FrameLensRangeName']=this.selectedRangeData['lensrangename']
            this.FramesGroupForms[controls].FramesDetailsForm[controls].frame_lens_range_id.patchValue(this.DisaplayLensRangeData);
            this.FramesGroupForms[controls].FramesDetailsForm[controls].frame_lens_range_id.patchValue(this.selectedRangeData.id);
            this.selectLensRange = false;
        }
    }



    /**
     * Disaplays lens range
     * @returns while clicking the button display all the  data for lensrange
     */
    DisaplayLensRange() {
        this.selectLensRange = true;
    }


    /**
     * Loads upc type
     * @returns displaying dropdowndata for upcType
     */
    loadUpcType() {

        this.framesUpcType = [];
        this.framesUpcType = this.inventoryService.upcTypeData;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.frameService.getUpcType().subscribe((values: InvUpcType) => {
                try {
                    const dropData = values.data;
                    this.framesUpcType.push({ Id: '', Name: 'Select UPC Type' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesUpcType.push({ Id: dropData[i].id, Name: dropData[i].upctype });
                    }
                    this.frameService.framesUpcTypeData = this.framesUpcType;

                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }




    /**
     * Loads brands
     * @returns displaying dropdowndata for collections
     */
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Closes click
     *  @returns closing the details page and refreshing the grid
     */
    closeClick() {
        this.frameDetailsOutput.emit('close');
    }
    /**
     * Loads lens style
     * @returns displaying dropdowndata for LensStyle
     */
    LoadLensStyle() {
        this.stylcopy = this.dropdownService.frameStyles;
        if (this.utility.getObjectLength(this.stylcopy) === 0) {
            this.dropdownService.getFrameLensStyle().subscribe((values: FrameStyleMaster) => {
                try {
                    const dropData = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].style_name, value: dropData[i].id });
                    }
                    this.stylcopy = this.styles;
                    this.dropdownService.frameStyles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



    /**
     * Loadmaterials frame details component
     * @returns displaying dropdowndata for materials
     */
    Loadmaterials() {
        this.materials = this.dropdownService.lensMaterial;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materials.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.lensMaterial = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

}
