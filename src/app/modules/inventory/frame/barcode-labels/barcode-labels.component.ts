import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core'; 
import { InventoryService } from '@app/core/services/inventory/inventory.service';
 
import { UtilityService } from '@app/shared/services/utility.service';
// import { FrameService } from '../../../ConnectorEngine/services/frame.service';

import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'barcodelabels',
  templateUrl: 'barcode-labels.component.html',
  styleUrls: ['./barcode-print.component.css']
})
export class BarCodeLabelsComponent implements OnInit {
  title = 'barcode';
  elementType = 'svg';
  value = '012345678936';
  format = 'UPC';
  lineColor = '#000000';
  width = 1.2;
  height = 30;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 18;
  background = '#ffffff';
  margin = 0;
  marginTop = 0;
  marginBottom = 0;
  marginLeft = 0;
  marginRight = 0;
  display: boolean;
  barcodeGridData: any[] = [];
  /**
   * Print style class of bar code labels component
   */
  printStyleClass: string;
  formatothers = 'CODE128';
  testdata = '33';
  onedata = '';
  twodata = '';
  threedata = '';
  fourdata = '';
  fivedata = '';
  marginr = '20px';
  // barCodeSubscription: Subscription;
  FrameData = false;
  SpectacleData = false;
  ContactData = false;
  OthersData = false;
  barCodeLabelDropData = [];
  /**
   * Drop down value of bar code labels component
   */
  dropDownValue: any;
  /**
   * Columns  of bar code labels component
   */
  Columns: any;
  /**
   * Columns spacing of bar code labels component
   */
  ColumnsSpacing: any;
  /**
   * Top margin of bar code labels component
   */
  TopMargin: any;
  /**
   * Right margin of bar code labels component
   */
  RightMargin: any;
  /**
   * Height  of bar code labels component
   */
  Height: any;
  /**
   * Rows  of bar code labels component
   */
  Rows: any;
  /**
   * Rows spacing of bar code labels component
   */
  RowsSpacing: any;
  /**
   * Left margin of bar code labels component
   */
  LeftMargin: any;
  /**
   * Bottom margin of bar code labels component
   */
  BottomMargin: any;
  /**
   * Width  of bar code labels component
   */
  Width: any;
  /**
   * Start position of bar code labels component
   */
  StartPosition: any;
  /**
   * Top margin of bar code labels component
   */
  topMargin: any;
  /**
   * Right margin of bar code labels component
   */
  rightMargin: any;
  /**
   * Left margin of bar code labels component
   */
  leftMargin: any;
  /**
   * Bottom margin of bar code labels component
   */
  bottomMargin: any;
  /**
   * Width margin of bar code labels component
   */
  widthMargin: any;
  /**
   * Height margin of bar code labels component
   */
  heightMargin: any;
  /**
   * No of colums of bar code labels component
   */
  noOfColums: any;
  /**
   * Row spacing of bar code labels component
   */
  rowSpacing: any;
  /**
   * Column spacing of bar code labels component
   */
  columnSpacing: any;
  /**
   * Exclude lable margin of bar code labels component
   */
  excludeLableMargin: Array<any>;
  /**
   * Start position value of bar code labels component
   */
  startPositionValue: number;

  /**
   * Frames sort data of bar code labels component
   */
  framesSortData: any;
  /**
   * Check box click of bar code labels component
   */
  displayFilter: boolean;

  /**
   * Manufacturer  of bar code labels component
   */
  manufacturer: boolean;
  /**
   * Upc  of bar code labels component
   */
  barcode: boolean;
  /**
   * Color  of bar code labels component
   */
  color: boolean;
  /**
   * Brand  of bar code labels component
   */
  brand: boolean;
  /**
   * Color code of bar code labels component
   */
  color_code: boolean;
  /**
   * Name  of bar code labels component
   */
  name: boolean;
  /**
   * Eye  of bar code labels component
   */
  eye: boolean;
  /**
   * Bridge  of bar code labels component
   */
  bridge: boolean;
  /**
   * Temple  of bar code labels component
   */
  temple: boolean;

  /**
   * Adv filter of bar code labels component
   */
  advFilter:boolean;
  /**
   * Sort filter of bar code labels component
   */
  sortFilter:boolean;
  /**
   * Printer setup of bar code labels component
   */
  printerSetup:boolean;
  /**
   * Zoom  of bar code labels component
   */
  zoom:boolean;
  /**
   * Coloumn cal width of bar code labels component
   */
  coloumnCalWidth: string;
  @ViewChild('printsection') pdfContent: ElementRef;



  /**
   * Creates an instance of bar code labels component.
   * @param frameService for calling frameservice methods
   * @param utility for calling numbers-only method
   */
  constructor(private _InventoryCommonService: InventoryService,public utility:UtilityService) {
    this.excludeLableMargin = [];
    if (this._InventoryCommonService.barCodeLable === 'FramesBarcode') {
      this.FrameData = true;
    }
    if (this._InventoryCommonService.barCodeLable === 'SpectacleBarcode') {
      this.SpectacleData = true;
    }
    if (this._InventoryCommonService.barCodeLable === 'ContactBarcode') {
      this.ContactData = true;
    }
    if (this._InventoryCommonService.barCodeLable === 'OtherBarcode') {
      this.OthersData = true;
    }
    if (this._InventoryCommonService.barCodeLable === 'LensTreatmentBarcode') {
      this.OthersData = true;
    }
  }

  /**
   * on init for initilizing methods
   */
  ngOnInit() {
    this.manufacturer = true;
    this.barcode = true;
    this.color = true;
    this.brand = true;
    this.color_code = true;
    this.name = true;
    this.eye = true;
    this.bridge = true;
    this.temple = true;
    this.Columns = '4';
    this.ColumnsSpacing = '';
    this.TopMargin = '';
    this.RightMargin = '';
    this.Height = '';
    this.Rows = '';
    this.RowsSpacing = '';
    this.LeftMargin = '';
    this.BottomMargin = '';
    this.Width = '';
    this.StartPosition = '0';
    this.topMargin = '0.5in';
    this.rightMargin = '0.437in';
    this.leftMargin = '0.438in';
    this.bottomMargin = '0.540in';
    this.widthMargin = '1.625in';
    this.heightMargin = '1.625in';
    this.noOfColums = '1.625in';
    this.rowSpacing = '0.042in';
    this.columnSpacing = '0.375in';
    this.dropDownValue = 1;
    this.printStyleClass = 'col col-four';
    this.startPositionValue = 0;
    this.coloumnCalWidth =  '8.5in';
    this.framesSortData = [{ header: 'manufacturer', checked: true },
    { header: 'upc', checked: true },
    { header: 'barcode', checked: true },
    { header: 'color', checked: true },
    { header: 'brand', checked: true },
    { header: 'color_code', checked: true },
    { header: 'name', checked: true },
    { header: 'eye', checked: true },
    { header: 'bridge', checked: true },
    { header: 'temple', checked: true }];
    this.barcodeGridData = this._InventoryCommonService.barCodelist;
    this._InventoryCommonService.barcodeLabelsData().subscribe((element: any[]) => {
      this.barCodeLabelDropData = element;
      this.Columns = this.barCodeLabelDropData[0].columns;
      this.ColumnsSpacing = this.barCodeLabelDropData[0].columns_spacing;
      this.TopMargin = this.barCodeLabelDropData[0].top_margin;
      this.RightMargin = this.barCodeLabelDropData[0].right_margin;
      this.Height = this.barCodeLabelDropData[0].height;
      this.Rows = this.barCodeLabelDropData[0].rows;
      this.RowsSpacing = this.barCodeLabelDropData[0].rows_spacing;
      this.LeftMargin = this.barCodeLabelDropData[0].left_margin;
      this.BottomMargin = this.barCodeLabelDropData[0].bottom_margin;
      this.Width = this.barCodeLabelDropData[0].width;
      console.log(element);
    });
    this.excludeLableMargin = [];
    for (let i = 0; i <= this.barcodeGridData.length; i++) {
      if (i % 4 == 0 && i != 0) {
        this.excludeLableMargin[i - 1] = '0in';
      } else if (i != 0) {
        this.excludeLableMargin[i - 1] = this.columnSpacing;
      }
    }

  }
 
  /**
   * Prints bar code labels component 
   * for printing the barcode labels
   */
  print(): void {

    let printContents;
    let popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
          <html>
            <head>
              <style>
              .test {
                margin: 20px;
              }

                body {
                  margin: 0px;
                  padding: 0px;
                    font-family: Arial, Helvetica, "sans-serif";
                  font-size: 12pt;
                }
                * {
                  box-sizing: border-box;
                }
                  .container {
                  width: 95%;
                  margin: 15px auto;
                  padding: 10px;
                  height: 100%;
                }
                .col {
                  margin: 0;
                  padding: 0;
                  list-style: none;
                }
  
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
    );
    popupWin.document.close();
  }
  /**
   * Deliverys popup
   * for showing setting options for barcodelabels
   */
  deliveryPopup() {
    this.display = true;
  }
  /**
   * Dropdowns change
   * for setting the barcode labels 
   */
  dropdownChange() {
    this.dropDownValue;
    this.barCodeLabelDropData.forEach((element, index) => {
      if (element.id === this.dropDownValue) {
        this.Columns = element.columns;
        this.ColumnsSpacing = element.columns_spacing;
        this.TopMargin = element.top_margin;
        this.RightMargin = element.right_margin;
        this.Height = element.height;
        this.Rows = element.rows;
        this.RowsSpacing = element.rows_spacing;
        this.LeftMargin = element.left_margin;
        this.BottomMargin = element.bottom_margin;
        this.Width = element.width;

      }
    });
  }
  /**
   * Determines whether restor click on
   * for restoring the barcode label size
   */
  onRestorClick() {
    this.barCodeLabelDropData.forEach(element => {
      if (element.id === this.dropDownValue) {

        this.Columns = element.columns;
        this.ColumnsSpacing = element.columns_spacing;
        this.TopMargin = element.top_margin;
        this.RightMargin = element.right_margin;
        this.Height = element.height;
        this.Rows = element.rows;
        this.RowsSpacing = element.rows_spacing;
        this.LeftMargin = element.left_margin;
        this.BottomMargin = element.bottom_margin;
        this.Width = element.width;
        this.StartPosition = '0';

      }
    });
  }

  /**
   * Determines whether ok click on
   * for getting values from setting popup
   */
  onOkClick() {
    let margin = this.LeftMargin / 1000 + this.RightMargin / 1000 + this.ColumnsSpacing / 1000;
    const numberval = 8.5 - margin;
    //  this.ColumnsSpacing =
    this.noOfColums = numberval / this.Columns + 'in';
    this.topMargin = this.TopMargin / 1000 + 'in';
    this.rightMargin = this.RightMargin / 1000 + 'in';
    this.heightMargin = this.Height / 1000 + 'in';
    // this.Rows =
    this.rowSpacing = this.RowsSpacing / 1000 + 'in';
    this.columnSpacing = this.ColumnsSpacing / 1000 + 'in';
    // this.noOfColums = this.noOfColums - this.columnSpacing;
    this.leftMargin = this.LeftMargin / 1000 + 'in';
    this.bottomMargin = this.BottomMargin / 1000 + 'in';
    this.widthMargin = this.Width / 1000 + 'in';
    this.display = false;

    console.log($("#printsection").width());
    console.log($("#printsection").height());
    this.excludeLableMargin = [];
    for (let i = 0; i <= this.barcodeGridData.length; i++) {
      if (i % this.Columns == 0 && i != 0) {
        this.excludeLableMargin[i - 1] = '0in';
      } else if (i != 0) {
        this.excludeLableMargin[i - 1] = this.columnSpacing;
      }
    }
    console.log(this.excludeLableMargin);
    const ColumnWidht = this.Columns * this.Width / 1000;
    const tightMargin = this.Columns - 1 * this.ColumnsSpacing / 1000;
    const totalWidth = ColumnWidht + tightMargin + this.LeftMargin / 1000 + this.RightMargin / 1000;
    if (totalWidth <= 8.5) {
      this.coloumnCalWidth = ColumnWidht + tightMargin + 'in';
    } else {
      this.coloumnCalWidth = '8.5in';
    }

  }
  /**
   * Closeclicks bar code labels component
   * for closing setting popup
   */
  closeclick() {
    this.display = false;
  }


  /**
   * Numbers only
   * @param event for getting keypress value
   * @returns true if only 
   */
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  /**
   * Positions apply for Right Margin
   */
  positionApply() {
    this.layOutClick();
    if (this.startPositionValue > 0) {
      for (let i = 1; i <= this.startPositionValue; i++) {
        this.barcodeGridData.unshift({ id: 0 });
      }
      for (let i = 0; i <= this.barcodeGridData.length; i++) {
        if (i % this.Columns == 0 && i != 0) {
          this.excludeLableMargin[i - 1] = '0in';
        } else if (i != 0) {
          this.excludeLableMargin[i - 1] = this.columnSpacing;
        }
      }
    }

  }

  /**
   * Laysoutclick for getting origin layout 
   */
  layOutClick() {
    for (var i = this.barcodeGridData.length - 1; i--;) {
      if (this.barcodeGridData[i].id == 0) {
        this.barcodeGridData.splice(i, 1);
      }
    }
  }

  /**
   * Checks box click
   * @param headername getting clicked value name
   * @param event for getting checked status
   */
  checkBoxClick(headername, event) {
    if (headername.header == 'manufacturer') {
      this.manufacturer = event.checked;
    }
    if (headername.header == 'barcode') {
      this.barcode = event.checked;
    }
    if (headername.header == 'color') {
      this.color = event.checked;
    }
    if (headername.header == 'brand') {
      this.brand = event.checked;
    }
    if (headername.header == 'color_code') {
      this.color_code = event.checked;
    }
    if (headername.header == 'name') {
      this.name = event.checked;
    }
    if (headername.header == 'eye') {
      this.eye = event.checked;
    }
    if (headername.header == 'bridge') {
      this.bridge = event.checked;
    }
    if (headername.header == 'temple') {
      this.temple = event.checked;
    }
  }
  /**
   * Saves as for saving grid data as pdf
   */
  saveAs() {
    html2canvas(document.querySelector("#printsection")).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/png')
      console.log(contentDataURL);
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      let pdf = new jspdf(); // A4 size page of PDF  
      var position = 40;
      
      pdf.addImage(contentDataURL, 'PNG', 1, 10, imgWidth, imgHeight);
      pdf.save(new Date() + 'MYPdf.pdf');
    });

    // html2canvas(document.body).then(function (canvas) {
    //   document.body.appendChild(canvas);
    // });
    // // let doc = new jspdf('p', 'mm', 'a4');
    // // let data = this.pdfContent.nativeElement.innerHTML;
    // // doc.addImage(data.toDataURL("image/jpeg"), 'PNG', 0, 0, 208, 295)
    // // doc.save('test.pdf');
    // var doc = new jspdf();
    // var specialElementHandlers = {
    //   '#editor': function (element, renderer) {
    //     return true;
    //   }
    // };
    // var data = this.pdfContent.nativeElement;
    // doc.fromHTML($(data).html(), -100, 15, {
    //   'width': 170,
    //   'elementHandlers': specialElementHandlers
    // });
    // doc.save('sample-file.pdf');


    // html2canvas(data).then(canvas => {  
    //   // Few necessary setting options  
    //   var imgWidth = 208;   
    //   var pageHeight = 295;    
    //   var imgHeight = canvas.height * imgWidth / canvas.width;  
    //   var heightLeft = imgHeight;  
    //   const contentDataURL = canvas.toDataURL('image/JPEG')  
    //   // let pdf = new jspdf('p', 'cm', 'a4'); // A4 size page of PDF  
    //   let pdf = new jspdf(); // A4 size page of PDF  
    //   var position = 40;  
    //   pdf.addImage(contentDataURL, 'JPEG', 0, 0, imgWidth, imgHeight)  
    //   pdf.save('MYPdf.pdf'); // Generated PDF   
    // });  
  }
}
