import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';



@Component({
    selector: 'app-frames-lens-range',
    templateUrl: 'add-frames-lens-range.component.html'
})
export class AddFramesLensRangeComponent implements OnInit {
    LensRangeData: any[];
    FarmesLensRangeForm: FormGroup;
    LensRangemsg: any = '';
    DeletedId: any;
    AddLensRange = false;
    selectedRangeData: any;
    DisaplayLensRangeData: string;
    @Output() lensrangeOutput = new EventEmitter<any>();
    ngOnInit() {
        this.LensRangeDataDisaplay();
        this.loadRangeFormData();
    }
    constructor(
        private frameService: FramesService,
        private fb: FormBuilder, ) {

    }

    loadRangeFormData() {
        this.FarmesLensRangeForm = this.fb.group({
            sphere_min: '',
            sphere_max: '',
            cylinder_max: '',
            min_pd: '',
            max_pd: '',
            min_add: '',
            max_add: '',
            use_sphere_equivalent: '',
        });
    }
    LensRangeDataDisaplay() {
        let lensData: any = [];
        this.frameService.getFrameLensRange().subscribe(data => {
            lensData = data;
            this.LensRangeData = lensData.data;
        });

    }
    PostLensRange() {
        this.frameService.postLensRangeComponent(this.FarmesLensRangeForm.value).subscribe(data => {
            this.LensRangemsg = data;
            if (this.LensRangemsg.status === 'New Lens Range added successfully') {
                this.LensRangeDataDisaplay();
                this.AddLensRange = false;
            } else {
                alert(this.LensRangemsg.status);
            }
        });
    }
    onRowSelect(selected) {
        // alert(selected)
        this.DeletedId = '';
        this.DeletedId = selected.data.id;
    }
    addClick() {
        this.AddLensRange = true;
    }
    deleteLensRange() {
        this.frameService.frameDeleteLensRange(this.DeletedId).subscribe(data => {
            this.LensRangemsg = data;
            if (this.LensRangemsg.status === 'Frame Lens Range deleted successfully') {
                this.LensRangeDataDisaplay();
            } else {
                alert(this.LensRangemsg.status);
            }
        });
    }
    DoubleClickLensRange(data) {
        this.selectedRangeData = data;
        this.lensrangeOutput.emit(this.selectedRangeData);
        // this.frameService.lensRangeDataSub.next(this.selectedRangeData);
    }
    lensRangeClose() {
        this.lensrangeOutput.emit('close');
    }

}

