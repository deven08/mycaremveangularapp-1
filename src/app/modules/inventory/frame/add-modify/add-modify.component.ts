import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { SortEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { InventoryService } from '@services/inventory/inventory.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameBrand } from '@app/core/models/inventory/Brands.model';
import { FrameColorData } from '@app/core/models/inventory/Colors.model';
import { Frame } from '@app/core/models/inventory/frames.model';
import { CommonValidator } from '@app/shared/validator/common-validator';
import { FramesLensMaterials, LensMaterials, StylesList, StyleMaterialDataList } from '@app/core/models/inventory/frameLensMaterial.model';
import { GetProcedureCode, Data } from '@app/core/models/inventory/inventoryItems.model';
import {
    ManufacturerDropData,
    LensColorMaster,
    CommissionStructure,
    CommissionType,
    FrameSupplier,
    FrameTypeMaster,
    FrameShapeMaster,
    FrameGender,
    Material,
    InvUpcType,
    FrameSource,
    FrameStyleMaster,
    LocationsMaster,
    TransactionTypeMaster,
    RimType,
    FrameUsage,
    FrameBrands,
    MasterDropDown
} from '@app/core/models/masterDropDown.model';
import { LenstreatmentService } from '@app/core/services/inventory/lenstreatment.service';

@Component({
    selector: 'app-frame-add-modify',
    templateUrl: 'add-modify.component.html'
})
export class AddModifyComponent implements OnInit {
    LensRangeDataDropdown: boolean;
    selectedRangeData: any;
    DisaplayLensRangeData: string;
    LensrangeSubscribtion: Subscription;
    FramedataSubscribtion: Subscription;
    selectedFramedata: any;
    StockDetails: any = [];
    gridFrameTransactionDetails: any = [];
    duplicate: any[];
    subscriptionOrderType: any;
    savelensReqbody: any[];
    frameDoubleClickId: any = '';
    frameDoubleClick: Subscription;
    selectFrameSidebar: boolean;
    selectLensRange = false;
    frameAddTransaction = false;
    FarmesForm: FormGroup;
    Accesstoken = '';
    public manufacturerlist: Manufacturer[];
    public Brandlist: FrameBrand[];
    public framedata: Frame[];
    public materiallist: any[];
    public supplierslist: any[];
    Manufacturers: any[];
    private locationlist: any[];
    Brands: any[];
    /**
     * Colors  of add modify component
     * holding list frame colorId, colorName
     */
    colors: Array<MasterDropDown>;
    /**
     * Suppliers  of add modify component
     */
    suppliers: Array<object>;
    materials: any = [];
    uploadedTraceFiles: any[] = [];
    postframes: any;
    price: any;
    val2: any = true;
    @ViewChild('framesAlert') FramesAlerts: DialogBoxComponent;
    @ViewChild('confirmdialog') ConfirmDialogs: DialogBoxComponent;
    locations: Array<LocationsMaster>;
    activestatus: any[];
    targetCars: any[];
    styles = [];
    lenscolor: Array<object>;
    stylcopy: any[];
    stylecheckedvalueobj: any;
    valuestopushinstylecheckbox: any;
    selectedUserRoles: SelectItem[];
    upcdisable = false;
    transactionsBtnHide = false;
    framesTypeDropDown = [];
    framesShapeDropDown = [];
    framesRimType = [];
    framesSource = [];
    framesUsage = [];
    framesUpcType = [];
    cities: SelectItem[];
    activatedeacticeframesvalues = [];
    genderDropDown: any = [];
    subscriptionStockDetailsUpdate: Subscription;
    UpdateStock: any;
    /**
     * Type of transaction data of add modify component
     */
    typeOfTransactionData: Array<object>;
    transacctionStockDetails: any = [];
    LensRangeData: any;
    @Input() locationItem: any;
    @Input() imageItem: any;
    locationNameData: any[] = [];
    locationGridData: any[] = [];
    url: '';
    FilteredTrasaction: any = [];
    uploadedFiles: any[] = [];
    TypeDropData: any[] = [];
    StructureDropData: any[] = [];
    listImages: any[] = [];
    getlistImage: any[] = [];
    // OldLoginToken = '';
    traceFileDelete = '';
    traceFileView = '';
    traceFileViewSidenav = false;
    /**
     * Lens material of add modify component
     * For Handling lens material details
     */
    lensMaterial: any = [];
    @Output() addModifyOutput = new EventEmitter<any>();
    @Output() paginator = new EventEmitter();
    /**
     * Procedurecoderesults  of add modify component
     * stores getting requested payload data
     */
    procedurecoderesults: Array<Data>;
    /**
     * Filtered procedures of add modify component
     * storing procedure data
     */
    filteredProcedures: Array<object>;
    brand: string;
    locationsList: any = [];
    /**
     * Saving pay load of add modify component
     */
    SavingPayLoad: any;
    /**
     * Save update con of add modify component
     */
    saveUpdateCon: boolean;
    procedureCodes: any = [];
    frameTrasactionItemId: any;

    /**
     * Valuestopushin materialcheckbox here we can load the items which are already saved for that item
     */
    valuestopushinMaterialcheckbox: Array<StyleMaterialDataList>;

    /**
     * Materialcheckedvalueobj  is the response of the materials fetch api to bind the data to the materials selected.
     */
    materialcheckedvalueobj: FramesLensMaterials;
    constructor(
        private _fb: FormBuilder,
        private frameService: FramesService,
        private inventoryService: InventoryService,
        private error: ErrorService,
        private dropdownService: DropdownService,
        public utility: UtilityService,
        private lensTreatmentService: LenstreatmentService
    ) {



    }



    ngOnInit() {
        // alert(this.frameService.editframeid)
        // this.listImages = [];
        // this.getlistImage = [];
        // this.frameService.framesListImages().subscribe(data => {
        //     this.listImages = data['result']['Optical']['GetImages'];
        //     for (var i = 0; i <= this.listImages.length - 1; i++) {
        //         this.frameService.framesGetImages(this.listImages[i].Id, this.listImages[i].ItemId).subscribe(Value => {
        //             this.getlistImage.push(Value['result']['Optical']['GetImage'][0]);
        //             //     this.sanitizer.bypassSecurityTrustResourceUrl(this.getlistImage[0].Image);
        //             //    // this.getlistImage[i].Image.toString();
        //         })
        //     }
        // }){ 'Id': 'Male', 'Name': 'Male' }, { 'Id': 'Female', 'Name': 'Female' }, { 'Id': 'Unknown', 'Name': 'Unknown' }


        this.loadFramesShape();
        // this.LoadmaterialDetails();
        this.loadFramesType();
        this.Loadmasters();
        this.GetSupplierDetails();
        this.Loadformdata();
        this.LoadBrands();
        this.LoadColors();
        this.LoadLensmaterial();
        this.Loadmaterials();
        this.LoadLocations();
        this.LoadGender();
        this.lensTreatmentService.getLensTreatmentProcedures().subscribe(data => {
            this.procedureCodes = data['data'];
        })
        this.cities = [
            { label: 'Select City', value: null },
            { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
            { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
            { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
            { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } }
            // {label:'Paris', value:{id:5, name: 'Paris', code: 'PRS'}}
        ];

        this.targetCars = [];

        // this.materials = [
        //     { name: 'Plastic (Index <=1.530)' },
        //     { name: 'Plastic (Height Index <=1.530' },
        //     { name: 'Glass (Height Index <=1.530' },
        //     { name: 'Plastic Mid Index' },
        //     { name: 'Glass' },
        //     { name: 'Trivex' },
        //     { name: 'CR 39' },
        //     { name: 'Glass 1.60 Hi-Index' },
        //     { name: '1.67 Hi-Index' }
        // ];
        this.LoadLensStyle();
        // let locatioData: any = [];
        if (this.locationItem !== undefined) {
            this.locationNameData = this.dropdownService.inventoryLocation;
            if (this.utility.getObjectLength(this.locationNameData) === 0) {
                this.dropdownService.getInventtoryLocations().subscribe(data => {
                    this.locationNameData = data['result']['Optical']['Locations'];
                    this.dropdownService.inventoryLocation = data['result']['Optical']['Locations'];
                    this.inventoryService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                        this.locationGridData = data['data'];
                        this.locationGridData.forEach(element => {
                            for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                if (element.loc_id === this.locationNameData[i].Id) {
                                    element.loc_id = this.locationNameData[i].Name;
                                }
                            }
                        });
                    });
                })
            } else {
                this.inventoryService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                    this.locationGridData = data['data'];
                    this.locationGridData.forEach(element => {
                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                            if (element.loc_id === this.locationNameData[i].Id) {
                                element.loc_id = this.locationNameData[i].Name;
                            }
                        }
                    });
                });
            }


            // this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
            //     locatioData = DataSet;
            //     this.locationNameData = locatioData.result.Optical.Locations;
            //     this.lensTreatmentService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
            //         this.locationGridData = data['data'];
            //         this.locationGridData.forEach(element => {
            //             for (let i = 0; i <= this.locationNameData.length - 1; i++) {
            //                 if (element.loc_id === this.locationNameData[i].Id) {
            //                     element.loc_id = this.locationNameData[i].Name;
            //                 }
            //             }
            //         });

            //     });
            // });

        }

        // this.frameService.framesTraceAccessToken().subscribe(data => {
        //     this.OldLoginToken = data['result']['Token'];
        // });
        // this.Manufacturers = [];
        // this.Manufacturers = this._DropdownsPipe.manufacturersDropData();
        // this.Brands = [];
        // this.Brands = this._DropdownsPipe.brandsDropData();
        // this.Colors = [];
        // this.Colors = this._DropdownsPipe.coloursDropData();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
        // this.framesUsage = [];
        // this.framesUsage = this._DropdownsPipe.usageDropdown();
    }
    // disableButton = this.FarmesForm.dirty;
    /**
     * Loads colors
     * @returns Color list details
     */
    LoadColors() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                                this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }

    /**
     * Loads gender dropdown values
     */
    LoadGender() {
        this.genderDropDown = this.frameService.gender;
        if (this.utility.getObjectLength(this.genderDropDown) === 0) {
            this.frameService.getGender().subscribe(
                (values: FrameGender) => {
                    try {
                        const dropData = values.data;
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.genderDropDown.push(dropData[i]);
                        }
                        this.frameService.gender = this.genderDropDown;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loadformdatas add modify component for formgroup object
     */
    Loadformdata() {
        this.frameTrasactionItemId = this.inventoryService.FortrasactionItemID;
        this.upcdisable = false;
        this.Accesstoken = ''
        // this._StateInstanceService.accessTokenGlobal;
        this.FarmesForm = this._fb.group({
            accessToken: this.Accesstoken,
            module_type_id: '1',
            upc_code: ['', [Validators.required, CommonValidator.numberValidator, Validators.maxLength(14), Validators.minLength(1)]],
            name: ['', [Validators.required]],
            manufacturer_id: ['', [Validators.required]],
            vendor_id: '',
            brand_id: ['', [Validators.required]],
            frame_shape: '',           /////////////
            frame_style: '',
            a: '',
            b: '',
            ed: '',
            dbl: '0',
            temple: '',
            bridge: '',
            fpd: '',
            colorid: '',
            color_code: '',
            retail_price: '',
            frameId: '0',
            supplierId: '0',
            material_id: ['', [Validators.required]],
            locationId: '',
            id: '',
            wholesale_cost: '',
            profit: '',                           /////////////
            eyesize: '',
            rimtypeid: '',
            sourceid: '',
            frameusageid: '',
            upctypeid: '',
            min_qty: '',
            max_qty: '',
            gender: null,
            notes: '',
            sku: '',
            cost: '',
            groupcost: [null, [ Validators.min(1), CommonValidator.numberValidator]],
            msrp: '',
            procedure_code: '',
            selectedstyles: '',
            inventory: false,
            lenscolor: '',
            del_status: '',
            structure_id: '',
            commission_type_id: '',
            type_id: '',
            DisaplayLensRangeData: '',
            // frame_lens_range_id: "",
            frame_lens_range_id: '',
            trace_file: '',
            FrameLensRangeName: "",
            // frameId:""
            ida_id: '',
            amount: '',
            gross_percentage: '',
            spiff: '',
            SelectedMaterials: '',
            imagesList: this._fb.array([
                //  this.initlanguage(),
            ]),
        });
        const selectedFrameid = this.frameService.editframeid || this.frameService.editframeidDoubleClick;
        if (selectedFrameid) {
            this.upcdisable = true;
            this.updateFormData(selectedFrameid)
        }
        const duplicateId = this.frameService.editDuplicateId;
        if (duplicateId) {
            this.upcdisable = false;
            this.updateFormData(duplicateId)
        }




    }
    /**
     * Updates form data
     * @param selectedFrameid for getting selected item data
     */
    updateFormData(selectedFrameid) {
        let frameGet: any = [];
        this.frameService.getframes(selectedFrameid).subscribe(
            data => {
                frameGet = data;
                this.lensRangeDetails(frameGet)
                this.FarmesForm = this._fb.group({
                    accessToken: this.Accesstoken,
                    upc_code: [frameGet.upc_code, [Validators.required]],
                    name: [frameGet.name, [Validators.required]],
                    manufacturer_id: [frameGet.manufacturer_id, [Validators.required]],
                    vendor_id: frameGet.vendor_id === 0 ? '' : frameGet.vendor_id,
                    brand_id: [frameGet.brand_id.toString(), [Validators.required]],
                    frame_shape: frameGet.frame_shape === '' ? '' :
                        parseInt(frameGet.frame_shape, 10),                    //////////
                    frame_style: frameGet.frame_style,
                    a: frameGet.a,
                    b: frameGet.b,
                    ed: frameGet.ed,
                    dbl: frameGet.dbl === 0 ? null :
                        parseInt(frameGet.dbl, 10),
                    eyesize: frameGet.eyesize,
                    temple: frameGet.temple,
                    bridge: frameGet.bridge,
                    fpd: frameGet.fpd,
                    colorid: frameGet.colorid === 0 ? '' : frameGet.colorid,
                    color_code: frameGet.color_code,
                    retail_price: frameGet.retail_price,
                    type_id: frameGet.type_id === '' ? '' :
                    parseInt(frameGet.type_id, 10),
                    supplierId: '',
                    material_id: [frameGet.material_id === '' ? null :
                        parseInt(frameGet.material_id), [Validators.required]],
                    locationId: '',                                                         ///////////////////////
                    id: frameGet.id,
                    SelectedMaterials: '',
                    wholesale_cost: frameGet.wholesale_cost,
                    profit: '',                                                               /////////////////////
                    rimtypeid: frameGet.rimtypeid === 0 || frameGet.rimtypeid === null ? '' :
                        parseInt(frameGet.rimtypeid, 10),
                    sourceid: frameGet.sourceid === 0 || frameGet.sourceid === null ? '' :
                        parseInt(frameGet.sourceid, 10),
                    frameusageid: frameGet.frameusageid === 0 || frameGet.frameusageid === null ? '' :
                        parseInt(frameGet.frameusageid, 10),
                    upctypeid: frameGet.upctypeid === 0 || frameGet.upctypeid === null ? '' :
                        parseInt(frameGet.upctypeid, 10),
                    min_qty: frameGet.min_qty,
                    max_qty: frameGet.max_qty,
                    gender: frameGet.gender,
                    notes: frameGet.notes,
                    sku: frameGet.sku,
                    cost: '',
                    groupcost: frameGet.groupcost === '' || frameGet.groupcost === null ? '' :
                        parseInt(frameGet.groupcost, 10),
                    msrp: frameGet.msrp === '' || frameGet.msrp === null ? '' :
                        parseInt(frameGet.msrp, 10),
                    procedure_code: frameGet.procedure_code === '' ? null : parseInt(frameGet.procedure_code),
                    selectedstyles: '',
                    inventory: frameGet.inventory === 1 ? true : false,
                    //   inventory:"false",
                    lenscolor: frameGet.lenscolor === '0' ? '' : parseInt(frameGet.lenscolor, 10),
                    del_status: frameGet.status.toString(),
                    structure_id: frameGet.structure_id === null ? '' : frameGet.structure_id,
                    commission_type_id: frameGet.commission_type_id === 0 ? '' : frameGet.commission_type_id,
                    frame_lens_range_id: '',
                    DisaplayLensRangeframeGet: '',
                    trace_file: '',
                    ida_id: frameGet.ida_id,
                    imagesList: this._fb.array([]),
                    FrameLensRangeName: "",
                    // frame_lens_range_id: '',
                    amount: frameGet.amount,
                    gross_percentage: frameGet.gross_percentage,
                    spiff: frameGet.spiff
                    // frame_lens_range_id: data["result"]["Optical"]["Frames"][0]["frame_lens_range_id"],
                    // FrameLensRangeName: data["result"]["Optical"]["Frames"][0]["FrameLensRangeName"]

                });

                     
                    this.filterProcedureCodeDetails(frameGet.procedure_code);
                    if (this.upcdisable == false) {
                        this.FarmesForm.controls.upc_code.patchValue('');
                        this.FarmesForm.controls.id.patchValue('');
                    }
                    this.imageListing();
                    this.traceFileGet();
                    this.LoadLensMaterialsSelected(frameGet.id);
                    this.LoadLensStyleCheckboxsavedvalues(frameGet.id);
                    this.LoadPrice(frameGet.id);

                });
         
    }
    /**
     * Lens range details
     * @param {any} frameGet for frame list by selected id
     */
    lensRangeDetails(frameGet) {
        if (frameGet.frame_lens_range_id !== '' && frameGet.frame_lens_range_id !== null) {
            // alert(frameGet["result"]["Optical"]["Frames"][0]["frame_lens_range_id"])
            this.frameService.getLensRangeDetail(frameGet.frame_lens_range_id).subscribe
                (lensData => {
                    this.selectedRangeData = lensData;

                    this.DisaplayLensRangeData = '';
                    this.DisaplayLensRangeData = this.selectedRangeData.sphere_max + '  ' + 'to' + '  ' +
                        this.selectedRangeData.sphere_min;
                    //  alert(this.selectedRangeData)
                    this.FarmesForm.controls.frame_lens_range_id.patchValue(this.selectedRangeData.id);
                    // this.FarmesForm.controls.FrameLensRangeName.patchValue('apple');
                    const displayData = this.DisaplayLensRangeData;
                    this.FarmesForm.controls.FrameLensRangeName.patchValue(displayData);

                });
        }
    }

    /**
     * Images listing
     */
    imageListing() {
        this.listImages = [];
        this.getlistImage = [];
        let frameDataList: any = [];
        let frameImage: any = [];
        this.frameService.framesListImages(this.imageItem).subscribe(frameListData => {
            frameDataList = frameListData;
            if (frameDataList.result.Optical !== 'No record found') {
                this.listImages = frameDataList.result.Optical.GetImages;
                for (let i = 0; i <= this.listImages.length - 1; i++) {
                    this.frameService.framesGetImages(this.listImages[i].Id, this.listImages[i].ItemId).subscribe(images => {
                        frameImage = images;
                        this.getlistImage.push(frameImage.result.Optical.GetImage[0]);
                        const control = this.FarmesForm.controls.imagesList as FormArray;
                        control.push(
                            this._fb.group({
                                Id: frameImage.result.Optical.GetImage[0].Id,
                                Image: 'data:image/JPEG;base64,' + frameImage.result.Optical.GetImage[0].Image
                            })
                        );

                    });
                }

            } else {
                // const control = <FormArray>this.FarmesForm.controls['imagesList'];
                // control.push(this.initlanguage());
            }
        });
    }
    traceFileGet() {
        this.traceFileDelete = '';
        let traceDataValues: any = [];
        this.frameService.framesTraceFileGet('', this.imageItem).subscribe(traceData => {
            traceDataValues = traceData;
            if (traceDataValues.result.Optical.name) {
                this.FarmesForm.controls.trace_file.patchValue(traceDataValues.result.Optical.name);
                this.traceFileDelete = traceDataValues.result.Optical.traceFile;
                this.traceFileView = traceDataValues.result.Optical.traceFile;
            }
        });
    }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }


    /**
     * Checkminmaxvalues add modify component
     * @returns for min and max validations
     */
    checkminmaxvalue(): boolean {
        const minvalue = parseInt(this.FarmesForm.controls.min_qty.value, 10);
        const maxvalue = parseInt(this.FarmesForm.controls.max_qty.value, 10);
        if (maxvalue != null && maxvalue !== 0 && minvalue != null && minvalue !== 0 && minvalue > maxvalue) {
            this.error.displayError({
                severity: 'warn', summary: 'Warning Message',
                detail: 'Min qty should be less then Max qty/Max qty should be Grater then Min qty.'
            });

            // this.WarningShow('Min qty should be less then Max qty/Max qty should be Grater then Min qty.');



            return false;


        }
    }

    /**
     * Profitcalculates add modify component
     */
    profitcalculate() {
        if (this.FarmesForm.controls.wholesale_cost.value !== '' || this.FarmesForm.controls.wholesale_cost.value != null ||
            this.FarmesForm.controls.retail_price.value !== '' || this.FarmesForm.controls.retail_price.value != null) {
            const diffvalue = this.FarmesForm.controls.retail_price.value - this.FarmesForm.controls.wholesale_cost.value;
            this.FarmesForm.controls.profit.patchValue(diffvalue.toFixed(2));
        }
        if (this.FarmesForm.controls.wholesale_cost.value === '' && this.FarmesForm.controls.retail_price.value === '') {
            this.FarmesForm.controls.profit.patchValue('');
        }
    }
    /**
     * Loads price
     * @param [id] for getting item id
     */
    LoadPrice(id = '') {
        if (id !== '') {
            this.frameService.getframeRetailprice(id).subscribe(

                res => {
                    this.price = res;
                    const value = this.price.retailPrice === 0 ? '' : this.price.retailPrice;
                    this.FarmesForm.controls.retail_price.patchValue(value);
                    // const cost = this.price.wholesale_cost === 0 ? '' : this.price.wholesale_cost;
                    // this.FarmesForm.controls.wholesale_cost.patchValue(cost);
                    this.profitcalculate();

                });
        }
    }

    /**
     * Filters procedures
     *  @returns filtering procedure codes details based on hiting alphabets
     */
    filterProcedures(event) {
        this.procedurecoderesults = [];
        const values = {

            filter: [{
                field: 'cpt_prac_code',
                operator: 'LIKE',
                value: '%' + event.query.toLowerCase() + '%'
            }]
        };
        this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
            this.procedurecoderesults = data.data;
            this.filteredProcedures = [];
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.procedurecoderesults.length; i++) {
                const procedure = this.procedurecoderesults[i].cpt_prac_code;
                const procedureId = this.procedurecoderesults[i].cpt_fee_id;
                if (procedure.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                    this.filteredProcedures.push({ Id: procedureId, Name: procedure });
                }
            }
            if (this.filteredProcedures.length === 0) {
                this.error.displayError({
                    severity: 'error',
                    summary: 'error Message', detail: 'No data found.'
                });

            }
        });
    }


    /**
     * Filters procedure code details
     * getting seleted item procrdure details
     */
    filterProcedureCodeDetails(procedureValue) {
        this.procedurecoderesults = [];
        if (procedureValue !== null) {
        const values = {

            filter: [{
                field: 'cpt_fee_id',
                operator: '=',
                value: procedureValue
            }]
        };
        this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
            this.procedurecoderesults = data.data;
            this.FarmesForm.controls.procedure_code.patchValue({
                Id: this.procedurecoderesults[0].cpt_fee_id, Name: this.procedurecoderesults[0].cpt_prac_code
            });
        });
        }
    }
    // Loadlocations() {
    //     this.locations = [];
    //     this.frameService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];
    //             this.locations.push({ Id: '', Name: 'Select Location' });
    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {

    //                     if (this.locationlist[i]['Name'] !== '') {
    //                         this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                     }

    //                 }
    //             }
    //         });
    // }
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }


    Loadstatus() {
        this.activestatus = [];
        this.activestatus.push({ Id: '0', Name: 'Active' });
        this.activestatus.push({ Id: '1', Name: 'Discontinue' });

    }


    Loadmasters() {
        // this.Loadmaterials();
        this.loadRimType();
        this.loadUsage();
        this.loadSource();
        this.loadUpcType();
        this.LoadLensColor();
        this.Loadstatus();
        this.loadStructures();
        this.loadCommissionType();
        this.LoadManufacturers();
    }
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loads 
     * @returns loads structures data 
     */
    loadStructures() {
        this.StructureDropData = this.inventoryService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.inventoryService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this.inventoryService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }

    /**
     * Loads commission type
     *  @returns loads  commition type data
     */
    loadCommissionType() {
        this.TypeDropData = this.inventoryService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.inventoryService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id, Name: dropData[i].commissiontype });
                    }
                    this.inventoryService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    /**
     * Loads frames type
     * @returns loads frames type data
     */
    loadFramesType() {
        this.framesTypeDropDown = this.frameService.framesTypeDropDownData;
        if (this.utility.getObjectLength(this.framesTypeDropDown) === 0) {
            this.frameService.getFramesType().subscribe((values: FrameTypeMaster) => {
                try {
                    const dropData = values.data;
                    this.framesTypeDropDown.push({ Id: '', Name: 'Select Frametype' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesTypeDropDown.push({ Id: dropData[i].id, Name: dropData[i].type_name });
                    }
                    this.frameService.framesTypeDropDownData = this.framesTypeDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    /**
     * Loads lens style
     * @returns laoding lensStyleDetails
     */
    LoadLensStyle() {
        this.stylcopy = this.dropdownService.frameStyles;
        if (this.utility.getObjectLength(this.stylcopy) === 0) {
            this.dropdownService.getFrameLensStyle().subscribe((values: FrameStyleMaster) => {
                try {
                    const dropData = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].style_name, value: dropData[i].id });
                    }
                    this.stylcopy = this.styles;
                    this.dropdownService.frameStyles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads lens color
     * @returns lenscolors data
     */
    LoadLensColor() {
        this.lenscolor = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.lenscolor) === 0) {
            try {
                this.dropdownService.getFrameLenscolor().subscribe((values: LensColorMaster) => {
                    const dropData = values.data;
                    this.lenscolor.push({ Id: '', Name: 'Select Lens Color' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        {
                            this.lenscolor.push({ Id: dropData[i].id, Name: dropData[i].colorname });
                        }
                        this.dropdownService.lensColors = this.lenscolor;
                    }
                });
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        }
    }

    keyPress(event: any) {
        const pattern = /[0-9]/;

        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    /**
     * Loads rim Type
     * @returns rimtype data
     * 
     */
    loadRimType() {
        this.framesRimType = this.dropdownService.rimType;
        if (this.utility.getObjectLength(this.framesRimType) === 0) {
            this.dropdownService.getRimType().subscribe((values: RimType[]) => {
                try {
                    this.framesRimType.push({ Id: '', Name: 'Select Rim Type', ColorCode: '' });
                    for (let i = 0; i <= values.length - 1; i++) {
                        this.framesRimType.push({ Id: values[i].id, Name: values[i].rimtype, ColorCode: values[i].colorcode });
                    }
                    this.dropdownService.rimType = this.framesRimType;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    loadUsage() {
        this.framesUsage = this.dropdownService.framesUsage;
        if (this.frameService.framesUsageData.length === 0) {
            this.frameService.getUsage().subscribe((values: FrameUsage) => {
                try {
                this.framesUsage.push({ Id: '', Name: 'Select Usage' });
                for (let i = 0; i <= values.data.length - 1; i++) {
                    this.framesUsage.push({ Id: values.data[i].id, Name: values.data[i].usagetype });
                }
                this.frameService.framesUsageData = this.framesUsage;
            } catch (error) {
                this.error.syntaxErrors(error);
            }
            });
        }
    }

    // loadUsage() {
    //     this.framesUsage = [];
    //     this.frameService.getUsage().subscribe((data: any[]) => {
    //         data['data'].forEach((element: any) => {
    //             this.framesUsage.push({ Id: element.id, Name: element.usagetype });
    //         });
    //     });

    // }
    /**
     * Loads source
     * @returns source data
     */
    loadSource() {
        this.framesSource = this.inventoryService.frameSourceData;
        if (this.utility.getObjectLength(this.framesSource) === 0) {
            this.frameService.getSource().subscribe((values: FrameSource) => {

                try {
                    const dropData = values.data;
                    this.framesSource.push({ Id: '', Name: 'Select Source Type' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesSource.push({ Id: dropData[i].id, Name: dropData[i].sourcename });
                    }
                    this.framesSource = this.inventoryService.frameSourceData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }
    }

    /**
     * Loads upc type
     * @returns upctype data
     */
    loadUpcType() {
        this.framesUpcType = [];
        this.framesUpcType = this.inventoryService.upcTypeData;
        if (this.utility.getObjectLength(this.framesUpcType) === 0) {
            // if (this.utility.getObjectLength(this.TypeDropData) === 0) {
                this.frameService.getUpcType().subscribe((values: InvUpcType) => {
                    try {
                        const dropData = values.data;
                        this.framesUpcType.push({ Id: '', Name: 'Select UPC Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.framesUpcType.push({ Id: dropData[i].id, Name: dropData[i].upctype });
                        }
                        this.frameService.framesUpcTypeData = this.framesUpcType;

                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
            // }
        }
    }
    /**
     * Loads frames shape
     * @returns frames shape data
     */
    loadFramesShape() {
        // if (this.frameService.framesShapeDropDownData.length === 0) {
        this.framesShapeDropDown = [];
        this.framesShapeDropDown = this.inventoryService.frameShape;
        if (this.utility.getObjectLength(this.framesShapeDropDown) === 0) {
            this.frameService.getFramesshape().subscribe((values: FrameShapeMaster) => {
                try {
                    const dropData = values.data;
                    this.framesShapeDropDown.push({ Id: '', Name: 'Select Frameshape' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesShapeDropDown.push({ Id: dropData[i].id, Name: dropData[i].shape_name });
                    }
                    this.frameService.framesShapeDropDownData = this.framesShapeDropDown;

                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }
        // } else {
        //     this.framesShapeDropDown = this.frameService.framesShapeDropDownData;
        // }
    }


    /**
     * Loadmaterials add modify component
     *  @return {Object} for getting material drop data
     */
    Loadmaterials() {
        this.materials = this.dropdownService.framesmaterials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getFrameMaterial().subscribe((values: Material[]) => {
                try {
                    const dropData = values;
                    this.materials.push({ id: '', materialname: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materials.push(dropData[i]);
                    }
                    this.dropdownService.framesmaterials = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }


    }

    /**
     * Gets supplier details
     * @returns supplier data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    onChange(event) {
        alert(event.data.Id);
    }


    customSort(event: SortEvent) {
        event.data.sort((data1, data2) => {
            const value1 = data1[event.field];
            const value2 = data2[event.field];
            let result = null;

            if (value1 == null && value2 != null) {
                result = -1;
            } else if (value1 != null && value2 == null) {
                result = 1;
            } else if (value1 == null && value2 == null) {
                result = 0;
            } else if (typeof value1 === 'string' && typeof value2 === 'string') {
                result = value1.localeCompare(value2);
            } else {
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
            }

            return (event.order * result);
        });
    }
    addFrameAlert() {
        // this.FramesAlerts.dialogBox();
    }

    confirmBox() {
        // this.ConfirmDialogs.dialogBox();
    }
    /**
     * Closes add modifyer
     */
    closeAddModifyer() {
        const itemValue = {
            'NewValue': 'close',
            'Update': '',
            'Data': ''
        };
        this.addModifyOutput.emit(itemValue);
        // this.frameService.framecloseObj.next('');
    }

    LoadLensStyleCheckboxsavedvalues(id = '') {
        if (id !== undefined) {
            this.stylecheckedvalueobj = [];
            this.valuestopushinstylecheckbox = [];
            this.frameService.getFrameLensStylesaved(id).subscribe(itemvalue => {
                this.stylecheckedvalueobj = itemvalue;
                this.stylecheckedvalueobj.forEach(element => {
                    const pushvalue = this.stylcopy.filter(x => x.value === element.lens_style_id);
                    const a = { name: pushvalue[0].name, value: pushvalue[0].value };
                    this.valuestopushinstylecheckbox.push(a);
                });
                this.FarmesForm.controls.selectedstyles.patchValue(this.valuestopushinstylecheckbox);
            });

        }
    }


    /**
     * Savethelensstyles material here we are capturing the item id and item weather item is style or materials 
     * based on that binding the data.
     * @param {number} frameid 
     * @param {string} type 
     */
    savethelensstyleMaterial(frameid: number, type: string) {
        if (frameid) {
            this.savelensReqbody = [];
            if (type === 'style' && this.FarmesForm.controls.selectedstyles.value !== '') {
                this.frameStyleMaterial(this.FarmesForm.controls.selectedstyles.value, type, frameid);
            } else if (type === 'material' && this.FarmesForm.controls.SelectedMaterials.value !== '') {
                this.frameStyleMaterial(this.FarmesForm.controls.SelectedMaterials.value, type, frameid);
            }
        }
    }

    /**
     * Frames style material building the object for the save of the frame lens style material items
     * @param {Array} styleMaterialData 
     * @param {string} type 
     * @param {number} id 
     */
    frameStyleMaterial(styleMaterialData: Array<StyleMaterialDataList>, type: string, id: number) {
        styleMaterialData.forEach(element => {
            this.savelensReqbody.push(element.value);
        });
        if (type === 'style') {
            const activatedata = {
                styleId: this.savelensReqbody
            };
            this.saveLensStyles(activatedata, id);
        }
        if (type === 'material') {
            const activatedata = {
                materials: this.savelensReqbody
            };
            this.saveLensMaterials(activatedata, id);
        }

    }

    /**
     * Saves lens styles here we can able to save the lens styles for the particular item we are saved.
     * @param {object} styleObject;
     * @param {number} itemId;
     */
    saveLensStyles(styleObject: object, itemId: number) {
        this.frameService.saveFrameLensStyle(itemId, styleObject)
            .subscribe((data: StylesList) => {
                try {
                    this.savelensReqbody = [];
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Success Message', detail: 'Frame LensStyle is Updated Successfully'
                    });
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
    }

    /**
     * Saves lens materials here we can able to save the lens materials for the particular item we are saved.
     * @param {object} styleObject 
     * @param {number} itemId 
     */
    saveLensMaterials(styleObject: object, itemId: number) {
        this.frameService.saveFrameLensMaterials(itemId, styleObject)
            .subscribe((data: LensMaterials) => {
                try {
                    this.savelensReqbody = [];
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Success Message', detail: data.status
                    });
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
    }

    /**
     * Activatedeacticeframes add modify component
     * @param frameid acrive and deactive details
     */
    activatedeacticeframes(frameid) {
        if (frameid !== undefined && frameid !== '') {
            this.activatedeacticeframesvalues.push(frameid);
            const activatedata = {
                itemId: this.activatedeacticeframesvalues
            };

            const status = this.FarmesForm.controls.del_status.value;
            // the API has 0 as active so used the same  condtion
            // "0" means  "Active"
            // "1" means  "InActive"
            if (status === 0) {
                this.inventoryService.ActivateInventory(activatedata).subscribe(data => {
                    this.activatedeacticeframesvalues = [];
                });
            }
            if (status === 1) {
                this.inventoryService.DeactivateInventory(activatedata).subscribe(data => {
                    this.activatedeacticeframesvalues = [];
                });
            }
        }
    }

    /**
     * Saveframes add modify component
     * @returns {object} for saving item 
     */
    saveframe() {
        const maxqtyrcheck = this.checkminmaxvalue();
        if (maxqtyrcheck === false) {
            return;
        }
        if (!this.FarmesForm.valid) {
            return;
        }
        const retailPrice = this.FarmesForm.controls.retail_price.value === '' ? 0 : this.FarmesForm.controls.retail_price.value;
        const pricevalue = this.FarmesForm.controls.wholesale_cost.value === '' ? 0 : this.FarmesForm.controls.wholesale_cost.value;
        this.FarmesForm.controls.retail_price.patchValue(retailPrice);
        this.FarmesForm.controls.wholesale_cost.patchValue(pricevalue);
        this.savingReqPayLoad();
        const SaveData = this.utility.formatWithOutControls(this.SavingPayLoad);
        if (this.FarmesForm.controls.id.value == "") {
            this.frameService.SaveFrames(SaveData)
                .subscribe(data => {
                    this.saveUpdateCon = true;
                    this.framesImageSaving(data);
                    this.paginator.emit();

                },
                    error => {
                        if (error.statusText === 'Conflict') {
                            this.error.displayError({
                                severity: 'error',
                                summary: 'Error Message', detail: 'Item with same upc already exists, but it is not the frame.'
                            });
                        } else {
                            if (error.statusText === 'Bad Request') {
                                this.error.displayError({
                                    severity: 'error',
                                    summary: 'Error Message', detail: error.error.Error
                                });
                            } else {
                                this.error.displayError({
                                    severity: 'error',
                                    summary: 'Error Message', detail: 'Please provide valid UPC Code.'
                                });
                            }
                        }
                    }
                );
        } else {
            this.frameService.updateFrames(this.SavingPayLoad, this.FarmesForm.controls.id.value)
                .subscribe(data => {
                    this.saveUpdateCon = false;
                    this.framesImageSaving(data);
                },
                    error => {
                        if (error.statusText === 'Conflict') {
                            this.error.displayError({
                                severity: 'error',
                                summary: 'Error Message', detail: 'Item with same upc already exists, but it is not the frame.'
                            });
                        } else {
                            if (error.statusText === 'Bad Request') {
                                this.error.displayError({
                                    severity: 'error',
                                    summary: 'Error Message', detail: error.error.Error
                                });
                            } else {
                                this.error.displayError({
                                    severity: 'error',
                                    summary: 'Error Message', detail: 'Please provide valid UPC Code.'
                                });
                            }
                        }
                    }
                );
        }

    }

    /**
     * Saving  payload
     */
    savingReqPayLoad() {
        if (this.FarmesForm.controls.inventory.value === true) {
            this.FarmesForm.controls.inventory.patchValue('1');
        } else {
            this.FarmesForm.controls.inventory.patchValue('0');
        }
        this.SavingPayLoad = {
            "module_type_id": '1',
            "upc_code": this.FarmesForm.controls.upc_code.value,
            "manufacturer_id": this.FarmesForm.controls.manufacturer_id.value,
            "brand_id": this.FarmesForm.controls.brand_id.value,
            "name": this.FarmesForm.controls.name.value,
            "colorid": this.FarmesForm.controls.colorid.value,
            "color_code": this.FarmesForm.controls.color_code.value,
            "material_id": parseInt(this.FarmesForm.controls.material_id.value),
            "vendor_id": this.FarmesForm.controls.vendor_id.value == "0" ? '' : this.FarmesForm.controls.vendor_id.value,
            "type_id": this.FarmesForm.controls.type_id.value,
            "rimtypeid": this.FarmesForm.controls.rimtypeid.value == null ? '' : this.FarmesForm.controls.rimtypeid.value,
            "frame_shape": this.FarmesForm.controls.frame_shape.value.toString(),
            "gender": this.FarmesForm.controls.gender.value,
            "sourceid": this.FarmesForm.controls.sourceid.value != null ? this.FarmesForm.controls.sourceid.value.toString() : this.FarmesForm.controls.sourceid.value,
            "eyesize": this.FarmesForm.controls.eyesize.value != null ? this.FarmesForm.controls.eyesize.value.toString() : this.FarmesForm.controls.eyesize.value,
            "a": this.FarmesForm.controls.a.value,
            "b": this.FarmesForm.controls.b.value,
            "ed": this.FarmesForm.controls.ed.value,
            "bridge": this.FarmesForm.controls.bridge.value,
            "temple": this.FarmesForm.controls.temple.value,
            "status": this.FarmesForm.controls.del_status.value == null ? null : this.FarmesForm.controls.del_status.value.toString(),
            "lenscolor": this.FarmesForm.controls.lenscolor.value == null ? null : this.FarmesForm.controls.lenscolor.value.toString(),
            "frameusageid": this.FarmesForm.controls.frameusageid.value == null ? '' : this.FarmesForm.controls.frameusageid.value,
            "procedure_code": this.FarmesForm.controls.procedure_code.value === '' ? null :
                this.FarmesForm.controls.procedure_code.value.Id.toString(),
            "min_qty": this.FarmesForm.controls.min_qty.value,
            "max_qty": this.FarmesForm.controls.max_qty.value,
            "upctypeid": this.FarmesForm.controls.upctypeid.value == null ? '' : this.FarmesForm.controls.upctypeid.value,
            "sku": this.FarmesForm.controls.sku.value,
            "ida_id": this.FarmesForm.controls.ida_id.value,
            "retail_price": this.FarmesForm.controls.retail_price.value,
            "wholesale_cost": this.FarmesForm.controls.wholesale_cost.value,
            "groupcost": this.FarmesForm.controls.groupcost.value,
            "msrp": this.FarmesForm.controls.msrp.value,
            "structure_id": this.FarmesForm.controls.structure_id.value,
            "commission_type_id": this.FarmesForm.controls.commission_type_id.value,
            "amount": this.FarmesForm.controls.amount.value,
            "gross_percentage": this.FarmesForm.controls.gross_percentage.value,
            "spiff": this.FarmesForm.controls.spiff.value,
            "trace_file": this.FarmesForm.controls.trace_file.value,
            "frame_lens_range_id": this.FarmesForm.controls.frame_lens_range_id.value,
            "notes": this.FarmesForm.controls.notes.value,
            "inventory": this.FarmesForm.controls.inventory.value
        };
    }
    /**
     * Frames image saving
     * @param data frame saved data list
     */
    framesImageSaving(data) {
        if (this.imageItem === undefined) {
            if (data['item_Id']) {
                for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                    const formData: FormData = new FormData();
                    formData.append('itemId', data['item_Id']);
                    formData.append('item_pic', this.uploadedFiles[i]);
                    this.frameService.framesImageAdding(formData).subscribe(value => {

                    });
                }
                for (let i = 0; i <= this.uploadedTraceFiles.length - 1; i++) {
                    const formData: FormData = new FormData();
                    formData.append('accessToken', '');
                    formData.append('itemId', data['item_Id']);
                    formData.append('traceFile', this.uploadedTraceFiles[i]);
                    this.frameService.framesTraceFileAdding(formData).subscribe(value => {

                    });
                }
            }
        } else {
            for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                const formData: FormData = new FormData();
                formData.append('itemId', this.imageItem);
                formData.append('item_pic', this.uploadedFiles[i]);
                this.frameService.framesImageAdding(formData).subscribe(value => {

                });
            }
            for (let i = 0; i <= this.uploadedTraceFiles.length - 1; i++) {
                const formData: FormData = new FormData();
                formData.append('accessToken', '');
                formData.append('itemId', this.imageItem);
                formData.append('traceFile', this.uploadedTraceFiles[i]);
                this.frameService.framesTraceFileAdding(formData).subscribe(value => {

                });
            }
        }
        this.postframes = data;
        // if (this.postframes.Success !== undefined && this.postframes.Success === 1) {
        this.activatedeacticeframes(this.postframes['item_Id']);
        this.savethelensstyleMaterial(this.postframes['item_Id'], 'style');
        this.savethelensstyleMaterial(this.postframes['item_Id'], 'material')
        this.error.displayError({
            severity: 'success',
            summary: 'Success Message', detail: this.postframes['status']
        });
        // }
        var filterPayLoad = {
            "filter": [
                {
                    "field": "module_type_id",
                    "operator": "=",
                    "value": "1"
                },
                {
                    "field": "id",
                    "operator": "=",
                    "value": this.postframes['item_Id']
                }

            ]

        }
        this.frameService.getFramesSearchData(filterPayLoad).subscribe(data => {
            if (this.saveUpdateCon == true) {
                const itemValue = {
                    'NewValue': 'New',
                    'Update': this.postframes['item_Id'],
                    'Data': data['data'][0]
                };
                this.addModifyOutput.emit(itemValue)
            } else {
                const itemValue = {
                    'NewValue': '',
                    'Update': this.postframes['item_Id'],
                    'Data': data['data'][0]
                };
                this.addModifyOutput.emit(itemValue)
            }

        })
    }
    // tslint:disable-next-line:use-life-cycle-interface
    ngOnDestroy(): void {
        // this.subscriptionOrderType.unsubscribe();
        // this.subscriptionOrderType.unsubscribe();
        // this.subscriptionStockDetailsUpdate.unsubscribe();

    }
    /**
     * Colors code value
     */
    colorCodeValue() {
        const colorId = this.FarmesForm.controls.colorid.value;
        this.colors.forEach(element => {
            if (colorId === element.Id) {
                this.FarmesForm.controls.color_code.patchValue(element.Name);
            }
        });

    }
    // SuccessShow() {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Frames Saved Successfully' });
    // }
    // ErrorShow() {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please provide valid UPC Code.' });
    // }
    // ConflictShow() {
    //     this.msgs.push({ severity: 'error', summary: 'error Message',
    // detail: 'Item with same upc already exists, but it is not the frame.' });
    // }
    // ErrorShowDetails(value) {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: value });
    // }
    // WarningShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: message });
    // }

    addTransaction() {
        if (this.frameTrasactionItemId !== '') {
            this.frameAddTransaction = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Select any item or save new item for saving transactions'
            });
            this.frameAddTransaction = false;
        }
    }

    onFramesTabsClick(event) {
        if (event.index === 0) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 1) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 2) {
            this.transactionsBtnHide = true;
            // this.typeOfInventoryTrasaction()
            this.filterTransactionOnLocation();
        }
        if (event.index === 3) {
            this.transactionsBtnHide = false;
        }
    }

    // getFramesStockDetails() {
    //     // let SelectedFrameId=localStorage.getItem('FortrasactionItemID')
    //     this.frameService.getstockDetail().subscribe(data => {
    //         alert(data)
    //
    //         for (let i = 0; i <= data['data'].length - 1; i++) {
    //
    //             let loca = this.locationlist.filter(x => x.Id == data['data'][i]["loc_id"])
    //
    //             data['data'][i]["loc_id"] = loca[0]['Name'];
    //             data['data'][i]["trans_type"] = data['data'][i]["transaction_type"]["transactiontype"]

    //             //    var a = (1==2) ? 'dsfas' : 'dfasd';
    //             //     let transactionType = this.typeOfTransactionData[data['data'][i]["trans_type"]];
    //             //

    //             //     data['data'][i]["transaction_type"]["transactiontype"]

    //             //     if(data['data'][i]["trans_type"]!=0 && data['data'][i]["trans_type"]!=null ){
    //             //         data['data'][i]["trans_type"] = transactionType[0]['transactiontype'];
    //             //     }else{
    //             //         alert("test")
    //             //         data['data'][i]["trans_type"] = null
    //             //     }

    //         }
    //         alert("sample")
    //         alert(data['data'])
    //         this.StockDetails = data['data'];
    //         this.transacctionStockDetails = this.StockDetails
    //
    //     })

    // }

    getFramesStockDetails() {
        this.frameService.getstockDetail().subscribe(data => {
            for (let i = 0; i <= data['data'].length - 1; i++) {

                const loca = this.locationlist.filter(x => x.Id === data['data'][i].loc_id);

                data['data'][i].loc_id = loca[0].Name;
                data['data'][i].trans_type = data['data'][i].transaction_type.transactiontype;
                //    var a = (1==2) ? 'dsfas' : 'dfasd';
                //     let transactionType = this.typeOfTransactionData[data['data'][i]["trans_type"]];
                //

                //     data['data'][i]["transaction_type"]["transactiontype"]

                //     if(data['data'][i]["trans_type"]!=0 && data['data'][i]["trans_type"]!=null ){
                //         data['data'][i]["trans_type"] = transactionType[0]['transactiontype'];
                //     }else{
                //         alert("test")
                //         data['data'][i]["trans_type"] = null
                //     }

            }
            this.StockDetails = data['data'];
            this.transacctionStockDetails = this.StockDetails;

        });

    }

    typeOfInventoryTrasaction() {

        this.typeOfTransactionData = this.dropdownService.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdownService.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData  = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdownService.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    salesApi() {
        // alert("sales")
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i].order_id > 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
                // alert(this.transacctionStockDetails.push(this.StockDetails))
            }
        }

    }
    trasactionApi() {
        // alert("transaction")
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i].order_id >= 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
                // alert(this.transacctionStockDetails.push(this.StockDetails))
            }
        }

    }


    /**
     * Filters transaction on location
     * Here filter tranction location details
     */
    filterTransactionOnLocation() {
        this.FilteredTrasaction = [];

        this.transacctionStockDetails = [];
        if (this.inventoryService.FortrasactionItemID === '') {
            // localStorage.getItem('FortrasactionItemID')
            this.transacctionStockDetails = [];

        } else {
            const dataFilter = {
                filter: [
                    {
                        field: 'item_id',
                        operator: '=',
                        value: this.inventoryService.FortrasactionItemID
                        // localStorage.getItem('FortrasactionItemID'),
                    }
                ]
            };
            this.frameService.filterTrasactionData(dataFilter).subscribe(data => {
                this.FilteredTrasaction = data['data'];
                this.StockDetails = [];
                this.transacctionStockDetails = [];
                this.StockDetails = this.FilteredTrasaction;
                for (let i = 0; i <= this.StockDetails.length - 1; i++) {
                    // const loca = this.locationsList.filter(x => x.Id === this.StockDetails[i]['loc_id'].toString());
                    // this.StockDetails[i]['loc_id'] = loca[0]['Name'];
                    this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];

                }
                // this.StockDetails = data['data'];
                this.transacctionStockDetails = this.StockDetails;

            });
        }
    }

    /**
     * Filters transaction on location based
     * Here location based filtering the trancations
     */
    filterTransactionOnLocationBased() {
        // alert(locationData)
        this.FilteredTrasaction = [];
        this.transacctionStockDetails = [];
        if (this.inventoryService.FortrasactionItemID !== '') {
            if (this.FarmesForm.controls['locationId'].value === '') {
                this.filterTransactionOnLocation();
            } else {
                const dataLocationBased = {
                    filter: [
                        {
                            field: 'item_id',
                            operator: '=',
                            value: this.inventoryService.FortrasactionItemID.toString()
                            // localStorage.getItem('FortrasactionItemID'),
                        },
                        {
                            field: 'loc_id',
                            operator: '=',
                            value: this.FarmesForm.controls['locationId'].value,
                        }
                    ]
                };
                this.frameService.filterTrasactionData(dataLocationBased).subscribe(data => {
                    this.StockDetails = [];
                    this.transacctionStockDetails = [];
                    this.FilteredTrasaction = data['data'];
                    this.StockDetails = this.FilteredTrasaction;
                    for (let i = 0; i <= this.StockDetails.length - 1; i++) {
                        // const loca = this.locationsList.filter(x => x.Id === this.StockDetails[i]['loc_id'].toString());
                        // this.StockDetails[i]['loc_id'] = loca[0]['Name'];
                        this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];
                    }
                    // this.StockDetails = data['data'];
                    this.transacctionStockDetails = this.StockDetails;

                });
            }
        } else {
            this.transacctionStockDetails = [];
        }
    }
    DisaplayLensRange() {
        this.selectLensRange = true;
    }
    LensRangeDataDisaplay() {
        this.LensRangeDataDropdown = !this.LensRangeDataDropdown;
        this.frameService.getFrameLensRange().subscribe(data => {
            this.LensRangeData = data['data'];
        });
    }
    /**
     * Doubles click lens range
     * @param data for getting lens selected data
     */
    DoubleClickLensRange(data) {
        this.selectedRangeData = data;
        this.DisaplayLensRangeData = '';
        this.DisaplayLensRangeData = this.selectedRangeData.sphere_max + '  ' + 'to' + '  ' + this.selectedRangeData.sphere_min;
        //
        this.FarmesForm.controls.frame_lens_range_id.patchValue(this.selectedRangeData.id);
        /* this.FarmesForm.controls['FrameLensRangeName'].patchValue(this.selectedRangeData['lensrangename'] != null  ?
         this.selectedRangeData['lensrangename'] : "" ) */
        const Displaydata = this.DisaplayLensRangeData;
        this.FarmesForm.controls.FrameLensRangeName.patchValue(Displaydata.toString());
        this.LensRangeDataDropdown = false;

    }
    FramesTransactionevent(data) {
        if (data === 'cancle') {
            this.frameAddTransaction = false;
        } else {
            this.UpdateStock = data;
            this.frameAddTransaction = false;
            this.filterTransactionOnLocation();
        }
    }
    /**
     * Lens rangeevent
     * @param event for getting data from frames range component
     */
    lensRangeevent(event) {
        if (event === 'close') {
            this.selectLensRange = false;
        } else {
            this.selectedRangeData = event;
            // alert("@@@@@@@@@@@@@@@@@@@@@@")
            this.DisaplayLensRangeData = '';
            this.DisaplayLensRangeData = this.selectedRangeData.sphere_max + '  ' + 'to' + '  ' + this.selectedRangeData.sphere_min;
            // alert(this.selectedRangeData)
            // this.FarmesForm.controls['frame_lens_range_id']=this.selectedRangeData['id']
            // this.FarmesForm.controls['FrameLensRangeName']=this.selectedRangeData['lensrangename']
            const Displaydata = this.DisaplayLensRangeData;
            this.FarmesForm.controls.FrameLensRangeName.patchValue(Displaydata.toString());
            this.FarmesForm.controls.frame_lens_range_id.patchValue(this.selectedRangeData.id);
            this.selectLensRange = false;
        }
    }
    upload(event) {
        alert(event);
    }

    initlanguage() {
        return this._fb.group({
            Id: [],
            Image: []

        });
    }
    onSelectFile(event) {

        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = (value) => { // called once readAsDataURL is completed
                this.url = value.target['result'];
            };
        }
    }
    add(event) {
        //  this.uploadedFiles = [];
        for (const file of event.target.files) {
            this.uploadedFiles.push(file);

            // if (this.imageItem == undefined) {
            // this.uploadedFiles.forEach(data => {
            const reader = new FileReader();

            reader.readAsDataURL(file);
            const control = this.FarmesForm.controls.imagesList as FormArray;
            reader.onload = (value) => { // called once readAsDataURL is completed
                control.push(
                    this._fb.group({
                        Image: value.target['result']
                    })
                );
            };
        }

    }

    deleteImage(i) {
        if (this.imageItem === undefined) {
            const control = this.FarmesForm.controls.imagesList as FormArray;
            control.removeAt(i);
            this.uploadedFiles.splice(i, 1);
        } else {
            if (this.FarmesForm.controls.imagesList['controls'][i].value.Id) {
                const formData: FormData = new FormData();
                formData.append('accessToken', ''
                    // this._StateInstanceService.accessTokenGlobal
                );
                formData.append('Id', this.FarmesForm.controls.imagesList['controls'][i].value.Id);
                formData.append('itemId', this.imageItem);
                this.frameService.framesImageDeleting(formData).subscribe(data => {
                    const control = this.FarmesForm.controls.imagesList as FormArray;
                    control.removeAt(i);
                });
            } else {
                const control = this.FarmesForm.controls.imagesList as FormArray;
                control.removeAt(i);
                this.uploadedFiles.splice(i, 1);
            }


        }


    }
    traceFile(event) {
        this.uploadedTraceFiles = [];
        for (const file of event.target.files) {
            this.uploadedTraceFiles.push(file);
            this.FarmesForm.controls.trace_file.patchValue(this.uploadedTraceFiles[0].name);
        }
    }
    deleteTraceFile() {
        this.uploadedTraceFiles = [];
        this.FarmesForm.controls.trace_file.patchValue('');
        if (this.traceFileDelete !== '') {
            const formData: FormData = new FormData();
            formData.append('accessToken', ''
                // this._StateInstanceService.accessTokenGlobal
            );
            formData.append('itemId', this.imageItem);
            this.frameService.framesTraceFileDeleting(formData).subscribe(data => {
                this.traceFileDelete = '';
            });
        }
    }
    viewTraceFile() {

        if (this.uploadedTraceFiles.length !== 0) {
            const reader = new FileReader();
            reader.readAsText(this.uploadedTraceFiles[0]);
            reader.onload = (value) => { // called once readAsDataURL is completed
                this.traceFileView = value.target['result'];
                this.traceFileViewSidenav = true;
            };
        } else {
            if (this.traceFileDelete !== '') {
                this.traceFileView = (atob(this.traceFileDelete));
                this.traceFileViewSidenav = true;
            } else {
                this.traceFileView = '';
                this.traceFileViewSidenav = true;
            }
        }

    }
    /**
     * Framesdatasubs output event
     * @param data 
     */
    FramesdatasubOutputEvent(data) {
        this.selectedFramedata = data;
        this.FarmesForm.controls.ida_id.patchValue(this.selectedFramedata.fpc);
        this.selectFrameSidebar = false;
    }
    /**
     * Loads lensmaterial
     * @returns loading lensmaterial
     */
    LoadLensmaterial() {
        const tempData = [];
        this.lensMaterial = [];
        this.lensMaterial = this.dropdownService.lensMaterial;
        if (this.utility.getObjectLength(this.lensMaterial) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: any[]) => {
                try {
                    const key = 'data';
                    values[key].forEach(element => {
                        tempData.push({ name: element.material_name, value: element.id });
                    });
                    this.lensMaterial = tempData;
                    this.dropdownService.lensMaterial = tempData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads lens materials selected here in this we are loading the lens materials which are binded to item selected
     * @param {number} id 
     */
    LoadLensMaterialsSelected(id: number) {
        if (id) {
            this.valuestopushinMaterialcheckbox = [];
            this.frameService.getFrameLensMaterials(id).subscribe((itemvalue: FramesLensMaterials) => {
                try {
                    this.materialcheckedvalueobj = itemvalue;
                    this.materialcheckedvalueobj.data.forEach(element => {
                        const pushvalue = this.lensMaterial.filter(x => x.value === element.lens_material_id);
                        const a = { name: pushvalue[0].name, value: pushvalue[0].value };
                        this.valuestopushinMaterialcheckbox.push(a);
                    });
                    this.FarmesForm.controls.SelectedMaterials.patchValue(this.valuestopushinMaterialcheckbox);
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });

        }
    }

    /**
     * Selects pocedure data
     * @param event getting click event values
     */
    selectPocedureData(event: any) {
        if (event.target.value !== undefined) {
            event.target.select();
        }
    }
}
