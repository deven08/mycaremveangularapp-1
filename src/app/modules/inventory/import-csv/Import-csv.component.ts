import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';

// import { FrameService } from '../../../ConnectorEngine/services/frame.service';


import { Subscription } from '../../../../../node_modules/rxjs';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { ErrorService } from '@app/core';
import { ImportCsv } from '@app/core/models/inventory/addotherinventory.model';



@Component({
    selector: 'app-import-csv',
    templateUrl: './Import-csv.component.html'
})

export class ImportCsvComponent implements OnInit, OnDestroy {

    display = false;
    uploadDisplay = false;
    uploadedFiles: any[] = [];
    label = true;
    upload = false;
    getCsvPopup: Subscription;
    InventoryTypeCSV = '';
    testdata: any = [];
    uploadedDemoFiles: any[] = [];
    @ViewChild('dt') public dt: any;
    /**
     * Output  of import csv component of update the data
     */
    @Output() csvOutput = new EventEmitter<any>();
    constructor(private error: ErrorService,
                private inventoryService: InventoryService) {

    }

    ngOnInit() {

    }
    ngOnDestroy() {
        // this.getCsvPopup.unsubscribe();
    }

    importCsvbox(data) {
        this.uploadedFiles = [];
        this.uploadedDemoFiles = [];
        this.InventoryTypeCSV = data;
        this.display = true;
        this.uploadDisplay = false;
        this.dt.clear();
    }

    onUpload(event) {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }
        this.uploadedDemoFiles = this.uploadedFiles;
        //  this.messageService.add({ severity: 'info', summary: 'File Uploaded', detail: '' });
        // this.messageService.add({ severity: 'info', summary: 'Success', detail: 'File Imported' });
    }
    onRemove(event) {
        if (this.uploadedDemoFiles.length !== 0) {
            // for(const i in this.uploadedFiles){
            for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                if (this.uploadedFiles[i].name === event.file.name) {
                    this.uploadedDemoFiles.splice(i, 1);
                }
            }
        }
        this.uploadedFiles = this.uploadedDemoFiles;
    }
    CsvUpload() {
        const formData: FormData = new FormData();
        if (this.uploadedFiles.length > 0) {
            for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                formData.append('file', this.uploadedFiles[i]);
            }
            this.inventoryService.importcsv(formData, this.InventoryTypeCSV).subscribe((data: ImportCsv) => {
                try {
                    this.uploadedFiles = [];
                    this.uploadedDemoFiles = [];
                    this.uploadDisplay = false;
                    this.error.displayError({ severity: 'success', summary: 'Success', detail: data.status });
                    this.csvOutput.emit('updateCsvData');
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    onOkClick() {
        this.display = false;
        this.uploadDisplay = true;
    }
    onImportClick() {
        // if (this.InventoryTypeCSV == "frame") {
        //     this.frameService.framesCSVexport.next("Frame")
        // }
        // if (this.InventoryTypeCSV == "lens") {
        //     this._LensTreatmentService.lensCSVexport.next("lens")
        // }
        // if (this.InventoryTypeCSV == "contact") {
        //     this._ContactlensService.contactCSVexport.next("contact")
        // }
        // if (this.InventoryTypeCSV == "spectacle") {
        //     this._SpectcalelensServicee.spectCSVexport.next("spectacle")
        // }
        // if (this.InventoryTypeCSV == "others") {
        //     this._OtherinventoryService.otherCSVexport.next("others")
        // }
    }
}
