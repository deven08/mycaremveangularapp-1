import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'inventory-barcodelabels',
    templateUrl: 'inventory-barcodelabels.component.html'
})

export class InventoryBarcodelabelsComponent implements OnInit {
    cols: any[];
    gridlists: any[];
    locations1: any[];
    cities1: any[];
    ngOnInit(): void {
        this.cols = [
            { field: 'location', header: 'ID Location' },
            { field: 'label', header: 'Labels' },
            { field: 'style', header: 'Style' },
            { field: 'material', header: 'Material' },
            { field: 'lens', header: 'Lens' },
            { field: 'upc', header: 'UPC' },
            { field: 'unit', header: 'Unit' },
            { field: 'retail', header: 'Retail Price' },
            { field: 'pricing', header: 'Pricing' }
        ];
        this.gridlists = [
            // { "location": "MVE: My Vision", "type": '241', "code": "Marketing",
            // "source": "Referrals", "description": " Marketing Offer" },

        ];
        this.locations1 = [
            { label: 'Select Location', value: null },
            { label: 'MVE: My Vission Express' }
        ];
        this.cities1 = [
            { label: 'Select Type', value: null },
            { label: 'Spectacle Lens' },
            { label: 'Contact Lens' },
            { label: 'Other' }
        ];

    }
}
