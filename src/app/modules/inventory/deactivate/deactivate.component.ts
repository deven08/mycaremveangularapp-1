import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { ErrorService } from '@app/core';

@Component({
    selector: 'deactivate',
    templateUrl: 'deactivate.component.html'
})
export class DeactivateComponent implements OnInit {
    @Output() FrameDeactive = new EventEmitter<any>();
    @Output() LensDeactive = new EventEmitter<any>();
    @Output() ContactDeactive = new EventEmitter<any>();
    @Output() specDeactive = new EventEmitter<any>();
    @Output() otherDeactive = new EventEmitter<any>();
    display = false;
    deactivatedata: any;
    lensTreatReqBody: any[];
    FrameReqBody: any[];
    contactlensReqBody: any[];
    SpectacleReqBody: any[];
    othersReqBody: any[];

   
    /**
     * Creates an instance of deactivate component.
     * @param _FrameService provide FrameService
     * @param inventoryService provideinventory Service
     * @param _SpectcalelensService provide Spectcalelens Service
     * @param _LensTreatmentService provide LensTreatment Service
     * @param _OtherinventoryService provide Otherinventory Service
     * @param error provide error service
     */
    constructor(
        private inventoryService: InventoryService,       
        private error: ErrorService,) {

    }

    ngOnInit() { }

    deactivate(data: any) {
        this.display = true;
        this.deactivatedata = data;
    }
    /**

     * Deactivates data for deactivating items 
     * Deactivates data
     * @returns deactivat details
     */
    DeactivateData() {
        this.lensTreatReqBody = [];
        this.contactlensReqBody = [];
        this.FrameReqBody = [];
        this.SpectacleReqBody = [];
        this.othersReqBody = [];
        if (this.deactivatedata['LensSelection']) {
            this.deactivatedata['LensSelection'].forEach(element => {
                this.lensTreatReqBody.push(element.id);
            });
            const LensReqBody = {
                'itemId': this.lensTreatReqBody
            };
            this.inventoryService.DeactivateInventory(LensReqBody).subscribe(data => {
                this.LensDeactive.emit('Deactive');
                // this._LensTreatmentService.cancelLenstreatmentObj.next(dataobj);
            });
            this.display = false;
        }
        if (this.deactivatedata['SpectacleSelection']) {
            this.deactivatedata['SpectacleSelection'].forEach(element => {
                this.SpectacleReqBody.push(element.id);
            });
            const SpecReqBody = {
                'itemId': this.SpectacleReqBody
            };
            this.inventoryService.DeactivateInventory(SpecReqBody).subscribe(data => {
                this.specDeactive.emit('specDeactivate');
                
            });
            this.display = false;
        }
        if (this.deactivatedata['FrameSelection']) {
            this.deactivatedata['FrameSelection'].forEach(element => {
                this.FrameReqBody.push(element.id);
            });
            const frameReqBody = {
                'itemId': this.FrameReqBody
            };           
            this.inventoryService.DeactivateInventory(frameReqBody).subscribe(data => {
                this.FrameDeactive.emit('FrameDeactive');
                
            });

            this.display = false;
        }
        if (this.deactivatedata['ContactLensSelection']) {
            this.deactivatedata['ContactLensSelection'].forEach(element => {
                this.contactlensReqBody.push(element.id);
            });
            const name = {
                'itemId': this.contactlensReqBody
            };
            this.inventoryService.DeactivateInventory(name).subscribe(data => {
                this.ContactDeactive.emit('ContactDeactivate');
                // this._FrameService.deactivateinventoryObj.next('value');
            });

            this.display = false;
        }
        if (this.deactivatedata['OtherSelection']) {
            this.deactivatedata['OtherSelection'].forEach(element => {
                this.othersReqBody.push(element.id);
            });
            const name = {
                'itemId': this.othersReqBody
            };
            this.inventoryService.DeactivateInventory(name).subscribe(data => {
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Other Inventory Deactivated Successfully'
                });
                this.otherDeactive.emit('otherdeactivate');
            
            });

            this.display = false;
        }


    }
}

