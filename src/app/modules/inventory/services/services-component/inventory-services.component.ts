import { Component, OnInit} from '@angular/core';



@Component({
    selector: 'app-inventory-services-component',
    templateUrl: 'inventory-services.component.html'
})
export class InventoryServicesComponent implements OnInit {
    rowData: any;
    addservices = false;
    sortallinventory = false;
    pricecalculation = false;
    servicesdata: any[];
    servicesAdvancedFilter;
    ngOnInit() {
        this.servicesdata = [
            { field: 'ID', header: 'ID' },
            { field: 'Location', header: 'Location' },
            { field: 'Code', header: 'Code' },
            { field: 'Category', header: 'Category' },
            { field: 'Name', header: 'Name' },
            { field: 'Retail Price', header: 'Retail Price' },
            { field: 'UPC', header: 'UPC' },
            { field: 'Appointment', header: 'Appointment' },
            { field: 'Favorite', header: 'Favorite' }
        ];
    }
    constructor() {

    }



}

