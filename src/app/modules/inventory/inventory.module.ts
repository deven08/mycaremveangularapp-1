
import { CommonModule } from '@angular/common';
import { ContactConfigComponent } from './contact-lens/contact-config/contact-config.component';
import { ContactlensRreactivateComponent } from './contact-lens/inventory-contactlens-reactivate/inventory-contactlens-reactivate.component';
import { FrameDetailsComponent } from './frame/frame-details/frame-details.component';
import { InventoryRoutingModule } from './/inventory-routing.module';
import { LensOnDemandComponent } from './contact-lens/lens-on-demand/lens-on-demand.component';
import { NgModule } from '@angular/core';
import { OthersReactivateInventoryItemsComponent } from './inventory-others/others-reactivate-inventory/others-reactivate-inventory-items.component';


import {
  AddContactlensConfigComponent,
  AddContactLensInvTransactionComponent,
  AddFramesLensRangeComponent,
  AddGridInventorySpectaclelensComponent,
  AddInventoryComponent,
  AddInventoryLensTreatmentComponent,
  AddInventoryOthersComponent,
  AddInventorySpectaclelensComponent,
  AddLensTreatmentInvTransactionComponent,
  AddModifyComponent,
  AddOtherInvTransactionComponent,
  AddPrismgridInventorySpectaclelensComponent,
  AddServiceInvTransactionComponent,
  AddSpectaclelensInvTransactionComponent,
  CommPdfviewerComponent,
  ContactLensComponent,
  ContactlensReceivingComponent,
  ExcessDataViewComponent,
  FrameAddTransactionComponent,
  FramesDataComponent,
  FrameComponent,
  FramesReceivingComponent,
  InventoryDetailsComponent,
  InventoryAddServicesComponent,
  InventoryFrameAdvancedFilterComponent,
  InventoryLenstreatmentAdvancedFilterComponent,
  InventoryLensTreatmentComponent,
  InventoryOthersComponent,
  InventoryPurchaseOrderComponent,
  InventoryServicesAdvancedFilterComponent,
  InventoryServicesComponent,
  InventorySpectaclelensAdvancedFilterComponent,
  InventoryTransferComponent,
  LensTreatmentsImportComponent,
  LenstreatmentReactivateComponent,
  LensReceivingComponent,
  LoadFramesComponent,
  OtherReceivingComponent,
  PhysicalCountComponent,
  PhysicalCountImportFileComponent,
  PhysicalSearchComponent,
  PoItemsComponent,
  PoOrdersComponent,
  PoReceivingComponent,
  PoReceivedComponent,
  PoSearchComponent,
  PoStatusComponent,
  PoSupplierComponent,
  ProcedureCodeSearchComponent,
  ReceivingInventoryComponent,
  ReactivateInventoryItemsComponent,
  SelectInvDetailsComponent,
  SelectProcedureCodeComponent,
  SpectaclelensReactivateInventoryComponent,
  SearchShipmentComponent,
  SearchReceivingComponent,
  SpectacleLensComponent,
  SpectaclelensDetailsComponent,
  TraceFileViewComponent,
  PhysicalCountBatchSearchComponent,
  PriceCalculationComponent,
  InventoryContactlensAdvancedFilterComponent,
  SortAllInventoryComponent,
  ConfirmDialogComponent,
  AddLocationComponent,
  DeactivateComponent,
  BarCodeLabelsComponent,
  InventoryBarcodelabelsComponent,
  PoItemsAddComponent,
  OthersAdvancedFilterComponent
} from './';
import { InventoryService } from '@services/inventory/inventory.service';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { FramesService } from '@services/inventory/frames.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { OthersService } from '@services/inventory/others.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { PhysicalcountService } from '@services/inventory/physicalcount.service';
import { ImportCsvComponent } from './import-csv/Import-csv.component';
import { SharedModule } from '@shared/shared.module';
import { ReceivingService } from '@services/inventory/receiving.service';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
// import { PoItemsAddComponent } from './purchase-order/po-items-add/po-items-add/po-items-add.component';

@NgModule({
  imports: [
    CommonModule,
    InventoryRoutingModule,
    SharedModule
  ],
  declarations: [
    FrameComponent, AddModifyComponent,
    ContactLensComponent, SpectacleLensComponent,
    InventoryOthersComponent, InventoryServicesComponent,
    InventoryDetailsComponent,
    PhysicalCountComponent,
    BarCodeLabelsComponent,
    LensTreatmentsImportComponent,
    PriceCalculationComponent,
    InventoryPurchaseOrderComponent,
    InventoryLensTreatmentComponent,
    InventoryBarcodelabelsComponent,
    ReceivingInventoryComponent, LensOnDemandComponent,
    AddInventoryComponent,
    AddContactlensConfigComponent, AddInventoryLensTreatmentComponent,
    ReactivateInventoryItemsComponent, SpectaclelensReactivateInventoryComponent,
    LenstreatmentReactivateComponent, OthersReactivateInventoryItemsComponent,
    ContactlensRreactivateComponent, PhysicalSearchComponent,
    ExcessDataViewComponent, AddInventorySpectaclelensComponent,
    AddPrismgridInventorySpectaclelensComponent, AddInventoryOthersComponent,
    AddGridInventorySpectaclelensComponent, InventoryLenstreatmentAdvancedFilterComponent,
    InventoryContactlensAdvancedFilterComponent, ProcedureCodeSearchComponent,
    InventoryFrameAdvancedFilterComponent, InventorySpectaclelensAdvancedFilterComponent,
    InventoryServicesAdvancedFilterComponent, SearchShipmentComponent,
    SearchReceivingComponent,
    InventoryAddServicesComponent, FramesDataComponent,
    AddFramesLensRangeComponent,
    SelectInvDetailsComponent, LoadFramesComponent,
    FrameAddTransactionComponent,
    AddOtherInvTransactionComponent, SelectProcedureCodeComponent,
    AddSpectaclelensInvTransactionComponent,
    AddLensTreatmentInvTransactionComponent, AddServiceInvTransactionComponent,
    AddContactLensInvTransactionComponent,
    ContactConfigComponent, FrameDetailsComponent, SpectaclelensDetailsComponent,
    InventoryTransferComponent,
    FramesReceivingComponent,
    PoReceivingComponent, LensReceivingComponent, OtherReceivingComponent,
    ContactlensReceivingComponent,
    TraceFileViewComponent, PhysicalCountImportFileComponent,
    PhysicalCountBatchSearchComponent,
    PoSupplierComponent, PoItemsComponent, PoOrdersComponent, PoReceivedComponent,
    PoStatusComponent, PoSearchComponent,
    CommPdfviewerComponent, SortAllInventoryComponent,
    ConfirmDialogComponent, AddLocationComponent,
    DeactivateComponent, ImportCsvComponent, PoItemsAddComponent,
    OthersAdvancedFilterComponent

  ],
  providers: [
    ContactlensService,
    FramesService,
    LenstreatmentService,
    OthersService,
    SpectaclelensService,
    PhysicalcountService,
    ReceivingService,
    InventoryService,
    PurchaseOrderService
  ]
})
export class InventoryModule { }




