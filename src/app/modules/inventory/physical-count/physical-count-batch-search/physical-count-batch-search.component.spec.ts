import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalCountBatchSearchComponent } from './physical-count-batch-search.component';

describe('PhysicalCountBatchSearchComponent', () => {
  let component: PhysicalCountBatchSearchComponent;
  let fixture: ComponentFixture<PhysicalCountBatchSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalCountBatchSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalCountBatchSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
