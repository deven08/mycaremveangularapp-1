import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { FrameService } from '../../../ConnectorEngine/services/frame.service';
// import { SpectcalelensService } from '../../../ConnectorEngine/services/spectcalelens.service';
// import { LensTreatmentService } from '../../../ConnectorEngine/services/lenstreatments.service';
// import { ContactlensService } from '../../../ConnectorEngine/services/contactlens.service';
// import { OtherinventoryService } from '../../../ConnectorEngine/services/otherinventory.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { FramesService } from '@services/inventory/frames.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { OthersService } from '@services/inventory/others.service';

@Component({
    selector: 'app-select-inv-details',
    templateUrl: 'select-inventory-details.component.html'
})

export class SelectInvDetailsComponent implements OnInit {
    framelist = [];
    Specatablelist = [];
    lenstreatmentGridlist = [];
    contactlenslist = [];
    otherinventorylist = [];
    frameGrid = false;
    spectacleGrid = false;
    lensGrid = false;
    contactGrid = false;
    otherGrid = false;
    selectUPC = '';
    @Input() GridName: string;
    @Output() UPCinventory = new EventEmitter<any>();

    constructor(private frameService: FramesService,
        private spectcalelensService: SpectaclelensService,
        private lensTreatmentService: LenstreatmentService,
        private contactlensService: ContactlensService,
        private othersService: OthersService) {

    }
    ngOnInit() {
        if (this.GridName === 'frames') {
            this.GetFramesList('', '', '', '', '', '', '');
            this.frameGrid = true;
        }
        if (this.GridName === 'spectacle') {
            this.GetSpectablelenssList('', '', '', '', '');
            this.spectacleGrid = true;
        }
        if (this.GridName === 'lens') {
            this.GetLensTreatmentDetails('', '', '', '', '', '', '');
            this.lensGrid = true;
        }
        if (this.GridName === 'contact') {
            this.GetcontactlensList('', '', '', '', '', '');
            this.contactGrid = true;
        }
        if (this.GridName === 'other') {
            this.GetinventoryotherList('', '', '', '', '', '');
            this.otherGrid = true;
        }
    }
    DoubleClickGrid(data) {
        this.UPCinventory.emit(data.UPC);
    }
    onSelectClick() {
        if (this.selectUPC !== '') {
            this.UPCinventory.emit(this.selectUPC);
        }
    }
    onCancleClick() {
        this.UPCinventory.emit('cancle');
    }
    onRowSelect(event) {
        this.selectUPC = event.data.UPC;
    }
    onRowUnselect(data) {
        this.selectUPC = '';
    }
    /**
     * Gets frames list
     * @param [upc]  fro upce value
     * @param [manufacturerid] for manufacturer id
     * @param [brandid] for brand id
     * @param [colorid] for color id
     * @param [Framename] for frame name
     * @param [colorcode] for color code
     * @param [delete_status] for deleted status
     */
    GetFramesList(upc = '', manufacturerid = '', brandid = '', colorid = '', Framename = '', colorcode = '', delete_status = '') {
        // Get History Params
        this.framelist = [];
        this.frameService.getframesDetails(upc, manufacturerid, brandid, colorid, '200000', Framename, colorcode, delete_status).subscribe(
            data => {
                if (data['result']['Optical'] !== 'No record found') {
                    for (let i = 0; i <= data['result']['Optical']['Frames'].length - 1; i++) {
                        data['result']['Optical']['Frames'][i]['itemId'] = parseInt(data['result']['Optical']['Frames'][i]['itemId'], 10);
                        data['result']['Optical']['Frames'][i]['del_status'] =
                         data['result']['Optical']['Frames'][i]['del_status'] === 0 ? 'Active' : 'Discontinue';
                    }
                    this.framelist = data['result']['Optical']['Frames'];
                }
                console.log(this.framelist);
            });
    }

    GetSpectablelenssList(upc = '', Name = '', ManufactureId = '', LensStyleId = '', MaterialId = '') {
        this.Specatablelist = [];
        this.spectcalelensService.getSpectacles(upc, '200000', MaterialId, LensStyleId, '', ManufactureId, Name).subscribe(
            data => {
                if (data['result']['Optical'] !== 'No record found') {
                    for (let i = 0; i <= data['result']['Optical']['SpectaclesLenses'].length - 1; i++) {
                        data['result']['Optical']['SpectaclesLenses'][i]['ItemId'] =
                         parseInt(data['result']['Optical']['SpectaclesLenses'][i]['ItemId'], 10);
                    }
                    this.Specatablelist = data['result']['Optical']['SpectaclesLenses'];
                } else {
                    this.Specatablelist = [];
                }

            });
    }
    GetLensTreatmentDetails(Id = '', Name = '', upc = '', categoryId = '', maxRecord = '', offset = '', unitmeasure = '') {
        this.lenstreatmentGridlist = [];
        this.lensTreatmentService.getLensTreatmentList(Id, Name, upc, categoryId, maxRecord, offset, unitmeasure).subscribe(
            data => {
                for (let i = 0; i <= data['result']['Optical']['LensTreatment'].length - 1; i++) {
                    data['result']['Optical']['LensTreatment'][i]['itemId'] =
                     parseInt(data['result']['Optical']['LensTreatment'][i]['itemId'], 10);
                }
                this.lenstreatmentGridlist = data['result']['Optical']['LensTreatment'];
            });

    }


    GetcontactlensList(ID = '', upc = '', manufacturerid = '', brandid = '', colorid = '', name = '') {
        this.contactlensService.getcontactlens(ID, upc, manufacturerid, brandid, colorid, '200000', name).subscribe(
            data => {
                this.contactlenslist = [];
                if (data['result']['Optical'] !== 'No record found') {
                    for (let i = 0; i <= data['result']['Optical']['ContactLenses'].length - 1; i++) {
                        data['result']['Optical']['ContactLenses'][i]['ItemId'] =
                         parseInt(data['result']['Optical']['ContactLenses'][i]['ItemId'], 10);

                    }
                    this.contactlenslist = data['result']['Optical']['ContactLenses'];
                }

            });
    }

    GetinventoryotherList(upc = '', Name = '', Id = '', vendorId = '', categoryid = '', retailprice = '') {
        this.otherinventorylist = [];
        this.othersService.getotherinventories(upc, Name, Id, vendorId, categoryid, retailprice).subscribe(
            data => {
                for (let i = 0; i <= data['result']['Optical']['OtherInventory'].length - 1; i++) {
                    data['result']['Optical']['OtherInventory'][i]['itemId'] =
                     parseInt(data['result']['Optical']['OtherInventory'][i]['itemId'], 10);

                }
                this.otherinventorylist = data['result']['Optical']['OtherInventory'];

            });

    }
}
