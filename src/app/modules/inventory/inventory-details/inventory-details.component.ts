import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { OthersService } from '@services/inventory/others.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService } from '@app/core/services/error.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameBrand } from '@app/core/models/inventory/Brands.model';
import { Frame } from '@app/core/models/inventory/frames.model';
import {
    FrameColor,
    Data, FrameColorData, LenColorData, LensData,
    CommissionStructure,
    MasterDropDown, 
    ManufacturerDropData, 
    LensColorMaster,
    CommissionType,
    FramesMaterial,
    FrameSupplier,
    FrameTypeMaster,
    FrameShapeMaster,
    FrameGender,
    VcpDropDown,
    ContactPackage,
    ContactReplacement,
    Material,
    OtherInvCategory,
    InvUpcType,
    FrameSource,
    FrameStyleMaster,
    LensStyleMaster,
    RimType,
    FrameBrands,
    SpectacleMaterial,
    TreatmentCategory,
    LensLab
} from '@app/core/models/masterDropDown.model';
@Component({
    selector: 'app-inventory-details',
    templateUrl: 'inventory-details.component.html'
})

export class InventoryDetailsComponent implements OnInit {

    DetailsForm: FormGroup;
    orderlists: any[];
    FarmesDetailsForm: FormGroup;
    spectacleDetailsForm: FormGroup;
    lensTreatmentDetailsForm: FormGroup;
    OtherDetailsForm: FormGroup;
    Accesstoken = '';
    upcdisable = false;
    selectInvdetails = false;
    DetailDropDown = [
        { id: 'null', name: 'Select Type' },
        { id: '1', name: 'Frames' },
        { id: '2', name: 'Spectacle lens' },
        { id: '3', name: 'Lens Treatment' },
        { id: '4', name: 'Contact Lens' },
        { id: '5', name: 'Other Details' }
    ];
    genderDropDown = [{ 'Id': 'Male', 'Name': 'Male' }, { 'Id': 'Female', 'Name': 'Female' }, { 'Id': 'Unisex', 'Name': 'Unisex' }];
    FramesDetails = false;
    SpectacleDetails = false;
    LensTreatmentDetails = false;
    ContactDetails = false;
    otherDetails = false;
    Manufacturers: any[];
    Brands: any[];
    /**
     * Colors  of inventory details component
     * holding list of frame colors
     */
    colors: Array<object>;
    materials: any[];
    framesTypeDropDown = [];
    framesSource = [];
    framesShapeDropDown = [];
    framesRimType = [];
    framesUsage = [];
    framesUpcType = [];
    lenscolor: Array<object>;
    /**
     * Lens color of inventory details component
     * holding list of lens colors
     */
    lensColor: Array<object>;
    /**
     * Suppliers  of inventory details component
     */
    suppliers: Array<object>;
    activestatus: any[];

    lensstylcopy: any[];
    styles = [];
    MaterialListDropDown = [];
    colorcopy: any[];
    /**
     * Color  of inventory details component
     * Holding contact lens color list
     */
    color: Array<object>;
    packageData: any[];
    vcpDropData: any[] = [];
    TypeDropData: any[] = [];
    StructureDropData: any[] = [];
    lenslab = [];

    categories: any[];
    public traillenslist: any;
    traillens: any[];
    replacements: any[];
    public replacementlist: any;
    private categorylist: any;

    public manufacturerlist: Manufacturer[];
    public Brandlist: FrameBrand[];
    public supplierslist: any[];
    public LensTreatments: any[];
    public lensTreatmentslist: any;
    public FrameGetData: any[] = [];
    locationNameData: any[] = [];
    locationGridData: any[] = [];
    Detailstext = '';
    DetailsControl = 'null';
    OpenSideNav: string;
    /**
     * Contac lenst color of inventory details component
     * Holding list of contactlens color data
     */
    contacLenstColor:  Array<object>;
    constructor(private _fb: FormBuilder,
        private frameService: FramesService,
        private spectcalelensService: SpectaclelensService,
        private lensTreatmentService: LenstreatmentService,
        private contactlensService: ContactlensService,
        private othersService: OthersService,
        private dropdownService: DropdownService,
        private error: ErrorService,
        private utility: UtilityService,
        private inventory: InventoryService
    ) {

    }
    ngOnInit(): void {
        this.Loadformdata();
        // this.LoadSpectacleLensformdata()
        this.loadFramesShape();
        this.loadFramesType();
        this.Loadmasters();
        this.GetSupplierDetails();
        // this.Manufacturers = [];
        // this.Manufacturers = this._DropdownsPipe.manufacturersDropData();
        // this.Brands = [];
        // this.Brands = this._DropdownsPipe.brandsDropData();
        // this.Colors = [];
        // this.Colors = this._DropdownsPipe.coloursDropData();
        // this.LensTreatments = [];
        // this.lensTreatmentslist = [];
        // this.LensTreatments = this._DropdownsPipe.treatCategoryDropDown();
        // this.lensTreatmentslist = this._DropdownsPipe.treatCategoryDropDown();
        // this.framesUsage = [];
        // this.framesUsage = this._DropdownsPipe.usageDropdown();
    }

    Loadmasters() {
        this.LoadColorCopy();
        this.Loadmaterials();
        this.loadRimType();
        // this.loadUsage();
        this.loadSource();
        this.loadUpcType();
        this.LoadLensColor();

        this.LoadLensStyle();
        this.loadMaterialList();
        this.loadDropDowns();
        this.GetcategoryDetails();
        this.Getcontacttraillens();
        this.LoadLenstreatmentdetails();
        this.getPackagingData();
        this.LoadManufacturers();
        this.LoadBrands();
        this.LoadColors();
        this.loadContactLensColors()
        this.getlensLab();
    }
    /**
     * Loads colors
     * @returns list of color deatils
     */
    LoadColors() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                                this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    Loadstatus() {
        this.activestatus = [];
        this.activestatus.push({ Id: '0', Name: 'Active' });
        this.activestatus.push({ Id: '1', Name: 'Discontinue' });

    }

    /**
     * Loadformdata add modify component for formgroup object
     */
    Loadformdata() {

        this.upcdisable = false;
        this.Accesstoken = ''
        // this._StateInstanceService.accessTokenGlobal;
        this.FarmesDetailsForm = this._fb.group({

            accessToken: this.Accesstoken,
            upc: '',
            name: '',
            manufacturerId: '',
            vendorId: null,
            brandId: '',
            shapeId: '',
            styleId: '',
            a: '',
            b: '',
            ed: '',
            dbl: '0',
            temple: '',
            bridge: '',
            fpd: '',
            colorId: '',
            colodCode: '',
            retailPrice: '',
            frameId: null,
            supplierId: '',
            materialId: '',
            locationId: '',
            itemId: '',
            purchasePrice: '',
            profit: '',
            eye: '',
            rimTypeId: '',
            sourceId: '',
            usageId: '',
            upcTypeId: '',
            minQty: '',
            maxQty: '',
            gender: '',
            notes: '',
            userSKU: '',
            cost: '',
            groupCost: '',
            msrp: '',
            structureId: '',
            commissionTypeId: '',
            procedureCode: '',
            selectedstyles: '',
            inventory: false,
            lensColor: '',
            del_status: '0',
            frameamount: '',

            // spectaclelens
            speupc: '',
            spectaclename: '',
            spectaclemanufacturerId: '',
            spectaclevendorId: 0,
            lensTypeId: '',
            spectaclematerialId: '',
            designId: '',
            treatmentId: [''],
            LocationId: '',
            spectaclesupplierId: '',
            unit: '0',
            spectacleretailPrice: '',
            spectaclecost: '',
            spectaclecolorCode: '',
            spectaclelabId: '',
            returnableFlag: false,
            spectacleinventory: false,
            spectacleminQty: '',
            spectaclemaxQty: '',
            maxAddPower: '',
            spectacleadd: '',
            maxDiameter: '',
            oversizeDiameterCharge: '',
            spectaclesphere: '',
            spectaclecylinder: '',
            baseCurve: '',
            minHeight: '',
            spectaclenotes: '',
            pricing: '',
            spectaclevspCode: '',
            spectacleprofit: '',
            spectaclestyleId: '',
            type: '',
            spectaclecolorId: '',

            // LensTreatment
            lensname: '',
            lensvspCode: '',
            Id: '',
            lenslocationId: '',
            lensupc: '',
            unitmeasure: '',
            lensretailPrice: '',

            // purchasePrice:'',
            lenscategoryId: '',
            lensprofit: '',
            lenscost: '',
            lensinventory: false,
            lenssendToLab: false,
            lensprintOnOrder: false,
            lensStructureId: '',
            lensVcpModifiercode: '',
            lensCommissionTypeId: '',
            lensamount: '',
            grossPercentage: '',
            spiff: '',
            lensnote: '',

            // otherDetails
            otherupc: '',
            othername: '',
            categoryId: '',
            otherVendorId: null,
            otherretailPrice: null,
            othercost: null,
            otherinventory: false,
            othersendToLab: false,
            otherprintOnOrder: false,
            otherprocedureCode: '',
            modifierId: '',
            otherstructureId: '',
            othernotes: '',
            otherprofit: '',
            otherminQty: '',
            othermaxQty: '',
            otherCommissionTypeId: '',

            // contactlens
            contname: '',
            contmanufacturerId: '',
            contvendorId: '',
            contbrandId: '',
            packagingId: '',
            typeId: [''],
            contmaterialId: [''],
            wearScheduleId: [''],
            replacementId: [null],

            hardContact: false,
            continventory: false,
            supplyId: [''],
            contcolorId: '',
            contretailPrice: '',
            contupc: '',
            contsupplierId: '',
            contlocationid: '',
            configlocationid: '',
            contmaxQty: '',
            contminQty: '',
            contnotes: '',
            traillensId: '',
            contunit: '',
            ReplacementScheduleId: '',
            contcost: '',
            contprocedureCode: '',
            contprofit: '',
            six_months_price: '',
            six_months_qty: '',
            twelve_months_price: '',
            twelve_months_qty: '',
            sixmonthsprofit: '',
            twelvemonthsprofit: '',
            contReplacementScheduleId: '',
            contpackagingId: '',





        });

    }
    selectInvDetailsclick() {
        if (this.FramesDetails === true) {
            this.OpenSideNav = 'frames';
        }
        if (this.SpectacleDetails === true) {
            this.OpenSideNav = 'spectacle';
        }
        if (this.LensTreatmentDetails === true) {
            this.OpenSideNav = 'lens';
        }
        if (this.ContactDetails === true) {
            this.OpenSideNav = 'contact';
        }
        if (this.otherDetails === true) {
            this.OpenSideNav = 'other';
        }
        this.selectInvdetails = true;
    }
    dropDownClick() {
        this.Loadformdata();
        const value = this.DetailsControl;
        if (value === '1') {
            this.FramesDetails = true;
            this.SpectacleDetails = false;
            this.LensTreatmentDetails = false;
            this.ContactDetails = false;
            this.otherDetails = false;
        }
        if (value === '2') {
            this.FramesDetails = false;
            this.SpectacleDetails = true;
            this.LensTreatmentDetails = false;
            this.ContactDetails = false;
            this.otherDetails = false;
        }
        if (value === '3') {
            this.FramesDetails = false;
            this.SpectacleDetails = false;
            this.LensTreatmentDetails = true;
            this.ContactDetails = false;
            this.otherDetails = false;
        }

        if (value === '4') {
            this.FramesDetails = false;
            this.SpectacleDetails = false;
            this.LensTreatmentDetails = false;
            this.ContactDetails = true;
            this.otherDetails = false;
        }
        if (value === '5') {
            this.FramesDetails = false;
            this.SpectacleDetails = false;
            this.LensTreatmentDetails = false;
            this.ContactDetails = false;
            this.otherDetails = true;
        }
    }
    keyPress(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }


    /**
     * Determines whether detail click on
     */
    onDetailClick() {
        this.Loadformdata();
        this.frameService.InvnetoryDetailsFilter(this.Detailstext).subscribe(data => {
            if (data['data'][0].module_type_id === 1) {
                this.DetailsControl = '1';
                this.FramesDetails = true;
                this.SpectacleDetails = false;
                this.LensTreatmentDetails = false;
                this.ContactDetails = false;
                this.otherDetails = false;
            }
            if (data['data'][0].module_type_id === 2) {
                this.DetailsControl = '2';
                this.FramesDetails = false;
                this.SpectacleDetails = true;
                this.LensTreatmentDetails = false;
                this.ContactDetails = false;
                this.otherDetails = false;
            }
            if (data['data'][0].module_type_id === 3) {
                this.DetailsControl = '4';
                this.FramesDetails = false;
                this.SpectacleDetails = false;
                this.LensTreatmentDetails = false;
                this.ContactDetails = true;
                this.otherDetails = false;
            }
            if (data['data'][0].module_type_id === 5) {
                this.DetailsControl = '3';
                this.FramesDetails = false;
                this.SpectacleDetails = false;
                this.LensTreatmentDetails = true;
                this.ContactDetails = false;
                this.otherDetails = false;
            }
            if (data['data'][0].module_type_id === 6) {
                this.DetailsControl = '5';
                this.FramesDetails = false;
                this.SpectacleDetails = false;
                this.LensTreatmentDetails = false;
                this.ContactDetails = false;
                this.otherDetails = true;
            }


            if (this.FramesDetails === true) {
                let frameData: any = [];
                this.frameService.getframesDetails(this.Detailstext, '', '', '', '', '', '').subscribe(framesGrid => {
                    frameData = framesGrid;
                    if (frameData.result.Optical !== 'No record found') {
                        this.FrameGetData = frameData.result.Optical.Frames;
                        this.FarmesDetailsForm = this._fb.group({
                            upc: [frameData.result.Optical.Frames[0].UPC, [Validators.required]],
                            name: [frameData.result.Optical.Frames[0].Name, [Validators.required]],
                            manufacturerId: [frameData.result.Optical.Frames[0].ManufacturerId, [Validators.required]],
                            vendorId: frameData.result.Optical.Frames[0].VendorId,
                            brandId: [frameData.result.Optical.Frames[0].BrandId, [Validators.required]],
                            shapeId: frameData.result.Optical.Frames[0].ShapeId === 0 ? null :
                                frameData.result.Optical.Frames[0].ShapeId,
                            styleId: frameData.result.Optical.Frames[0].StyleId,
                            a: frameData.result.Optical.Frames[0].a,
                            b: frameData.result.Optical.Frames[0].b,
                            ed: frameData.result.Optical.Frames[0].ed,
                            dbl: frameData.result.Optical.Frames[0].dbl === 0 ? null :
                                parseInt(frameData.result.Optical.Frames[0].dbl, 10),
                            eye: frameData.result.Optical.Frames[0].Eye,
                            temple: frameData.result.Optical.Frames[0].temple,
                            bridge: frameData.result.Optical.Frames[0].bridge,
                            fpd: frameData.result.Optical.Frames[0].fpd,
                            colorId: frameData.result.Optical.Frames[0].ColorId = '' ? '0' :
                                frameData.result.Optical.Frames[0].ColorId,
                            colodCode: frameData.result.Optical.Frames[0].ColorCode,
                            retailPrice: '',
                            typeId: frameData.result.Optical.Frames[0].FrameTypeId === '' ? null :
                                frameData.result.Optical.Frames[0].FrameTypeId,
                            supplierId: '',
                            materialId: frameData.result.Optical.Frames[0].MaterialId === '' ? '' :
                                frameData.result.Optical.Frames[0].MaterialId,
                            locationId: '',
                            itemId: frameData.result.Optical.Frames[0].itemId,
                            purchasePrice: '',
                            profit: '',
                            rimTypeId: frameData.result.Optical.Frames[0].RimTypeId === 0 ? ''
                                : parseInt(frameData.result.Optical.Frames[0].RimTypeId, 10),
                            sourceId: frameData.result.Optical.Frames[0].SourceId === 0 ? null :
                                parseInt(frameData.result.Optical.Frames[0].SourceId, 10),
                            usageId: frameData.result.Optical.Frames[0].UsageId === 0 ? null :
                                parseInt(frameData.result.Optical.Frames[0].UsageId, 10),
                            upcTypeId: frameData.result.Optical.Frames[0].Upctypeid === 0 ? '' :
                                parseInt(frameData.result.Optical.Frames[0].Upctypeid, 10),
                            minQty: frameData.result.Optical.Frames[0].min_qty,
                            maxQty: frameData.result.Optical.Frames[0].max_qty,
                            gender: frameData.result.Optical.Frames[0].Gender,
                            notes: frameData.result.Optical.Frames[0].Notes,
                            userSKU: frameData.result.Optical.Frames[0].UserSKU,
                            cost: '',
                            groupCost: frameData.result.Optical.Frames[0].Groupcost === '' ? '' :
                                parseInt(frameData.result.Optical.Frames[0].Groupcost, 10),
                            msrp: frameData.result.Optical.Frames[0].MSRP === '' ? '' :
                                parseInt(frameData.result.Optical.Frames[0].MSRP, 10),
                            procedureCode: frameData.result.Optical.Frames[0].ProcedureCode,
                            selectedstyles: '',
                            inventory: frameData.result.Optical.Frames[0].Inventory === 1 ? true : false,
                            //   inventory:"false",
                            lenscolor: frameData.result.Optical.Frames[0].lenscolor === '0' ? '' :
                                parseInt(frameData.result.Optical.Frames[0].lenscolor, 10),
                            del_status: frameData.result.Optical.Frames[0].del_status,
                            structureId: frameData.result.Optical.Frames[0].StructureId,
                            commissionTypeId: frameData.result.Optical.Frames[0].CommissionTypeId,
                            frameamount: frameData.result.Optical.Frames[0].Amount,
                        });

                        this.locationNameData = [];
                        this.orderlists = [];
                        this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
                            this.locationNameData = DataSet['result']['Optical']['Locations'];
                            this.inventory.getLocationByFilter(
                                frameData['result']['Optical']['Frames'][0]['itemId']).subscribe((filterData: any[]) => {
                                    this.orderlists = filterData['data'];
                                    this.orderlists.forEach(element => {
                                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                            if (element.loc_id === this.locationNameData[i].Id) {
                                                element.loc_id = this.locationNameData[i].Name;
                                            }
                                        }
                                    });
                                });
                        });
                    }
                });
            }
            if (this.SpectacleDetails === true) {
                let SpecData: any = [];
                this.spectcalelensService.getSpectacles(this.Detailstext, '', '', '', '', '', '').subscribe(specGrid => {
                    SpecData = specGrid;
                    if (SpecData['result']['Optical'] !== 'No record found') {
                        this.FarmesDetailsForm = this._fb.group({
                            // accessToken: this.Accesstoken,
                            speupc: [SpecData['result']['Optical']['SpectaclesLenses'][0]['UPC'], [Validators.required]],
                            spectaclename: [SpecData['result']['Optical']['SpectaclesLenses'][0]['Name'], [Validators.required]],
                            spectaclemanufacturerId: [SpecData['result']['Optical']['SpectaclesLenses'][0]['ManufacturerId'] === 0 ? null :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['ManufacturerId'], [Validators.required]],
                            lensTypeId: '',
                            spectaclematerialId: [SpecData['result']['Optical']['SpectaclesLenses'][0]['MaterialId'] === 0 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['MaterialId']],
                            designId: '',
                            treatmentId: [''],
                            LocationId: '',
                            spectaclevendorId: [SpecData['result']['Optical']['SpectaclesLenses'][0]['VendorId'] === 0 ? 0 :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['VendorId']],
                            unit: SpecData['result']['Optical']['SpectaclesLenses'][0]['Unit'],
                            spectacleretailPrice: SpecData['result']['Optical']['SpectaclesLenses'][0]['RetailPrice'],
                            spectaclecost: SpecData['result']['Optical']['SpectaclesLenses'][0]['Cost'],
                            spectaclecolorCode: SpecData['result']['Optical']['SpectaclesLenses'][0]['Color Code'],
                            labId: [SpecData['result']['Optical']['SpectaclesLenses'][0]['LabId'] === 0 ? 0 :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['LabId']],
                            returnableFlag: SpecData['result']['Optical']['SpectaclesLenses'][0]['ReturnableFlag'] === '0' ?
                                false : SpecData['result']['Optical']['SpectaclesLenses'][0]['ReturnableFlag'] === '' ? false :
                                    SpecData['result']['Optical']['SpectaclesLenses'][0]['ReturnableFlag'] === '1' ? true :
                                        SpecData['result']['Optical']['SpectaclesLenses'][0]['ReturnableFlag'],
                            spectacleinventory: SpecData['result']['Optical']['SpectaclesLenses'][0]['Inventory'] === '0' ?
                                false : SpecData['result']['Optical']['SpectaclesLenses'][0]['Inventory'] === '' ? false :
                                    SpecData['result']['Optical']['SpectaclesLenses'][0]['Inventory'] === '1' ? true :
                                        SpecData['result']['Optical']['SpectaclesLenses'][0]['Inventory'],
                            // inventory:data["result"]["Optical"]["SpectaclesLenses"][0]["Inventory"],
                            spectacleminQty: SpecData['result']['Optical']['SpectaclesLenses'][0]['MinQty'] === 0 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['MinQty'],
                            spectaclemaxQty: SpecData['result']['Optical']['SpectaclesLenses'][0]['MaxQty'] === 0 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['MaxQty'],
                            maxAddPower: SpecData['result']['Optical']['SpectaclesLenses'][0]['maxAddPower'] === 0.00 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['maxAddPower'],
                            spectacleadd: SpecData['result']['Optical']['SpectaclesLenses'][0]['Sadd'] === 0.00 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['Sadd'],
                            maxDiameter: SpecData['result']['Optical']['SpectaclesLenses'][0]['Max Diameter'] === 0 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['Max Diameter'],
                            oversizeDiameterCharge:
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['OversizeDiameterCharge'] === 0.00 ? '' :
                                    SpecData['result']['Optical']['SpectaclesLenses'][0]['OversizeDiameterCharge'],
                            spectaclesphere: SpecData['result']['Optical']['SpectaclesLenses'][0]['Sphere'] === 0.00 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['Sphere'],
                            spectaclecylinder: SpecData['result']['Optical']['SpectaclesLenses'][0]['Cylinder'] === 0.00 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['Cylinder'],
                            baseCurve: SpecData['result']['Optical']['SpectaclesLenses'][0]['BaseCurve'] === 0 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['BaseCurve'],
                            minHeight: SpecData['result']['Optical']['SpectaclesLenses'][0]['MinHeight'] === 0.00 ? '' :
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['MinHeight'],
                            spectaclenotes: SpecData['result']['Optical']['SpectaclesLenses'][0]['Notes'],
                            pricing: SpecData['result']['Optical']['SpectaclesLenses'][0]['pricing'],
                            spectaclevspCode: SpecData['result']['Optical']['SpectaclesLenses'][0]['VspCode'],
                            spectacleprofit: '',
                            // styleId:parseInt(data["result"]["Optical"]["SpectaclesLenses"][0]["StyleId"]),
                            type: '',
                            // colorId:parseInt(data["result"]["Optical"]["SpectaclesLenses"][0]["ColorId"])
                            spectaclecolorId: SpecData['result']['Optical']['SpectaclesLenses'][0]['ColorId'] === '' ? '' :
                                parseInt(SpecData['result']['Optical']['SpectaclesLenses'][0]['ColorId'], 10),
                            spectaclestyleId: SpecData['result']['Optical']['SpectaclesLenses'][0]['StyleId'] === '' ? '' :
                                parseInt(SpecData['result']['Optical']['SpectaclesLenses'][0]['StyleId'], 10),
                        });

                        this.locationNameData = [];
                        this.orderlists = [];
                        this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
                            this.locationNameData = DataSet['result']['Optical']['Locations'];
                            this.inventory.getLocationByFilter(
                                SpecData['result']['Optical']['SpectaclesLenses'][0]['ItemId']).subscribe((filterData: any[]) => {
                                    this.orderlists = filterData['data'];
                                    this.orderlists.forEach(element => {
                                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                            if (element.loc_id === this.locationNameData[i].Id) {
                                                element.loc_id = this.locationNameData[i].Name;
                                            }
                                        }
                                    });
                                    console.log(this.locationGridData);
                                });
                        });

                    }
                });
            }
            if (this.LensTreatmentDetails === true) {
                this.lensTreatmentService.getLensTreatmentList('', '', this.Detailstext, '', '', '', '').subscribe(lensData => {
                    if (lensData['result']['Optical'] !== 'No record found') {
                        this.FarmesDetailsForm = this._fb.group({
                            // accessToken: this.Accesstoken,
                            Id: lensData['result']['Optical']['LensTreatment'][0]['ID'],
                            lensname: [lensData['result']['Optical']['LensTreatment'][0]['Name'], [Validators.required]],
                            lensvspCode: lensData['result']['Optical']['LensTreatment'][0]['vspCode'],
                            lenslocationId: '',
                            unitmeasure: [lensData['result']['Optical']['LensTreatment'][0]['unitmeasure'], [Validators.required]],
                            lenscategoryId: [lensData['result']['Optical']['LensTreatment'][0]['CategoryId'], [Validators.required]],
                            lensupc: [lensData['result']['Optical']['LensTreatment'][0]['UPC'], [Validators.required]],
                            lensretailPrice: lensData['result']['Optical']['LensTreatment'][0]['retail_price'] === 0.00 ? '' :
                                lensData['result']['Optical']['LensTreatment'][0]['retail_price'],
                            lensprofit: '',
                            lenscost: lensData['result']['Optical']['LensTreatment'][0]['Cost'] === 0.00 ? '' :
                                lensData['result']['Optical']['LensTreatment'][0]['Cost'],
                            lensinventory: lensData['result']['Optical']['LensTreatment'][0]['Inventory'] === '0' ? false :
                                lensData['result']['Optical']['LensTreatment'][0]['Inventory'] === '' ? false :
                                    lensData['result']['Optical']['LensTreatment'][0]['Inventory'] === '1' ? true :
                                        lensData['result']['Optical']['LensTreatment'][0]['Inventory'],
                            lenssendToLab: lensData['result']['Optical']['LensTreatment'][0]['sendToLab'] === '0' ? false :
                                lensData['result']['Optical']['LensTreatment'][0]['sendToLab'] === '' ? false :
                                    lensData['result']['Optical']['LensTreatment'][0]['sendToLab'] === '1' ? true :
                                        lensData['result']['Optical']['SpectaclesLenses'][0]['sendToLab'],
                            lensprintOnOrder: lensData['result']['Optical']['LensTreatment'][0]['printOnOrder'] === '0' ? false :
                                lensData['result']['Optical']['LensTreatment'][0]['printOnOrder'] === '' ? false :
                                    lensData['result']['Optical']['LensTreatment'][0]['printOnOrder'] === '1' ? true :
                                        lensData['result']['Optical']['LensTreatment'][0]['printOnOrder'],
                            lensStructureId: lensData['result']['Optical']['LensTreatment'][0]['StructureId'],
                            lensVcpModifiercode: lensData['result']['Optical']['LensTreatment'][0]['VcpModifierId'],
                            lensCommissionTypeId: lensData['result']['Optical']['LensTreatment'][0]['CommissionTypeId'],

                            lensamount: lensData['result']['Optical']['LensTreatment'][0]['Amount'],
                            grossPercentage: lensData['result']['Optical']['LensTreatment'][0]['GrossPercentage'],
                            spiff: lensData['result']['Optical']['LensTreatment'][0]['Spiff'],
                            lensnotes: lensData['result']['Optical']['LensTreatment'][0]['Notes'],

                        });
                        this.locationNameData = [];
                        this.orderlists = [];
                        this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
                            this.locationNameData = DataSet['result']['Optical']['Locations'];
                            this.inventory.getLocationByFilter(
                                lensData['result']['Optical']['LensTreatment'][0]['itemId']).subscribe((filterData: any[]) => {
                                    this.orderlists = filterData['data'];
                                    this.orderlists.forEach(element => {
                                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                            if (element.loc_id === this.locationNameData[i].Id) {
                                                element.loc_id = this.locationNameData[i].Name;
                                            }
                                        }
                                    });
                                    console.log(this.locationGridData);
                                });

                        });
                    }
                });
            } if (this.ContactDetails === true) {
                this.contactlensService.getcontactlens('', this.Detailstext, '', '', '', '').subscribe(contData => {
                    if (contData['result']['Optical'] !== 'No record found') {
                        this.FarmesDetailsForm = this._fb.group({
                            // accessToken: this.Accesstoken,
                            contupc: [contData['result']['Optical']['ContactLenses'][0]['UPC'], [Validators.required]],
                            contname: [contData['result']['Optical']['ContactLenses'][0]['Name'], [Validators.required]],
                            contmanufacturerId: [contData
                            ['result']['Optical']['ContactLenses'][0]['ManufacturerId'], [Validators.required]],
                            contvendorId: contData['result']['Optical']['ContactLenses'][0]['VendorId'],
                            contbrandId: ['', [Validators.required]],

                            contpackagingId: contData['result']['Optical']['ContactLenses'][0]['PackagingId'] === '' ?
                                contData['result']['Optical']['ContactLenses'][0]['PackagingId'] = null :
                                parseInt(contData['result']['Optical']['ContactLenses'][0]['PackagingId'], 10),

                            typeId: [''],
                            contmaterialId: [''],
                            wearScheduleId: [''],
                            // replacementId:data["result"]["Optical"]["ContactLenses"][0]["Replacement"]["length"]==0 ? 0 :
                            // data["result"]["Optical"]["ContactLenses"][0]["Replacement"][0],
                            supplyId: [''],
                            contcolorId: contData['result']['Optical']['ContactLenses'][0]['ColorId'] === '' ?
                                contData['result']['Optical']['ContactLenses'][0]['ColorId'] = '0' :
                                parseInt(contData['result']['Optical']['ContactLenses'][0]['ColorId'], 10),
                            // retailPrice: data["result"]["Optical"]["ContactLenses"][0]["RetailPrice"]== "" ?null:
                            // parseInt(data["result"]["Optical"]["ContactLenses"][0]["RetailPrice"]),
                            contretailPrice: contData['result']['Optical']['ContactLenses'][0]['RetailPrice'],
                            contsupplierId: '',
                            contlocationid: '',
                            configlocationid: '',
                            contmaxQty: contData['result']['Optical']['ContactLenses'][0]['max_qty'],
                            contminQty: contData['result']['Optical']['ContactLenses'][0]['min_qty'],
                            contnotes: contData['result']['Optical']['ContactLenses'][0]['Notes'],
                            traillensId: '',
                            contunit: contData['result']['Optical']['ContactLenses'][0]['Unit'],
                            ReplacementScheduleId:
                                contData['result']['Optical']['ContactLenses'][0]['ReplacementScheduleId'] === '' ? null :
                                    parseInt(contData['result']['Optical']['ContactLenses'][0]['ReplacementScheduleId'], 10),
                            // hardContact: data["result"]["Optical"]["ContactLenses"][0]["HardContact"] == 1 ? true : false,
                            // inventory: data["result"]["Optical"]["ContactLenses"][0]["Inventory"] == 1 ? true : false,
                            // cost:data["result"]["Optical"]["ContactLenses"][0]["Cost"]== ""?null:
                            // parseInt(data["result"]["Optical"]["ContactLenses"][0]["Cost"]),
                            contcost: contData['result']['Optical']['ContactLenses'][0]['Cost'],
                            contprocedureCode: contData['result']['Optical']['ContactLenses'][0]['ProcedureCode'],
                            contprofit: '',
                            six_months_price: contData['result']['Optical']['ContactLenses'][0]['six_months_price'],
                            six_months_qty: contData['result']['Optical']['ContactLenses'][0]['six_months_qty'],
                            twelve_months_price: contData['result']['Optical']['ContactLenses'][0]['twelve_months_price'],
                            twelve_months_qty: contData['result']['Optical']['ContactLenses'][0]['twelve_months_qty'],
                        });
                        this.locationNameData = [];
                        this.orderlists = [];
                        this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
                            this.locationNameData = DataSet['result']['Optical']['Locations'];
                            this.inventory.getLocationByFilter(
                                contData['result']['Optical']['ContactLenses'][0]['ItemId']).subscribe((filterData: any[]) => {
                                    this.orderlists = filterData['data'];
                                    this.orderlists.forEach(element => {
                                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                            if (element.loc_id === this.locationNameData[i].Id) {
                                                element.loc_id = this.locationNameData[i].Name;
                                            }
                                        }
                                    });
                                    console.log(this.locationGridData);
                                });
                        });

                    }
                });
            } if (this.otherDetails === true) {
                this.othersService.getotherinventories(this.Detailstext, '', '', '', '', '').subscribe(otherData => {
                    if (otherData['result']['Optical'] !== 'No record found') {
                        this.FarmesDetailsForm = this._fb.group({
                            // accessToken: this.Accesstoken,
                            otherupc: [otherData['result']['Optical']['OtherInventory'][0]['UPC'], [Validators.required]],
                            othername: [otherData['result']['Optical']['OtherInventory'][0]['Name'], [Validators.required]],
                            categoryId: [otherData['result']['Optical']['OtherInventory'][0]['CategoryId'] === '' ? null :
                                parseInt(otherData['result']['Optical']['OtherInventory'][0]['CategoryId'], 10), [Validators.required]],
                            otherVendorId: otherData['result']['Optical']['OtherInventory'][0]['VendorId'],
                            otherretailPrice: otherData['result']['Optical']['OtherInventory'][0]['RetailPrice'],
                            othercost: otherData['result']['Optical']['OtherInventory'][0]['Cost'],
                            otherinventory: otherData['result']['Optical']['OtherInventory'][0]['Inventory'] === 1 ? true : false,
                            othersendToLab: otherData['result']['Optical']['OtherInventory'][0]['SendToLab'] === 1 ? true : false,
                            otherprintOnOrder: otherData['result']['Optical']['OtherInventory'][0]['PrintOnOrder'] === 1 ? true : false,
                            otherprocedureCode: otherData['result']['Optical']['OtherInventory'][0]['ProcedureCode'],
                            modifierId: '',
                            otherstructureId: '',
                            othernotes: otherData['result']['Optical']['OtherInventory'][0]['Notes'],
                            otherprofit: '',
                            otherminQty: '',
                            othermaxQty: '',
                            otherCommissionTypeId: otherData['result']['Optical']['OtherInventory'][0]['CommissionTypeId'],
                            // locationid: '',

                        });
                        this.locationNameData = [];
                        this.orderlists = [];
                        this.dropdownService.getInventtoryLocations().subscribe(DataSet => {
                            this.locationNameData = DataSet['result']['Optical']['Locations'];
                            this.inventory.getLocationByFilter(
                                otherData['result']['Optical']['OtherInventory'][0]['ItemId']).subscribe((filterData: any[]) => {
                                    this.orderlists = filterData['data'];
                                    this.orderlists.forEach(element => {
                                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                            if (element.loc_id === this.locationNameData[i].Id) {
                                                element.loc_id = this.locationNameData[i].Name;
                                            }
                                        }
                                    });
                                    console.log(this.locationGridData);
                                });
                        });

                    }
                });
            }
        });
    }




    Loadmaterials() {
        this.materials = this.dropdownService.framesmaterials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getFrameMaterial().subscribe((values: Material[]) => {      
                try {
                    const dropData  = values;
                    this.materials.push({id: '', materialname: 'Select Material'});
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        dropData[i].id = dropData[i].id.toString();
                        this.materials.push(dropData[i]);
                    }
                    this.dropdownService.framesmaterials = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    /**
     * Loads frames type
     * @returns frametype 
     */
    loadFramesType() {
        this.framesTypeDropDown = [];
        this.framesTypeDropDown = this.frameService.framesTypeDropDownData;
        if (this.utility.getObjectLength(this.framesTypeDropDown) === 0) {
            this.frameService.getFramesType().subscribe((values: FrameTypeMaster) => {
                try {
                    const dropData = values.data;
                    this.framesTypeDropDown.push({ Id: '', Name: 'Select Frametype' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesTypeDropDown.push({ Id: dropData[i].id, Name: dropData[i].type_name });
                    }
                    this.frameService.framesTypeDropDownData = this.framesTypeDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads rim type
     * @returns rimtype data
     */
    loadRimType() {
        this.framesRimType = this.dropdownService.rimType;
        if (this.utility.getObjectLength(this.framesRimType) === 0) {
            this.dropdownService.getRimType().subscribe((values: RimType[]) => {
                try {
                    this.framesRimType.push({ Id: '', Name: 'Select Rim Type', ColorCode: '' });
                    for (let i = 0; i <= values.length - 1; i++) {
                        this.framesRimType.push({ Id: values[i].id, Name: values[i].rimtype, ColorCode: values[i].colorcode });
                    }
                    this.dropdownService.rimType = this.framesRimType;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads source
     * @returns source data
     */
    loadSource() {
        this.framesSource = this.inventory.frameSourceData;
        if (this.utility.getObjectLength(this.framesSource) === 0) {
            this.frameService.getSource().subscribe((values: FrameSource) => {

                try {
                    const dropData = values.data;
                    this.framesSource.push({ Id: '', Name: 'Select Source Type' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesSource.push({ Id: dropData[i].id, Name: dropData[i].sourcename });
                    }
                    this.framesSource = this.inventory.frameSourceData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }
    }
    /**
     * Loads upc type
     * @returns upc type data
     */
    loadUpcType() {


        this.framesUpcType = [];
        this.framesUpcType = this.inventory.upcTypeData;
        if (this.utility.getObjectLength(this.framesUpcType) === 0) {
            this.frameService.getUpcType().subscribe((values: InvUpcType) => {
                try {
                    const dropData = values.data;
                    this.framesUpcType.push({ Id: '', Name: 'Select UPC Type' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framesUpcType.push({ Id: dropData[i].id, Name: dropData[i].upctype });
                    }
                    this.frameService.framesUpcTypeData = this.framesUpcType;

                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads frames shape
     * @returns\ frameshape data
     */
    loadFramesShape() {
        this.framesShapeDropDown = [];
        this.framesShapeDropDown = this.inventory.frameShape;
        if (this.utility.getObjectLength(this.framesShapeDropDown) === 0) {
            this.frameService.getFramesshape().subscribe((values: FrameShapeMaster) => {
                try {
                    const dropData = values.data;
                    this.framesShapeDropDown.push({ Id: '', Name: 'Select Frameshape' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                    this.framesShapeDropDown.push({ Id: dropData[i].id, Name: dropData[i].shape_name });
                     }
                    this.frameService.framesShapeDropDownData = this.framesShapeDropDown;

                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }
    }

    /**
     * Loads lens 
     * @returns lenscolor data
     */
    LoadLensColor() {
        this.lenscolor = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.lenscolor) === 0) {
            try {
                this.dropdownService.getFrameLenscolor().subscribe((values: LensColorMaster) => {
                    const dropData = values.data;
                    this.lenscolor.push({ Id: '', Name: 'Select Lens Color' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        {
                            this.lenscolor.push({ Id: dropData[i].id, Name: dropData[i].colorname });
                        }
                        this.dropdownService.lensColors = this.lenscolor;
                    };
                });
            } catch (error) {
                this.error.syntaxErrors(error);
            }

        }
    }
    // loadUsage() {
    //     this.framesUsage = [];
    //     this.frameService.getUsage().subscribe((data: any[]) => {
    //         data['data'].forEach((element: any) => {
    //             this.framesUsage.push({ Id: element.id, Name: element.usagetype });
    //         });
    //     });

    // }
    /**
     * Gets supplier details
     * @returns supplier details 
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }


    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        this.lensstylcopy = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    this.lensstylcopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads material 
     * @returns material data
     */
    loadMaterialList() {
        this.MaterialListDropDown = this.dropdownService.materials;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ value: '', name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.materials = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    LoadLenstreatmentdetails() {
        this.LensTreatments = this.dropdownService.lensTreatments;
        if (this.utility.getObjectLength(this.LensTreatments) === 0) {
            this.dropdownService.getLensCategories().subscribe(
                (Values:TreatmentCategory) => {
                    try {
                        const dropData = Values.data;
                        this.LensTreatments.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.LensTreatments.push({ Id: dropData[i].id, Name: dropData[i].categoryname });
                        }
                        this.dropdownService.lensTreatments = this.LensTreatments;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loads drop 
     * @returns vcp data
     */
    loadDropDowns() {
        this.vcpDropData = this.inventory.vpcData;
        if (this.utility.getObjectLength(this.vcpDropData) === 0) {
            this.inventory.getVCPdropDownData().subscribe((values: VcpDropDown) => {
                try {
                    const dropData = values.data;
                    this.vcpDropData.push({ Id: '', Name: 'Select VCP Vision' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.vcpDropData.push({ Id: dropData[i]['id'].toString(), Name: dropData[i]['description'] });
                    }
                    this.inventory.vpcData = this.vcpDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }


        this.StructureDropData = this.inventory.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.inventory.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].structure_name });
                    }
                    this.inventory.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
        this.GetCommissionType();

    }


    /**
     * Gets commission type
     * @returns commission details
     */
    GetCommissionType() {
        this.TypeDropData = this.inventory.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.inventory.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this.inventory.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Getcategorys 
     * @returns category data
     */
    GetcategoryDetails() {
        this.categories = this.othersService.categoriesData;
        if (this.utility.getObjectLength(this.categories) === 0) {
            this.othersService.getcategorydata().subscribe((values: OtherInvCategory) => {
                try {
                    const dropData = values.data;
                    this.categories.push({ Id: '', Name: 'Select Category' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.categories.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                    }
                    this.othersService.categoriesData = this.categories;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    Getcontacttraillens() {

        this.traillens = [];
        this.traillenslist = [];

        this.traillens.push({ Id: '0', Name: 'Select Lens' });
        this.contactlensService.getcontacttraillens().subscribe(
            data => {
                this.traillenslist = data['result']['Optical']['ContactLenses'];
                this.traillenslist.forEach(item => {
                    if (!this.traillens.find(a => a.Name === item.Name)) {
                        this.traillens.push({ Id: item.ItemId, Name: item.Name });
                    }
                });
            });
    }

    getPackagingData() {
        this.packageData = this.contactlensService.contactPackage;
        if (this.utility.getObjectLength(this.packageData) === 0) {
        this.contactlensService.getcontactreplacements().subscribe(
            (values: ContactPackage) => {
                try {
                    const dropData = values.data;
                    this.packageData.push({ id: '', opt_val: 'Select Package' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.packageData.push(dropData[i]);
                    }
                    this.contactlensService.contactPackage = this.packageData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }

    /**
     * Loads color 
     * @returns color copy data
     */
    LoadColorCopy() {
        this.color = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.color) === 0) {
            try {
                this.dropdownService.getFrameLenscolor().subscribe((values: LensColorMaster) => {
                    const dropData = values.data;
                    this.color.push({ Id: '', Name: 'Select Lens Color' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        {
                            this.color.push({ Id: dropData[i].id, Name: dropData[i].colorname, colorcode: dropData[i].colorcode });
                        }
                        this.dropdownService.lensColors = this.color;
                    };
                });
            } catch (error) {
                this.error.syntaxErrors(error);
            }

        }

    }
    Getcontactlensreplacements() {
        this.replacements = this.contactlensService.contactReplacement;
        if (this.utility.getObjectLength(this.replacements) === 0) {
        this.contactlensService.getcontactreplacements().subscribe(
            (values: ContactReplacement) => {
                try {
                    const dropData = values.data;
                    this.replacements.push({ Id: '', Name: 'Select Schedule' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.replacements.push({ Id: dropData[i].id, Name: dropData[i].opt_val });
                    }
                    this.contactlensService.contactReplacement = this.replacements;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    // sidenav
    GetUpc(data) {
        if (data === 'cancle') {
            this.selectInvdetails = false;
        } else {
            this.Detailstext = '';
            this.Detailstext = data;
            this.onDetailClick();
            this.selectInvdetails = false;
        }

    }

    /**
     * Loads contact lens colors
     * @returns  contactlens color details
     */
    loadContactLensColors() {
            this.contacLenstColor =  this.contactlensService.colorsData;
            if (this.utility.getObjectLength(this.contacLenstColor) === 0) {
            this.contactlensService.getContactlensColors('').subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.contacLenstColor.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                                this.contacLenstColor.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.contactlensService.colorsData = this.contacLenstColor;
                } catch(error) {
                    this.error.syntaxErrors(error);
                }
                });
            }
    }
    /**
     * Loads labs
     * @returns  labs details
     */
    getlensLab() {
        this.lenslab = this.dropdownService.lensLab;
        if (this.utility.getObjectLength(this.lenslab) === 0) {
            this.dropdownService.getLensLabDetails().subscribe((values: LensLab) => {
                try {
                    const dropData = values.data;
                    this.lenslab.push({ Id: '', Name: 'Select Lab' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.lenslab.push({ Id: dropData[i].id, Name: dropData[i].lab_name });
                    }
                    this.dropdownService.lensLab = this.lenslab;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
}
