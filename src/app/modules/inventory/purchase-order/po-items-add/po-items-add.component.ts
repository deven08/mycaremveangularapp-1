import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
import { UtilityService } from '@app/shared/services/utility.service';

@Component({
  selector: 'app-po-items-add',
  templateUrl: './po-items-add.component.html',
  styleUrls: ['./po-items-add.component.css']
})
export class PoItemsAddComponent implements OnInit {
  @Input() moduleTypeId: string;
  @Output() ItemsEmit: EventEmitter<any> = new EventEmitter();
  itemsData: any = [];
  selectedItems: any = [];
  checkedSelectedItems: any = [];
  constructor(private purchaseOrderService: PurchaseOrderService,
    public utilityService: UtilityService) { }

  ngOnInit() {
    this.gettingItems();

  }
  checkBoxClick(event, index, data) {

    // if(this.selectedItems.length != 0){
    if (event == true) {
      this.selectedItems.push(data)
    } else {
      if (this.checkedSelectedItems.length != 0) {
        for (let i = 0; i <= this.checkedSelectedItems.length - 1; i++) {
          if (data.id == this.checkedSelectedItems[i].id) {
            this.selectedItems.splice(i, 1);
          }
        }
      }
    }
    // }
    this.checkedSelectedItems = this.selectedItems;
  }
  itemsSelect() {
    this.ItemsEmit.emit(this.selectedItems)
  }
  gettingItems() {
    if (this.purchaseOrderService.POsupplier != '' && this.purchaseOrderService.POlocation != '') {
      let reqBody = {
        "filter": [
          {
            "field": "module_type_id",
            "operator": "=",
            "value": this.moduleTypeId
          },
          {
            "field": "vendor_id",
            "operator": "=",
            "value": this.purchaseOrderService.POsupplier
          },
          {
            "field": "loc_id",
            "operator": "=",
            "value": this.purchaseOrderService.POlocation
          },

        ],
        "sort": [
          {
            "field": "name",
            "order": "ASC"
          }
        ]
      }
      this.itemsData = [];
      this.purchaseOrderService.getitemByModuleType(reqBody).subscribe(data => {
        this.itemsData = data['data'];
      })
    }
  }
}
