import { Component, OnInit } from '@angular/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService, AppService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
import { DatePipe } from '@angular/common';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-inventory-purchaseorder',
    templateUrl: 'inventory-purchaseorder.component.html'
})

export class InventoryPurchaseOrderComponent implements OnInit {

    poSupplierShow = true;
    poItemsShow = false;
    poOrdersShow = false;
    poReceivedShow = false;
    poStatusShow = false;
    poSearchSidebar = false;
    /**
     * Updateitems grid data of inventory purchase order component
     */
    updateitemsGridData: any = [];
    /**
     * Purchaselocations  of inventory purchase order component
     */
    purchaselocations: any;
    locations: any[];
    /**
     * Poform  of inventory purchase order component
     */
    POForm: FormGroup;
    /**
     * Posupplier data of inventory purchase order component
     */
    POsupplierData: any;
    /**
     * Items  of inventory purchase order component
     */
    items: any = [];
    /**
     * Update id of inventory purchase order component
     */
    updateId: any = '';
    /**
     * Labstatusdata  of inventory purchase order component
     */
    labstatusdata: any = [];
    /**
     * Labstatuslist  of inventory purchase order component
     */
    labstatuslist: any = [];
    /**
     * Creates an instance of inventory purchase order component.
     * @param dropdownService  for common dropdowns
     * @param errorService for handling errors
     * @param utility for getting array length
     * @param _fb for formgroups
     * @param purchaseOrderService  for purchase service methods
     * @param error for toster alerts
     * @param datePipe for formating date pipes
     */
    constructor(public dropdownService: DropdownService,
        private errorService: ErrorService,
        private utility: UtilityService,
        private _fb: FormBuilder,
        public purchaseOrderService: PurchaseOrderService,
        private error: ErrorService,
        public app: AppService,
        private datePipe: DatePipe) {

    }

    /**
     * on init for initilizing methods
     */
    ngOnInit() {
        this.typrDropDown();
        this.purchaseOrderService.supplieerData = '';
        this.purchaseOrderService.searchsupplieerData = '';
        this.purchaseOrderService.POsupplier = '';
        this.purchaseOrderService.POlocation = '';
        this.purchaseOrderService.savingItemsList = [];
        this.purchaseOrderService.UpdateId = '';
        this.locations = [
            { name: 'MVE: My Vision', retail: '', source: '', Purchased: '', Sold: '', onhand: '', onorder: '', committed: '' },
            { name: 'MVE: My Vision', retail: '', source: '', Purchased: '', Sold: '', onhand: '', onorder: '', committed: '' }
        ];
        this.LoadLocations();
        this.Loadlabstatus();
        this.LoadForms();
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
        if (this.purchaseOrderService.POsupplier != '') {
            this.POForm.controls.supplier_name.patchValue(this.purchaseOrderService.POsupplier);
        }
        if (this.purchaseOrderService.POlocation != '') {
            this.POForm.controls.location_id.patchValue(this.purchaseOrderService.POlocation);
        }
        if (this.purchaseOrderService.searchsupplieerData != '') {
            let event = this.purchaseOrderService.searchsupplieerData;
            this.POForm = this._fb.group({
                supplier_name: event.supplier_name,
                location_id: [event.locationid.toString(), [Validators.required]],
                id: [event.id, [Validators.required]],
                po_date: event.po_date,
                po_status: [event.po_status, [Validators.required]],
                reference: event.reference,
                notes: event.reference

            });
            this.purchaseOrderService.formValidate = this.POForm.valid;
            this.POSaveEnable();
            // this.POForm.controls.id.patchValue(event.id);
            // this.POForm.controls.po_date.patchValue(event.po_date);
            // this.POForm.controls.po_status.patchValue(event.po_status);
            // this.POForm.controls.location_id.patchValue(event.locationid.toString());
            // this.POForm.controls.supplier_name.patchValue(event.supplier_name);
            // this.POForm.controls.reference.patchValue(event.reference);
            // this.POForm.controls.notes.patchValue(event.reference);
        }

    }
    /**
     * Loadlabstatus inventory purchase order component
     * for loading status dropdowns
     */
    Loadlabstatus() {
        this.labstatusdata = [];
        this.labstatusdata = this.purchaseOrderService.labstatus;
        if (this.utility.getObjectLength(this.labstatusdata) === 0) {
            this.labstatusdata.push({ Id: '', Name: 'Select Status' });
            this.purchaseOrderService.Getstatusdropdata().subscribe(
                data => {
                    try {
                        this.labstatuslist = data;
                        for (let i = 0; i <= this.labstatuslist.length - 1; i++) {
                            if (!this.labstatusdata.find(a => a.Name === this.labstatuslist[i].description)) {
                                this.labstatusdata.push({ Id: this.labstatuslist[i].id.toString(), Name: this.labstatuslist[i].description });
                            }
                        }
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.purchaseOrderService.labstatus = this.labstatusdata;
                });
        }
    }
    /**
     * Loads forms
     * for formgroup
     */
    LoadForms() {
        this.POForm = this._fb.group({
            supplier_name: '',
            location_id: ['', [Validators.required]],
            id: ['', [Validators.required]],
            po_date: '',
            po_status: ['', [Validators.required]],
            reference: '',
            notes: ''
        });
    }

    /**
     * Suppliers namedata
     * @param event for getting supplier name data
     */
    SupplierNamedata(event) {
        this.POForm.controls.supplier_name.patchValue(event);
    }
    /**
     * Loads locationsload location dropdown
     */
    LoadLocations() {

        this.purchaselocations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.purchaselocations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.purchaselocations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.purchaselocations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.purchaselocations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }

    }
    poSupplier() {
        this.poSupplierShow = true;
        this.poItemsShow = false;
        this.poOrdersShow = false;
        this.poReceivedShow = false;
        this.poStatusShow = false;
    }
    /**
     * items click
     * for shownig warning
     */
    poItemsClick() {
        if (this.purchaseOrderService.POlocation != '' && this.purchaseOrderService.POsupplier) {
            this.poSupplierShow = false;
            this.poItemsShow = true;
            this.poOrdersShow = false;
            this.poReceivedShow = false;
            this.poStatusShow = false;
        } else {
            this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please Select location and Supplier' });
        }


        // if (this.purchaseOrderService.supplieerData != '') {
        //     this.POForm.controls.supplier.patchValue(this.purchaseOrderService.supplieerData['vendor_name']);
        // }


    }
    poOrdersClick() {
        this.poSupplierShow = false;
        this.poItemsShow = false;
        this.poOrdersShow = true;
        this.poReceivedShow = false;
        this.poStatusShow = false;
    }
    poReceivedClick() {
        this.poSupplierShow = false;
        this.poItemsShow = false;
        this.poOrdersShow = false;
        this.poReceivedShow = true;
        this.poStatusShow = false;
    }
    poStatusClick() {
        this.poSupplierShow = false;
        this.poItemsShow = false;
        this.poOrdersShow = false;
        this.poReceivedShow = false;
        this.poStatusShow = true;
    }
    /**
     * search side
     */
    poSearchSide() {
        this.poSearchSidebar = true;
        this.poSupplierShow = true;
        this.poItemsShow = false;
    }
    /**
     * Locations change
     * @param event for getting location id
     */
    locationChange(event) {
        this.purchaseOrderService.POlocation = this.POForm.controls.location_id.value
        console.log(this.POForm.controls.location_id.value);
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
    }
    /**
     * Types change for type id
     */
    typeChange() {
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
    }
    /**
     * Status change for getting status id 
     */
    statusChange() {
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
    }
    /**
     * Poselected data event
     * @param event for getting search data
     */
    POSelectedDataEvent(event) {
        this.poSearchSidebar = false;
        this.purchaseOrderService.supplieerData = '';
        this.updateitemsGridData = []
        this.purchaseOrderService.POlocation = event.locationid.toString();
        this.POForm = this._fb.group({
            supplier_name: event.supplier_name,
            location_id: [event.locationid.toString(), [Validators.required]],
            id: [event.po_type_id, [Validators.required]],
            po_date: event.po_date,
            po_status: [event.po_status, [Validators.required]],
            reference: event.reference,
            notes: event.reference

        });
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
        // this.POForm.controls.id.patchValue(event.po_type_id);
        // this.POForm.controls.po_date.patchValue(event.po_date);
        // this.POForm.controls.po_status.patchValue(event.po_status);
        // this.POForm.controls.location_id.patchValue(event.locationid.toString());
        // this.POForm.controls.supplier_name.patchValue(event.supplier_name);
        // this.POForm.controls.reference.patchValue(event.reference);
        // this.POForm.controls.notes.patchValue(event.reference);

        this.updateId = event.id;
        this.purchaseOrderService.getPOitems(this.updateId).subscribe(data => {
            for (let i = 0; i <= data['data'].length - 1; i++) {
                let itemObject = {
                    "id": data['data'][i].id,
                    "type_id": data['data'][i].module_type_id,
                    "type_desc": data['data'][i].item_description,
                    "upc_code": data['data'][i].upc,
                    "sourceid": data['data'][i].source,
                    "quantity": data['data'][i].quantity,
                    "wholesale_cost": data['data'][i].cost,
                    "notes": data['data'][i].notes
                }
                this.updateitemsGridData.push(itemObject);
            }
            this.purchaseOrderService.savingItemsList = this.updateitemsGridData;
        })
    }
    /**
     * Type drop down 
     */
    typrDropDown() {
        this.items = [];
        this.items = this.purchaseOrderService.POtype;
        if (this.utility.getObjectLength(this.items) === 0) {
            this.purchaseOrderService.getTypesDropDown().subscribe(data => {
                this.items.push({ "id": '', "description": "Select Type" })
                try {
                    for (let i = 0; i <= data['length'] - 1; i++) {
                        this.items.push(data[i]);
                    }
                } catch (error) {
                    this.errorService.syntaxErrors(error);
                }
                this.purchaseOrderService.POtype = this.items;
            })

        }

    }
    /**
     * Supliers save form data
     * @param event for getting supplier form data
     */
    suplierSaveFormData(event) {
        this.POsupplierData = event;
    }
    /**
     * Poforms change for validation
     */
    POformChange() {
        this.purchaseOrderService.formValidate = this.POForm.valid;
        this.POSaveEnable();
    }
    /**
     * Posaves enable for saving
     */
    POSaveEnable() {
        if (this.purchaseOrderService.supplierValidate == true && this.purchaseOrderService.formValidate == true) {
            this.purchaseOrderService.savebuttonEnable = true;
        } else {
            this.purchaseOrderService.savebuttonEnable = false;
        }
    }
    /**
     * Saving po for saving 
     */
    savingPO() {
        // console.log(this.POsupplierData);
        // let warningmessage = '';
        // Object.keys(this.POForm.controls).forEach(key => {
        //     if (this.POForm.get(key).value == '' || this.POForm.get(key).value == null) {
        //         if (warningmessage == '') {
        //             warningmessage = key;
        //         }
        //         else {
        //             warningmessage = warningmessage + ',' + key;
        //         }
        //     }
        // })
        // if (warningmessage == '') {
        //     // this.purchaseOrderService.POSaveData.next(this.POForm.value);
        //     let warningmessagesupplier = '';
        //     Object.keys(this.POsupplierData.controls).forEach(key => {
        //         if (this.POsupplierData.get(key).value == '' || this.POsupplierData.get(key).value == null) {
        //             if (warningmessagesupplier == '') {
        //                 warningmessagesupplier = key;
        //             }
        //             else {
        //                 warningmessagesupplier = warningmessagesupplier + ',' + key;
        //             }
        //         }
        //     })
        // if (warningmessagesupplier == '') {
        // this.savingPOsupplier(PoOrderdata, this.PurchaseForm.value)


        if (this.purchaseOrderService.savingItemsList.length != 0) {

            if (this.purchaseOrderService.UpdateId == '') {
                this.SavingPOitem();
            } else {
                this.UpdatingPOitem();
            }
        } else {
            this.errorService.displayError({ severity: 'info', summary: 'info Message', detail: 'Please Select Items' });
        }
        //     } else {
        //         this.errorService.displayError({ severity: 'info', summary: 'info Message', detail: 'Please Select ' + warningmessage });
        //     }

        // } else {
        //     this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please Select ' + warningmessage });
        // }
    }
    /**
     * Saving poitem
     * for saving the POitems
     */
    SavingPOitem() {
        let itemsArray: any = [];
        for (let i = 0; i <= this.purchaseOrderService.savingItemsList.length - 1; i++) {
            if (this.purchaseOrderService.savingItemsList[i].sourceid === null) {
                this.purchaseOrderService.savingItemsList[i].sourceid = '';
            }
            itemsArray.push({
                "itemid": this.purchaseOrderService.savingItemsList[i].id,
                "quantity": this.purchaseOrderService.savingItemsList[i].quantity,
                "module_type_id": this.purchaseOrderService.savingItemsList[i].module_type_id,
                // "item_description": this.purchaseOrderService.savingItemsList[i].type_desc,
                // "notes":this.purchaseOrderService.savingItemsList[i].notes,
                "upc": this.purchaseOrderService.savingItemsList[i].upc_code,
                "source": this.purchaseOrderService.savingItemsList[i].sourceid.toString(),
                "cost": this.purchaseOrderService.savingItemsList[i].wholesale_cost
            })
            for (const key in itemsArray[i]) {
                if (itemsArray[i][key] === null || itemsArray[i][key] === '') {
                    delete itemsArray[i][key];
                }
            }
        }
        let reqbody = {
            "po_date": this.datePipe.transform(this.POForm.value.po_date, 'yyyy-MM-dd'),
            "po_status": this.POForm.value.po_status,
            "supplierid": this.POsupplierData.controls.vendor_id.value,
            "locationid": this.POForm.value.location_id,
            "po_type_id": this.POForm.value.id.toString(),
            "supplier_name": this.POForm.value.supplier_name,
            "saddress1": this.POsupplierData.controls.vendor_address1.value,
            "saddress2": this.POsupplierData.controls.vendor_address2.value,
            "scity": this.POsupplierData.controls.city.value,
            "sstate": this.POsupplierData.controls.state.value,
            "szip": this.POsupplierData.controls.zip.value,
            "sphone": this.POsupplierData.controls.mobile.value,
            "sfax": this.POsupplierData.controls.fax.value,
            "semail": this.POsupplierData.controls.email.value,
            "reference": this.POForm.value.reference,
            "notes": this.POForm.value.notes,
            "PoItems": itemsArray
        }
        let modifiedReqSave = this.utility.formatWithOutControls(reqbody)
        // for (const key in reqbody) {
        //     if (reqbody[key] === null || reqbody[key] === '') {
        //         delete reqbody[key];
        //       }
        // }
        this.purchaseOrderService.savePOdata(modifiedReqSave).subscribe(data => {
            this.LoadForms();
            this.poSearchSidebar = false;
            this.poSupplierShow = true;
            this.poItemsShow = false;
            this.poOrdersShow = false;
            this.poReceivedShow = false;
            this.poStatusShow = false;
            this.purchaseOrderService.savingItemsList = [];
            this.purchaseOrderService.UpdateId = '';
            this.purchaseOrderService.supplieerData = '';
            this.purchaseOrderService.searchsupplieerData = '';
            this.purchaseOrderService.POsupplier = '';
            this.purchaseOrderService.POlocation = '';
            this.purchaseOrderService.POitemsUpdate.next();
            this.errorService.displayError({ severity: 'success', summary: 'Success Message', detail: 'Purchase Order Saved Successfully' });
        })
    }
    /**
     * Updating poitem
     *  for updating the POitems
     */
    UpdatingPOitem() {
        let itemsupdateArray: any = [];
        for (let i = 0; i <= this.purchaseOrderService.savingItemsList.length - 1; i++) {
            if (this.purchaseOrderService.savingItemsList[i].sourceid === null) {
                this.purchaseOrderService.savingItemsList[i].sourceid = '';
            }
            itemsupdateArray.push({
                "id": this.purchaseOrderService.savingItemsList[i].id,
                "quantity": this.purchaseOrderService.savingItemsList[i].quantity,
                "module_type_id": this.purchaseOrderService.savingItemsList[i].module_type_id,
                // "item_description": this.purchaseOrderService.savingItemsList[i].type_desc,
                // "notes":this.purchaseOrderService.savingItemsList[i].notes,
                "upc": this.purchaseOrderService.savingItemsList[i].upc_code,
                "source": this.purchaseOrderService.savingItemsList[i].sourceid.toString(),
                "cost": this.purchaseOrderService.savingItemsList[i].wholesale_cost
            })
            for (const key in itemsupdateArray[i]) {
                if (itemsupdateArray[i][key] === null || itemsupdateArray[i][key] === '') {
                    delete itemsupdateArray[i][key];
                }
            }
        }
        let updateReqBody = {
            "notes": this.POForm.value.notes,
            "PoItems": itemsupdateArray
        }
        let modifiedReqSave = this.utility.formatWithOutControls(updateReqBody)
        this.purchaseOrderService.updatePOdata(modifiedReqSave, this.purchaseOrderService.UpdateId).subscribe(data => {
            this.LoadForms();
            this.poSearchSidebar = false;
            this.poSupplierShow = true;
            this.poItemsShow = false;
            this.poOrdersShow = false;
            this.poReceivedShow = false;
            this.poStatusShow = false;
            this.purchaseOrderService.savingItemsList = [];
            this.purchaseOrderService.UpdateId = '';
            this.purchaseOrderService.supplieerData = '';
            this.purchaseOrderService.searchsupplieerData = '';
            this.purchaseOrderService.POsupplier = '';
            this.purchaseOrderService.POlocation = '';
            this.purchaseOrderService.POitemsUpdate.next();
            this.errorService.displayError({ severity: 'success', summary: 'Success Message', detail: 'Purchase Order Updated Successfully' });
        })
    }
}
