import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
// import { LensTreatmentService } from '../../../ConnectorEngine/services/lenstreatments.service';

@Component({
    selector: 'app-procedure-code-search',
    templateUrl: 'procedure-code-search.component.html'
})

export class ProcedureCodeSearchComponent implements OnInit {
    procedureCodeGrid: any[];
    selectedProcedure: any;
    @Output() procedureData = new EventEmitter<any>();
    constructor(private lensTreatmentService: LenstreatmentService) {

    }
    ngOnInit() {
        this.procedureCodeGrid = [];
        this.lensTreatmentService.getLensTreatmentProcedures().subscribe((data: any[]) => {
            this.procedureCodeGrid = data['data'];
            console.log(this.procedureCodeGrid);
        });
    }
    onRowSelect(event) {
        this.selectedProcedure = event.data;
    }
    onRowUnselect(event) {
        this.selectedProcedure = '';
    }
    DoubleClickCpt(event) {
        this.procedureData.emit(event);
    }
    onOkClick() {
        this.procedureData.emit(this.selectedProcedure);
    }
    onCancleClick() {
        this.procedureData.emit('cancle');
    }
}
