import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';
import { Input } from '@angular/core';
import { FramesService } from '@services/inventory/frames.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';

@Component({
    selector: 'app-contactlens-receiving',
    templateUrl: 'contactlens-receiving.component.html'
})

export class ContactlensReceivingComponent implements OnInit {
    framelist: any[];
    orderTypes: any[];
    groupBy: any[];
    RecievingInventorySelection = [];
    @Input() dataFromRecievingComponent: any;
    @Output() contactlensoutput = new EventEmitter<any>();
    constructor(private frameService: FramesService, private inventory: InventoryService) {

    }
    ngOnInit() {
        this.GetList();
    }
    GetList() {
        this.framelist = [];
        // console.log(this.dataFromRecievingComponent)
        // console.log(this.dataFromRecievingComponent.loc_id);
        const vendorId = this.dataFromRecievingComponent.supplierid == null ? '' : this.dataFromRecievingComponent.supplierid;
        const locationId = this.dataFromRecievingComponent.loc_id == null ? '' : this.dataFromRecievingComponent.loc_id;
        const data = {
            filter: [
                {
                    field: 'vendor_id',
                    operator: '=',
                    value: vendorId
                },
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '3'
                }],
        };
        this.inventory.filterInventories(data).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            data => {
                // console.log(data);
                this.framelist = data['data'];
                // alert(this.framelist);
                console.log(this.framelist);
            });
    }
    onRowSelect(event) {
        console.log(event);

    }
    onRowUnselect(event) {
        console.log(event);
    }
    addProduct() {
        console.log(this.RecievingInventorySelection);
        this.contactlensoutput.emit(this.RecievingInventorySelection);
        // this.frameService.selectedRecievingValue.next(this.RecievingInventorySelection);
    }
    onClosecontactLens() {
        this.contactlensoutput.emit('close');
    }
}
