import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core/services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService } from '@app/core';
import { ManufacturerDropData, FrameColorData, FrameBrands } from '@app/core/models/masterDropDown.model';
import { ContactlensService } from '@app/core/services/order/contactlens.service';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';



@Component({
    selector: 'app-inventory-contactlens-advancedfilter',
    templateUrl: 'inventory-contactlens-advancedfilter.component.html'
})
export class InventoryContactlensAdvancedFilterComponent implements OnInit {
    AdvanceSearchForm: FormGroup;
    Brandlist: any;
    Brands: any[];
    Manufacturers: any[];

    /**
     * Colors  of inventory contactlens advanced filter component
     * Holding contact lens colors
     */
    colors: Array<object>;
    orderTypes: any[];
    @Output() contactAdvFilter = new EventEmitter<any>();
    constructor(
        private _fb: FormBuilder,
        private utility: UtilityService,
        private error: ErrorService,
        private dropdownService: DropdownService,
        public app: AppService,
        private contactlensService: ContactlensService
    ) {

    }

    PostPolicyformObj: object = {
        Id: '',
        manufacturerId: '',
        colorId: '',
    };
    ngOnInit() {
        this.AdvanceSearchForm = this._fb.group(this.PostPolicyformObj);
        this.LoadManufacturers();
        this.LoadBrands();
        this.LoadColors();
    }


    /**
     * Loads colors
     * @returns getting contactlens colors
     */
    LoadColors() {
        this.colors = this.dropdownService.colorsData;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getContactlensColors('').subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colorsData = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loads brands for masters
     */
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loads manufacturers for masters
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }


    /**
     * Advances search for contactlens
     */
    advanceSearch() {
        const form = this.AdvanceSearchForm.value;
        this.contactAdvFilter.emit(form);
        // this._ContactlensService.advanceSearchObj.next(form);
    }

    /**
     * Clears all for rest all the values
     */
    clearAll() {
        this.AdvanceSearchForm.reset();
    }


}




