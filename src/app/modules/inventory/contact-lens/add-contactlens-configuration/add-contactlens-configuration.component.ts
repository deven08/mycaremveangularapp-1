import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter, Input } from '@angular/core';
// import { ContactlensService } from '../../../ConnectorEngine/services/contactlens.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';

// import { StateInstanceService } from 'src/app/shared/state-instance.service';

import { Subscription } from 'rxjs';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { ErrorService } from '@app/core/services/error.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { FrameColorData, Data } from '@app/core/models/inventory/Colors.model';
import { SpectacleMaterial } from '@app/core/models/masterDropDown.model';



@Component({
    selector: 'app-add-contactlens-configuration',
    templateUrl: 'add-contactlens-configuration.component.html'
})
export class AddContactlensConfigComponent implements OnInit, OnDestroy {

    addlocationId: any;
    deletelocationId: any;
    valuesforupdate: any;
    locations: any[];
    public contactlenslistconfigurationslist: any[];
    public templocvalue: any[];
    public activecontactlenslistconfigurationslist: any[];
    Contactlensconfiglocationcolumns: any[];
    parameters: FormGroup;
    visible = true;
    /**
     * Colors  of add contactlens config component
     * storing list of contact lens colors
     */
    colors: Array<object>;
    materiallistdetails: any;
    /**
     * Materials values of add contactlens config component
     * storing list of material dropdown data
     */
    materialsValues: Array<object>;
    postdata: any;
    datavalues: any;
    contactLensSubscription: Subscription;
    RxSphereValues: any = [];
    RxCylinderValues: any = [];
    AxisValues: any = [];
    RxAddValues: any = [];
    contactLensconfigurationForm: FormGroup;
    configurationitemid: any;
    Accesstoken = '';
    @ViewChild('dtlocationconfig') public dtlocationconfig: any;
    @Output() cancleContConfig = new EventEmitter<any>();
    @Input() configurationItem;
    constructor(private _fb: FormBuilder, private _utilityService: UtilityService,
        private contactLenService: ContactlensService,
        private dropdownService: DropdownService,
        private utility: UtilityService,
        // private _StateInstanceService: StateInstanceService,
        //  private _DropdownsPipe: DropdownsPipe ,
        private error: ErrorService) {
        // this.contactLensSubscription = contactLenService.configurationItemId$.subscribe(Id => {
        //     this.configurationitemid = Id;
        //     if (this.configurationitemid !== '') {


        //         this.LoadtheConfigValues(this.configurationitemid);
        //         this.Getcontactlensconfigurations(this.configurationitemid);
        //         this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
        //         this.clearlocationinputs();

        //     } else {
        //         this.contactlenslistconfigurationslist = [];
        //         this.formGroupData();
        //         this.locations = [];
        //         this.locations = this._DropdownsPipe.locationsDropData();

        //     }
        // });
        this.formGroupData();
    }

    /**
     * Cancels add contactlens config component
     * closed side bar
     */
    cancel() {
        this.cancleContConfig.emit('cancle');
        // this.contactLenService.cancelbutton.next(true);
        // this.AddConfiguration=false;
    }

    /**
     * Forms group data
     * @returns form controllers
     */
    formGroupData() {
        this.Accesstoken = '';
        // this._StateInstanceService.accessTokenGlobal;
        this.contactLensconfigurationForm = this._fb.group({
            accessToken: this.Accesstoken,
            upc: ['', [Validators.required]],
            basecurve: '',
            diameter: '',
            sphere: '',
            cylinder: '',
            axis: '',
            addpower: '',
            multifocal: '',
            color_id: '',
            material_id: '',
            price: '',
            locationvalue: '',
            location_id: '',
            order_qty: '',
            reorder: '',
            quantitysold: '',
            quantitypurchased: '',
            cost: '',
            max_inventory: ''

        });
    }

    /**
     * Loadthes config values
     * @returns displaying selected id configuration details
     */
    LoadtheConfigValues(configID = '') {
        if (configID !== '') {
            this.contactLenService.getSingleContactconfigurationdetails(this.contactLenService.editlensid, configID).subscribe(
                datatofill => {
                    this.datavalues = [];
                    this.datavalues = datatofill;
                    this.contactLensconfigurationForm = this._fb.group({
                        accessToken: this.Accesstoken,
                        upc: [this.datavalues.upc, [Validators.required]],
                        basecurve: this.datavalues.basecurve,
                        diameter: this.datavalues.diameter,
                        sphere: this.datavalues.sphere,
                        cylinder: this.datavalues.cylinder,
                        axis: parseFloat(this.datavalues.axis).toFixed(0),
                        addpower: this.datavalues.addpower,
                        multifocal: this.datavalues.multifocal,
                        color_id: this.datavalues.color_id,
                        material_id: this.datavalues.material_id,
                        price: this.datavalues.price,
                        locationvalue: '',
                        location_id: '',
                        order_qty: '',
                        reorder: '',
                        quantitysold: '',
                        quantitypurchased: '',
                        cost: '',
                        max_inventory: ''
                    });
                    const axisvalue = this.contactLensconfigurationForm.value.axis;
                    const value = ('00' + axisvalue).slice(-3);
                    this.contactLensconfigurationForm.controls['axis'].patchValue(value);
                });
            const retailPrice = this.contactLensconfigurationForm.controls['price'].value === '' ? '' :
                this.contactLensconfigurationForm.controls['price'].value == null ? '' :
                    this.contactLensconfigurationForm.controls['price'].value;
            if (retailPrice == null) {
                this.contactLensconfigurationForm.controls.price.patchValue('');
            } else {
                const pricevalue = parseFloat(retailPrice).toFixed(0);

                this.contactLensconfigurationForm.controls.price.patchValue(pricevalue);
            }


        }
    }


    // LoadLocations() {
    //     this.locations = [];
    //     this.contactLenService.getLocations().subscribe(DataSet => {
    //         this.locations.push({ Id: '', Name: 'Select Location' });
    //         DataSet['result']['Optical']['Locations'].forEach(item => {
    //             if (!this.locations.find(a => a.Name === item.Name)) {
    //                 this.locations.push({ Id: item.Id, Name: item.Name });
    //             }
    //         });
    //     });
    // }

    // location cofig load
    Getcontactlensconfigurations(configID = '') {
        this.contactlenslistconfigurationslist = [];
        this.activecontactlenslistconfigurationslist = [];
        this.dtlocationconfig.selection = false;
        // const contactlensid = this.contactLenService.editlensid;
        this.contactLenService.getContactconfigurationlocationdetails(this.contactLenService.editlensid, configID).subscribe(
            data => {
                this.contactlenslistconfigurationslist = data['data'];
                this.activecontactlenslistconfigurationslist = this.contactlenslistconfigurationslist.filter(x => x.del_status === 0);
                this.activecontactlenslistconfigurationslist.forEach(obj => {

                    const value = this.locations.filter(x => x.Id === obj.location_id);
                    obj.locname = value[0].Name;

                });

                this.contactlenslistconfigurationslist = this.activecontactlenslistconfigurationslist;
            });


    }

    loadContactlensconfigurations() {
        this.Contactlensconfiglocationcolumns = [
            { field: 'id', header: 'ID', visible: false },
            { field: 'location_id', header: 'Location', visible: false },
            { field: 'locname', header: 'Location Name', visible: this.visible },
            { field: 'order_qty', header: 'Order Qty', visible: this.visible },
            { field: 'reorder', header: 'Reorder', visible: this.visible },
            { field: 'quantitysold', header: 'Old Qty', visible: this.visible },
            { field: 'quantitypurchased', header: 'Purchase Qty', visible: this.visible },
            { field: 'cost', header: 'Cost', visible: this.visible },
            { field: 'max_inventory', header: 'Inventory', visible: this.visible }

        ];
    }

    // Duplicate location check
    checklocationduplicates() {
        this.clearlocationinputs();
        const duplicatescheck = this.valuesforupdate = this.contactlenslistconfigurationslist.filter(x => x.location_id ===
            this.contactLensconfigurationForm.controls['locationvalue'].value);
        if (duplicatescheck.length === 0) {

        } else {
            this.dtlocationconfig.selection = true;
            this.dtlocationconfig.selection = false;
            this.error.displayError({
                severity: 'info',
                summary: 'Info Message', detail: 'Location already exists for configuration'
            });

            // this.InfoMessageShow('Location already exists for configuration');
            this.contactLensconfigurationForm.controls.locationvalue.patchValue('');

            return;
        }
    }

    clearlocationinputs() {
        this.contactLensconfigurationForm.controls.order_qty.patchValue('');
        this.contactLensconfigurationForm.controls.reorder.patchValue('');
        this.contactLensconfigurationForm.controls.quantitysold.patchValue('');
        this.contactLensconfigurationForm.controls.quantitypurchased.patchValue('');
        this.contactLensconfigurationForm.controls.cost.patchValue('');
        this.contactLensconfigurationForm.controls.max_inventory.patchValue('');
    }

    /**
     * Adds update locations
     * @returns  updated locations details
     */
    AddUpdateLocations() {
        if (this.contactLensconfigurationForm.controls['locationvalue'].value === '') {
            this.error.displayError({
                severity: 'info',
                summary: 'Info Message', detail: 'Please Select Location'
            });

            // this.InfoMessageShow('Please Select Location');
            this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
            this.clearlocationinputs();
            this.dtlocationconfig.selection = false;

            return;
        }


        const locationdetails = this.locations.filter(x => x.Id === this.contactLensconfigurationForm.controls['locationvalue'].value);
        const locationname = locationdetails[0].Name;
        const configlocid = locationdetails[0].Id;
        this.valuesforupdate = [];
        this.valuesforupdate = this.contactlenslistconfigurationslist.filter(x => x.location_id ===
            this.contactLensconfigurationForm.controls['locationvalue'].value);
        const remove = this.contactlenslistconfigurationslist.findIndex(x => x.location_id ===
            this.contactLensconfigurationForm.controls['locationvalue'].value);
        if (remove !== -1) {
            this.contactlenslistconfigurationslist.splice(remove, 1);
        }
        let idsave;
        if (this.valuesforupdate.length === 0) {
            idsave = undefined;
        } else {
            idsave = this.valuesforupdate[0].id;

        }

        const valuetopush = {
            'id': idsave,
            'location_id': this.contactLensconfigurationForm.controls['locationvalue'].value,
            'order_qty': this.contactLensconfigurationForm.controls['order_qty'].value,
            'locname': locationname,
            'reorder': this.contactLensconfigurationForm.controls['reorder'].value,
            'quantitysold': this.contactLensconfigurationForm.controls['quantitysold'].value,
            'quantitypurchased': this.contactLensconfigurationForm.controls['quantitypurchased'].value,
            'cost': this.contactLensconfigurationForm.controls['cost'].value,
            'max_inventory': this.contactLensconfigurationForm.controls['max_inventory'].value,
            'IsAdd': true
        };
        this.contactlenslistconfigurationslist.push(valuetopush);
        this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
        this.contactLensconfigurationForm.controls.order_qty.patchValue('');
        this.contactLensconfigurationForm.controls.reorder.patchValue('');
        this.contactLensconfigurationForm.controls.quantitysold.patchValue('');
        this.contactLensconfigurationForm.controls.quantitypurchased.patchValue('');
        this.contactLensconfigurationForm.controls.cost.patchValue('');
        this.contactLensconfigurationForm.controls.max_inventory.patchValue('');
        this.dtlocationconfig.selection = false;
        //    this.addlocationId=="";
        //    this.deletelocationId="";
    }

    ngOnDestroy(): void {
        // this.contactLensSubscription.unsubscribe();
    }

    ngOnInit(): void {

        this._utilityService.lensPopupOrderCondi = true;
        this.LoadColors();
        this.LoadMaterialTypes();
        // Sphere Powers
        this.RxSphereValues = this._utilityService.genDropDowns(
            { start: -20, end: 20, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'PL' });

        // Cylinder Power
        this.RxCylinderValues = this._utilityService.genDropDowns(
            { start: -10, end: 10, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'D.S.' });

        // Axis Degrees
        this.AxisValues = this._utilityService.genDropDowns(
            { start: 1, end: 180, step: 1, sign: false, roundOff: 0, slice: 3, neutral: '0' });

        // Add Values
        this.RxAddValues = this._utilityService.genDropDowns(
            { start: 0.25, end: 3.75, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: '0' });


        this.formGroupData();
        this.loadContactlensconfigurations();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();

        // setTimeout(() => this.LoadMaterialTypes(), 1000);

        // this.Colors = [];
        // this.Colors = this._DropdownsPipe.coloursDropData();
        if (this.configurationItem !== '') {
            this.configurationitemid = this.configurationItem;
            if (this.configurationitemid !== '') {
                this.LoadtheConfigValues(this.configurationitemid);
                this.Getcontactlensconfigurations(this.configurationitemid);
                this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
                this.clearlocationinputs();
            } else {
                this.contactlenslistconfigurationslist = [];
                this.formGroupData();
                // this.locations = [];
                // this.locations = this._DropdownsPipe.locationsDropData();

            }
        }
        // this.materialsvalues = [];
        // this.materialsvalues = this._DropdownsPipe.specticalMaterialDropDown();
    }

    /**
     * Loads colors
     * @returns ContactLens colors details
     */
    LoadColors() {
            this.colors = this.contactLenService.colorsData;
            if (this.utility.getObjectLength(this.colors) === 0) {
            this.contactLenService.getContactlensColors('').subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.contactLenService.colorsData = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
            }

    }




    LoadMaterialTypes() {
        this.materialsValues = this.dropdownService.materials;
        if (this.utility.getObjectLength(this.materialsValues) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    this.materialsValues.push({ value: '', name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materialsValues.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.materials = this.materialsValues;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



    // locationselect
    onRowConfigurationLocationSelect(datavalue) {

        this.deletelocationId = datavalue.data.id;
        this.addlocationId = datavalue.data.location_id;
        const value = this.contactlenslistconfigurationslist.filter(x => x.id === datavalue.data.id);
        this.contactLensconfigurationForm.controls.locationvalue.patchValue(datavalue.data.location_id.toString());
        this.contactLensconfigurationForm.controls.order_qty.patchValue(value[0].order_qty);
        this.contactLensconfigurationForm.controls.reorder.patchValue(value[0].reorder);
        this.contactLensconfigurationForm.controls.quantitysold.patchValue(value[0].quantitysold);
        this.contactLensconfigurationForm.controls.quantitypurchased.patchValue(value[0].quantitypurchased);
        this.contactLensconfigurationForm.controls.cost.patchValue(value[0].cost);
        this.contactLensconfigurationForm.controls.max_inventory.patchValue(value[0].max_inventory);
    }

    /**
     * Savecontactlensconfigurations add contactlens config component
     * @returns Save contactlens configurations data
     */
    savecontactlensconfiguration() {
        if (this.configurationitemid !== '') {
            this.postdata = [];

            const retailPrice = this.contactLensconfigurationForm.controls['price'].value === '' ? 0 :
                this.contactLensconfigurationForm.controls['price'].value === null ? 0 :
                    this.contactLensconfigurationForm.controls['price'].value;
            this.contactLensconfigurationForm.controls.price.patchValue(retailPrice);
            this.contactLenService.UpdateContactLensCongfiguration(this.contactLenService.editlensid,
                this.contactLensconfigurationForm.value, this.configurationitemid).subscribe(details => {
                    this.postdata = details;
                    if (this.postdata !== '') {
                        const locationtosaveid = this.postdata.id;
                        const savelocations = this.contactlenslistconfigurationslist.filter(x => x.IsAdd === true);

                        savelocations.forEach(obj => {

                            if (obj.id === undefined) {
                                const saveobjectjson =
                                    [
                                        {
                                            'location_id': obj.location_id,
                                            'order_qty': obj.order_qty,
                                            'reorder': obj.reorder,
                                            'quantitysold': obj.quantitysold,
                                            'quantitypurchased': obj.quantitypurchased,
                                            'cost': obj.cost,
                                            'max_inventory': obj.max_inventory
                                        }
                                    ];

                                this.contactLenService.SaveContactLensCongfigurationLocations(
                                    this.contactLenService.editlensid, saveobjectjson, locationtosaveid).subscribe(data => {
                                    });
                            } else {
                                const updateobject =
                                    [
                                        {
                                            'id': obj.id,
                                            'location_id': obj.location_id,
                                            'order_qty': obj.order_qty,
                                            'reorder': obj.reorder,
                                            'quantitysold': obj.quantitysold,
                                            'quantitypurchased': obj.quantitypurchased,
                                            'cost': obj.cost,
                                            'max_inventory': obj.max_inventory
                                        }
                                    ];
                                this.contactLenService.UpdateContactLensCongfigurationLocations(
                                    this.contactLenService.editlensid, updateobject, locationtosaveid).subscribe(data => {
                                    });
                            }
                            // this.SuccessShow();

                            // setTimeout(() =>   this.contactLenService.refreshconfig.next(true), 1000);
                        });

                        //   this.contactLenService.UpdateContactLensCongfigurationLocations(
                        // this.contactLenService.editlensid,savelocations,locationtosaveid).subscribe(data => {
                        //     this.SuccessShow();

                        this.error.displayError({
                            severity: 'success',
                            summary: 'Success Message', detail: 'Contactlens Configuration Saved Successfully'
                        });

                        // this.SuccessShow();
                        this.cancleContConfig.emit('save');
                        // setTimeout(() => this.contactLenService.refreshconfig.next(true), 1000);
                        // });
                    }

                },
                    error => {
                        let errormessage = [];
                        errormessage = error.error.upc;
                        errormessage = error.error.price;
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: error
                        });

                        // this.ErrorShowDetails(errormessage);

                    }
                );
        } else {

            this.contactLenService.SaveContactLensCongfiguration(
                this.contactLenService.editlensid, this.contactLensconfigurationForm.value).subscribe(data => {
                    this.postdata = data;
                    if (this.postdata !== '') {
                        const locationtosaveid = this.postdata.id;
                        const savelocations = this.contactlenslistconfigurationslist.filter(x => x.IsAdd === true);
                        savelocations.forEach(obj => {
                            delete savelocations['id'];
                        });
                        this.saveContactLensFun(locationtosaveid, savelocations);
                    }
                },
                    error => {
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: error.error.upc
                        });

                        // this.ErrorShowDetails(error.error.upc);

                    }
                );
        }




    }

    /**
     * Saves contact lens fun
     * @returns save contactlens
     */
    saveContactLensFun(locationtosaveid, savelocations) {
        this.contactLenService.SaveContactLensCongfigurationLocations(
            this.contactLenService.editlensid, savelocations, locationtosaveid).subscribe(data => {

                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Contactlens Configuration Saved Successfully'
                });
                // this.SuccessShow();
                this.cancleContConfig.emit('save');
                // setTimeout(() => this.contactLenService.refreshconfig.next(true), 1000);
            });
    }

    /**
     * Deleteconfiglocations add contactlens config component
     * @returns  delet contact lens configurations
     */
    deleteconfiglocation() {
        if (this.addlocationId !== '') {
            const remove = this.contactlenslistconfigurationslist.findIndex(
                x => x.location_id === this.addlocationId); // && x.IsAdd==true);
            if (remove !== -1) {
                this.contactlenslistconfigurationslist.splice(remove, 1);
                this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
                this.clearlocationinputs();
            }
            // this.addlocationId="removed";
        }

        if ((this.addlocationId === '' || this.addlocationId === undefined) &&
            (this.deletelocationId === undefined || this.deletelocationId === '')) {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select Config To Delete'
            });

            // this.InfoMessageShow('Please select config to delete');
            this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
            this.clearlocationinputs();
            // this.addlocationId="";

            return;
        }

        if (this.deletelocationId === undefined || this.deletelocationId === '') {
            return;
        }


        this.contactLenService.deleteContactLensCongfigurationLocations(
            this.contactLenService.editlensid, this.configurationitemid, this.deletelocationId).subscribe(deletevalue => {

                const value = deletevalue;
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Deleted Successfully'
                });
                // this.SuccessShowMessage('Deleted Successfully');
                this.templocvalue = this.contactlenslistconfigurationslist.filter(x => x.IsAdd === true);
                // this.Getcontactlensconfigurations(this.configurationitemid);
                this.deletelocationId = '';
                this.contactLensconfigurationForm.controls.locationvalue.patchValue('');
                this.clearlocationinputs();
                this.addlocationId = '';


            },

                error => {
                    this.error.displayError({
                        severity: 'error',
                        summary: 'Error Message', detail: error.error.status
                    });

                    // this.ErrorShowDetails(error.error.status);

                }
            );


    }

    // SuccessShow() {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Contactlens Configuration Saved Successfully' });
    // }
    // ErrorShow() {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Save Failed' });
    // }
    // ErrorShowDetails(value) {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: value });
    // }
    // InfoMessageShow(value) {
    //     this.msgs.push({ severity: 'info', summary: 'Info Message', detail: value });
    // }
    // SuccessShowMessage(value) {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: value });
    // }

}
