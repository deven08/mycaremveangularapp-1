
import { Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';




import { SelectItem } from 'primeng/primeng';
import { SortEvent } from 'primeng/primeng';




import { Subscription } from 'rxjs';
import { ContactlensService } from '@services/inventory/contactlens.service';
// import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';

import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { ExcessDataViewComponent } from './excess-data-view/excess-data-view.component';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameBrand } from '@app/core/models/inventory/Brands.model';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DeactivateComponent } from '../deactivate/deactivate.component';
import { ImportCsvComponent } from '../import-csv/Import-csv.component';
import { ManufacturerDropData, FrameBrands, FrameColorData } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-inventory-contactlens',
    templateUrl: 'inventory-contactlens.component.html'
})
export class ContactLensComponent implements OnInit {
    addLoctionContact = false;
    subscriptionDeactive: Subscription;
    searchcolour: any = null;
    searchbrand: any = null;
    searchManufa: any = '';
    advancedFilterSidebar = false;
    excessData: any;
    samplevisible = true;
    visible = true;
    colSpanVar: any;
    searchIsDot = '';
    searchIsTrial = '';
    searchPackaging = '';
    searchUPC = '';
    searchQtyOnHand = '';
    searchName = '';
    searchValue = '';
    selectedRowData: any[];
    excessdata = false;
    Supplydata: any[];
    WearScheduledata: any[];
    Replacedata: any[];
    Materialdata: any[];
    Typedata: any[];
    materialdata: any[];
    display = false;
    addInventory = false;
    public tableHeaderFilters = [];
    addLoction = false;
    rowData: any;
    sortallinventory: boolean;
    pricecalculation: boolean;
    contactlensReActivateItems = false;
    /**
     * Sort by name of the contact lens component of assigning names
     */
    sortByName: any;
    /**
     * Sortby value of the contact lens component of sort by id
     */
    sortbyValue: any = "id";
    /**
     * thenbyValue  of contact lens component of sort items
     */
    thenbyValue: object;
    // searchValue:string = '';

    public manufacturerlist: Manufacturer[];
    public Brandlist: FrameBrand[];
    public contactlenslist: any = [];
    @ViewChild('dt') public dt: any;
    @ViewChild('framesAlert') FramesAlerts: DialogBoxComponent;
    @ViewChild('excessDataView') excessDataView: ExcessDataViewComponent;
    @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    @ViewChild('deactivateRow') deactivateRows: DeactivateComponent;
    @ViewChild('importcsvBox') importCsvdialog: ImportCsvComponent;
    public Manufacturers: any[];
    public Brands: any[];
    public Colors: any[];
    public ColorCodes: SelectItem[];
    public QtyOnHands: SelectItem[];
    contactMultipleSelection = [];
    Contactlenscolumns: any[];
    Accesstoken = '';
    maufacturerid = '';
    upc = '';
    name = '';
    barndid = '';
    colorid = '';
    AdvFilteId = '';
    addbarcodelabels = false;
    subscriptionContactChange: Subscription;
    subscriptionContactclose: Subscription;
    subscriptionadvanceSearch: Subscription;
    subscriptionAddLocation: Subscription;
    subscriptionReactive: Subscription;
    subscriptionCSV: Subscription;
    locationFrameId: any;

    paginationValue: any = 0;
    totalRecords: string;
    paginationPage: any = 0;
    imageItemId: any;
    showconfigtab: any = '';

    /**
     * Row id of contact lens component for hide and seek row
     */
    rowID = true;

    /**
     * Row lens name of contact lens component for hide and seek row
     */
    rowLensName = true;

    /** 
     * Row color of contact lens component for hide and seek row
     */
    rowColor = true;

    /**
     * Row packaging of contact lens component for hide and seek row
     */
    rowPackaging = true;

    /**
     * Row trial of contact lens component for hide and seek row
     */
    rowTrial = true;

    /**
     * Row dot of contact lens component for hide and seek row
     */
    rowDot = true;

    /**
     * Row on hand of contact lens component for hide and seek row
     */
    rowOnHand = true;

    /**
     * Row upc of contact lens component for hide and seek row
     */
    rowUPC = true;

    /**
     * Row manufacturer of contact lens component for hide and seek row
     */
    rowManufacturer = true;
    filterdata: any;
    /**
     * Creates an instance of contact lens component. 
     * @param  contactlensService  provides contactlensService
     * @param _InventoryCommonService  provides InventoryCommonService
     * @param error provides  errorservice
     * @param dropdownService provides dropdownService
     * @param utility provides utilityservice
     */
    constructor(private contactlensService: ContactlensService,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private dropdownService: DropdownService,
        private utility: UtilityService) {
            
    }
    ngOnInit() {
        this.loadContactlenscolumns();
        this.GetcontactlensList();
        this.Loadmastersdata();
        this.LoadManufacturers();
        this.searchcolour = '';
    }

    /**
     * Loads brands masters
     */
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Adds loctionclick viewing the items avalible locations
     */
    addLoctionclick() {
        this.contactlensService.errorHandle.msgs = [];
        this._InventoryCommonService.InventoryType = 'ContactLens';



        if (this.contactMultipleSelection.length === 1) {
            this._InventoryCommonService.FortrasactionItemID = this.contactMultipleSelection[0].id;


            this.addLoctionContact = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select  Only One Contactlens.'
            });



        }
    }

    /**
     * Loadmastersdatas contact lens component masters
     */
    Loadmastersdata() {
        this.LoadColors();
        this.LoadBrands();

    }

    /**
     * Importcsvs contact lens component import of the external files
     */
    importcsv() {
        this.importCsvdialog.importCsvbox('contact');
    }

    /**
     * Advances filter
     * @param {object} data 
     */
    advanceFilter(data) {
        this.searchValue = data.Id;
        this.searchName = '';
        this.searchQtyOnHand = '';
        this.searchUPC = '';
        this.searchPackaging = '';
        this.searchIsTrial = '';
        this.searchIsDot = '';
        this.searchManufa = data.manufacturerId;
        this.searchbrand = null;
        this.searchcolour = data.colorId;
        this.upc = '';
        this.maufacturerid = '';
        this.barndid = '';
        this.colorid = '';
        this.AdvFilteId = '';
        // this.searchValue = this.AdvFilteId;

        this.GetcontactlensList();
        this.advancedFilterSidebar = false;
    }

    /**
     * Advanced filter sidebar click for viewing
     */
    advancedFilterSidebarClick() {
        this.advancedFilterSidebar = true;
    }

    /**
     * Contactlens re activate for viewing
     */
    contactlensReActivate() {
        this.contactlensReActivateItems = true;

    }





    /**
     * Loads colors masters
     */
    LoadColors() {
        this.Colors = this.dropdownService.colorsData;
        if (this.utility.getObjectLength(this.Colors) === 0) {
            this.dropdownService.getContactlensColors('').subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.Colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colorsData = this.Colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loads manufacturers master
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }



    dialogBox() {
        this.display = true;

    }



    /**
     * Deactive  is for making the deactivate
     */
    deActive() {
        if (this.contactMultipleSelection.length !== 0) {
            const Framedata = {
                ContactLensSelection: this.contactMultipleSelection
            };
            this.deactivateRows.deactivate(Framedata);
        }
    }

    /**
     * Loads contactlenscolumns loading of the headers
     */
    loadContactlenscolumns() {
        this.Contactlenscolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'manufacturer_id', header: 'Manufacturer', visible: this.visible },
            // { field: 'Brand', header: 'Brand' },
            { field: 'name', header: 'LensName', visible: this.visible },
            { field: 'colorid', header: 'Color', visible: this.visible },
            { field: 'cl_packaging', header: 'Packaging', visible: this.visible },
            { field: 'IsTrial', header: 'Trial', visible: this.visible },
            { field: 'IsDot', header: 'Dot', visible: this.visible },
            { field: 'qty_on_hand', header: 'OnHand', visible: this.visible },
            { field: 'upc_code', header: 'UPC', visible: this.visible }

        ];
    }

    loadPatientData(page: number = 0) {
        // Checking File Id
        // if (itemId === 0) {
        // Get Data from file
        this.filterPayLoad();
        this.contactlensService.getcontactlensbyPagination(this.filterdata, page).subscribe(
            data => {
                this.contactlenslist = data['data'];
            },
        );
        // }
    }
    /**
     * Lazys load file items
     * @returns Load lazyLoadPatientList data
     */
    GetContactListByPaginator(event) {
        // Generating Page
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        // Calling Page Data
        this.loadPatientData(page);
    }

    /**
     * Getcontactlens list fetching all the active contact lens
     */
    GetcontactlensList() {
        this.filterPayLoad();
        this.contactlenslist = [];
        let getcontactData: any = [];
        this.contactlensService.getcontactlensNew(this.filterdata).subscribe(
            data => {
                this.totalRecords = data['total'];
                // alert(data)
                getcontactData = data;
                this.Typedata = [];
                this.Materialdata = [];
                this.Replacedata = [];
                this.Supplydata = [];
                this.WearScheduledata = [];
                this.contactlenslist = [];
                this.contactlenslist = data['data'];

            });
        this.contactMultipleSelection = [];
    }

    /**
     * Duplicates contact lens same like the existing contact lens
     */
    duplicateContactLens() {
        this.contactlensService.errorHandle.msgs = [];
        if (this.contactMultipleSelection.length === 1) {
            this.contactlensService.editlensid = this.contactMultipleSelection[0].id;
            this.addInventory = true;
            this.contactlensService.modifyOrDuplicate = 'Duplicate';
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select one Item.'
            });
        }
        this.contactMultipleSelection = [];


    }

    /**
     * Determines whether row select on
     * @param {object} rowData 
     */
    onRowSelect(rowData) {
        this.contactlensService.editlensid = rowData.data.id;
        this.selectedRowData = rowData;
    }

    /** 
     * Determines whether row unselect on    
     */
    onRowUnselect() {
        this.contactlensService.editlensid = '';
        this.contactlensService.editduplicateContactLensId = '';

    }


    excessDataViewComp(selectedRowData) {
        this.excessData = '';
        this.excessData = selectedRowData;
        this.Typedata = [];
        this.Materialdata = [];
        this.Replacedata = [];
        this.Supplydata = [];
        this.WearScheduledata = [];

        if (this.excessData.data.Types.length && !undefined) {
            for (let j = 0; j <= this.excessData.data.Types.length; j++) {
                this.Typedata.push(this.excessData.data.Types[j]);
            }

        }
        if (this.excessData.data.Materials.length && !undefined) {

            for (let j = 0; j <= this.excessData.data.Materials.length; j++) {
                this.Materialdata.push(this.excessData.data.Materials[j]);
            }

        }
        if (this.excessData.data.WearSchedule.length && !undefined) {

            for (let j = 0; j <= this.excessData.data.WearSchedule.length; j++) {
                this.WearScheduledata.push(this.excessData.data.WearSchedule[j]);
            }

        }
        if (this.excessData.data.Replacement.length && !undefined) {

            for (let j = 0; j <= this.excessData.data.Replacement.length; j++) {
                this.Replacedata.push(this.excessData.data.Replacement[j]);
            }
            console.log(this.Typedata);
        }
        if (this.excessData.data.Supply.length && !undefined) {

            for (let j = 0; j <= this.excessData.data.Types.Supply; j++) {
                this.Supplydata.push(this.excessData.data.Supply[j]);
            }

        }

    }

    /**
     * Addcontacts contact lens component add new contact lens
     */
    addcontacts() {
        this.imageItemId = undefined;

        this._InventoryCommonService.FortrasactionItemID = '';
        this.locationFrameId = undefined;

        this.contactMultipleSelection = [];
        this.dt.selection = true;
        this.dt.selection = false;
        this.addInventory = true;
        this.contactlensService.editlensid = '';
        this.contactlensService.editlensidDoubleClick = '';
        this._InventoryCommonService.FortrasactionItemID = '';
        this.contactlensService.modifyOrDuplicate = '';


    }

    /**
     * Updatecontacts contact lens component modify the contact lens
     */
    updatecontacts() {
        this.contactlensService.errorHandle.msgs = [];
        this.contactlensService.modifyOrDuplicate = '';

        this._InventoryCommonService.FortrasactionItemID = this.contactMultipleSelection[0].id;
        if (this.contactMultipleSelection.length === 1) {
            this.locationFrameId = this.contactMultipleSelection[0].id;
            this.imageItemId = this.contactMultipleSelection[0].id;
        } else {
            this.locationFrameId = undefined;
            this.imageItemId = undefined;
        }

        if (this.contactMultipleSelection.length === 1) {

            this.contactlensService.editlensid = this.contactMultipleSelection[0].id;

            const selectedFrameid = this.contactlensService.editlensid;

            if (!selectedFrameid) {

                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Please Select ContactLens.'
                });



            } else {

                this.addInventory = true;

            }
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select one Item.'
            });



        }

    }

    /**
     * Doubles click contact lens for modify the contactlens
     * @param {object} rowData 
     */
    doubleClickContactLens(rowData) {

        this._InventoryCommonService.FortrasactionItemID = rowData.id;
        this.contactMultipleSelection = [];
        this.contactlensService.modifyOrDuplicate = '';

        this.contactlensService.editlensidDoubleClick = rowData.id;

        this.contactlensService.editlensid = rowData.id;
        this.contactMultipleSelection.push(rowData);
        this.addInventory = true;
        this.imageItemId = rowData.id;
    }



    /**
     * Checks box click items should hide and seek
    * @param {number} item 
     * @param {boolean} e  
     */
    checkBoxClick(item, e) {

        if (item === 'selectAll') {
            this.visible = e.checked;
            this.loadContactlenscolumns();
            this.rowID = e.checked;
            this.rowLensName = e.checked;
            this.rowColor = e.checked;
            this.rowPackaging = e.checked;
            this.rowTrial = e.checked;
            this.rowDot = e.checked;
            this.rowOnHand = e.checked;
            this.rowUPC = e.checked;
            this.rowManufacturer = e.checked;
            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    /**
     * Applys data view changes making the headers hide and seek
    * @param {number} item 
     * @param {boolean} selected 
     */
    applyDataViewChanges(item, selected) {
        this.Contactlenscolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;

            }
        });
        this.displayCols(item, selected);
        const colSpanVar = this.Contactlenscolumns.filter(x => x.visible).length;
        if (colSpanVar < this.Contactlenscolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }



    }

    /**
     * Displays cols making th ehide and seek the colums
     * @param {number} item 
     * @param {boolean} selected 
     */
    displayCols(item, selected) {
        if (item == 'ID') {
            this.rowID = selected;
        }
        if (item == 'LensName') {
            this.rowLensName = selected;
        }
        if (item == 'Color') {
            this.rowColor = selected;
        }
        if (item == 'Packaging') {
            this.rowPackaging = selected;
        }
        if (item == 'Trial') {
            this.rowTrial = selected;
        }
        if (item == 'Dot') {
            this.rowDot = selected;
        }
        if (item == 'OnHand') {
            this.rowOnHand = selected;
        }
        if (item == 'UPC') {
            this.rowUPC = selected;
        }
        if (item == 'Manufacturer') {
            this.rowManufacturer = selected;
        }
    }
    headerFilters(value, field, type = 'in') {
        this.dt.filter(value, field, type);
        setTimeout(() => {
            this.tableHeaderFilters = Object.keys(this.dt.filters);
        }, 500);

    }


    /**
     * Resets contact lens component is for the auto rest all to the normal values
     * @param {object}  table 
     */
    reset(table) {
        this.contactlensService.modifyOrDuplicate = '';
        this.dt.selection = false;
        table.reset();

        this.searchValue = '';
        this.searchName = '';
        this.searchQtyOnHand = '';
        this.searchUPC = '';
        this.searchPackaging = '';
        this.searchIsTrial = '';
        this.searchIsDot = '';
        this.searchManufa = '';
        this.searchbrand = null;
        this.searchcolour = null;
        this.upc = '';
        this.maufacturerid = '';
        this.barndid = '';
        this.colorid = '';
        this.paginationValue = 0;
        this.GetcontactlensList();
        this.sortbyValue = 'id';


    }

    pricecalculationSet() {
        this.pricecalculation = true;
    }
    addbarcodelabelsSet() {
        if (this.contactMultipleSelection.length !== 0) {
            this._InventoryCommonService.barCodelist = this.contactMultipleSelection;
            this._InventoryCommonService.barCodeLable = 'ContactBarcode';
        } else {
            this._InventoryCommonService.barCodelist = [];
            this._InventoryCommonService.barCodeLable = 'ContactBarcode';
        }
        this.addbarcodelabels = true;
    }
    /**
     * Sortallevents contactelens component
     * @param event for sort the data grid
     */
    sortallevent(event) {
            this.contactlenslist = event.gridData;
            this.sortallinventory = false;
            this.sortbyValue = event.sortValue;
            this.thenbyValue = event.thenValue;
    }
    /**
     * Gets sort all
     * for displaying the sort all side-bar
     */
    getSortAll() {
        this.sortallinventory = true;
        this.sortByName = 'contact-lens';
    }

    /**
     * Customs sort for sorting
     * @param {object} event 
     */
    customSort(event: SortEvent) {
        this.dt.first = this.paginationValue;
        const tmp = this.contactlenslist.sort((a: any, b: any): number => {
            if (event.field) {
                return a[event.field] > b[event.field] ? 1 : -1;
            }
        });
        if (event.order < 0) {
            tmp.reverse();
        }
        const thisRef = this;
        this.contactlenslist = [];
        tmp.forEach(function (row: any) {
            thisRef.contactlenslist.push(row);
        });
    }

    /**
     * Contacts  deactive event making for the deactivate the items
     * @param {object} event 
     */
    ContactDeactiveEvent(event) {
        if (event === 'ContactDeactivate') {
            this.GetcontactlensList();
            this.contactMultipleSelection = [];
        }
    }

    /**
     * Contacts locations event is for the listing of the items avalible based on the locations
     * @param {object} event 
     */
    contactLocationsEvent(event) {
        if (event === 'contact') {
            this.addLoctionContact = false;
        } else {
            this.addLoctionContact = false;
            this.GetcontactlensList();
        }
    }

    /**
     * Contacts reactive event  making reactivate the items
     * @param {object} event 
     */
    ContactReactiveEvent(event) {
        if (event === 'contactClose') {
            this.contactlensReActivateItems = false;
        } else {
            this.GetcontactlensList();
            this.contactlensReActivateItems = false;
        }
    }



    /**
     * Contacts add item event
     * @param data for added new item data
     */
    contactAddItemEvent(event) {
        // if (data === 'contactclose') {
        //     this.addInventory = false;
        // } else {
        //     this.GetcontactlensList();
        //     // for (let i = 0; i <= data.length - 1; i++) {
        //     //     if (data.id === this.contactlenslist[i].id) {
        //     //         this.singleGridValue(data);
        //     //     }
        //     // }
        //     this.addInventory = false;
        // }


        if (event.status === 'saved') {
            this.addInventory = false;
            //  this.cleardetails();
            this.GetcontactlensList();
        } if (event.status === 'othercancle') {
            this.addInventory = false;
        }
        if (event.status === 'Updated') {
            let payload = {
                "filter": [

                    {
                        "field": "module_type_id",
                        "operator": "=",
                        "value": "3"
                    },
                    {
                        "field": "id",
                        "operator": "=",
                        "value": event.data
                    }
                ],

                "sort": [
                    {
                        "field": "module_type_id",
                        "order": "ASC"
                    }
                ]
            }
            this.contactlensService.getcontactlensNew(payload).subscribe(data => {
                this.addInventory = false;
                let req = data['data'][0];
                for (let i = 0; i <= this.contactlenslist.length - 1; i++) {
                    if (event.data == this.contactlenslist[i]['id']) {
                        this.contactlenslist[i]['upc_code'] = req.upc_code;
                        this.contactlenslist[i]['name'] = req.name;
                        this.contactlenslist[i]['categoryid'] = req.categoryid;
                        this.contactlenslist[i]['vendor_id'] = req.vendor_id;
                        this.contactlenslist[i]['retail_price'] = req.retail_price;
                        this.contactlenslist[i]['wholesale_cost'] = req.wholesale_cost;
                        this.contactlenslist[i]['inventory'] = req.inventory;
                        this.contactlenslist[i]['send_to_lab_flag'] = req.send_to_lab_flag;
                        this.contactlenslist[i]['print_on_order'] = req.print_on_order;
                        this.contactlenslist[i]['procedure_code'] = req.procedure_code;
                        this.contactlenslist[i]['modifier_id'] = req.modifier_id;
                        this.contactlenslist[i]['structure_id'] = req.structure_id;
                        this.contactlenslist[i]['notes'] = req.notes;
                        this.contactlenslist[i]['manufacturer_id'] = req.manufacturer_id;
                        this.contactlenslist[i]['colorid'] = req.colorid;
                        this.contactlenslist[i]['min_qty'] = req.min_qty;
                        this.contactlenslist[i]['max_qty'] = req.max_qty;
                        this.contactlenslist[i]['locationid'] = req.locationid;
                        this.contactlenslist[i]['commission_type_id'] = req.commission_type_id;
                        this.contactlenslist[i]['spiff'] = req.spiff;
                        this.contactlenslist[i]['gross_percentage'] = req.gross_percentage;
                        this.contactlenslist[i]['amount'] = req.amount;
                        this.contactlenslist[i]['cl_packaging'] = req.cl_packaging;

                    }
                }
            })
        }
    }

    /**
     * Contacts adv filter event filtering from the advance filter
     * @param event 
     */
    contactAdvFilterEvent(event) {
        this.advanceFilter(event);
    }

    /**
     * Filters pay load for lsiting of the contactlens
     */
    filterPayLoad() {
        const constactLensSearchObject = {
            filter: [
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '3'
                },
                {
                    field: 'id',
                    operator: 'LIKE',
                    value: '%' + this.searchValue + '%'
                },
                {
                    field: 'name',
                    operator: 'LIKE',
                    value: '%' + this.searchName + '%'
                },
                {
                    field: 'colorid',
                    operator: '=',
                    value: this.searchcolour
                },
                {
                    field: 'cl_packaging',
                    operator: 'LIKE',
                    value: '%' + this.searchPackaging + '%'
                },
                {
                    field: 'qty_on_hand',
                    operator: 'LIKE',
                    value: '%' + this.searchQtyOnHand + '%'
                }, {
                    field: 'upc_code',
                    operator: 'LIKE',
                    value: '%' + this.searchUPC + '%'
                },
                {
                    field: 'manufacturer_id',
                    operator: '=',
                    value: this.searchManufa
                },
                {
                    field: 'del_status',
                    operator: '=',
                    value: 0
                }

            ],
        }
        const resultData = constactLensSearchObject.filter.filter(p => p.value !== "" && p.value !== null && p.value !== "null" && p.value !== "%  %" && p.value !== "%%");
        this.filterdata = {
            filter: resultData,
            sort: [
                {
                    field: this.sortbyValue,
                    order: 'DESC'
                }
            ]
        };
        this.thenbyValue = [];
        let  sortValuelength;
        sortValuelength = this.utility.getObjectLength(this.thenbyValue);
        if (sortValuelength !== 0) {
            for (let i = 0; i <= sortValuelength; i++) {
                this.filterdata.sort.push({
                    field: this.thenbyValue[i],
                    order: 'DESC'
                });
            }
        }
    }
}
