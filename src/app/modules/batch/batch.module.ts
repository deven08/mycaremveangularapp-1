
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { BatchRoutingModule } from './batch-routing.module';

import {
  CriteriaSearchComponent,
  EraClaimRejectionComponent,
  EraComponent,
  EraManualPaymentComponent,
  EraPostPaymentComponent,
  LabOrdersComponent,
  PaymentsComponent,
  UploadEraComponent,
  BatchOrderStatusComponent,
  BatchAddAllComponent,
  BatchOrderStatusUpdateComponent
} from './'



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BatchRoutingModule,
  ],
  declarations: [
    EraComponent,
    UploadEraComponent,
    EraPostPaymentComponent,
    EraManualPaymentComponent,
    EraClaimRejectionComponent,
    BatchOrderStatusUpdateComponent,
    BatchOrderStatusComponent,
    BatchAddAllComponent,
    PaymentsComponent,
    LabOrdersComponent,
    CriteriaSearchComponent
  ],

})
export class BatchModule { }
