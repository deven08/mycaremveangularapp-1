import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EraManualPaymentComponent } from './era-manual-payment.component';

describe('EraManualPaymentComponent', () => {
  let component: EraManualPaymentComponent;
  let fixture: ComponentFixture<EraManualPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EraManualPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EraManualPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
