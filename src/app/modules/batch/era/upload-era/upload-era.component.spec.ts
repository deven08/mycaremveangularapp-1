import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadEraComponent } from './upload-era.component';

describe('UploadEraComponent', () => {
  let component: UploadEraComponent;
  let fixture: ComponentFixture<UploadEraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadEraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadEraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
