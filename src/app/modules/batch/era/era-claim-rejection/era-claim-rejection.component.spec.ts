import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EraClaimRejectionComponent } from './era-claim-rejection.component';

describe('EraClaimRejectionComponent', () => {
  let component: EraClaimRejectionComponent;
  let fixture: ComponentFixture<EraClaimRejectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EraClaimRejectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EraClaimRejectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
