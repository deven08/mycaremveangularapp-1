// Core Modules
import { Component, OnInit } from '@angular/core';
// Libraries
import { DashboardService } from '@services/dashboard/dashboard.service';
import { PatientSearchService } from '@services/patient/patient-search.service';
import { AppService } from '@services/app.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core';
// Setting Up Component
@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html'
})


export class DashboardComponent implements OnInit {
  today: number = Date.now();
  public username: string;
  toDoList: any = [];
  appList: any = [];
  ordersListdata: any = [];
  appStatusList: any[] = [];
  /**
   * Order type of dashboard component for order type
   */
  orderType: object;

  /**
   * Status  of dashboard componentfor order type status
   */
  status: object;
  /* getting of count of ToDo, Appointment & Orders */
  toDoCount: any = '0';
  appCount: number;
  ordersCount: any = '0';
  pendingCount: any = '0';
  /**
   * Normaltime  of dashboard component
   * not getting time zone showing the local time
   */
  normaltime: boolean;
  /**
   * Showtime zone of dashboard component
   * based on the time zone display the time
   */
  showtimeZone: boolean;
  /**
   * Logged time zone of dashboard component for getting the time zone
   */
  LoggedTimeZone: number;

  /**
   * Creates an instance of dashboard component.
   * @param dashboard provide dashboard service data
   * @param patient provide patient service data
   * @param app provide app service data
   * @param utility provide utility service data
   * @param error Error Service for handling errors.
   * @param dropdownService provide dropdown service data
   */
  constructor(
    private dashboard: DashboardService,
    private patient: PatientSearchService,
    private app: AppService,
    private utility: UtilityService,
    private error: ErrorService,
    private dropdownService: DropdownService
  ) {
    this.username = this.app.userName;
  }

  ngOnInit() {
    this.appCount = 0;
    this.showtimeZone = false;
    this.normaltime = false;
    this.updateTimeZone();
    this.liveTimer();
    // orders list
    this.ordersList();
    // Appointment List
    this.dashboardAppoinment();
    // To Do List
    this.dashboardToDolist();

  }

  /**
   * Updates time zone for updated time based on the time zone
   */
  updateTimeZone() {
    if (this.app.timeZone.length > 0) {
      this.LoggedTimeZone = this.app.timeZone;
      this.showtimeZone = true;
      this.normaltime = false;
    } else {
      this.showtimeZone = false;
      this.normaltime = true;
    }
  }

  /**
   * Lives timer
   * for showing live time
   */
  liveTimer() {
    setInterval(() => {
      this.today = Date.now();
    }, 1000);
  }

  // Load ToDo List grid
  dashboardToDolist() {
    this.dashboard.loadTodoList().subscribe(data => {
      this.toDoList = data;
      for (let i = 0; i <= this.toDoList.length - 1; i++) {
        if (this.toDoList[i].finished === 0) {
          this.toDoList[i].finished = 'Completed';
        } else {
          this.toDoList[i].finished = 'pending';
        }
      }
    });
  }

  // Load Appoinment List grid
  dashboardAppoinment() {
    const getdata = 'data';
    this.appStatusList = [];
    this.dashboard.loadAppList().subscribe(data => {
      this.appList = data[getdata];
      for (let i = 0; i <= this.appList.length - 1; i++) {
        if (this.appList[i].status === null) {
          this.appStatusList.push(this.appList[i]);
          this.appList[i].status = 'Pending';
        }
      }
      // Appointment Status Pending Count
      if (this.appStatusList.length !== 0) {
        this.pendingCount = this.appStatusList.length;
      } else {
        this.pendingCount = '0';
      }

      // Appointments Total Count
      this.appCount = this.appList.length;
    });
  }

  /**
   * Orders list for loading ordre list data
   */
  ordersList() {
    const getdata = 'data';
    const reqBody = {
      filter: [
        {
          field: 'entered_date',
          operator: '<=',
          value: new Date()
        }

      ],
      sort: [
        {
          field: 'id',
          order: 'DESC'
        }
      ]

    };
    this.dashboard.loadOrdersList(reqBody).subscribe(data => {
      this.ordersListdata = data[getdata];
      this.orderType = this.dropdownService.orderType;
      if (this.utility.getObjectLength(this.orderType) === 0) {
        this.dropdownService.loadOrderType().subscribe(orderdata => {
          try {
            this.orderType = orderdata;
            this.dropdownService.orderType = this.orderType;
          } catch (error) {
            this.error.syntaxErrors(error);
          }
        }
        );
      }
      this.status = this.dropdownService.status;
      if (this.utility.getObjectLength(this.status) === 0) {
        this.dropdownService.loadOrderstatus().subscribe(orderstatusdata => {
          try {
            this.status = orderstatusdata;
            this.dropdownService.status = this.status;
          } catch (error) {
            this.error.syntaxErrors(error);
          }
        }
        );
      }
    });
  }

  // Contact-us
  contactus() {
    window.open('https://eyecareleaders.com/contact-us/');
  }
}
