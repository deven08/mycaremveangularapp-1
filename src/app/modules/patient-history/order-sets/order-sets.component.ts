// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-order-sets',
  templateUrl: './order-sets.component.html'
})
export class OrderSetsComponent implements OnInit {
  PatientHistoryUrl: Object;
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=order_sets');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
