/**
 * This component is used to show immunity page under patient history using iframe
 */
// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-immunizations',
  templateUrl: './immunizations.component.html'
})
export class ImmunizationsComponent implements OnInit {

  PatientHistoryUrl: Object;

  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=immunizations');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
