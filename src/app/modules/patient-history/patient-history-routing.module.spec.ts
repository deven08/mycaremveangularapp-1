import { PatientHistoryRoutingModule } from './patient-history-routing.module';

describe('PatientHistoryRoutingModule', () => {
  let patientHistoryRoutingModule: PatientHistoryRoutingModule;

  beforeEach(() => {
    patientHistoryRoutingModule = new PatientHistoryRoutingModule();
  });

  it('should create an instance', () => {
    expect(patientHistoryRoutingModule).toBeTruthy();
  });
});
