// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-vital-signs',
  templateUrl: './vital-signs.component.html'
})
export class VitalSignsComponent implements OnInit {

  PatientHistoryUrl: Object;
  constructor(private _AppSettings: IframesService) {
     this.PatientHistoryUrl = this._AppSettings.routeUrl('PatientHistoryUrl', true, '?showpage=vs');
  }

  ngOnInit() {
  }

  onLoad() {
    this._AppSettings.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
