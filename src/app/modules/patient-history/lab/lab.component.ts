// Core Modules
import { Component, OnInit } from '@angular/core';

// Services
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html'
})
export class LabComponent implements OnInit {

  PatientHistoryUrl: Object;
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=lab');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
