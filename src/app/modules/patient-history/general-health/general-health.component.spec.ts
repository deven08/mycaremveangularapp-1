import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralHealthComponent } from './general-health.component';

describe('GeneralHealthComponent', () => {
  let component: GeneralHealthComponent;
  let fixture: ComponentFixture<GeneralHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
