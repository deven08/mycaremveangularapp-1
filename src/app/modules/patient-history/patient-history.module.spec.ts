import { PatientHistoryModule } from './patient-history.module';

describe('PatientHistoryModule', () => {
  let patientHistoryModule: PatientHistoryModule;

  beforeEach(() => {
    patientHistoryModule = new PatientHistoryModule();
  });

  it('should create an instance', () => {
    expect(patientHistoryModule).toBeTruthy();
  });
});
