import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PatientSearchService } from '@services/patient/patient-search.service';
import { Router } from '@angular/router';
import { AppService } from '@services/app.service';
import { ErrorService } from '@services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { OrderService } from '@app/core/services/order/order.service';

@Component({
  selector: 'app-patient-search',
  templateUrl: './search.component.html',
  providers: [DatePipe]
})


export class PatientSearchComponent implements OnInit {
  cols: any;
  arrPatients: any[];
  patientcolumns: any[];
  copypatients: any[];
  patientSearch: FormGroup;
  reqbody: any = [];
  searchPatientDate = '';
  data: any = null;
  /**
   * Getting current date for DoB
   */
  currentDate = new Date();
  
  /**
   * Storing max and min years for DoB
   */
  maxMinYears:string;
  /**
   * Provider  of patient search component for provider dropdown data
   */
  public provider: any;
  /**
   * Providerlist  of patient search component for provider dropdown data
   */
  public providerlist: any;

  location: any;
  fname: any;
  lname: any;
  phone: any;
  dob: any;
  ssn: any;
  email: any;
  address: any;
  zipcode: any;
  reecall: any;
  id: any;
  type: any;
  active: any;
  exam: any;
  order: any;
  tray: any;
  claim: any;
  patientdataempty = true;
  fileData: any;
  total: any;
  rows: any;
  progressBar: boolean;
  activeFileId: any;
  countItems: number;
  loading: boolean;
  reqbodysearch: any;
  temp: any;

  @ViewChild('dt') public dt: any;
  resultData: any;
  /**
   * Creates an instance of patient search component.
   * @param router for navization
   * @param patient for calling patientservice methods
   * @param datePipe for transform formate
   * @param varservicce for getting id's
   * @param error for displayError
   * @param dropdownService for calling common methods
   * @param utility for calling getObjectLength method
   */
  constructor(
    private router: Router,
    private patient: PatientSearchService,
    private datePipe: DatePipe,
    public varservicce: AppService,
    private error: ErrorService,
    private dropdownService: DropdownService,
    private utility: UtilityService,
    private fb: FormBuilder,
    private orderService: OrderService
  ) {
    this.progressBar = false;
    this.countItems = 0;
    this.total = 0;

    // Setting File Id to 0
    this.activeFileId = 0;
  }


  /**
   * on init for initilize the methods
   */
  ngOnInit() {
    const minYear = new Date().getFullYear() - 100;
    const maxYear = new Date().getFullYear();
    this.maxMinYears = minYear  +':' + maxYear;
    if (this.varservicce.patientId === 0) {
      this.error.displayError({
        severity: 'warn',
        summary: 'Warning Message',
        detail: 'No Patient Selected Yet'
      });
    }
    this.LoadFormData();
    this.patientcolumns = [
      { field: 'fname', header: 'First Name', datetime: false, details: 'First Name' },
      { field: 'lname', header: 'Last Name', datetime: false, details: 'Last Name' },
      { field: 'DOB', header: 'Date of Birth', datetime: false, details: 'Date of Birth' },
      { field: 'id', header: 'PatientId', datetime: false, details: 'PatientId' },

    ];

    this.providerList();
  }

  // TO DO with the filter for Date
  getPatientbydate(datedetails) {
    if (datedetails !== null || datedetails !== undefined) {
      const patientDate = this.patientSearch.controls.dob.value;
      const temp = this.datePipe.transform(patientDate, 'yyyy-MM-dd');
      const value = this.arrPatients.filter(d => d.DateOfBirth === temp);
      this.arrPatients = value;
    } else {
      this.arrPatients = this.copypatients;
    }
  }
  onBlurMethod(e) {
    if (e.value === null || e.value === undefined) {
      this.arrPatients = this.copypatients;
      return;
    }
  }


  // Patient Search API
  /**
   * Gets patient search results
   * @param {string} [location] for location id
   * @param {string} [fname] for first name
   * @param {string} [lname]  for last name
   * @param {string} [phone] for phone number
   * @param {string} [dob] for date of birth
   * @param {string} [ssn] for ssn number
   * @param {string} [email] for emil
   * @param {string} [address] for address
   * @param {string} [zipcode] for zip code
   * @param {string} [reecall] for reecall date
   * @param {string} [id] for patient id
   * @param {string} [provider] for provider id
   * @param {string} [type] for type
   * @param {string} [active] for status
   * @param {string} [exam] for exam
   * @param {string} [order] for order
   * @param {string} [tray] for try
   * @param {string} [claim] for claim id
   */
  private getPatientSearchResults(): void {
    this.dateFormate();

    if (this.patientSearch.controls.active.value === 'Yes') {
      this.patientSearch.controls.active.patchValue('Active');
    }
    if (this.patientSearch.controls.active.value === 'No') {
      this.patientSearch.controls.active.patchValue('Inactive');
    }

    if (this.getAllPatients() === true) {

      this.reqbody = {
        filter: [
          {
            field: 'patientStatus',
            operator: '=',
            value: 'Active'
          }
        ]
      };

    } else {

      this.reqbody = {
        filter: [
          {
            field: 'lname',
            operator: 'LIKE',
            value: this.patientSearch.controls.lname.value === null ? '%  %' : '%' + this.patientSearch.controls.lname.value + '%'
          },
          {
            field: 'fname',
            operator: 'LIKE',
            value: this.patientSearch.controls.fname.value === null ? '%  %' : '%' + this.patientSearch.controls.fname.value + '%'
          },
          {
            field: 'phone_contact',
            operator: 'LIKE',
            value: this.patientSearch.controls.phone.value === null ? '%  %' : '%' + this.patientSearch.controls.phone.value + '%'
          },
          {
            field: 'DOB',
            operator: '=',
            value: this.patientSearch.controls.dob.value === null ? '%  %' : this.temp
          },
          {
            field: 'ss',
            operator: '=',
            value: this.patientSearch.controls.ssn.value === null ? '%  %' : this.patientSearch.controls.ssn.value
          },
          {
            field: 'email',
            operator: '=',
            value: this.patientSearch.controls.email.value === null ? '%  %' : this.patientSearch.controls.email.value
          },
          {
            field: 'default_address',
            operator: 'LIKE',
            value: this.patientSearch.controls.address.value === null ? '%  %' : '%' + this.patientSearch.controls.address.value + '%'
          },
          {
            field: 'postal_code',
            operator: '=',
            value: this.patientSearch.controls.zipcode.value === null ? '%  %' : this.patientSearch.controls.zipcode.value
          },
          {
            field: 'PatientId',
            operator: '=',
            value: this.patientSearch.controls.id.value === null ? '%  %' : this.patientSearch.controls.id.value
          },
          {
            field: 'patientStatus',
            operator: '=',
            value: this.patientSearch.controls.active.value === null ? '%  %' : this.patientSearch.controls.active.value
          },
          {
            field: 'providerID',
            operator: '=',
            value: this.patientSearch.controls.provider.value === null ? '%  %' : this.patientSearch.controls.provider.value
          },
          {
            field: 'recalldate',
            operator: '=',
            value: this.patientSearch.controls.reecall.value === null ? '%  %' : this.patientSearch.controls.reecall.value
          },
          {
            field: 'orderId',
            operator: '=',
            value: this.patientSearch.controls.order.value === null ? '%  %' : this.patientSearch.controls.order.value
          },
          {
            field: 'TrayNumber',
            operator: '=',
            value: this.patientSearch.controls.tray.value === null ? '%  %' : this.patientSearch.controls.tray.value
          },
          {
            field: 'Claim',
            operator: '=',
            value: this.patientSearch.controls.claim.value === null ? '%  %' : this.patientSearch.controls.claim.value
          },
          {
            field: 'exam',
            operator: '=',
            value: this.patientSearch.controls.exam.value === null ? '%  %' : this.patientSearch.controls.exam.value
          }
        ]
      };
      // this.patientdataempty = true; 
    }
    const resultData = this.reqbody.filter.filter(p => p.value !== "" && p.value !== null && p.value !== "%  %" && p.value !== "%%");
    if (resultData.length === 0) {
      const data = [{
        "field": 'patientStatus',
        "operator": '=',
        "value": 'Active'
      }]
      this.reqbodysearch = {
        "filter": data
      };

    } else {
      this.reqbodysearch = {
        "filter": resultData
      };
    }
    this.patient.loadNewPatientApi(this.reqbodysearch).subscribe(newdata => {
      this.arrPatients = [];
      this.arrPatients = newdata['data'];
      this.copypatients = this.arrPatients;
      this.patientdataempty = true;
      this.total = newdata['total'];
      this.rows = newdata['per_page'];
    },
      error => {
        this.arrPatients = [];
        this.copypatients = [];
        this.error.displayError({
          severity: 'info', summary: 'Info Message',
          detail: 'Patient does not exist'
        });
      }
    );
    if (this.patientSearch.controls.active.value === 'Active') {
      this.patientSearch.controls.active.patchValue('Yes');
    }
    if (this.patientSearch.controls.active.value === 'Inactive') {
      this.patientSearch.controls.active.patchValue('No');
    }
  }
  keyPress(event: any) {
    const pattern = /[0-9]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  // Patient Search Form
  /**
   * Searchs patient
   * @param e  for getting form values
   */

  onRowClick() {
    this.router.navigate(['patient/demographics'], { queryParams: { grid: 'demographics' } });

  }


  /**
   * Determines whether row select on
   * @param event for getting seleted patient details
   */
  onRowSelect(event) {
    this.orderService.orderId = 'New Order';
    const PatientId = event.data.id;
    const data = {
      patientId: PatientId
    };
    // tslint:disable-next-line:no-shadowed-variable
    this.patient.loadPatient(data).subscribe(data => {
      try {
        this.data = data;
        this.varservicce.patientId = 0;
        this.varservicce.patientId = PatientId;
        this.varservicce.isPatientSigned = true;
      } catch (error) {
        this.error.syntaxErrors(error);
      }
    });
    //     this.data = data;
    //     if (this.data.Success !== undefined && this.data.Success === 1) {
    //       if (this.data.result !== undefined) {

    //         //  Resseting Previous Patient
    //         this.varservicce.patientId = 0;

    //         // Setting Patient Active
    //         this.varservicce.patientId = PatientId;

    //         this.varservicce.isPatientSigned = true;

    //       } else {
    //         alert('Can\'t Able to load patient');
    //       }

    //     } else {
    //       alert('Can\'t Able to load patient');
    //     }
    //   },
    //   (err) => {
    //     alert('Can\'t Able to load patient');
    //   }
    // );
  }
  // Get the patient ID
  GetPatientId(data: any) {
    this.varservicce.patientId = data.ID;
  }

  /**
   * Providers list
   * @param {Array} provider for binding dropdown data
   * @returns {object}  for dropdown data binding
   */
  providerList() {
    this.provider = [];
    this.providerlist = [];
    this.provider = this.dropdownService.provider;
    if (this.utility.getObjectLength(this.provider) === 0) {
      // this.provider.push({ Id: '', Name: 'Select User' });
      this.dropdownService.getUsers().subscribe(
        data => {
          try {
            this.providerlist = data['data'];
            for (let i = 0; i <= this.providerlist.length - 1; i++) {
              if (!this.provider.find(a => a.Name === this.providerlist[i].username)) {
                this.provider.push({ Id: this.providerlist[i].id.toString(), Name: this.providerlist[i].username });
              }
            }
            this.providerlist = this.provider;
            this.dropdownService.provider = this.provider;
          } catch (error) {
            this.error.syntaxErrors(error);
          }
        });
    }
  }


  LoadFormData() {
    this.patientSearch = this.fb.group({
      location: '',
      fname: '',
      lname: '',
      phone: '',
      dob: '',
      ssn: '',
      email: '',
      address: '',
      zipcode: '',
      reecall: '',
      id: '',
      provider: '',
      // type: '',
      active: '',
      exam: '',
      order: '',
      tray: '',
      claim: '',
    });
  }


  /**
   * Searching details
   * @returns Displaying patient details
   */
  searchingDetails() {
    this.arrPatients = [];
    this.patientdataempty = false;
    this.getPatientSearchResults();
  }
  getAllPatients() {

    // patientdataempty = true;

    if (this.patientdataempty === true) {
      return true;
    }

    Object.keys(this.patientSearch.controls).forEach(key => {

      if (this.patientSearch.get(key).value !== '' || this.patientSearch.get(key).value !== null) {
        this.patientdataempty = false;
      }

    });

    return this.patientdataempty;
  }
  clearData() {
    // this.patientdataempty = true;
    this.arrPatients = [];
    this.patientSearch.reset();
    // this.getPatientSearchResults();
  }

  /**
   * Loads file data
   * @returns load patient data
   */
  loadPatientData(itemId: number, page: number = 0) {
    // Checking File Id
    if (itemId === 0) {
      // Get Data from file
      this.patient.getPatientDetails(this.reqbodysearch, page).subscribe(
        data => {
          this.arrPatients = data['data'];
        },
      );
    }
  }
  /**
   * Lazys load file items
   * @returns Load lazyLoadPatientList data
   */
  loadLazyItems(event: LazyLoadEvent) {
    // Generating Page
    let page = 0;
    page = ('first' in event && 'rows' in event)
      ? (event.first / event.rows) + 1
      : 0;
    // Calling Page Data
    this.loadPatientData(this.activeFileId, page);
  }
  /**
   * Dates formate
   * @returns date formate
   */
  dateFormate() {
    const patientDate = this.patientSearch.controls.dob.value;
    this.temp = this.datePipe.transform(patientDate, 'yyyy-MM-dd');
  }
}
