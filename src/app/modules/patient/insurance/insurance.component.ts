// App Configuration Module
import { IframesService } from '@shared/services/iframes.service';
// Core Modules
import { Component } from '@angular/core';


// Setting Up Component
@Component({
  selector: 'app-patient-insurance',
  templateUrl: './insurance.component.html'
})

export class PatientInsuranceComponent {
  PatientInsuranceUrl: Object;
  constructor(private service: IframesService) {
    this.PatientInsuranceUrl = this.service.routeUrl('PatientInsuranceUrl', true);

  }
// implemented for the loading of the patient banner
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
