import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem, LazyLoadEvent } from 'primeng/primeng';
import { AppService } from '@services/app.service';
import { PatientSearchService } from '@services/patient/patient-search.service';
import { ErrorService } from '@services/error.service';
import { OrderHistoryService } from '@services/patient/order-history.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { OrderStatus } from '@app/core/models/order/order.model';


@Component({
    selector: 'app-order-history',
    templateUrl: 'order-history.component.html'
})
export class PatientOrderHistoryComponent implements OnInit {

    orderHistoryCopy: any[];
    /**
     * Order history of patient order history component
     */
    orderHistory: any = [];
    orderTypesCopy: any[];
    /**
     * Order types of patient order history component
     */
    orderTypes: any = [];
    checked = false;
    data: any = null;
    pastOrderBy: SelectItem[];
    groupBy: SelectItem[];
    selectedGroupBy = 'Group by';
    selectedPastOrderBy = 'Patient';
    selectedPastOrderByFamily = 'Family';

    /* Declarations */
    Orderhistorycolumns: any[];
    OrderhistoryForm: FormGroup;
    familydetails: any;
    patientID: any;
    patientnames: any;
    orderDetailsItemsData: any = [];
    chargeItemslist: any = [];
    orderItemsData: any = [];
    totalRecords: string;
    paginationPage: any = 0;
    paginationValue: any = 0;
    pageRowsCount: any = 15;
    pageRowsvalue: any = 15;
    patientfirstname: string;
    patientlastname: string;
    dataList: any;
    orderHistoryList: { "filter": { "field": string; "operator": string; "value": any; }[]; "sort": { "field": string; "order": string; }[]; };
    searchFilterdataList: { filter: { "field": string; "operator": string; "value": any; }[]; sort: { field: string; order: string; }[]; };

    /**
     * Order type of patient order history component
     */
    orderType: any = [];
    /**
     * Value  of patient order history component
     */
    value: any;
    /**
     * Values  of patient order history component
     */
    values: number;
    /**
     * Status  of patient order history component
     */
    status: any = [];
    /**
     * Order type list of patient order history component
     */
    orderTypeList: any = [];
    /**
     * Status list of patient order history component
     */
    statusList: any = [];

    /**
     * Creates an instance of patient order history component.
     * @param fb for formbuilder
     * @param router Angular Router Library for handling routes.
     * @param datePipe for the format of the date
     * @param appService for fetching the common variables
     * @param patient for calling patientservice methods
     * @param orderHistoryService common service for complete order
     * @param error Error Service for handling errors.
     */
    constructor(
        private fb: FormBuilder,
        private router: Router,
        private datePipe: DatePipe,
        public appService: AppService,
        private patient: PatientSearchService,
        private orderHistoryService: OrderHistoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private dropdownService: DropdownService
    ) {
        this.pastOrderBy = [
            { label: 'Past Order by', value: 'Past Order by' },
            { label: 'Patient', value: 'Patient' },
            { label: 'Family', value: 'Family' }
        ];
        this.groupBy = [
            { label: 'Group by', value: 'Group by' }
        ];
    }

    /**
     * Loadformdatas patient order history component
     *  load React FormBuilder
     */
    Loadformdata() {
        this.OrderhistoryForm = this.fb.group({
            id: '',
            enddate: '',
            entered_date: '',
            selectedPastOrderByFamily: 'Patient',
            selectedGroupBy: 'Group by',
            upccode: '',
            itemname: '',
            ordertypeid: '',
            order_status_id: ''
        });
    }

    ngOnInit() {
        this.Loadformdata();
        this.orderData();
        this.getPatientSearchResults(this.appService.patientId);
        this.loadorderhistorycolumns();
        if (this.appService.patientId === 0) {
            this.error.displayError({
                severity: 'error',
                summary: 'Info Message',
                detail: 'No Patient Selected Yet'
            });
        } else {
            this.patient.patientSearch('', '', '', '0').subscribe(data => {
                this.patientnames = data['result']['SearchPatient'];
                this.LoadOrdersData(this.appService.patientId, '', '', '', '', '', '', '', '');
            });
        }
    }

    /**
     * Loadorderhistorycolumns patient order history component
     */
    loadorderhistorycolumns() {
        this.Orderhistorycolumns = [
            { field: 'OperatorId', header: 'RowID', visible: false },
            { field: 'PatientId', header: 'Patient#', visible: true },
            { field: 'First Name', header: 'First Name', visible: true },
            { field: 'Last Name', header: 'Last Name', visible: true },
            { field: 'Id', header: 'Order', visible: true },
            { field: 'EnteredDate', header: 'Order Date', visible: true },
            { field: 'description', header: 'Order Type', visible: true },
            { field: 'TotalQuantity', header: 'Qty', visible: true },
            { field: 'TotalPrice', header: 'Charges', visible: true },
            { field: 'patientPaidAmt', header: 'Pat.Payments', visible: true },
            { field: 'insPaidAmt', header: 'Ins.Payments', visible: true },
            { field: 'deductibleTotalAmt', header: 'Write Offs', visible: true },
            { field: 'LocationId', header: 'Location', visible: true },
            { field: 'OrderStatus', header: 'Order Status', visible: true }

        ];
    }

    /**
     * Getcompltepatientdetails patient order history component
     * @param id for the get complete patinetId details
     */
    private getcompltepatientdetails(id): void {
        this.patient.patientSearch('', '', '', id).subscribe(data => {
            this.patientnames = data['result']['SearchPatient'];
            if (this.patientnames !== undefined && this.patientnames.length !== undefined) {
                if (this.patientnames[0]['FirstName'] !== undefined) {
                    this.patientfirstname = this.patientnames[0]['FirstName'];
                }
                if (this.patientnames[0]['LastName'] !== undefined) {
                    this.patientlastname = this.patientnames[0]['LastName'];
                }
            }
        });
    }

    /**
     * Params patient order history component
     * Pull the Patient search details data added and filters data with below parameters
     * @param [patientId] 
     */
    private getPatientSearchResults(patientId = '') {

        if (this.patientnames == null || this.patientnames == undefined ||
            this.patientnames.length != undefined || this.patientnames.length <= 1) {
            this.getcompltepatientdetails(patientId);
        }
    }

    /**
     * Params patient order history component
     * @param [page] for the pagination data
     */
    getOrderSearchPagination(page: number = 0) {
        this.orderHistory = [];
        this.orderDetailsItemsData = [];
        this.getFilterData();
        this.orderHistoryService.getOrderHistoryBypagination(this.orderHistoryList, page).subscribe(
            data => {
                this.dataList = data['data'];
                this.getOrderList();
            },
        );
    }
    /**
    * Lazys load file items
    * @returns Load lazyLoad order history lens data
    */
    loadLazyOrderSearchItems(event: LazyLoadEvent) {
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.getOrderSearchPagination(page);
    }

    /**
     * Gets filter data
     * @returns order history filter payload
     */
    getFilterData() {
        this.orderHistoryList = {
            "filter": [
                {
                    field: "patient_id",
                    operator: "=",
                    value: this.appService.patientId
                }

            ], "sort": [
                {
                    "field": "id",
                    "order": "DESC"
                }
            ]
        }
    }

    /**
     * Gets order list
     * @returns assign item type data list
     */
    getOrderList() {
        for (let i = 0; i <= this.dataList.length - 1; i++) {
            this.dataList[i].OperatorId = i;
            this.dataList[i].FirstName = this.patientfirstname;
            this.dataList[i].LastName = this.patientlastname;
            // this.orderHistory.push(this.dataList[i])
        }
        this.orderHistory = this.dataList;
    }
    /**
     * Loads orders data
     * Pull the Order search details data added and filters data with below parameters
     * @param [patientid] 
     * @param [orrdernumber] 
     * @param [orderfromDate] 
     * @param orderToDate  for the dispay the order date
     * @param [upccode] 
     * @param [itemname] 
     * @param [itemtype] 
     * @param [orderStatus] 
     * @param [familydetails] 
     */
    LoadOrdersData(patientid = '', orrdernumber = '', orderfromDate = '', orderToDate, upccode = '',
        itemname = '', itemtype = '', orderStatus = '', familydetails = '') {
        this.orderHistory = [];
        this.orderHistoryCopy = [];
        this.orderDetailsItemsData = [];
        this.getFilterData();
        this.orderHistoryService.getOrderHistoryData(this.orderHistoryList).subscribe(data => {
            this.totalRecords = data['total']
            this.dataList = data['data'];
            this.getOrderList();

        })
    }

    /**
    * Determines whether row select on on particular row select 
    * @param {string} event for select the row
    */
    onRowSelect(event) {
        const PatientId = event;
        const data = {
            accessToken: '',
            patientId: PatientId
        };
        this.patient.loadPatient(data).subscribe(
            data => {
                try {
                    this.data = data;
                    this.appService.patientId = 0;
                    this.appService.patientId = PatientId;
                    this.appService.isPatientSigned = true;
                }
                catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
    }
    /**
     * Doubles click patient
     * @param rowData display seleted data for textbox
     */
    DoubleClickPatient(rowData) {
        this.onRowSelect(rowData.patient_id);
        this.appService.patientId = rowData.patient_id;

        switch (rowData.ordertypeid) {
            case 7: {
              this.navigateToOrders(rowData, 'spectacle-lens', 7);
              break;
            }
            case 1: {
              this.navigateToOrders(rowData, 'frame', 1);
              break;
            }
            case 5: {
              this.navigateToOrders(rowData, 'contact-lens', 5);
              break;
            }
            case 4: {
              this.navigateToOrders(rowData, 'contact-lens', 4);
              break;
            }
            case 6: {
              this.navigateToOrders(rowData, 'other-details', 6);
              break;
            }
          }

    }
    navigateToOrders(rowData, routevalue, ordertype) {
        this.router.navigateByUrl('/order/' + routevalue).then(success => {

            this.appService.ordersearchvalueService = [];
            const data = {
                'Id': rowData.id,
                'OrderType': ordertype
            };
            this.appService.ordersearchvalueService.push(data);
        });
    }
    /**
     * Searchdetails patient order history component
     * @param event search family details list
     * @returns  for display the search family deatails
     */
    Searchdetails(event) {
        const patientid = this.appService.patientId;
        if (patientid === null || patientid === undefined) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select a Patient'
            });

            return;
        }
        this.OrderhistoryForm.controls['upccode'].patchValue('');
        this.OrderhistoryForm.controls['itemname'].patchValue('');
        this.OrderhistoryForm.controls['ordertypeid'].patchValue('');
        this.OrderhistoryForm.controls['status'].patchValue('');

        if (event.value === 'Family') {
            let tempString = '';
            if (this.familydetails !== null && this.familydetails !== undefined) {
                this.familydetails.forEach(element => {
                    tempString += 'Family[]=' + element.FamilyDetails + '&';
                });
                if (tempString !== '') {
                    tempString = tempString.substring(0, tempString.length - 1);
                    this.LoadOrdersData(patientid, '', '', '', '', '', '', '', encodeURI(tempString));
                } else {
                    this.orderHistory = [];
                    this.familydetails = [];
                }
            }
        } else {
            this.LoadOrdersData(patientid, '', '', '', '', '', '', '', '');
        }
    }

    /**
     * Getpatientchargevalues patient order history component
     * @param id 
     * @param length 
     * @param patientid 
     */
    getpatientchargevalues(id, length, patientid) {
        if (id !== null && id !== undefined) {
            const reqbody = {
                'filter': [
                    {
                        'field': 'opt_order_id',
                        'operator': '=',
                        'value': id
                    },
                    {
                        'field': 'patient_id',
                        'operator': '=',
                        'value': patientid
                    }
                ]
            };
            this.orderHistoryService.getthechargevaluesfororder(reqbody).subscribe(details => {
                for (let i = 0; i <= this.orderItemsData.length - 1; i++) {
                    this.orderItemsData[i]['amtPaid'] = details[0]['amtPaid'];
                    this.orderItemsData[i]['enterdate'] = details[0]['charge_list_detail'][i]['entered_date'];
                }
            }
            );
        }
    }

    /**
     * Searchorderhistorys patient order history component
     * @returns  Order search details
     */
    searchorderhistory() {
        const patientid = this.appService.patientId;
        if (patientid === null || patientid === undefined || patientid === 0) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select a Patient'
            });
            return;
        }
        const startdate = this.OrderhistoryForm.controls['startdate'].value;
        let datedetail = '';
        if (startdate === null || startdate === undefined || startdate === '') {

            datedetail = '';
        } else {
            datedetail = this.datePipe.transform(startdate, 'yyyy-MM-dd');
        }
        const enddate = this.OrderhistoryForm.controls['enddate'].value;
        let dateenddetail = '';
        if (enddate === null || enddate === undefined || enddate === '') {
            dateenddetail = '';
        } else {
            dateenddetail = this.datePipe.transform(enddate, 'yyyy-MM-dd');
        }
        this.LoadOrdersData('', this.OrderhistoryForm.controls['id'].value, datedetail,
            dateenddetail, this.OrderhistoryForm.controls['upccode'].value, this.OrderhistoryForm.controls['itemname'].value,
            this.OrderhistoryForm.controls['ordertypeid'].value, this.OrderhistoryForm.controls['status'].value, '');

    }

    /**
     * Returns items
     * @param index get data Order Item details
     */
    returnItems(index) {
        this.orderDetailsItemsData = [];
        this.orderHistoryService.getOrderItemDetails(index.id).subscribe(data => {
            this.orderItemsData = [];
            this.orderItemsData = data;
            if (this.orderItemsData.length > 0) {
                for (let i = 0; i <= this.orderItemsData.length - 1; i++) {
                    this.getpatientchargevalues(this.orderItemsData[i].order_id, i, this.orderItemsData[i].patient_id);
                }
            }
            this.orderDetailsItemsData = this.orderItemsData;
        });
    }

    /**
     * Searchs filter data
     * @returns  Order History search filter details
     */
    searchFilterData() {
        let dateValue;
        if (this.OrderhistoryForm.controls.entered_date.value == "") {
            dateValue = "";
        } else {
            dateValue = this.datePipe.transform(this.OrderhistoryForm.controls.entered_date.value, 'yyyy-MM-dd');
        }
        const otherResultData = {
            filter: [
                {
                    field: "patient_id",
                    operator: "=",
                    value: this.appService.patientId
                },
                {
                    field: "id",
                    operator: '=',
                    value: this.OrderhistoryForm.controls.id.value
                },
                {
                    field: "ordertypeid",
                    operator: '=',
                    value: this.OrderhistoryForm.controls.ordertypeid.value
                },
                {
                    field: "order_status_id",
                    operator: "=",
                    value: this.OrderhistoryForm.controls.order_status_id.value
                },
                {
                    field: "entered_date",
                    operator: "=",
                    value: dateValue
                }
            ],
        };
        const resultData = otherResultData.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.searchFilterdataList = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }
            ]
        }
        this.orderHistory = [];
        this.orderHistoryService.getOrderHistoryData(this.searchFilterdataList).subscribe(data => {
            this.dataList = data['data'];
            this.getOrderList();
        })
    }

    /**
     * Orders data
     * @returns order type data and order status data
     */
    orderData() {
        this.orderType = [];
        this.orderType = this.dropdownService.orderType;
        if (this.utility.getObjectLength(this.orderType) === 0) {
            this.dropdownService.loadOrderType().subscribe(
                data => {
                    try {
                        this.orderType = data;
                        this.dropdownService.orderType = this.orderType;

                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
        this.status = this.dropdownService.status;
        if (this.utility.getObjectLength(this.status) === 0) {
          this.dropdownService.loadOrderstatus().subscribe((orderstatusdata: OrderStatus) => {
            try {
              this.status = orderstatusdata;
              this.dropdownService.status = this.status;
            } catch (error) {
              this.error.syntaxErrors(error);
            }
          }
          );
        }
    }

/**
 * Clear data
 * clearing the form controller details
 */
clearData() {
    this.OrderhistoryForm.reset();
    this.LoadOrdersData(this.appService.patientId, '', '', '', '', '', '', '', '');
}


}
