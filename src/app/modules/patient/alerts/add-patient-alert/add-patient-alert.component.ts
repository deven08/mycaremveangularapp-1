import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '@app/core';


@Component({
    selector: 'app-add-patient-alert',
    templateUrl: 'add-patient-alert.component.html'
})
export class AddPatientAlertComponent implements OnInit {
    ngOnInit() { }
    constructor(
        private _router: Router,
        public app: AppService) {

    }
}

