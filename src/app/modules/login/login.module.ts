import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { NgModule } from '@angular/core';

// Services
import {UserService} from '@services/user.service';
import { SharedModule } from '@shared/shared.module';

@NgModule ({
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule
  ],
  providers: [
    UserService
  ],
  declarations: [
    LoginComponent
  ]
})
export class LoginModule { }
