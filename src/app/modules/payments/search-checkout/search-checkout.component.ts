import { Component, OnInit } from '@angular/core';
import { AppService } from '@app/core';

@Component({
    selector: 'app-search-checkout',
    templateUrl: 'search-checkout.component.html'
})

export class PaymentSearchCheckoutComponent implements OnInit {

    constructor(public app: AppService) {

    }

    ngOnInit(): void {

    }

}
