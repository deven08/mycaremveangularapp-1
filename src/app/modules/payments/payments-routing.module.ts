import { NgModule } from '@angular/core';
import { PaymentCheckoutComponent } from './';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'payments',
        component: PaymentCheckoutComponent,
        data: { 'title': 'payments', 'banner': false }
      }]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
