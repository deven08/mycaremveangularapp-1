import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-leftnav',
  templateUrl: './leftnav.component.html'
})
export class LeftnavComponent {
  @Output() infoMsgShow: EventEmitter<any> = new EventEmitter();
  @Output() pdialogShow: EventEmitter<any> = new EventEmitter();
  /**
   * Output  of leftnav component
   */
  @Output() reload_Pages: EventEmitter<any> = new EventEmitter();

  /**
  * portalSSourl  will store Parctice Registered SSO portal Url 
  */
  portalSSourl: string 

  /**
    * descriptionMessage will store the valid message to display for End-User
    */
  descriptionMessage: string;
  
  /**
   * Inputs the Instance to the Appservice into component
   */
  @Input() app: any;


  constructor(   
  ) {

  }
  ngOnInit() {
    
  }
  /**
      * LaunchportalSSO will Launch a a web Browser in new window to login patient portal for valid Portal Users
      */
  LaunchportalSSO() {
    if (this.app.portalSSOUrl == "") {  
      this.descriptionMessage = "The Practice has not been Registered with the Portal.Please contact support";
      this.pdialogShow.emit(this.descriptionMessage)        
      return;
      }
      else {
        if (this.app.isUserRegisteredtoPortal == false) {
          this.descriptionMessage = "The Login user is not Registered with the Portal.Please contact support"
          this.pdialogShow.emit(this.descriptionMessage);          
          return;
      }
    }
    this.portalSSourl = this.app.portalSSOUrl;
    window.open(this.portalSSourl, "_blank");
  }

  
  
  
}
