import { Routes } from '@angular/router';

export const CONTENT_ROUTES: Routes = [
  {
    path: 'patient',
    loadChildren: './modules/patient/patient.module#PatientModule'
  },
  {
    path: 'patient-history',
    loadChildren: './modules/patient-history/patient-history.module#PatientHistoryModule',
  },
  {
    path: 'calendar',
    loadChildren: './modules/calendar/calendar.module#CalendarModule'
  },
  {
    path: 'exam',
    loadChildren: './modules/exam/exam.module#ExamModule'
  },
  {
    path: 'settings',
    loadChildren: './modules/settings/settings.module#SettingsModule'
  },
  {
    path: 'inventory',
    loadChildren: './modules/inventory/inventory.module#InventoryModule'
  },
  {
    path: 'rx',
    loadChildren: './modules/rx/rx.module#RxModule'
  },
  {
    path: 'dashboard',
    loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'reports',
    loadChildren: './modules/reports/reports.module#ReportsModule'
  },
  {
    path: 'accounting',
    loadChildren: './modules/accounting/accounting.module#AccountingModule'
  },
  {
    path: 'payments',
    loadChildren: './modules/payments/payments.module#PaymentsModule',
  },
  {
    path: 'employees',
    loadChildren: './modules/employees/employees.module#EmployeesModule',
  },
  {
    path: 'claims',
    loadChildren: './modules/claims/claims.module#ClaimsModule',
  },
  {
    path: 'order',
    loadChildren: './modules/order/order.module#OrderModule',
  },
  {
    path: 'maintenance',
    loadChildren: './modules/maintenance/maintenance.module#MaintenanceModule',
  },
  {
    path: 'batch',
    loadChildren: './modules/batch/batch.module#BatchModule'
  },
  {
    path: 'billing',
    loadChildren: './modules/billing/billing.module#BillingModule'
  }
];


