import { environment as env } from '@env/environment';
import { IframesSource } from '@models/iframes.model';
import { Injectable } from '@angular/core';
import { SafeUrlPipe } from '../pipes/safe-url.pipe'; 
@Injectable({
  providedIn: 'root'
})
export class IframesService {
 

  constructor(private _SafeUrlPipe: SafeUrlPipe) {}

   routeUrl(url, enableSafe, params: String = null) {

    if (enableSafe) {
       if (params === null) {
          return this._SafeUrlPipe.safeUrl(`${env.imedicUrl}`+IframesSource[url]); 
       } else {
          return this._SafeUrlPipe.safeUrl(`${env.imedicUrl}`+IframesSource[url] + params); 
       }
    } else {
       return `${env.imedicUrl}`+IframesSource[url];
    }

   }

   resizeIframe(child: boolean = false) {
      // Iframe
      const frame = $('iframe');
      let width_p = 0;
      let height_p = 0;

      // Parent Container Parameters
      if (child) {
         width_p = frame.parent('div').parent('div').width();
         height_p = $('body').height() - frame.offset().top;
      } else {
         width_p = $('body').width() - frame.offset().left;
         height_p = $('body').height() - frame.offset().top;
      }


      // Parent Container
      const p_container = frame.parent('div');

      // Setting Position Fixed
      p_container.css('position', 'fixed');


      // Setting Width and height
      p_container.width(width_p).height(height_p);


      // Setting Postion Relative
      frame.css('position', 'relative');

      frame.width(width_p).height(height_p);
  }
}
