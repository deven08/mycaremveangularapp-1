import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';
import { AppService } from '@app/core';


@Component({
    selector: 'patient-alert',
    templateUrl: 'patient-alert.component.html',
    styleUrls: ['./patient-alert.component.css']
})
export class patientAlertComponent implements OnInit {
    // display = false;

    /**
     * Patient alert url of patient alert component from imw url
     */
    patientAlertUrl: any;

    /**
     * Creates an instance of patient alert component.
     * @param iframeService is for the loading the Imw components
     * @param app is for the getting the most commonly used variables.
     */
    constructor(
        private iframeService: IframesService,
        public app:AppService){

    }
    ngOnInit() {
        this.patientAlertUrl = this.iframeService.routeUrl('patientAlertsURl', true);
    }
    // test() {
    //     this.display = true;
    // }
}

