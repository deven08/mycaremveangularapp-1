

/**
 * Spectacle lens custom rx is data for binding the data to the model, of spectacle rx data
 */
export class SpectacleLensCustomRx {
    BC_od: string;
    BC_os: string;
    Insert_od: string;
    Inset_od: string;
    Near_ou: string;
    Totaldec_od: string;
    Vbase_od: string;
    Vbase_os: string;
    Vprism_od: string;
    Vprism_os: string;
    add_od: string;
    add_od_va: string;
    add_os: string;
    add_os_va: string;
    axis_od: string;
    axis_od_va: string;
    axis_os: string;
    axis_os_va: string;
    balance_od: string;
    balance_os: string;
    base_od: string;
    base_os: string;
    custom_rx: number;
    cyl_od: string;
    cyl_os: string;
    dec_od: string;
    dec_os: string;
    del_status: number;
    det_order_id: number;
    discontinue_dt: string;
    dist_pd_od: string;
    dist_pd_os: string;
    dva_od: string;
    dva_os: string;
    dva_ou: string;
    expirydate: string;
    far_ou: string;
    id: number;
    insert_os: string;
    inset_os: string;
    location_id: number;
    lock_date: string;
    locked: string;
    mr_od_p: string;
    mr_od_prism: string;
    mr_od_sel: string;
    mr_od_splash: string;
    mr_os_p: string;
    mr_os_prism: string;
    mr_os_sel: string;
    mr_os_splash: string;
    mve_prescription_id: string;
    near_pd_od: string;
    near_pd_os: string;
    notes: string;
    nva_od: string;
    nva_os: string;
    nva_ou: string;
    oc_od: string;
    oc_os: string;
    order_id: number;
    outside_rx: number;
    patient_id: number;
    physician_id: number;
    physician_name: string;
    prism_od: string;
    prism_os: string;
    release_date: string;
    released: string;
    rx_dos: string;
    rx_duplicate: string;
    rx_type: string;
    rx_usage: string;
    seg_od: string;
    seg_os: string;
    spec_id_od: string;
    spec_id_os: string;
    spec_lens_prescription_type: string;
    speclensmaterial_id: string;
    speclensstyle_id: string;
    sphere_od: string;
    sphere_os: string;
    telephone: string;
    totaldec_os: string;
    vertex_od: string;
    vertex_os: string;
}

export class SpectacleLensExamRx {
    get_vision: Array<GetVisionData>;
    date_of_service: string;
    primary_care_physician: string;
}
export class GetVisionData {
    vis_id: number;
    vis_mr_od_s: string;
    vis_mr_od_c: string;
    vis_mr_od_a: string;
    vis_mr_od_p: string;
    vis_mr_od_prism: string;
    vis_mr_od_slash: string;
    vis_mr_od_sel_1: string;
    vis_mr_od_txt_1: string;
    vis_mr_od_txt_2: string;
    vis_mr_od_add: string;
    vis_mr_os_s: string;
    vis_mr_os_c: string;
    vis_mr_os_a: string;
    vis_mr_os_p: string;
    vis_mr_os_prism: string;
    vis_mr_os_slash: string;
    vis_mr_os_sel_1: string;
    vis_mr_os_txt_1: string;
    vis_mr_os_txt_2: string;
    vis_mr_os_add: string;
    vis_mr_od_given_s: string;
    vis_mr_od_given_c: string;
    vis_mr_od_given_a: string;
    vis_mr_od_given_p: string;
    vis_mr_od_given_prism: string;
    vis_mr_od_given_slash: string;
    vis_mr_od_given_sel_1: string;
    vis_mr_od_given_txt_1: string;
    vis_mr_od_given_add: string;
    vis_mr_os_given_s: string;
    vis_mr_os_given_c: string;
    vis_mr_os_given_a: string;
    vis_mr_os_given_p: string;
    vis_mr_os_given_prism: string;
    vis_mr_os_given_slash: string;
    vis_mr_os_given_sel_1: string;
    vis_mr_os_given_txt_1: string;
    vis_mr_os_given_add: string;
    form_id: number;
    vis_mr_od_given_txt_2: string;
    vis_mr_os_given_txt_2: string;
    visMrOtherOdS_3: string;
    visMrOtherOdC_3: string;
    visMrOtherOdA_3: string;
    visMrOtherOdTxt1_3: string;
    visMrOtherOdTxt2_3: string;
    visMrOtherOdAdd_3: string;
    visMrOtherOsS_3: string;
    visMrOtherOsC_3: string;
    visMrOtherOsA_3: string;
    visMrOtherOsTxt1_3: string;
    visMrOtherOsTxt2_3: string;
    visMrOtherOsAdd_3: string;
    visMrOtherOdP_3: string;
    visMrOtherOdPrism_3: string;
    visMrOtherOdSlash_3: string;
    visMrOtherOdSel1_3: string;
    visMrOtherOsP_3: string;
    visMrOtherOsPrism_3: string;
    visMrOtherOsSlash_3: string;
    visMrOtherOsSel1_3: string;
}

export class SpectacleLensRx {
    chartNote: Array<SpectacleLensExamRx>;
    customRx: Array<SpectacleLensCustomRx>;
}


/**
 * Vision rx mapping is binding values to model for filling UI screen with particular data
 */
export class VisionRxMapping {
    rxtype: string;
    usage: string;
    physician: string;
    rxdate: string;
    description: string;
    balance_od: string;
    balance_os: string;
    sphere_od: string;
    sphere_os: string;
    cylinder_od: string;
    cylinder_os: string;
    axis_od: string;
    axis_os: string;
    add_od: string;
    add_os: string;
    prism1_od: string;
    prism1_os: string;
    prism2_od: string;
    prism2_os: string;
    base1_od: string;
    base1_os: string;
    base2_os: string;
    base2_od: string;
    di_od: string;
    id: number;
    orderType: string;
    multifocal: string;
    notes: string;
    di_os: string;


}