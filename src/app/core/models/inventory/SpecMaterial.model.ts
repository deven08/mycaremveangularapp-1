
export interface SpecMaterial {
    Id: string;
    Name: string;    
    Active: string;
}

export interface Optical {
    Total: string;
    SpectacleLensMatreialList: SpecMaterial[];
}

export interface Result {
    Optical: Optical;
}

export interface SpecMaterialObject {
    Success: number;
    result: Result;
}
