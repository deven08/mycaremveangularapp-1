
export interface FrameColor {
    Id: string;
    Name: string;
    colorCode: string;
    Active: string;
}

export interface Optical {
    Total: string;
    FrameColors: FrameColor[];
}

export interface Result {
    Optical: Optical;
}

export interface ColorObject {
    Success: number;
    result: Result;
}
/**
 * Frame color data
 * frame color list payload
 */
export interface FrameColorData {
    current_page: number;
    data: [Data];
}
/**
 * Data
 * getting fields in frame color list payload
 */
export interface Data {
    id: string;
    color_name: string;
    color_code: string;
    del_status: number;
    import_id: string;
}
/**
 * Len color data
 * spectacel lens color list payload
 */
export interface LenColorData {
    current_page: number;
    data: [LensData];
}
/**
 * Lens data
 * getting fields in  spectacel lens color list payload
 */
export interface LensData {
    colorcode: number;
    colorname: string;
    del_status: number;
    id: string;
    vsp_code: number;
    vsp_description: string;
}


