

export interface Optical {
    ItemId: number;
}

export interface Result {
    Optical: Optical;
}

export interface FramesResponse {
    Success: number;
    result: Result;
}


export interface Frames {
    accessToken: string;
    upc: string;
    name: string;
    manufacturerId: string;
    vendorId: string;
    brandId: string;
    shapeId: string;
    styleId: string;
    a: string;
    b: string;
    ed: string;
    dbl: string;
    temple: string;
    bridge: string;
    fpd: string;
    colorId: string;
    colodCode: string;
    retailPrice: string;
}
