export interface Optical {
    ItemId: number;
}

export interface Result {
    Optical: Optical;
}

export interface OtherinventoriesResponse {
    Success: number;
    result: Result;
}


export interface Otherinventories {
    accessToken: string;
    upc: string;
    name: string;
    categoryId: string;
    vendorId: string;
    retailPrice: string;
    cost: string;
    inventory: boolean;
    sendToLab: boolean;
    printOnOrder: boolean;
    procedureCode: string;
    modifierId: string;
    structureId: string;
    notes: string;


}

export interface ImportCsv {
    status: string;
}
/**
 * Other inventory form data
 *  Other inventory form controller payload result
 */
export interface OtherInventoryFormData {
    upc_code: number;
    name: string;
    categoryid: number;
    vendor_id: number;
    retail_price: number;
    wholesale_cost: number;
    procedure_code: object;
    modifier_id: number;
    structure_id: number;
    notes: string;
    min_qty: number;
    max_qty: number;
    locationid: number;
    commission_type_id: number;
    spiff: string;
    gross_percentage: number;
    amount: number;
    module_type_id: string;

}
