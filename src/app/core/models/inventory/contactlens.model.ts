export interface ContactLens {
    ItemId: string;
    UPC: string;
    Name: string;
    ManufacturerId: string;
    Manufacturer: string;
    VendorId: string;
    Vendor: string;
    BrandId: string;
    Brand: string;
    PackagingId: string;
    Packaging: string;
    Types: any[];
    Materials: any[];
    WearSchedule: any[];
    Supply: any[];
    Color: string;
    ColorId: string;
    IsDot: string;
    RetailPrice: string;
    Cost: string;
    ProcedureCode: string;
    QtyOnHand: string;
    module_type_id: string;
    StructureId: string;
    StructureName: string;
    SpherePower: string;
    Cylinder: string;
    Addd: string;
    Bc2: string;
    Di2: string;
    ReplacementScheduleId: string;
    ReplacementScheduleOptionValue: string;
    Unit: string;
    HardContact: string;
    Inventory: string;
    IsTrial: string;
    min_qty: string;
    max_qty: string;
    Notes: string;
    trial_lens_id: string;
    six_months_price: string;
    six_months_qty: string;
    twelve_months_price: string;
    twelve_months_qty: string;
    Amount: string;
    CommissionTypeId: string;
    CommissionTypeName: string;
    Spiff: string;
    GrossPercentage: string;
    del_status: string;
}

export interface Optical {
    Total: string;
    ContactLenses: ContactLens[];
}

export interface Result {
    Optical: Optical;
}

export interface RootObject {
    Success: number;
    result: Result;
}

/**
 * Contact lens form data
 *  contact-lens form controller payload Result
 */
export interface ContactLensFormData {
    module_type_id: string;
    upc_code: number;
    manufacturer_id: string;
    name: string;
    cl_replacement: string;
    colorid: string;
    hard_contact: boolean;
    inventory: boolean;
    min_qty: number;
    max_qty: number;
    vendor_id: string;
    cl_packaging: number;
    retail_price: string;
    wholesale_cost: string;
    six_months_price: string;
    six_months_qty: string;
    twelve_months_price: string;
    twelve_months_qty: string;
    procedure_code: object;
    structure_id: number;
    commission_type_id: number;
    amount: string;
    gross_percentage: string;
    notes: string;
}
