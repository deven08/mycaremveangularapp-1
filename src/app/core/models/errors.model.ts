/**
 * ErrorCodes constant variable
 * It contains a list of API errors with their status code and description
 */
export const ErrorCodes = {
    301: { statusText: 'Moved Permanently', desc: 'We are sorry but this page is no longer available.' },
    302: { statusText: 'Found', desc: 'We are sorry but this page has moved and we can not seem to find it.' },
    303: { statusText: 'See Other', desc: 'We are sorry but this page has moved and we can not seem to find it.' },
    307: { statusText: 'Temporary Redirect', desc: 'We are sorry but this page has moved and we can not seem to find it.' },
    400: { statusText: 'Bad Request', desc: 'We are sorry but this page is making a request that is not understood.' },
    401: { statusText: ' Unauthorized', desc: 'Please enter username and password.' },
    402: { statusText: 'Payment Required', desc: 'Please contact myCare MVE for information about purchasing this module.' },
    403: { statusText: 'Forbidden', desc: 'You do not have the correct security setting to view this page.' },
    404: { statusText: 'Not Found', desc: 'WHOOPS! Something has gone wrong on this page.' },
    405: { statusText: 'Method Not Allowed', desc: 'Sorry, this request is not allowed.' },
    408: { statusText: 'Request Timeout', desc: 'Times Up! Something has apparently gone wrong on this page.' },
    409: { statusText: 'Conflict', desc: 'Oh No! This page has lost the conflict.' },
    410: { statusText: 'Gone', desc: 'Oh No! This page has apparently gone missing.' },
    422: { statusText: 'Unable to Save/Update Data.', desc: 'Some fields are invalid or required.' },
    500: { statusText: 'Internal Server Error', desc: 'WHOOPS! Something has gone wrong on this page.' },
    501: {
        statusText: 'Not Implemented', 
        desc: 'WHOOPS! Something has gone wrong on this page.' 
    },
    502: {
        statusText: 'Bad Gateway', 
        desc: 'WHOOPS! Something has gone wrong on this page.' 
    },
    503: { statusText: 'Service Unavailable', desc: 'OOPS!  It seems there is a traffic jam that has caused this page to stop.' },
    504: { statusText: 'Gateway Timeout', desc: 'Times Up! Something has apparently gone wrong on this page.' }

};

/**
 * ExcludeErrors constant variable
 * It contains error codes that should not display to client based on environment.
 */
export const ExcludeErrors = [404];

/**
 * Api Error Model
 * Used to couple API response with Error Handle Service
 */
export class ApiError {
    status: number;
}
