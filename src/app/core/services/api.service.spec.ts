import { TestBed, inject } from '@angular/core/testing';

import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { ApiService } from './api.service';

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
  });

  it(
    'should get users',
    inject(
      [HttpTestingController, ApiService],
      (
        httpMock: HttpTestingController,
        dataService: ApiService
      ) => {
        // ...our test logic here
      }
    )
  );
});
