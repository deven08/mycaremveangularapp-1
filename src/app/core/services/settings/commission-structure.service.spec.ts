import { TestBed } from '@angular/core/testing';

import { CommissionStructureService } from './commission-structure.service';

describe('CommissionStructureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommissionStructureService = TestBed.get(CommissionStructureService);
    expect(service).toBeTruthy();
  });
});
