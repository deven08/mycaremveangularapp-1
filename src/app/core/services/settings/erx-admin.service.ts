import { Injectable } from '@angular/core';
import { ApiService } from '@app/core';

@Injectable({
  providedIn: 'root'
})
export class ErxAdminService {
  /**
   * Mainurl  will store the actual API call syntax
   */
  Mainurl: string;
  /**
   * Creates an instance of erx admin service.
   * @param apiservice for run the get post put delete
   */
  constructor(private apiservice: ApiService) {
    this.Mainurl = '';
   }

  /**
   * Gets erxadmin
   * @returns  getting the erx details
   */
  geteRXAdmin() {
    this.Mainurl = '/erx/settings';
    return this.apiservice.getApiMicro(this.Mainurl);
  }


  /**
   * Updates erx admin setting
   * @returns for update the erx details
   */
  UpdateErxAdminSetting(payLoad) {
    this.Mainurl = '/erx/settings';
    return this.apiservice.putApiMicro(this.Mainurl, payLoad);
  }

}
