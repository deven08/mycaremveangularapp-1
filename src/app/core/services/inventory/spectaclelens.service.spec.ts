import { TestBed } from '@angular/core/testing';

import { SpectaclelensService } from './spectaclelens.service';

describe('SpectaclelensService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpectaclelensService = TestBed.get(SpectaclelensService);
    expect(service).toBeTruthy();
  });
});
