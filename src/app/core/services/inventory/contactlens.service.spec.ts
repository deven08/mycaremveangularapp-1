import { TestBed } from '@angular/core/testing';

import { ContactlensService } from './contactlens.service';

describe('ContactlensService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactlensService = TestBed.get(ContactlensService);
    expect(service).toBeTruthy();
  });
});
