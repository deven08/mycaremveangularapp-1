import { TestBed } from '@angular/core/testing';

import { LodService } from './lod.service';

describe('LodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LodService = TestBed.get(LodService);
    expect(service).toBeTruthy();
  });
});
