import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs/internal/Observable';
import { ErrorService } from '../error.service';
import { SpectacleLens } from '@app/core/models/inventory/Spectaclelens.model';
@Injectable()
export class SpectaclelensService {
  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  AddframesUrl = '';
  Getframesfilter: any = '';
  selectedspectacleupc: any = '';
  editDuplicatespectacleupc: any = '';
  selectedspectacleupcDoubleCLick: any = '';
  MaterialListDropDownData: any = [];
  colorcopyData: any = [];
  taxrate:any = [];
  /**
   * Selecteditems  of spectaclelens service for selected item id's
   */
  selecteditems: any = [];
  mainpart: string;
  inputpart: string;
  constructor(private http: HttpClient, private apiservice: ApiService, public errorHandle: ErrorService) { }

/**
   * Gets spectacles
   * @param {String} [upc]  for upc 
   * @param {String} [maxRecord] maximum records
   * @param {String} [materailid] material id
   * @param {String} [lensstyleid] style id
   * @param {String} [colorcode] color id
   * @param {String} [ManufactureId] manufacture id
   * @param {String} [Name] name
   * @returns {object} spectacle lens grid data 
   */
  getSpectacles(upc = '', maxRecord = '', materailid = '', lensstyleid = '', colorcode = '', ManufactureId = '',
    Name = '') {
    this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    this.APIstring = 'IMWAPI/optical/getSpectaclesLenses';
    this.inputpart = '&upc=' + upc + '&materialId=' + materailid + '&styleId=' + lensstyleid + '&colorId=&colorCode=' + colorcode +
      '&maxRecord=' + '15' + '&manufacturerId=' + ManufactureId + '&name=' + Name + '&offset=';
    return this.apiservice.GetApi(this.APIstring, this.inputpart);
    // .do(data => data);
  }
  /**
    * Gets spectacles master list
    * @returns  {object} for spectacle lens master list
    */
  getSpectaclesMasterList() {
    this.Mainurl = '/spectacleLens/material';
    return this.apiservice.getApiMicro(this.Mainurl);
  }

  getSpectaclens(spectaclelens) {
    this.Accesstoken = ''
    this.APIstring = '/item/filter';
    return this.apiservice.postApiMicro(this.APIstring, spectaclelens);
  }

  getSpectaclespagenation(tabledata, page: number) {
    this.mainpart = '/item/filter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, tabledata);
  }

  Savedata(spectaclesLens: SpectacleLens) {
    this.mainpart = '/inventory/item';
    return this.apiservice.postApiMicro(this.mainpart, spectaclesLens);
  }

  /**
   * Returns spectaclelens service
   * @returns spectaclelens list 
   */
  getSpectaclelensList(object): Observable<any> {
    this.mainpart = '/inventory/item';
    return this.apiservice.GetApi(this.mainpart, object);
  }

  /**
   * Returns spectaclelens service
   * @returns  Spectacle Lens details
   */
  updateSpectaclelensList(object, item) {
    this.mainpart = '/inventory/item/'.concat(item);
    return this.apiservice.putApiMicro(this.mainpart, object);
  }

  // To DO
  /**
   * Gets spectacles activeand deactive
   * @param {String} [upc] for upc
   * @param {String} [maxRecord] for maximum records
   * @param {String} [offset]  for offset value
   * @param {String} [del_status] for delete status
   * @returns  {object} getting spectacle activate deactivate records
   */
  getSpectaclesActiveandDeactive(upc = '', maxRecord = '', offset = '', del_status = '') {

    this.Accesstoken = ''
    this.APIstring = 'IMWAPI/optical/getSpectaclesLenses'
    this.inputpart = '&upc=' + upc + '&maxRecord=' + maxRecord + '&offset=' + offset + '&del_status=' + del_status;
    return this.apiservice.GetApi(this.APIstring, this.inputpart);
  }

  // gets the contactlens details
  savespectacletreatment(id = '', value) {
    this.mainpart = '/item/' + id + '/lensTreatments';
    const postdata = JSON.stringify(value);
    return this.apiservice.postApiMicro(this.mainpart, value);

  }

  getmeasurmentsmaster() {
    this.mainpart = '/spectacleLens/measurement';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  getSpectacleLensmeasurmenstByConfiqID(confiqid) {
    this.mainpart = '/item/' + confiqid + '/measurement';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  getTreatmentLensmeasurmenstByConfiqID(confiqid) {
    this.mainpart = '/item/' + confiqid + '/lensTreatment';
    return this.apiservice.getApiMicro(this.mainpart);
  }


  SaveSpectacleLensmeasurmenstByConfiqID(confiqid, data) {
    this.mainpart = '/item/' + confiqid + '/measurement';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  SavetreatmentsLensmeasurmenstByConfiqID(confiqid, data) {
    this.mainpart = '/item/' + confiqid + '/lensTreatment';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }
  SpctaclleLensConfiguratiion(SpecItemId) {
    this.mainpart = '/item/' + '4' + '/spectacleConfigurations/';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  /**
   * Batchs details save
   * @param {string} mainpart path that willbe added to the url
   * @param {object}  data payload for url
   * @returns {object} returns object for subscriing
   * update Batchs details save
   */
  batchDetailsSave(data) {
    this.mainpart = '/inventory/item/bulkItems';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }

  /**
   * Saves bulk measurement
   * @param {string} mainpart path that willbe added to the url
   * @param {object}  data payload for url
   * @returns {object} returns object for subscriing 
   */
  saveBulkMeasurement(data) {
    this.mainpart = '/inventory/item/bulkSpectacleLensMeasurement';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }

  /**
   * Saves bulk lens treatment
   * @param {string} mainpart path that willbe added to the url
   * @param {object}  data payload for url
   * @returns {object} returns object for subscriing
   */
  saveBulkLensTreatment(data) {
    this.mainpart = '/inventory/item/bulkSpectacleLensTreatment';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }

  /**
   * Saves bulk spectacle lens procedure
   * @param {string} mainpart path that willbe added to the url
   * @param {object}  data payload for url
   * @returns {object} returns object for subscriing
   */
  saveBulkSpectacleLensProcedure(data) {
    this.mainpart = '/inventory/item/bulkSpectacleLensProcedure';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }

  /**
   * Gets location tax
   * @param {string} mainpart path that willbe added to the url
   * @returns {object} returns object for subscriing 
   */
  getLocationTax() {
    this.mainpart = '/tax';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Saves location tax
   * @param {string} mainpart path that willbe added to the url
   * @param {object}  data payload for url
   * @returns {object} returns object for subscriing
   */
  saveLocationTax(data){
   this.mainpart =  '/itemLocTotal/tax';
   return this.apiservice.putApiMicro(this.mainpart, data);
  }

}
