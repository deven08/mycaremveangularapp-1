import { Injectable } from '@angular/core';
import { ErrorService, ApiService } from '@app/core';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService {
  mainpart: string;
  /**
   * Supplieer data of purchase order service
   */
  supplieerData:any = '';
  /**
   * Posupplier  of purchase order service
   */
  POsupplier:any='';
  /**
   * Polocation  of purchase order service
   */
  POlocation:any = '';
  /**
   * Searchsupplieer data of purchase order service
   */
  searchsupplieerData:any = '';
  /**
   * Saving items list of purchase order service
   */
  savingItemsList:any[] = [];
  /**
   * Update id of purchase order service
   */
  UpdateId:any='';
  /**
   * Form validate of purchase order service
   */
  formValidate:boolean;
  /**
   * Supplier validate of purchase order service
   */
  supplierValidate:boolean;
  /**
   * Savebutton enable of purchase order service
   */
  savebuttonEnable:boolean;
  /**
   * Poselected data of purchase order service for search data observale
   */
  /**
   * Labstatus  of purchase order service
   */
  labstatus: any = [];
  /**
   * Potype  of purchase order service
   */
  POtype:any = [];
  public POSelectedData = new Subject<any>();
  POSelectedData$ = this.POSelectedData.asObservable();
  /**
   * Poitems update of purchase order service for update subscribe
   */
  public POitemsUpdate = new Subject<any>();
  POitemsUpdate$ = this.POitemsUpdate.asObservable();
  constructor(private apiservice: ApiService, public errorHandle: ErrorService) {

   }
   getSupplierDetailsById(id){
    this.mainpart = '/vendor/'+ id ;
    return this.apiservice.getApiMicro(this.mainpart);
   }
   /**
    * Getitems by upc
    * @param reqBody  payload  data
    * @returns  {object} for getting items by upc
    */
   getitemByUpc(reqBody){
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart,reqBody);
   }
   /**
    * Getitems by module type
    * @param reqBody payload  data
    * @returns  {object} getting items by module type id
    */
   getitemByModuleType(reqBody){
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart,reqBody);
   }
   /**
    * Gets podata
    * @param reqBody payload  data
    * @returns  {object} fot getting po data
    */
   getPOdata(reqBody){
    this.mainpart = '/purchaseOrder/filter';
    return this.apiservice.postApiMicro(this.mainpart,reqBody);
   }
   /**
    * Saves podata
    * @param reqBody payload  data
    * @returns  {object} for savng po data
    */
   savePOdata(reqBody){
    this.mainpart = '/purchaseOrder';
    return this.apiservice.postApiMicro(this.mainpart,reqBody);
   }
   /**
    * Updates podata
    * @param reqBody payload  data
    * @param id  item id
    * @returns  {object} for updating po data
    */
   updatePOdata(reqBody,id){
    this.mainpart = '/purchaseOrder/'+id;
    return this.apiservice.putApiMicro(this.mainpart,reqBody);
   }
   /**
    * Gets poitems
    * @param id item id
    * @returns  {object} getting existing po items
    */
   getPOitems(id){
    this.mainpart = '/purchaseOrder/'+id+'/poDetail';
    return this.apiservice.getApiMicro(this.mainpart);
   }
   /**
    * Getstatusdropdatas purchase order service
    * @returns  {object} for status drop down
    */
   Getstatusdropdata(){
    this.mainpart = '/purchaseOrder/status';
    return this.apiservice.getApiMicro(this.mainpart);
   }
   /**
    * Gets types drop down
    * @returns  {object} for type drop down
    */
   getTypesDropDown(){
    this.mainpart = '/purchaseOrder/types';
    return this.apiservice.getApiMicro(this.mainpart);
   }
}
