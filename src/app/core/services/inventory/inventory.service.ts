import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from '../error.service';
@Injectable()
export class InventoryService {
  /**
   * Bar codelist of inventory service
   * stores multiple items
   */
  barCodelist: any[] = [];
  /**
   * Bar code lable of inventory service
   * display name of barcode lable
   */
  public barCodeLable = '';
  inputpart = '';
  mainpart = '';
  settings: any = null;
  cancel = false;
  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  typeOfInventory = '';
  InventoryType = '';
  FortrasactionItemID: string;
  multiLocationForOtherInventory: any = '';

  /**
   * Commission type of inventory service
   * storing commition type data
   */
  commissionType: any = [];
  /**
   * Structure data of inventory service
   * storing structure data
   */
  structureData: any = [];
  /**
   * Vpc data of inventory service
   * storing VPC  data
   */
  vpcData: any = [];
  /**
   * Frame lens range of inventory service
   * storing frames lens range data
   */
  frameLensRange: any = [];
  /**
   * Upc type data of inventory service
   * storing upc type data
   */
  upcTypeData: any = [];
  /**
   * Frame lens color of inventory service
   * storing lenscolor data
   */
  frameLensColor: any = [];
  /**
   * Frame source data of inventory 
   * storing frame source  data
   */
  frameSourceData: any = [];
  /**
   * Frame shape of inventory service
   * storing frame shape data
   */
  frameShape: any = [];
  
  constructor(private apiservice: ApiService, private http: HttpClient, public errorHandle: ErrorService) { }

   /**
   * filterInventories of inventory service
   * for getting the invertory data based on the filter 
   */
  filterInventories(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  /**
   * DeactivateInventory of inventory service
   * for deactivating the selected inventory 
   */
  DeactivateInventory(inventory: any) {

    this.mainpart = '/item/deactivate';
    const bodyString = JSON.stringify(inventory);
    return this.apiservice.putApiMicro(this.mainpart, bodyString.toString());

  }

  /**
   * ActivateInventory of inventory service
   * for activating  the selected inventory 
   */
  ActivateInventory(inventory: any) {

    this.mainpart = '/item/activate';
    const bodyString = JSON.stringify(inventory);
    return this.apiservice.putApiMicro(this.mainpart, bodyString.toString());

  }

  /**
   * Barcodes labels data
   * @returns  For loading bar codes
   */
  barcodeLabelsData() {
    this.Mainurl =  '/barCodeLabels';
    return this.apiservice.getApiMicro(this.Mainurl);
  }

  
  getLocationByFilter(data) {
    const reqBody = {
      'filter': [
        {
          'field': 'item_id',
          'operator': '=',
          'value': data
        }
      ]

    };
    this.Mainurl = '/itemLocTotal/filter';
    return this.apiservice.postApiMicro(this.Mainurl, reqBody);
  }

  GetSuppliersDetails(includeInactive, maxRecord, offset, id) {
    this.APIstring = 'IMWAPI/optical/getVendors';
    const inputpart = this.Accesstoken + '&Id=' + id + '&includeInactive=' +
      includeInactive + '&offset=' + offset + '&maxRecord=' + maxRecord + '';
    return this.apiservice.GetApi(this.APIstring, inputpart);
  }

  /**
   * Gets commission typedrop data
   * @returns  commition data
   */
  getCommissionTypedropData() {
    this.Mainurl = '/commissionType';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Gets structuredrop data
   * @returns   structures data
   */
  getStructuredropData() {
    this.Mainurl = '/commissionStructure';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Gets vcpdrop down data
   * @returns   vcp data
   */
  getVCPdropDownData() {
    this.Mainurl = '/vcp';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Gets frame lens range
   * @returns framelensrange data
   */
  getFrameLensRange() {
    this.mainpart = '/frame/lensRange';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Gets upc type
   * @returns   upctype data
   */
  getUpcType() {
    this.mainpart = '/upcType';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Gets source
   * @returns  source dat 
   */
  getSource() {
    this.mainpart = '/source';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  /**
   * Gets framesshape
   * @returns  frames shape data
   */
  getFramesshape() {
    this.mainpart = 'IMWAPI/optical/getFrameShapes';
    this.inputpart = '&Id=&Name=&includeInactive=&maxRecord=&offset=&sortBy=&sortOrder=';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
      // .do(data => data);
  }

  /**
   * Gets getInventoryBypage
   * @returns  Inventory data based on page input from the Grid 
   */
  getInventoryBypage(Griddata, page: number) {
    this.mainpart =  '/item/filter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, Griddata);
  }
/**
   * Gets Getinventorydetailsbyfilter
   * @returns  Inventory data for all inventories based on input provided
   */
 Getinventorydetailsbyfilter(inputpart) {
  this.Accesstoken = ''
  this.APIstring = '/item/filter';
  return this.apiservice.postApiMicro(this.APIstring, inputpart);  
}
/**
   * Saves inventory data
   * @returns  Saves inventory data
   */
  Saveinventorydetails(otherinventory) {
    this.mainpart = '/inventory/item';
    return this.apiservice.postApiMicro(this.mainpart, otherinventory);
  }  
  /**
   * Updates inventory others
   * @returns   Updates inventory details
   */
  updateInventorydetails(object, itemid) {
    this.mainpart = '/inventory/item/'.concat(itemid);
    return this.apiservice.putApiMicro(this.mainpart, object);
  }

  /**
   * Gets cpt desc
   * @param {number} procedureId 
   * @returns cpt deatils of particular prodcedure_id
   */
  getCptDesc(procedureId){
    this.mainpart = '/cpt/'.concat(procedureId);
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Gets sort by all
   * @returns getting the sort all data 
   */
  getSortByAll(gridData: object) {
    this.mainpart = '/item/advanceFilter';
    return this.apiservice.postApiMicro(this.mainpart, gridData);
  }
  /**
   * Gets getInventoryBypage
   * @returns  Inventory data based on page input from the Grid 
   */
  getSortByAllpaginator(gridData, page: number) {
    this.mainpart =  '/item/advanceFilter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, gridData);
  }

  /**
   * Importcsvs inventory service
   * @param formData for getting payload
   * @param type invenroty name
   * @returns {object} for import the csv file
   */
  importcsv(formData, type) {
    this.mainpart = '/import/inventory/'.concat(type);
    this.Accesstoken = '';
    return this.apiservice.postApiMicroUpload(this.mainpart, formData);
  }


}
