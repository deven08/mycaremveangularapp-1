import { TestBed } from '@angular/core/testing';

import { LenstreatmentService } from './lenstreatment.service';

describe('LenstreatmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LenstreatmentService = TestBed.get(LenstreatmentService);
    expect(service).toBeTruthy();
  });
});
