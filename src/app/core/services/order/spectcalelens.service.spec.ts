import { TestBed, inject } from '@angular/core/testing';

import { SpectcalelensService } from './spectcalelens.service';

describe('SpectcalelensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpectcalelensService]
    });
  });

  it('should be created', inject([SpectcalelensService], (service: SpectcalelensService) => {
    expect(service).toBeTruthy();
  }));
});
