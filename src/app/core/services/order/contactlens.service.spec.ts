import { TestBed, inject } from '@angular/core/testing';

import { ContactlensService } from './contactlens.service';

describe('ContactlensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactlensService]
    });
  });

  it('should be created', inject([ContactlensService], (service: ContactlensService) => {
    expect(service).toBeTruthy();


  }));
});
