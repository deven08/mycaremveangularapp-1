import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';
import { AppService } from '@services/app.service';
/**
* @ignore 
*/
@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {

  /**
   * Creates an instance of maintenance service.
   * @param api for run the get post put delete
   * @param app for fetching the common variables
   */
  constructor(
    private api: ApiService,
    private app: AppService
  ) { }

  /**
   * Gets list of daily closing items listing from get call
   * @param {string} mainpart url building
   * @returns  the daily closing data object
   */
  getListOfDailyClosingItems() {
    const mainpart = '/dailyClosing?location_id='.concat(this.app.locationId);
    return this.api.getApiMicro(mainpart);
  }

  /**
   * Dailys closingsaving
   * @param {object} DailyCLosingObject daily closing post api body
   * @returns  retruns object for subscription part
   */
  dailyClosingsaving(DailyCLosingObject) {
    const mainpart = '/dailyClosing';
    return this.api.postApiMicro(mainpart, DailyCLosingObject)
  }

  /**
   * Dailys closing retrval
   * @param postingDate date for the filter daily closing details
   * @returns  object for subscription
   */
  dailyCLosingRetrval(postingDate) {
    // alert(postingDate)
    const mainpart = '/dailyClosing/savedDetails?'
      .concat('location_id=').concat(this.app.locationId)
      .concat('&')
      .concat('closing_date=').concat(postingDate);
    return this.api.getApiMicro(mainpart);
  }

  /**
   * Dailyclosing process
   * @param DailyCLosingProcessId  is for the processclosing Id
   * @returns  the object for the subscription
   */
  dailyclosingProcess(DailyCLosingProcessId) {
    const mainpart = '/dailyClosing';
    return this.api.putApiMicro(mainpart, DailyCLosingProcessId);
  }
}
