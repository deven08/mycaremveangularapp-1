export const environment = {
  production: true,
  prodHideLinks : false,
  imedicUrl: '/imw/',
  apiEndPoint: '/microservices/public',
  apiEndPoint2: '/apigateway/api/v1',
  showAllErrors: false,
  ssrsurl: 'http://10.1.1.167/ReportServer_SQL01/Pages/ReportViewer.aspx?%2fMVE%2f'
 };